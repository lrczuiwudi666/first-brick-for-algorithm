<details>
<summary>2023民生π菁英挑战赛</summary>

#### platform 民生FinTech
#### T1 https://acexam.nowcoder.com/coding/?uid=C6C64A71F4DDBEA3&qid=10583722
<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这叙述真的是，能看懂个鬼了</details>
- <details><summary>tag</summary>sb题目</details>
- spendtime: ？？
- ACtime: 2023.7.2
- ACstatus: 不会
- score: 7
</details>
</details>

<details>
<summary>华泰2023初赛</summary>

#### platform 华泰证劵FinTech
#### T3 https://acexam.nowcoder.com/coding/?uid=9402C164A1E24AF2&qid=10557247
#### T4 https://acexam.nowcoder.com/coding/?uid=9402C164A1E24AF2&qid=10566604
<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接把我作废了，看都不想再看，直接不会</details>
- <details><summary>tag</summary>麻烦题目</details>
- spendtime: ？？
- ACtime: 2023.7.1
- ACstatus: 不会
- score: 7
</details>
</details>

<details>
<summary>2023</summary>

#### platform 招商银行FinTech
#### T1 https://acexam.nowcoder.com/coding/?uid=E8B1C16D20A15249&qid=10551175
#### T2 https://acexam.nowcoder.com/coding/?uid=E8B1C16D20A15249&qid=10550823
#### T2 https://acexam.nowcoder.com/coding/?uid=E8B1C16D20A15249&qid=10548026

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>题目在我见过的笔试里面，应该是算最高的了，而且也是最难，单看每个题目，都是一个比较难写的hard级别，放在一起确实也比较考验时间上，也不算特别可惜，a出两个，排20，还行吧，没打到比较top这还是比较可惜，我觉得以我的水平，ak确实不算很难，但谁都有脑子抽的时候啊，T1比较麻烦的最短路，T2比较麻烦的排列+dp+枚举，T3比较麻烦的dp，其中很坑的地方在于题目不好好说清楚，还得靠猜，使得直接wa了很多下，还有找bug的过程，整体来说素质还是很高的</details>
- <details><summary>tag</summary>麻烦题目</details>
- spendtime: 180
- ACtime: 2023.5.3
- ACstatus: 完全会
- score: 7
</details>
</details>

<details>
<summary>2023</summary>

#### platform 腾讯
#### id https://ac.nowcoder.com/acm/problem/112055

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>采用容斥去思考，从后往前，令g[i]为含有i因数的方案数，则g[i]=2^cnt[i]-1-g[2*i]-g[3*i]-g[i*4]....,先分解约数n^1.5复杂度，再容斥计算nlogn复杂度，最后再求出g[1]即可，n的复杂度</details>
- <details><summary>tag</summary>容斥</details>
- spendtime: ？？
- ACtime: 2023.3.27
- ACstatus: 不知道赛时啥感受
- score: 7
</details>
</details>

<details>
<summary>2023</summary>

#### platform 腾讯音乐
#### id 

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>有1-n个数，分别是1,2,3,...,n，将这些数分为两组，一组的大小为a,另一组就是n-a,使得两组之和的绝对值相差为1，可以这么考虑，假设奇数偶数的节点总数分别是a,b,只用看a就可以，这么思考，a初始为1,2,3,...,a,先判断和是否大于sum/2,大于则误解，恰好等于则直接输出答案，现在考虑< sum/2的情况，只需要先从最大的考虑，将b看成a的子集，最大的数一直++到和==sum/2或者不能加为止,再考虑次大的，一直++到sum/2或者不能加为止，以此类推，复杂度O（n），还可以一个思维题目</details>
- <details><summary>tag</summary>构造</details>
- spendtime: ？？
- ACtime: 2023.3.27
- ACstatus: 不知道赛时啥感受
- score: 7
</details>
</details>

<details>
<summary>2023</summary>

#### platform 蚂蚁
#### id 

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>对于n排列的，其中相邻的都是奇数的pair数目，有点难，没想到比率大法，考虑一种随机排列情况，取任意的临项有n*(n-1)种可能，其中只有odd*(odd-1)符合要求，所以答案就是n!*odd*(odd-1)*n/(n*(n-1))</details>
- <details><summary>tag</summary>数学</details>
- spendtime: ？？
- ACtime: 2023.3.21
- ACstatus: 不知道赛时啥感受
- score: 7
</details>
</details>

<details>
<summary>2023</summary>

#### platform 米哈游
#### id 

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>出了三道中等，不太懂为啥了，不信都是一眼，结果一看，果然是一眼，好吧，那就一眼吧，做完就忘反正</details>
- <details><summary>tag</summary>树状dp+线性dp+构造</details>
- spendtime: 40
- ACtime: 2023.3.19
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>2023阿里笔试</summary>

#### platform 阿里
#### id T3

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>题目的大意是说，给一个数组，必须执行k个操作，操作有两种，使得一个数乘2，使得一个数除2，问最大值-最小值最小为多少？就是贪心，将数组排序后分成三段，一段全除，一段全乘，一段不操作，算最小即可，题目有些编码上的细节，还算是个不错的hard</details>
- <details><summary>tag</summary>贪心</details>
- spendtime: 40
- ACtime: 2023.3.15
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>2021阿里笔试</summary>

#### platform 阿里
#### id T10

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>(https://www.nowcoder.com/questionTerminal/b65b8b1376d94d4b8d4718ba2f4c0022)这道题目出了一个很诡异的点，就是考了一个最长连续上升序列，这个建模过程我确实是没有想到，就当典题记录下来了，太失败了，建模不出来只能挂</details>
- <details><summary>tag</summary>最长连续上升序列</details>
- spendtime: ？？
- ACtime: 2023.3.13
- ACstatus: 不会
- score: 6
</details>
</details>

<details>
<summary>衍复笔试记录</summary>

#### platform 衍复
#### id T1

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接被干懵了，迭代器的实现不会，而且半天不知道c++的代码在哪里写，经过补救之后，依然没有在考试的时候写出来，这就是吃了c++lab的亏，因为从来没有写过，非常的生气，真的非常的生气</details>
- <details><summary>tag</summary>c++迭代器实现</details>
- spendtime: ？？
- ACtime: 2023.3.12
- ACstatus: 不会
- score: 6
</details>
</details>

<details>
<summary>衍复笔试记录</summary>

#### platform 衍复
#### id T3

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>一个病人两个可选择槽位，一个医生一次只能一个，问是否可以合理安排病人，需要符合偏好，并查集维护，使用访问频率如果大于必然不行，还是很巧妙的思路，看样子是微软出了这个题目，很思维，就这一个题目，非常的思维，其他的倒还行</details>
- <details><summary>tag</summary>思维+并查集</details>
- spendtime: 40
- ACtime: 2023.3.11
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>九坤笔试记录</summary>

#### platform 九坤
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>晚上还是忍不住把笔试做了，前面两道核心代码的题目，分别是删除链表之类的操作，比较简单，第一题有点略长，读的我有点紧张，虽然非常的简单，第二题更简单，第三题算是一个中等偏上级别的hard，题目的大意是这样，给定图和边，希望1和n连通，最多允许建两条边，一条边的代价是(i-j)^2,问如何才能最小代价连通，思路很简单，要么不建边，本来就连通，要么一条，两么借用一个中转点，其中对于一个连通块如何才能求得最小呢？对元素排序并且二分即可，还是非常中规中矩的hard，放在笔试中还是蛮难的</details>
- <details><summary>tag</summary>模拟+并查集</details>
- spendtime: 72
- ACtime: 2023.2.26
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>无名</summary>

#### platform 智臾科技
#### id t3

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>函数cumrank接受1个数组v为参数，对v中的每一个元素，返回其在从第一个元素到此元素为止的向量中的排序。每个元素的排序仅仅与其之前的元素相关，与其之后的元素无关。返回一个与v相同长度的的向量。请给出算法的复杂度。示例：cumrank([1,3,2,3,4]) 返回结果： [0,1,1,2,4] 题解：直接离散化，数状数组维护即可，感觉还行的一道题目，板子题目，笔试出这个也是确实可以</details>
- <details><summary>tag</summary>数状数组</details>
- spendtime: 20
- ACtime: 2022.12.12
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>无名</summary>

#### platform 智臾科技
#### id t2

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>小明新买了一台有两条内存条的电脑。这台电脑的在分配内存的时候，总是从可用内存最多的内存条来分配内存，若两条内存条可用内存一样，则从第一条内存条中分配内存。如果两条内存条都没有足够内存了，就会报Out Of Memory(OOM)。小明刚拿到新电脑，非常兴奋，赶紧写了一个hello world的程序，但是小明很快发现他的程序里有一个内存泄漏。具体地说，他的程序在第i秒的时候会申请i字节的内存。小明想知道他启动程序多久之后会OOM，以及OOM的时候两条内存条各剩下多少内存。你能帮帮他吗？输入为M1和M2，分别代表第一条内存条和第二条内存条的总内存容量，单位是字节。输出为3个值t, m1和m2。其中t代表电脑会在第t秒报OOM，m1和m2代表OOM的时候第一条内存条和第二条内存条还剩下多少内存。其中1 <= M1 <= 10^18, 1 <= M2 <= 10^18 （提示：这意味着你的算法的复杂度不能超过log(M1+M2)） 示例1：输入：2, 2 输出：3，1，0 示例2：输入：8, 11 输出：6，0，4 题解：一道挺码农的二分题目，觉得比较难写，写了我很长的时间，不过感觉还是不错的，就是比较冗长，需要对情况讨论清楚</details>
- <details><summary>tag</summary>二分</details>
- spendtime: 45
- ACtime: 2022.12.12
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>无名</summary>

#### platform 智臾科技
#### id t1

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>题目描述：函数topRange接受1个数组v为参数，对v中的每一个元素，返回其为前连续多少个值内的最大值。每个元素的对应的仅仅与其之前的元素相关，与其之后的元素无关。返回一个与v相同长度的向量。请给出算法的复杂度，并且证明它。示例：topRange([13.5,13.6,13.4,13.3,13.5,13.9,13.1,20.1,20.2,20.3]) 返回结果：[1,2,1,1,3,6,1,8,9,10] 解释：13.5为第一个值，其为连续1个值的最大值；13.6为子数组[13.5,13.6]的最大值，即为连续两个数的最大值；同理，最后的20.3是整个数组的最大值，故其为连续10个数的最大值。题解：直观看，应该是个单调栈，只是没想到第一题就是，虽然难度不大，但是没有测试用例也太难受了</details>
- <details><summary>tag</summary>单调栈</details>
- spendtime: 25
- ACtime: 2022.12.12
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>红和蓝</summary>

#### platform 牛客
#### id https://ac.nowcoder.com/questionTerminal/34bf2aaeb61f47be980b9ec0ab238c36?answerType=1 

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>作为天翼云第一题这是不是有点不合适了，我写了很久的好嘛？初次写的又臭又长，应该再尝试写一遍，这样代码量大大简化，调试得也是一遍过，一个红蓝配对问题，确实没有想象中那么好写啊</details>
- <details><summary>tag</summary>树形dp+回溯</details>
- spendtime: 70
- ACtime: 2022.10.17
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>无名1</summary>

#### platform 未知
#### id 未知

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>题目描述，两个长度为n（10^5）的数组分别为a,b,返回长度为i的数组，对于ai,bi来说，最小的(ai+aj)^2+bi+bj,则也就意味着，对于每一个i来说，求最小的aj^2+bj+2ai*aj,尝试数学化简，另b=aj^2+bj+2ai*aj,k=-2*ai,则对于x=aj来说，y=aj^2+bj,消去了ai，则(aj,aj^2+bj)点固定，则对这些点集来说求凸包算法，然后对于每一种斜率，在凸包上逐个遍历即可，应该是个经典题目，这种构造没见过肯定想不出来</details>
- <details><summary>tag</summary>数学凸包</details>
- spendtime: 30
- ACtime: 2022.10.29
- ACstatus: 不会
- score: 7
</details>
</details>