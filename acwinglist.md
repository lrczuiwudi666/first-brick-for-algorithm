<details>
<summary>有效类型</summary>

#### platform acwing
#### id acwing 4865

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>必须记录一下，中等题顶峰杀手，一个递归的题目，没想到会这么难，这么有corner case，许久中等题目不会了，递归还是难，相当的难写，而且代码需要被wa指正，遇见pair递归，其余不递归，需要记录一下，简单来说，字符串有pair和int，替换成别的也行，插入尖括号和逗号，就是递归，难！</details>
- <details><summary>tag</summary>思维+递归</details>
- spendtime: ？？
- ACtime: 2023.2.25
- ACstatus: 看提示后恍然大悟
- score: 6
</details>
</details>

<details>
<summary>最大数量</summary>

#### platform acwing
#### id acwing 4866

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>无向图中添加边，满足前面的条件，题意非常的绕，我一时没有读懂，错了好几次，这是第一个坑，第二个，数据范围没看清楚，以为是个10的五次方级别的，结果是个1000，那直接n方完事，不知道自己在干嘛，关键就是边数多的时候，可以合并最大的cnt个，来自92场周赛，这次是爆零的一次</details>
- <details><summary>tag</summary>思维</details>
- spendtime: 65
- ACtime: 2023.2.25
- ACstatus: 看提示后恍然大悟
- score: 6
</details>
</details>

<details>
<summary>加减乘</summary>

#### platform acwing
#### id acwing 4805

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>来自89场周赛，一个如此简单的dp我没搞明白，还想了一些贪心小步骤，没用的，直接线性dp，核心在于-1怎么处理，很简单，使用后一个除以2得来，这场这道题目给坐牢了，直接跪，半天嗑不出来，真的晦气，熟练度也太差劲了，可以加一减一，乘2，就是关键的不知道怎么处理，痛痛反思一下</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 65
- ACtime: 2023.2.4
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>88场周赛</summary>

#### platform acwing
#### id acwing 

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>写的速度还行吧，只能说，距离顶尖高手差不多两倍的手速，还是要加油，再写熟，题目这次还算可以，一遍过，我觉得还行，能接受，没啥坑题目，直接写就行，就模拟模拟dp即可</details>
- <details><summary>tag</summary>手速场</details>
- spendtime: 32
- ACtime: 2023.1.28
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>86场周赛</summary>

#### platform acwing
#### id acwing 

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>好久没写了，直接废了我，一个T2糊涂了，真的废物，题目中规中矩，我好废物啊，这就是手速场么，拉满，下次还是想清楚，再写</details>
- <details><summary>tag</summary>手速场</details>
- spendtime: 43
- ACtime: 2023.1.14
- ACstatus: 完全会
- score: 6
</details>
</details>


<details>
<summary>75场周赛</summary>

#### platform acwing
#### id acwing 

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>acwing纯纯摆烂吧</details>
- <details><summary>tag</summary>手速场</details>
- spendtime: 22
- ACtime: 2022.10.29
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>74场周赛</summary>

#### platform acwing
#### id acwing 

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>t3头一次wa麻木了，板子没有准备好啊，真的是，爆int的问题很常见了，又犯真的是，太傻逼了，arrtree的板子啊</details>
- <details><summary>tag</summary>手速场</details>
- spendtime: 30
- ACtime: 2022.10.22
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>72场周赛</summary>

#### platform acwing
#### id acwing 

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这个t3非常的，差了几分钟，有点愚蠢，反应有点慢，先把所有的环大小求出来，如果有一个非环，那必然不行，对于所有环的大小的队列，做个lcm即可，奇数是直接，偶数/2，失败失败</details>
- <details><summary>tag</summary>t3可以</details>
- spendtime: 80
- ACtime: 2022.10.8
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>69场周赛</summary>

#### platform acwing
#### id acwing 

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>大家都说原题的t2失误了，没办法，稍微有一点点慢，不过是个还不错的题目，t3找规律，一眼题目，式子写好就能看出来</details>
- <details><summary>tag</summary>t2不错</details>
- spendtime: 44
- ACtime: 2022.9.17
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>68场周赛</summary>

#### platform acwing
#### id acwing 

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>名次不错，没wa一发，稳过，t3写的非常的长，而且写的自己都懵掉了，不过竟然一遍过掉了，还不错，这么说吧，其实走了好几个弯路，重点在于预处理，想方设法也要将时间复杂度控制好，注意到k很小，可以这么思考，枚举子集，每个子集表示匹配的位数，对(0-1<< n)之间的数来说，若某些位匹配，不匹配位必然取反，先求出来，多少子集的值是0-100之间的，然后再求出对于一个val来说，符合匹配值为k的有多少个，最终得到的就是map[k][集合大小]，则查询的时候，将0-qk的全部加起来即可，确实挺绕的，不过总体还好</details>
- <details><summary>tag</summary>t3很烦</details>
- spendtime: 53
- ACtime: 2022.9.10
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>67场周赛</summary>

#### platform acwing
#### id acwing 

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>写崩了，不知道为啥，一个hashmap还要卡常数，真的无语，第二题暴力吧，真的，写崩了，什么毒瘤题目，以后多想想hashmap这种,脑子好想有一点点僵</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 60
- ACtime: 2022.9.3
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>66场周赛</summary>

#### platform acwing
#### id acwing 

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>还好，犯了一些特别粗心的错误，比如忘记写输出了，另一方面，条件写错了，wa了两发，不过还行吧，总体来说，t1过于简单，t2是一个分析模拟，也比较好弄，t3看似是个集合dp，实际上并不是，而是分析，对其中的数值做一些计数分析，分为1,2,>=3,的，只有1和>=3的有用，然后讨论下即可，这样就好了，比较清晰，没啥表达的难点</details>
- <details><summary>tag</summary>分析+模拟</details>
- spendtime: 38
- ACtime: 2022.8.27
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>65场周赛</summary>

#### platform acwing
#### id acwing 

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>写崩了的一周，t2写崩了，t3不会，t3非常的脑筋急转弯，因为是求曼哈顿距离，对于4点以上的凸变形来说，一定可以被一个外接四边形包围，所以直接就可以求边界点即可，然后对于三角形是个特殊情况，细心观察发现，对于任意一点来说，枚举四种边界即可，这道题目来自cf1044c，是一个2100分的题目，这都能出？行吧，看来以后得额外关注了cf的题目了</details>
- <details><summary>tag</summary>脑筋急转弯</details>
- spendtime: 120
- ACtime: 2022.8.20
- ACstatus: 不会
- score: 7
</details>
</details>

<details>
<summary>64场周赛</summary>

#### platform acwing
#### id acwing 

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>后两道题目还是有意思的，都是计数优化，T3就是使用两个哈希表去维护异或前缀和，T4最终分析出数学式子，如果相遇，则y-ax,的值要相同，且不能想等，也是使用两个哈希表去统计即可，还是分析太懒了一点，给后两题还是点个赞</details>
- <details><summary>tag</summary>计数优化</details>
- spendtime: 30
- ACtime: 2022.8.14
- ACstatus: 看了提示
- score: 6
</details>
</details>

<details>
<summary>63场周赛</summary>

#### platform acwing
#### id acwing 

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>看了一下T3，其实是个大胆分析猜测，最大的个数是3，最小是1 ，直接就做出来了,很简单的</details>
- <details><summary>tag</summary>分析</details>
- spendtime: 20
- ACtime: 2022.8.7
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>62场周赛</summary>

#### platform acwing
#### id acwing 

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>虽然没有参加但是赛后补了一下题目，前两道比较简单，最后一道题目，把我绕进去了，刚开始写了个n*logn^2的算法，tle了，debug了半天，后来仔细观察，加上一点点提示，恍然大悟，原来是自己想错了，其实直接就是不断调整平均数，最大的差一定是，从前面连续选择一段数，加最后一个数，而且观察可知，前面连续的一段数一定是不断增加的，则这不就一个O（n）的算法就出来了，代码也很短，这就是贪心，这个T3还是要给一定的优评的，还不错</details>
- <details><summary>tag</summary>贪心</details>
- spendtime: 120
- ACtime: 2022.8.1
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>61场周赛</summary>

#### platform acwing
#### id acwing 

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>什么玩意，T3我给差评了，又在搞笑，什么玩意啊，重合的特判有大大的问题啊，窒息，其他的非常简单</details>
- <details><summary>tag</summary>模拟+dp+数学</details>
- spendtime: 50
- ACtime: 2022.7.23
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>点的赋值</summary>

#### platform acwing
#### id acwing 4415

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这道题目还是可以的，acwing感觉比较好的一道题目了，难度适中，不难理解，代码也比较简单，首先我们判断一下，图中是否存在奇数环，使用图染色，如果存在，则直接为0，如果不存在，则全为偶环，但是需要统计一下，染色过程中，到底染色的情况，比如就0，1染色，都那么有多少的0，多少个1，则这个联通块的方案数，就是2^a+2^b的总数，这个可以简单画图进行规律探索，则将所有联通块乘起来即可</details>
- <details><summary>tag</summary>dp+奇环</details>
- spendtime: 50
- ACtime: 2022.5.5
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>查询字符串</summary>

#### platform acwing
#### id acwing 4398

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>感觉整了个nt题，不知道有啥做的</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 30
- ACtime: 2022.4.26
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>构造数组</summary>

#### platform acwing
#### id acwing 4412

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>acwing上面的题目重名率还是比较大的，这道题目考查的其实，是区间合并，方式有很多，但就是最前面一段的判断要小心处理，使用个set去维护即可，最终达到了nlogn的复杂度</details>
- <details><summary>tag</summary>区间合并</details>
- spendtime: 50
- ACtime: 2022.4.25
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>最大子矩阵</summary>

#### platform acwing
#### id acwing 4395

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>确实把我整住了，首先分析了一下，觉得这应该是个n^2logn的做法，但是还是比较卡的，因为确实处于临界，在这平台上就没给过，但，其实转念一想，因为要跟别的最大矩形的题目区别开，首先，对于元素全为固定的元素的，两种，0,1为例子，可以将每一维度的累积和加入，变成一维的时候，就可以使用单调栈，其次，对于每一个元素都不一样，既有负数又有正数，想办法优化即可，最后，就到了本题目，矩阵中的数，是有很强的规律性，所以，分析，发现，我们可以求，每一维度区间的最小值，贪心，即对于a来说，可以求长度为1，2，3，，，n的最小值即可，所以是个n^2的算法，好吧，晃了我</details>
- <details><summary>tag</summary>分析+贪心</details>
- spendtime: 40
- ACtime: 2022.4.23
- ACstatus: 看提示后会
- score: 7
</details>
</details>

<details>
<summary>翻转树边</summary>

#### platform acwing
#### id acwing 4381

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这种dp讲讲道理，这个名字起的还是不错的，换根dp一般求两次，状态定义和阶段多了一些内涵，增加维度，虽然都属于树形dp，不过技巧性还是蛮强，不算个特别难的技巧，或许硬生生的做，也是可以的，但这个是有个好听的名字，所以，尽量的使得自己的东西，更加的通用化，而不要自己做出来了，就不去看看，更加优雅的分析和做法。将原图换成，正向边0，反向边1，这样，第一遍dfs，就可以求出子树为根的需要的翻转数，第二遍，因为子节点和父节点之间有了dp的关系，即转移经分析是可以分析出来的。</details>
- <details><summary>tag</summary>换根dp</details>
- spendtime: xx
- ACtime: 2022.4.21
- ACstatus: 看提示后会
- score: 7
</details>
</details>

<details>
<summary>合适数对</summary>

#### platform acwing
#### id acwing 4319

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>不小心看错题目了一次，所以写的有点长，但就是一个hash和质因数分解的题目，主要的思路还是将状态化为最小的表示即可</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 70
- ACtime: 2022.4.20
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>选取数对</summary>

#### platform acwing
#### id acwing 4378

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>题目有点拉胯讲道理</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 14
- ACtime: 2022.4.19
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>合适数对</summary>

#### platform acwing
#### id acwing 4316

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>经典题目了，对于前缀和的子数组统计</details>
- <details><summary>tag</summary>离散化+树状数组</details>
- spendtime: 23
- ACtime: 2022.4.15
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>满二叉树等长路径 </summary>

#### platform acwing
#### id acwing 4313

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>不断在上层增加，不断递归下去，就可以了，因为，在上层增加，一定不可能比在下层增加更差，算是一个看了题目，思路就直接出来的</details>
- <details><summary>tag</summary>贪心+递归</details>
- spendtime: 35
- ACtime: 2022.4.14
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>树的DFS</summary>

#### platform acwing
#### id acwing 4310

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>java应对这种大规模的输入还是性能上还是不太友好，还是需要思考怎么样子能够应对，不过题目的思路应该是比较直观且简单，思路大概1分钟就出来了</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 20+(主要是java读入过慢的问题)
- ACtime: 2022.4.13
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>数字重构</summary>

#### platform acwing
#### id acwing 4307

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>枚举贪心，判断即可</details>
- <details><summary>tag</summary>贪心</details>
- spendtime: 30
- ACtime: 2022.4.12
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>截断数列</summary>

#### platform acwing
#### id acwing 4301

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>看到数据直接dp了，其实枚举段和也行，数据量反正小，无所谓</details>
- <details><summary>tag</summary>分析ordp</details>
- spendtime: 6
- ACtime: 2022.4.8
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>搭档</summary>

#### platform acwing
#### id acwing 4298

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>非常裸的二分图匹配，非常简单，直接开整就行，主要是数据量足够小，当数据量大的时候，那就不一定，可以考虑考虑网络流，但最佳的方法还是贪心，排序一下，然后尝试双指针，可能在另一道题目中，就会将数据范围进行更改，所以还是要小心啊</details>
- <details><summary>tag</summary>二分图匹配or贪心</details>
- spendtime: 6
- ACtime: 2022.4.7
- ACstatus: 完全会
- score: 5当数据量变大则6
</details>
</details>

<details>
<summary>机器人移动</summary>

#### platform acwing
#### id acwing 4217

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>再一次被二分判定逗得团团转了，无比的生气了，怎么可以这样，简直非常无语，思路被引得偏了，关于如何枚举区间我仔细想了想，在此处有一小点没有想清楚，二分可以如此操作，可以外面套个大二分，即直接二分区间长度L，而后枚举所有区间长度为L的区间，再去判断，个人认为这样子，应该还是可以做到全覆盖的，第二种，从前到后遍历，在每一个索引出，二分，此处就存在了一点小东西，当区间长度虽然大于需要的曼哈顿距离，但奇偶号不同时，是仍然将长度缩小，但答案不计入，这点还是存在一点小小的疑问，虽然可以ac，不过从直观上，还是觉得，没有从前往后枚举区间长度得来的好，特此记录又一个失败的二分，想复杂了</details>
- <details><summary>tag</summary>二分判断</details>
- spendtime: xx严重超时
- ACtime: 2022.4.6
- ACstatus: 看提示后会
- score: 7
</details>
</details>

<details>
<summary>三元组</summary>

#### platform acwing
#### id acwing 4214

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>两遍dp即可，树状数组可用可不用，只是一种优化</details>
- <details><summary>tag</summary>dp+树状数组</details>
- spendtime: 20
- ACtime: 2022.4.1
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>序列重排</summary>

#### platform acwing
#### id acwing 4211

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>随便搞，感觉问题不大</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 20
- ACtime: 2022.4.1
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>电话号码</summary>

#### platform acwing
#### id acwing 4208

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>就这困难？模拟吧</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: xx
- ACtime: 2022.4.1
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>构造矩阵</summary>

#### platform acwing
#### id acwing 4204

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>单纯思考，编码无压力，做法两步走，第一步从任意的全排列出发，每一行讲上一行的首元素移到尾部，第二步，将对称轴中间的元素置换成0，将被置换的元素移到尾部即可，对称的另一边也如此操作，还是稍微有一点需要脑筋急转弯的，看来自己的脑子还是不太行哦</details>
- <details><summary>tag</summary>脑筋急转弯</details>
- spendtime: 30
- ACtime: 2022.3.31
- ACstatus: 看提示会
- score: 5
</details>
</details>

<details>
<summary>树的增边</summary>

#### platform acwing
#### id acwing 4205

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接二分标记，乘起来直接减</details>
- <details><summary>tag</summary>分析</details>
- spendtime: 20
- ACtime: 2022.3.31
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>穿过圆</summary>

#### platform acwing
#### id acwing 4202

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>仔细读题，发现，就是求不同所在圆的个数，主要y总不是说测评机很牛吗，算了改成bitset了</details>
- <details><summary>tag</summary>分析</details>
- spendtime: 思考3+写30
- ACtime: 2022.3.30
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>公约数</summary>

#### platform acwing
#### id acwing 4196

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>求最大公约数，然后试除法，将结果存储，二分取结果即可，因为对于n的数来说，最多有n^0.5个约数</details>
- <details><summary>tag</summary>简单数论</details>
- spendtime: 20
- ACtime: 2022.3.29
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>最短路径</summary>

#### platform acwing
#### id acwing 4196

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>就是最短路的回溯</details>
- <details><summary>tag</summary>最短路</details>
- spendtime: 25
- ACtime: 2022.3.28
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>线段覆盖</summary>

#### platform acwing
#### id acwing 4195

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>fastinput还是有些问题，没办法，具体原因未知，遂改成bufferreader算了，这道题目贪心，差分做都可以，只是每一个点代表的意思需要仔细思考就行，时间长是因为有未知的bug干扰我</details>
- <details><summary>tag</summary>贪心or差分</details>
- spendtime: 60
- ACtime: 2022.3.28
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>号码牌</summary>

#### platform acwing
#### id acwing 4084

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>此题目又证明了fastinput的正确性，题目思路很直观，并一下，查一下即可</details>
- <details><summary>tag</summary>并查集</details>
- spendtime: 10
- ACtime: 2022.3.26
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>选数</summary>

#### platform acwing
#### id acwing 4081

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>悬案题目了，我不知道我fastinput这个templete有什么问题，非常之奇怪，把5作为维度压缩的，dp[i][j][k]表示选择i个数，且已经选择了j个数，此时已经有k个5的最多的2有多少个，确实有点小小的技巧，这点是我没有想到的，不过acwing支持10^8级别的运算我是真没想到</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 
- ACtime: 2022.3.26
- ACstatus: 看提示后会
- score: 7
</details>
</details>

<details>
<summary>01串</summary>

#### platform acwing
#### id acwing 4078

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>分析，问题具有层次的递推关系，经思考，状态，阶段，转移非常清楚，因为新追加的只能是0或1</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 30
- ACtime: 2022.3.24
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>染色</summary>

#### platform acwing
#### id acwing 4075

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>就是个并查集</details>
- <details><summary>tag</summary>并查集</details>
- spendtime: xx
- ACtime: 2022.3.23
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>习题册</summary>

#### platform acwing
#### id acwing 4072

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>分别存储就行了，很简单</details>
- <details><summary>tag</summary>分析</details>
- spendtime: 25
- ACtime: 2022.3.22
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>取石子游戏</summary>

#### platform acwing
#### id acwing 4005

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>只要在纸上分析，找规律，自然就很快</details>
- <details><summary>tag</summary>博弈论分析</details>
- spendtime: 30
- ACtime: 2022.3.21
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>构造数组</summary>

#### platform acwing
#### id acwing 4002

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>二维前缀和dp，简单分析即可，状态转移都不难想</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 35
- ACtime: 2022.3.19
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>训练</summary>

#### platform acwing
#### id acwing 4001

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>统计加排序即可</details>
- <details><summary>tag</summary>排序</details>
- spendtime: 20
- ACtime: 2022.3.19
- ACstatus: 完全会
- score: 4
</details>
</details>

<details>
<summary>变成1</summary>

#### platform acwing
#### id acwing 3998

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>分析出来即可线性即可解决</details>
- <details><summary>tag</summary>分析</details>
- spendtime: 15
- ACtime: 2022.3.18
- ACstatus: 完全会
- score: 4
</details>
</details>

<details>
<summary>最大公约数</summary>

#### platform acwing
#### id acwing 3999

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>只要一步一步思考，分析出，如何才能互质，先来一遍gcd，用m除，然后直接求欧拉即可</details>
- <details><summary>tag</summary>欧拉函数</details>
- spendtime: 38
- ACtime: 2022.3.18
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>最小的和</summary>

#### platform acwing
#### id acwing 3995

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>看清楚题目即可，简单的贪心</details>
- <details><summary>tag</summary>优先级队列+贪心</details>
- spendtime: 15
- ACtime: 2022.3.17
- ACstatus: 完全会
- score: 4
</details>
</details>

<details>
<summary>涂色</summary>

#### platform acwing
#### id acwing 3996

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>只要分析出来，最终的颜色一定是边界的颜色即可，并且转移来源于内部，对于区间(i,j)，如果i，j处的相同，那么一定是从(i+1,j-1)转移而来，如果不同，则从(i+1,j)or(i,j-1)转移而来，想明白则很好写，不过，问题在于，c++写的实在是太垃圾了，完全不会写，而且vector真的慎用啊，开辟，千万左右的数组，那最好就别开了，还是老老实实，静态开辟吧，真的是吃了大大的亏,而且有更诡异的事情，就在于，java和c++使用了同一套代码，但是，java死活说是有问题，不明白非常困扰我,替换了一下，好像是我缩点搞错了，那为什么c++能过，这也太诡异了,然后从新写了缩点的逻辑，又完全没有问题，我真的搞不懂了</details>
- <details><summary>tag</summary>区间dp</details>
- spendtime: 20+xx
- ACtime: 2022.3.17
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>树上有猴</summary>

#### platform acwing
#### id acwing 3992

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>累加即可，求出最大最小区间差值</details>
- <details><summary>tag</summary>分析</details>
- spendtime: 10
- ACtime: 2022.3.16
- ACstatus: 完全会
- score: 4
</details>
</details>

<details>
<summary>石子游戏</summary>

#### platform acwing
#### id acwing 3993

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>思路直接就出来了，主要是，语言写的不够熟练，所以时间上小长</details>
- <details><summary>tag</summary>差分数组</details>
- spendtime: 20
- ACtime: 2022.3.16
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>看图做题</summary>

#### platform acwing
#### id acwing 3989

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>题如题名，找规律即可</details>
- <details><summary>tag</summary>找规律</details>
- spendtime: 10
- ACtime: 2022.3.15
- ACstatus: 完全会
- score: 4
</details>
</details>

<details>
<summary>砍树</summary>

#### platform acwing
#### id acwing 3990

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这是个脑筋急转弯，重点在于分析，是否能分析出来才是重点，先求一遍，子树和，对于子树是偶数的即可加一，只要想明白这个就行了，时间很长还是主要是因为，c++用的不习惯</details>
- <details><summary>tag</summary>脑筋急转弯</details>
- spendtime: 60
- ACtime: 2022.3.15
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>平衡数组</summary>

#### platform acwing
#### id acwing 3778

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>读题，在纸上模拟一下，就出来了</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 10.0
- ACtime: 2021.11.16
- ACstatus: 完全会
- score: 3.0
</details>
</details>

<details>
<summary>相等的和</summary>

#### platform acwing
#### id acwing 3779

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这题出的，很可以，考秒了</details>
- <details><summary>tag</summary>数学</details>
- spendtime: 30.0
- ACtime: 2021.11.16
- ACstatus: 完全会
- score: 5.0
</details>
</details>
<details>
<summary>构造数组</summary>

#### platform acwing
#### id acwing 3780

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>编码比较长，而且思路不是那么直观，首先需要看题想题，去发现考得是什么，后来，通过，大胆猜测加读题意，得出了，我们最终需要构造成山丘的样子，那么需要知道哪一个山丘是最下消耗，结合动态规划加单调栈，可以写出，比较麻烦，但过程很爽</details>
- <details><summary>tag</summary>dp+单调栈</details>
- spendtime: 90.0
- ACtime: 2021.11.16
- ACstatus: 完全会
- score: 6.0
</details>
</details>
<details>
<summary>整除</summary>

#### platform acwing
#### id acwing3787

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>模拟</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 5.0
- ACtime: 2021.11.17
- ACstatus: 完全会
- score: 2.0
</details>
</details>
<details>
<summary>截断数组</summary>

#### platform acwing
#### id acwing 3788

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>枚举截断点</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 15.0
- ACtime: 2021.11.17
- ACstatus: 完全会
- score: 3.0
</details>
</details>
<details>
<summary>隐藏字符串</summary>

#### platform acwing
#### id acwing 3789

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>可以发现一个重要的性质，即，如果有一个更长的序列满足条件，一定存在比它更短的序列满足条件，所以，只需要枚举一下长度为2的所有序列的组成即可</details>
- <details><summary>tag</summary>看穿题本质</details>
- spendtime: 30.0
- ACtime: 2021.11.17
- ACstatus: 完全会
- score: 5.0
</details>
</details>
<details>
<summary>最大化最短路</summary>

#### platform acwing
#### id acwing 3797

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>题目出的不错，分析和结合是可以作为下一次训练的</details>
- <details><summary>tag</summary>bfs+贪心</details>
- spendtime: 60.0
- ACtime: 2021.11.18
- ACstatus: 完全会
- score: 6.0
</details>
</details>
<details>
<summary>凑平方</summary>

#### platform acwing
#### id acwing 3796

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>虽然第一次写没有最优但是，直接枚举真的不错</details>
- <details><summary>tag</summary>枚举</details>
- spendtime: 18.0
- ACtime: 2021.11.18
- ACstatus: 完全会
- score: 5.0
</details>
</details>
<details>
<summary>计算abc</summary>

#### platform acwing
#### id acwing 3795

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>利用大小关系，直接确定</details>
- <details><summary>tag</summary>排序</details>
- spendtime: 10.0
- ACtime: 2021.11.18
- ACstatus: 完全会
- score: 3.0
</details>
</details>
<details>
<summary>数组去重</summary>

#### platform acwing
#### id acwing 3803

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>收集，从后向前</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 8.0
- ACtime: 2021.11.19
- ACstatus: 完全会
- score: 3.0
</details>
</details>
<details>
<summary>构造字符串</summary>

#### platform acwing
#### id acwing 3804

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>简单分析，从后向前即可</details>
- <details><summary>tag</summary>贪心</details>
- spendtime: 22.0
- ACtime: 2021.11.19
- ACstatus: 完全会
- score: 4.0
</details>
</details>
<details>
<summary>环形数组</summary>

#### platform acwing
#### id acwing 3805

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>主要就是没有板子肯定不会</details>
- <details><summary>tag</summary>线段树</details>
- spendtime: 30.0
- ACtime: 2021.11.19
- ACstatus: 完全会
- score: 6.0
</details>
</details>
<details>
<summary>排列</summary>

#### platform acwing
#### id acwing 3811

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接强行构造就行</details>
- <details><summary>tag</summary>构造</details>
- spendtime: 6.0
- ACtime: 2021.11.20
- ACstatus: 完全会
- score: 3.0
</details>
</details>
<details>
<summary>机器人走迷宫</summary>

#### platform acwing
#### id acwing 3812

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>枚举分配，然后模拟</details>
- <details><summary>tag</summary>模拟+枚举</details>
- spendtime: 30.0
- ACtime: 2021.11.20
- ACstatus: 完全会
- score: 5.0
</details>
</details>
<details>
<summary>最大路径权值</summary>

#### platform acwing
#### id acwing 3813

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>代码很长，真的得好好考虑java得输入输出太费时间了</details>
- <details><summary>tag</summary>dp+拓扑排序</details>
- spendtime: 60.0
- ACtime: 2021.11.20
- ACstatus: 完全会
- score: 6.0
</details>
</details>
<details>
<summary>区间选数</summary>

#### platform acwing
#### id acwing 3821

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接取两端</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 6.0
- ACtime: 2021.11.22
- ACstatus: 完全会
- score: 3.0
</details>
</details>
<details>
<summary>食堂排队</summary>

#### platform acwing
#### id acwing 3822

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>模拟即可</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 15.0
- ACtime: 2021.11.22
- ACstatus: 完全会
- score: 4.0
</details>
</details>
<details>
<summary>寻找字符串</summary>

#### platform acwing
#### id acwing 3823

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>最大公共前后缀，一开始觉得字符串hash能直接二分解，后来，发现，是我天真的，对于这种前后缀问题，一定是kmp，其次，对于kmp的理解问题，求最大，次大，次次大，的求法，但个人觉得，最后产生了三种做法，第一种，直接先拿到next[n-1]，然后直接从大到小枚举，在原数组中直接check，因为数据的问题，算法很快，应该是数据很难构造，第二，从next[1]开始，只用判断，判断结尾有即可，算法的消耗体现在快速幂计算，会比O(n)慢，第三种，求出max(next[1]-next[n-2])，然后再通过...next[next[n-1]],求出最大，次大，次次大，直到有一个比max小，即为求到</details>
- <details><summary>tag</summary>kmp</details>
- spendtime: 40.0
- ACtime: 2021.11.22
- ACstatus: 完全会
- score: 6.0
</details>
</details>
<details>
<summary>青蛙跳</summary>

#### platform acwing
#### id acwing 3826

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接统计</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 8.0
- ACtime: 2021.11.23
- ACstatus: 完全会
- score: 2.0
</details>
</details>
<details>
<summary>最小正整数</summary>

#### platform leetcode
#### id acwing 3827

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>翻车了的一道题，记录一下，这么简单的，一道题目，想复杂化了，所以就应该直接想办法数学</details>
- <details><summary>tag</summary>数论</details>
- spendtime: 不详
- ACtime: 2021.11.23
- ACstatus: 完全会
- score: 5.0
</details>
</details>

<details>
<summary>行走路径</summary>

#### platform acwing
#### id acwing 3828

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>估计java建图实在是太了，但思路和3813几乎一样</details>
- <details><summary>tag</summary>建图+记忆化搜索</details>
- spendtime: 60.0
- ACtime: 2021.11.23
- ACstatus: 完全会
- score: 6.0
</details>
</details>
<details>
<summary>统一大小写</summary>

#### platform acwing
#### id AcWing 3955

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接转小写或者大写，比较不同即可</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 5.0
- ACtime: 2021.11.24
- ACstatus: 完全会
- score: 2.0
</details>
</details>

<details>
<summary>截断数组</summary>

#### platform acwing
#### id AcWing 3956

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>一种计数优化</details>
- <details><summary>tag</summary>计数优化</details>
- spendtime: 18.0
- ACtime: 2021.11.124
- ACstatus: 完全会
- score: 4.0
</details>
</details>
<details>
<summary>子序列</summary>

#### platform acwing
#### id AcWing 3957

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>最小的一定是长度是3，所以，我们只需要动态维护，前后的数组的信息即可</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 50.0
- ACtime: 2021.11.24
- ACstatus: 完全会
- score: 5.0
</details>
</details>
<details>
<summary>最小的商</summary>

#### platform acwing
#### id acwing 3971

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>模拟</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 5.0
- ACtime: 2021.12.4
- ACstatus: 完全会
- score: 2.0
</details>
</details>
<details>
<summary>方格集数量</summary>

#### platform acwing
#### id acwing 3972

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>子集个数</details>
- <details><summary>tag</summary>数学</details>
- spendtime: 13.0
- ACtime: 2021.12.4
- ACstatus: 完全会
- score: 4.0
</details>
</details>
<details>
<summary>无线网络</summary>

#### platform acwing
#### id acwing 3973

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>左右求最小即可</details>
- <details><summary>tag</summary>二分</details>
- spendtime: 13.0
- ACtime: 2021.12.4
- ACstatus: 完全会
- score: 4.0
</details>
</details>
<details>
<summary>乌龟棋</summary>

#### platform acwing
#### id acwing312

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>虽然是个简单题，但其实状态的定义都有不一样的，我想到的是跟到达那个点有关的dp,而使用所剩下的数目作为状态也行，而且转移竟然更加的直观 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>花店橱窗</summary>

#### platform acwing
#### id acwing313

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>dp+回溯，虽然是进阶指南的简单题，但我觉得麻烦程度丝毫不输给hard，注意回溯数组的定义，保证字典序最小的话，就从后向前 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>回家</summary>

#### platform acwing
#### id acwing 408

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>只是一个知识点问题，没啥特殊的，证明了，写的模板还是十分的正确的，但是此模板是有弊端的，只能使用于，数据在int的范围内的情况，当需要大的数据的时候，需要double的时候，还是有一些差别的，二分带权图匹配的KM算法 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>蚂蚁</summary>

#### platform acwing
#### id acwing 375

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>难在转化，不相交转化为距离之和最短，另外的难点在于，一定开根之和处理，double的比较注意注意，一定是小于一个精度，确实懒得写了，哈哈，二分权图匹配km算法执行就可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>