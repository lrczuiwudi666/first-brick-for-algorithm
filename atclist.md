<details>
<summary>AtCoder Beginner Contest 126</summary>
<details>
<summary>lrc</summary>

<details>
<summary>D - Even Relation</summary>

- <details><summary>comment</summary>只需要固定根节点即可，直接往下搜索即可，想明白这件事，就比较容易了，偶数不变，奇数变</details>
- <details><summary>tag</summary>dfs</details>
- spendtime: 25
- ACtime: 2022.12.6
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>E - 1 or 2</summary>

- <details><summary>comment</summary>就是求联通域的个数，看破之后就比较简单</details>
- <details><summary>tag</summary>并查集</details>
- spendtime: 25
- ACtime: 2022.12.7
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>F - XOR Matching</summary>

- <details><summary>comment</summary>就差一点的思维题，已经想出来前一半，后一半傻逼了，因为0-2^(m-1)的异或结果就是0，所以抽出一个数就是x,则很容易构造，x 0 1 2 ... x ... 2 1 0 即可，就差一点唉</details>
- <details><summary>tag</summary>脑筋急转弯</details>
- spendtime: 30
- ACtime: 2022.7.19
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 127</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Cell Distance</summary>

- <details><summary>comment</summary>这种数论题目，其实代码量非常的短，但是思维要求很高，一般直接就跪了，题越短，思维就越高，对于组合数学非常的难跨越，本题目中只需要先固定两点，将x，y，拆开来计算，枚举d，则对于每一种d来说，有m*m个选择，一共有n-d种距离d的选择，则这种d的所有贡献就是d*m*m*(n-d)*C(n*m-2,k-2)因为固定了两个之后剩余的随便选，不错的数学题目</details>
- <details><summary>tag</summary>组合数学</details>
- spendtime: 90
- ACtime: 2022.7.19
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 128</summary>
<details>
<summary>lrc</summary>

<details>
<summary>D - equeue</summary>

- <details><summary>comment</summary>直接模拟最后的操作结果即可，然后贪心选择剔除最小的</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 30
- ACtime: 2022.12.7
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>E - Roadwork</summary>

- <details><summary>comment</summary>感觉难度上不算是个这个分数的题目，只是代码量稍微大点，这道题目，让我联想到了，abc260的E题目，有异曲同工之妙，感觉就是个很简单的东西，把维护的东西维护好，将问题转为对区间的处理即可，思路还是比较好出来，果然又是数据类型溢出的锅，让我debug了老半天</details>
- <details><summary>tag</summary>分析模拟</details>
- spendtime: 75
- ACtime: 2022.7.18
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 129</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Sum Equals Xor</summary>

- <details><summary>comment</summary>有点看走眼，还是有点走偏，数位dp其实挺直观的，还是没有明白啊，包括细节错好多</details>
- <details><summary>tag</summary>数位dp</details>
- spendtime: 80
- ACtime: 2022.7.18
- ACstatus: 看提示会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 130</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E: Common Subsequence</summary>

- <details><summary>comment</summary>离成功就差一点点，还是有个转化过程没想清楚，dp[i][j]为公共的方案数，不想等时候的转移很清楚，dp[i][j]=dp[i][j-1]+dp[i-1][j]-dp[i-1][j-1],但是想等的时候，不知道为啥没想清楚，其实就是在不选的基础上，多加个dp[i-1][j-1]即可，唉，真的就差一点,想了想还是比较抽象，建议使用(1,1)(1,1)来试一试，纸上画一画就比较清楚</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 60
- ACtime: 2022.7.18
- ACstatus: 不会
- score: 6
</details>
</details>
</details>
</details>


<details>
<summary>AtCoder Beginner Contest 131</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Friendships</summary>

- <details><summary>comment</summary>还不错，构造题不错，很思维，但自己这次思维竟然很精准，我觉得非常的好，好久没看到思维题目了，真的不错，非常的精准的进了我的舒适圈。</details>
- <details><summary>tag</summary>构造题</details>
- spendtime: 28
- ACtime: 2022.12.3
- ACstatus: 完全会
- score: 7
</details>

<details>
<summary>F - Must Be Rectangular!</summary>

- <details><summary>comment</summary>问题转换牛逼！！！，这应该是某种经典转换，行列个看成点，属于一个连通域的均可相互转化，并查集分别统计下左部和右部即可，问题只要一转化，超级简单，不会是正常的，积累积累</details>
- <details><summary>tag</summary>转换+并查集</details>
- spendtime: 50
- ACtime: 2022.7.18
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 132</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Hopscotch Addict</summary>

- <details><summary>comment</summary>没反应过来，直接dp，bfs探索即可，有点犯傻，确实有点不应该</details>
- <details><summary>tag</summary>dp+bfs</details>
- spendtime: 80
- ACtime: 2022.7.18
- ACstatus: 没反应过来
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 133</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Sum Equals Xor</summary>

- <details><summary>comment</summary>直接dfs就可以了，我实属没有想到，我觉得还是有一定的思维难度的，感觉卡这种东西是我智商的问题吗，没转过弯来，嗨真烦</details>
- <details><summary>tag</summary>dfs</details>
- spendtime: 60
- ACtime: 2022.12.2
- ACstatus: 看提示会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 134</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Sequence Decomposing</summary>

- <details><summary>comment</summary>维护一个treemap即可，每次尽可能的使得一个递增队列增长最缓慢即可</details>
- <details><summary>tag</summary>贪心</details>
- spendtime: 28
- ACtime: 2022.12.6
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 135</summary>
<details>
<summary>lrc</summary>

<details>
<summary>D - Digits Parade</summary>

- <details><summary>comment</summary>简单dp，没啥好说的，直接暴力dp即可</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 30
- ACtime: 2022.12.6
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 136</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Max GCD</summary>

- <details><summary>comment</summary>最大的能转变成的公约数一定是sum的公约数，则枚举所有的约数即可，然后计算最少需要多少步，其中是贪心思想，从小到大排序，前面的补后面的就是最小，这样子就可以在sqrt(sum)*NlogN时间完成</details>
- <details><summary>tag</summary>分析+贪心</details>
- spendtime: 50
- ACtime: 2022.7.18
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 137</summary>
<details>
<summary>lrc</summary>

<details>
<summary>D - Summer Vacation</summary>

- <details><summary>comment</summary>这么考虑，直接对价值先排序，再用时间的大小再排序，从前向后，另一种，考虑从后往前，选满足条件后最大的即可，考虑这两个区别，我觉得这道题特别坑，贪心是否能对，完全取决于到底有没有想反，可以考虑下面的例子（（1，2），（1，4），（2，3）），如果按照第一种贪心过程，则会先选4，再选2，但如果按照第二种，则会先选择4，再选择3，这就是区别，这块贪心太恶心了，贪心靠直觉我直觉没有想出来，还是十分的不知所措的，真的是毒瘤了</details>
- <details><summary>tag</summary>贪心</details>
- spendtime: 80
- ACtime: 2022.12.5
- ACstatus:  看提示依然存有疑问
- score: 7
</details>

<details>
<summary>E - Coins Respawn</summary>

- <details><summary>comment</summary>建图的过程没错，选择的算法也没错，但是疑问就在于，有三个test有点奇怪，可能是增量的过程很久才能传导，所以还是老实的改一下写法会更好一些，跑一遍逆图，然后在逆图的基础上再跑bellman算法即可，温习了一下含负权重的图的最长路问题</details>
- <details><summary>tag</summary>bellman最长路</details>
- spendtime: 90
- ACtime: 2022.7.16
- ACstatus: 小疑问
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 138</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Strings of Impurity</summary>

- <details><summary>comment</summary>使用treeset就可以了</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 30
- ACtime: 2022.12.5
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 139</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - League</summary>

- <details><summary>comment</summary>这个题目还是让我写了比较长的时间的，为什么呢？理由如下，刚开始写了每次循环时候都检查，所以复杂度是n^3，肯定超标，则思考算法的优化改进，其实可以观察，每一步我们只需要关注变换的索引即可，大大降低复杂度，虽然会带来一部分常数开销，还是比较码农的一道题目，我觉得确实还不错</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 120
- ACtime: 2022.12.1
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 140</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Second Sum</summary>

- <details><summary>comment</summary>只需要分析出来，将原始的求和过程，变成每个元素能有多少贡献值，即这个数作为第二大的数在多少个区间内部即可，找出左边只要比x大的两个数且最近，右边比x大的两个数即可，一般想要找出一个使用单调栈即可，二个的话我就无脑分块了，其实找也没有那么困难，利用贪心，从大往小的来，使用treeset存一下就行，很容易就求出，我竟然没有想到，真的愚蠢</details>
- <details><summary>tag</summary>分析+（分块or二分）</details>
- spendtime: 90
- ACtime: 2022.7.16
- ACstatus: 完全会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 141</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Who Says a Pun?</summary>

- <details><summary>comment</summary>还是经验给少了，试了好多素数都不行，只能用最愚蠢的办法了，增加一层判断，还是有点蠢的,也同样可以使用z函数来写这道题目，效率也是很高的，重新复习一下z函数的语义</details>
- <details><summary>tag</summary>字符串hash+解决冲突</details>
- spendtime: 50
- ACtime: 2022.11.30
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 142</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Get Everything</summary>

- <details><summary>comment</summary>这个dp还不错，我想当然的写错了一次，不过知迷途而返，总体上还行，算是个简单但是中间转移的过程确实需要仔细想清楚的题目，适合作为lc周赛的T4</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 30
- ACtime: 2022.11.29
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 143</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Travel by Car</summary>

- <details><summary>comment</summary>想到了floyd算法，但没做好处理，简单来说，先求一遍最短路，生成一张图的最短路距离，然后把(i,j)之间<=L的记为加一次油，则在这个图上再跑一遍floyd即可，还是没处理好，学到了</details>
- <details><summary>tag</summary>floyd</details>
- spendtime: 90
- ACtime: 2022.7.14
- ACstatus: 不会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 144</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Gluttony</summary>

- <details><summary>comment</summary>二分不小心搞错了，第一次还胡乱写了一个，贪心思考中间过程</details>
- <details><summary>tag</summary>二分</details>
- spendtime: 33
- ACtime: 2022.11.29
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 145</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - All-you-can-eat</summary>

- <details><summary>comment</summary>经典背包问题，加了一些别的小处理，注意排序，其余的还行</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 42
- ACtime: 2022.11.29
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 146</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Rem of Sum is Num</summary>

- <details><summary>comment</summary>先写数学式子，然后就能得到技术优化的推导，再者，考虑到一个细节，即这个区间是不断变换的，因为有个数的限制，区间范围是k-1，此处的k-1还是要格外注意，第一次就是想了很久为啥结果老比正常的大，在纸上重新想了一遍之后，发现，是区间没有搞好，然后就可以了</details>
- <details><summary>tag</summary>计数优化</details>
- spendtime: 80
- ACtime: 2022.7.14
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>F - Sugoroku</summary>

- <details><summary>comment</summary>调的还算顺利吧，这种题目时间确实是要花这么多的，这倒是没有办法，加上回溯的都很麻烦，然后还有区间最值dp，再融合上贪心，还是有一些困难的，就是码农，但不至于不会，小细节还是要仔细写，我要快速写代码啊</details>
- <details><summary>tag</summary>贪心+回溯</details>
- spendtime: 50
- ACtime: 2022.7.14
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 147</summary>
<details>
<summary>lrc</summary>

<details>
<summary>D - Xor Sum 4</summary>

- <details><summary>comment</summary>仔细读题，差点把我晃过去了，走饶了一次，不过还好，知迷途而反，这么低的难度没有快速写出来，真的是，注意在异或运算中，取模不能直接参与操作</details>
- <details><summary>tag</summary>异或</details>
- spendtime: 48
- ACtime: 2022.11.28
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>E - Balanced Path</summary>

- <details><summary>comment</summary>非常愚蠢的一道题目，这么简单调了巨久，傻逼，输入搞错了，hashset的遍历还是耗时，还是觉得bitset更快一点，愚蠢中的愚蠢</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 95
- ACtime: 2022.7.2
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 148</summary>
<details>
<summary>lrc</summary>

<details>
<summary>F - Playing Tag on Tree</summary>

- <details><summary>comment</summary>这个题目写起来，还是比较难受的，看起来像个博弈问题其实根本不是，因为先手去的方向决定了整个局势，所以只需要枚举交汇点即可，然后再从中找到最大的，其中细节是非常非常的多，有些案例拍脑袋想，还真的是不好想，所以是个细节满满的题目，而且思考的时候，也相当的不确定，无愧7分题目</details>
- <details><summary>tag</summary>脑筋急转弯</details>
- spendtime: 120
- ACtime: 2022.11.26
- ACstatus: 完全会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 149</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Handshake</summary>

- <details><summary>comment</summary>先通过二分一个x，使得大于等于x的数比m要大，最大x，则得到了a1,a2,a3,...x,总共m个数，求出a1,a2,a3,...,的总和，并且计数count，则最终的值就是(m-count)*x+sum,使用双指针来遍历这个过程，所以就是一个二分加收集的过程，题目的技巧性没有那么强，算是一个还算不错的题目</details>
- <details><summary>tag</summary>二分</details>
- spendtime: 60
- ACtime: 2022.7.13
- ACstatus: 完全会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 150</summary>
<details>
<summary>lrc</summary>

<details>
<summary>D - Semi Common Multiple</summary>

- <details><summary>comment</summary>我的做法稍微有点欠缺化简，不过也还行，就是在寻找第一个可行的解的时候是有些幼稚的做法，但也比较有效，不过正解是直接化简式子，使得ai(p+0.5)变成，ai/2(2p+1)，这样的化简，有如下的好处，直接对ai/2求最小公倍数，验证是否对每一个都是奇数倍即可，整体的代码就简单很多</details>
- <details><summary>tag</summary>脑筋急转弯</details>
- spendtime: 40
- ACtime: 2022.11.26
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>E - Change a Little Bit</summary>

- <details><summary>comment</summary>可以从大到小逐个枚举，可以得到这样的式子，假设比x大的数有n个,则C(n,0)+C(n,1)*2+C(n,2)*3+C(n,3)*4+....,这就是计算的关键，已知m*C(n,m)=n*C(n-1,m-1),所以C(n,1)+C(n,2)*2+C(n,3)*3+...=n*2^(n-1),则原始的式子可以化简为，C(n,0)+C(n,1)+C(n,2)+....+C(n,1)+C(n,2)*2+C(n,3)*3+...=n*2(n-1)+2^n，则就很容易去计算了，化简了好长时间，最终才确定了式子，还算不错</details>
- <details><summary>tag</summary>数学公式化简</details>
- spendtime: 100
- ACtime: 2022.7.13
- ACstatus: 完全会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 151</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Max-Min Sums</summary>

- <details><summary>comment</summary>主要还是把，max的和与min的和分离，再使用组合数学即可，分离这点是真的没想道，所以，一直化简不出来，我真的是醉了，好题，立马提高难度</details>
- <details><summary>tag</summary>数学化简</details>
- spendtime: 75
- ACtime: 2022.11.25
- ACstatus: 看了提示
- score: 7
</details>

<details>
<summary>F - Enclose All</summary>

- <details><summary>comment</summary>二分的切入点没问题，但是就是判断的时候傻逼了，其实就是枚举两个点之间的交点，然后判断是否到达每个点的距离小于等于半径即可，n^3次方的判定，数据量比较小，二分可过，其中求交点需要数学推导一下，我看了这道题目的高配版本，洛谷上p1742，当n到了1e5次方级别的时候，方法就不能这样了，做法是先讲数据随机打乱，然后采用O（n^3）的时间复杂度算法，看似3次方，实际上是线性的，就是三层循环，每个循环都是从小到大，三点确定一个圆，均摊时间复杂度等等正确性证明还是依赖网上的，不过这样的做法就使得算法可以在规定时间内执行完，作为一种经典版子</details>
- <details><summary>tag</summary>二分判定</details>
- spendtime: 150
- ACtime: 2022.7.11
- ACstatus: 看了提示
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 152</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Flatten</summary>

- <details><summary>comment</summary>先求最小公倍数，不过这次需要使用质因子分解的方法来做，得到的最小公倍数使用质因子数组表示，然后再除每一个，转化成求逆元即可，加起来就可以了，题目还可以，属于基本算法的应用结合，还可以的题目</details>
- <details><summary>tag</summary>最小公倍数</details>
- spendtime: 56
- ACtime: 2022.11.24
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>F - Tree and Constraints</summary>

- <details><summary>comment</summary>现在似乎随便一个容斥原理我都被干爆了，还算是非常清楚的一道题目，写起来也就一遍过掉，将符合约束的求法看其反面，求不符合约束的，则将每一条约束形式化为xi，则不符合的所有的方式为|xi|-|xi^xj|+|xi^xj^xk|-.....,其中都是求和，则就出来了，切记乘个都不受影响的即可，可能感觉确实比较板，如果会的话，几乎就是瞬时就出来了</details>
- <details><summary>tag</summary>容斥原理</details>
- spendtime: 65
- ACtime: 2022.7.10
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 153</summary>
<details>
<summary>lrc</summary>

<details>
<summary>F - Silver Fox vs Monster</summary>

- <details><summary>comment</summary>从头往后，贪心，区间统一加某一个数，使用差分数组维护，范围用离散化加平衡树维护，第一次不小心写超时了，后来意识到是区间没处理好，题目还是可以的，刚开始被绕进去，还以为是个dp</details>
- <details><summary>tag</summary>贪心</details>
- spendtime: 60
- ACtime: 2022.11.24
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 154</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Almost Everywhere Zero</summary>

- <details><summary>comment</summary>一个数位dp不知道为啥写错了这么多次，真的快吐血了，时间白白浪费的感觉真的太讨厌了，写错了好多次，什么鬼真的是，感觉做出来人挺多的，为啥我就被绊住了呢</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 120
- ACtime: 2022.11.23
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>F - Many Many Paths</summary>

- <details><summary>comment</summary>一个常见模型另一个角度的思考，这么考虑，只允许朝上走和朝右走，从(0,0)到(i,j)的方案数使用朴素dp很容易求出，另外一种思考的角度是组合数学，需要做i+j次选择，所以方案数是C(i+j,j)orC(i+j,i)，而g(i,j)表示0-i.0-j组成的二维矩阵的所有的方案和，则数学上可以写成f(0,0)+f(0,1)+f(0,2)+...+f(0,j)=f(1,j),f(1,0)+f(1,1)+...+f(1,j)=f(2,j),所以全部展开得f(1,j)+f(2,j)+...+f(i+1,j),补个1变成，f(0,j)+f(1,j)+f(2,j)+...+f(i+1,j)=f(i+1,j+1),最后再将1减去，则式子可以化简为，g(i,j)=f(i+1,j+1)-1=C(i+j+2,i+1)-1,再用二维前缀和即可，关于C(i,0)+C(i+1,1)+C(i+2,2)+...+C(i+j,j)=C(i+j+1,j)的推导是很容易证明的</details>
- <details><summary>tag</summary>组合数学</details>
- spendtime: 120
- ACtime: 2022.7.9
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 155</summary>
<details>
<summary>lrc</summary>

<details>
<summary>D. Pairs</summary>

- <details><summary>comment</summary>这题很简单，不像是1800分的题目，与lc上两个有序数组的第 K 小乘积比较相像，很简单，分类讨论清楚即可</details>
- <details><summary>tag</summary>二分</details>
- spendtime: 45
- ACtime: 2022.7.7
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>E - Payment</summary>

- <details><summary>comment</summary>还算挺失败的一道题目，首先是dp的方向反了，应该是从低位到高位，第二是，转移方程写错了，在转移的时候没有特别想清楚过程，这道题目还可以，记录下来，翻车的一道题目</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 78
- ACtime: 2022.11.22
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 156</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Roaming</summary>

- <details><summary>comment</summary>可以这么考虑，先确定最终的位置，然后再确定在这种分段下，有多少种方案，两遍组合数学即可，思维稍微想了一下，倒也不算很难</details>
- <details><summary>tag</summary>组合数学</details>
- spendtime: 53
- ACtime: 2022.11.22
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 157</summary>
<details>
<summary>lrc</summary>

<details>
<summary>D - Friend Suggestions</summary>

- <details><summary>comment</summary>第一步，读题读清楚，第二步，并查集组成联通块，第三步，枚举block边即可</details>
- <details><summary>tag</summary>并查集</details>
- spendtime: 25
- ACtime: 2022.11.21
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>E - Simple String Queries</summary>

- <details><summary>comment</summary>给7分的原因还是因为在当这种线段树出错的时候，调试变得特别麻烦，因为不知道到底错在哪里了，所以因为这样的特性，耽误了很多的时间，最后证明了，还是在线段树中有一些地方，写错了，要不然是个蛮简单的题目</details>
- <details><summary>tag</summary>线段树</details>
- spendtime: 85
- ACtime: 2022.11.21
- ACstatus: 完全会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 158</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Divisible Substring</summary>

- <details><summary>comment</summary>非常的不知道怎么说，可以想到最直观的做法，n*p的，时间上肯定过不去，但是能过去小范围p，对于大的p从后向前，因为p和10互质，所以从后向前的ui-uj本来要/10^j,因为互质的原因，所以就不需要管，直接能否除以p即可，就变成了一个普通的计数优化，反思就是确实没想到从后向前，也没有大胆猜，非常的无奈</details>
- <details><summary>tag</summary>记数优化</details>
- spendtime: 100
- ACtime: 2022.7.7
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 159</summary>
<details>
<summary>lrc</summary>

<details>
<summary>F - Knapsack for All Segments</summary>

- <details><summary>comment</summary>又是一道我没想出来的dp，状态的设计相当的可以，考虑这么设计,dp[i][j]表示前i个元素中和为j的方案的左端点的坐标之和，则对于每一个i来说，dp[i][s-a[i]]*(n-i+1)就表示在固定右端点的情况下，对每个答案的贡献值，这个dp的状态设计很精巧，非常的想不到，转移就非常的自然了，dp[i][j]=dp[i-1][j]+dp[i-1][j-a[i]]当j>a[i],当j==a[i],则dp[i][j]=dp[i-1][j]+i,对于相等的情况，思考其语义，这块要特盼一下，其他的就没有了，代码非常的短，n^3方法太容易想，但没有优化的空间</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 120
- ACtime: 2022.7.7
- ACstatus: 不会
- score: 7
</details>

<details>
<summary>E - Dividing Chocolate</summary>

- <details><summary>comment</summary>觉得还不错的一道题目，但是思路出的比较快，没啥思维难点，主要是编码不太好编码，对于横的直接二进制枚举，当横向的切割确定了之后，一个一个枚举竖着的切割即可，是一道中等难度的码农题目</details>
- <details><summary>tag</summary>dp+枚举</details>
- spendtime: 60
- ACtime: 2022.11.19
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 161</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Yutori</summary>

- <details><summary>comment</summary>又一次被爆干，脑筋急转弯啊，脑筋急转弯，从前到后贪心一遍，从后到前贪心一遍，如果两次都选中的即为一定要选的，我真傻</details>
- <details><summary>tag</summary>脑筋急转弯</details>
- spendtime: 150
- ACtime: 2022.7.6
- ACstatus: 不会
- score: 7
</details>

<details>
<summary>F - Division or Subtraction</summary>

- <details><summary>comment</summary>取个平方，枚举一下，我觉得颜色标的是有问题的，因为简单的题目放到了后面，可能都在死磕前面的题目，后面的题目就没有看了，代码很短，适合作为一个周赛t4</details>
- <details><summary>tag</summary>枚举</details>
- spendtime: 29
- ACtime: 2022.11.19
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 162</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Sum of gcd of Tuples (Hard)</summary>

- <details><summary>comment</summary>又一次被容斥原理给绊住了，虽然我的思考方向没有问题，考虑到底gcd（序列）=x的个数，但是我不会求，最终的式子是这么推导的，f(i)=(k/i)^n-f(2*i)-f(3*i)-f(4*i)-...,总的时间复杂度为klogk,可以通过举例子，来表象了解容斥原理的使用方式，还挺头疼，过不去容斥的关卡</details>
- <details><summary>tag</summary>容斥原理</details>
- spendtime: 120
- ACtime: 2022.7.4
- ACstatus: 不会
- score: 7
</details>

<details>
<summary>F - Select Half</summary>

- <details><summary>comment</summary>考虑dp[i]表示长度为i选了i/2个数，最大的值，则考虑转移,当i为偶数时候，来自a[i]+dp[i-2]和s[i-1],s为隔一个选一个，i为奇数时候，dp[i-1]和dp[i-2]+a[i]仔细理解理解即可</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 120
- ACtime: 2022.7.5
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 164</summary>
<details>
<summary>lrc</summary>

<details>
<summary>D - Multiple of 2019</summary>

- <details><summary>comment</summary>对于前缀来说，如果一段满足，pre[i]与pre[j]*10^len在模2019的意义下相同，10并不会改变什么，直接走前缀即可，这算是个数学问题，虽然第一次ac，但不是最优的方法，所以还要从中分析出结论</details>
- <details><summary>tag</summary>脑筋急转弯</details>
- spendtime: 40
- ACtime: 2022.11.19
- ACstatus: 正解更好解释
- score: 6
</details>

<details>
<summary>E. Two Currencies</summary>

- <details><summary>comment</summary>建图是关键，又一次的失败在建图上，觉得直接bfs就行，看来还是要冷静分析，这样的模型，观察数据范围，将dis[i][j]看成i点下剩j钱的最短时间，将本来的点再加一个维度，则有利于建图，之前还走偏一个建图的思路，就是觉得花费x/y分钟换一个银元可以使用一个double来做路径值，然后再向上取整，后来发现精度还是有问题，还是应该使用整数代价比较好</details>
- <details><summary>tag</summary>最短路</details>
- spendtime: 180
- ACtime: 2022.7.2
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 165</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Rotation Matching</summary>

- <details><summary>comment</summary>让其间隔差不一样就行，间隔差为0,1,2,...,m-1即可，则构造这样的对数即可,不写了，很生气，不写了</details>
- <details><summary>tag</summary>脑筋急转弯</details>
- spendtime: 120
- ACtime: 2022.6.30
- ACstatus: 不会
- score: 6
</details>

<details>
<summary>F - LIS on Tree</summary>

- <details><summary>comment</summary>直接用支持segmenttreeXY做了，当然是可以的，就是时间上常数项多了些，但是使用回溯替换也挺好，就是需要记录上一个值是啥，做之前纠结的点一直是，这个序列会增，然后从中间删除是个问题，但是后来想到，lis的使得序列增的缓慢的做法其实是替换，最大的值才是直接跟在后面，所以删除只会从最后面删除，所以dfs回溯更好</details>
- <details><summary>tag</summary>dp+dfs+回溯</details>
- spendtime: 120
- ACtime: 2022.7.1
- ACstatus: 正解更好
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 166</summary>
<details>
<summary>lrc</summary>

<details>
<summary>F - Three Variables Game</summary>

- <details><summary>comment</summary>魔幻贪心，从直觉上感觉1,2,更多是不同的情况，而且贪心过程中可以撤回上一步，如果上一步无法撤回，则一定不行，更加形式化的可以讨论，a+b+c=1,a+b+c>=2，就可以了，这种东西就是魔幻往上写，写的时间很长很长</details>
- <details><summary>tag</summary>贪心</details>
- spendtime: 120
- ACtime: 2022.6.29
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 167</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Colorful Blocks</summary>

- <details><summary>comment</summary>当然传统dp是不行的，转移肯定超标，重新思考，到底要怎么做，相当于对一个线段分段即可，n-段数=k,则可以反向计算段数，使用组合数学计算到底分成a段有多少种，对于m个颜色的a个线段，又有m*(m-1)*(m-1)..种，乘起来加即可，还是卡住了呢，简单数学还是要了熟于心的</details>
- <details><summary>tag</summary>组合数学</details>
- spendtime: 90
- ACtime: 2022.11.17
- ACstatus: 完全会
- score: 7
</details>

<details>
<summary>F - Bracket Sequencing</summary>

- <details><summary>comment</summary>将情况直接化简为((,))((,))这三种情况，((直接可以全部合并，))也可以全部合并，对于))((的，考虑这么贪心，))括号记为x,((括号记为y,则在当前贪心的局面下，只可能存在((，所以，记为sum，则需要满足，x<=sum,且y-x最大的，则就引出了我一直想要写的数据结构，在某一范围的约束下，另一维度最大，则使用线段树来完成，支持删除，编码非常非常的麻烦，调了我好久好久，这觉的这样的贪心才是好理解的贪心</details>
- <details><summary>tag</summary>贪心</details>
- spendtime: 240
- ACtime: 2022.6.29
- ACstatus: 完全会
- score: 7
</details>
</details>
</details>
</details>


<details>
<summary>AtCoder Beginner Contest 168</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - ∙ (Bullet)</summary>

- <details><summary>comment</summary>又被魔幻到了，首先说说收获，对于(x,y)如何快速查询到其正交向量，先将其分子分母互质化，即x/gcd(x,y),y/gcd(x,y)，然后查询(y/gcd(x,y),-x/gcd(x,y))即可，这个东西我想了好久，没解决，怕double的精度会坑人，其次就是组合数学问题了，对于互斥的方案数相加计算，对于独立的乘起来，最后记着减去空集</details>
- <details><summary>tag</summary>组合数学</details>
- spendtime: 180
- ACtime: 2022.6.28
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 169</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Count Median</summary>

- <details><summary>comment</summary>就是一个意会结论题，可以这么考虑，当是奇数的时候，就是a的中位数和b的中位数的范围，当是偶数的时候，就是就a的中位数(此处其实是两个值加起来)到b的中位数，因为可以是0.5,所以比奇数情况要多一些范围，结论题目，背吧</details>
- <details><summary>tag</summary>思维结论题</details>
- spendtime: 75
- ACtime: 2022.11.18
- ACstatus: 看提示后会
- score: 6
</details>

<details>
<summary>F - Knapsack for All Subsets</summary>

- <details><summary>comment</summary>又是一个魔性dp，简单容易想的N^3做法，但是肯定是错的，这么考虑，对于二元组(x,y)，y是x的子集，使用dp[i]表示，y之和为i的二元组的种类数，考虑处理到a[i]时候，a[i]可以只放到x中，则对于每一个dp[i]都会*2(其中就包含了既不放在x也不放在y中的情况)，a[i]可以放到x中也可以放到y中，则dp[i+a[i]]+=dp[i]，这道题的状态还需要好好思考，算是个比较绕的dp</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 120
- ACtime: 2022.6.27
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 170</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Smart Infants</summary>

- <details><summary>comment</summary>题目太码农了，treemap倒来倒去，小心编码，码农题目，放到笔试中，死一片</details>
- <details><summary>tag</summary>treemap</details>
- spendtime: 60
- ACtime: 2022.11.16
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>F - Strivore</summary>

- <details><summary>comment</summary>这题就是时间超，没办法，数据量太大，刚刚好好就把java卡了，这确实也是没办法，不过，透露出来的建图的一半我是没问题，但是失败在了不知道对于k怎么处理，合理的处理方案是，同一个方向1/k的代价，需要转向时候，需要向上取整即可，就这一步，很难</details>
- <details><summary>tag</summary>最短路</details>
- spendtime: 150
- ACtime: 2022.6.25
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 171</summary>
<details>
<summary>lrc</summary>

<details>
<summary>F - Strivore</summary>

- <details><summary>comment</summary>真的非常套路了，关键在于去重，结果做法非常的，去重去的非常的套路，可以随意搜索，解法都出奇的一致，这就是这种题目的套路做法，收获就是把Cmn的板子重新补了一下，以前的板子没注意写的其实是个线性的时间复杂度，其实应该是nlogn预处理，再O（1）求得</details>
- <details><summary>tag</summary>套路</details>
- spendtime: 120
- ACtime: 2022.6.24
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 172</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - NEQ</summary>

- <details><summary>comment</summary>我又不会容斥原理了，好吧，学习学习</details>
- <details><summary>tag</summary>容斥原理+组合数学</details>
- spendtime: 120
- ACtime: 2022.6.24
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 173</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Multiplication 4</summary>

- <details><summary>comment</summary>本题比较简单，贪心法，直接贪心蛮好的，分清楚情况即可，奇偶情况即可</details>
- <details><summary>tag</summary>贪心</details>
- spendtime: 68
- ACtime: 2022.6.22
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>F - Intervals on Tree</summary>

- <details><summary>comment</summary>这个题真的很赞，非常的牛逼，在森林的条件约束下，使用点数-边等于连通域数目，这个转换瞬间使得题目变得异常的简单，O（N）的时间即可</details>
- <details><summary>tag</summary>脑筋急转弯</details>
- spendtime: 90
- ACtime: 2022.6.23
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 174</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Logs</summary>

- <details><summary>comment</summary>直接二分整数，不用二分浮点</details>
- <details><summary>tag</summary>二分</details>
- spendtime: 30
- ACtime: 2022.11.16
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>F - Range Set Query</summary>

- <details><summary>comment</summary>莫队算法还是不太好过这个的，怎么优化时间常数还是很大，最后看了一眼提示，所以把这道题目的难度还是拔高一点，记录一下，一个没做出来的东西，在这种数据规模下，还是对数据结构的选择还是比较有唯一性的，就是用数状数组，对于不同的，这里要记一下小技巧，从左到右遍历的过程中，一直去更新一种颜色的最右端点即可，这样再用数状数组的区间查询即可，这步还是没想到，大意了</details>
- <details><summary>tag</summary>数状数组</details>
- spendtime: 120
- ACtime: 2022.11.15
- ACstatus: 看提示后会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 175</summary>
<details>
<summary>lrc</summary>

<details>
<summary>D - Moving Piece</summary>

- <details><summary>comment</summary>这种在环上的处理，非常的容易写错，非常的愚蠢，非常的码农，代码非常的不优雅，写了很多类似的处理，觉得语义都不是很好，没办法，就是这么个情况，所以还真的挺怕这种题目，不是那么的好调试</details>
- <details><summary>tag</summary>码农题目</details>
- spendtime: 90
- ACtime: 2022.11.12
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>E - Picking Goods</summary>

- <details><summary>comment</summary>直接dp吧，没啥好说的，思路也比较简单，刚开始犯了一些很蠢的问题</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 45
- ACtime: 2022.11.14
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 176</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Bomber</summary>

- <details><summary>comment</summary>感觉十分小瞧大意的一个题目，本来看上去一眼，实际忽略了重要的地方，最后还要来一遍双重循环，切记及时的break，这道题目还行，提高了评价等级，觉得作为笔试面试还是一个不错的选项</details>
- <details><summary>tag</summary>枚举+技巧</details>
- spendtime: 50
- ACtime: 2022.11.15
- ACstatus: 完全会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 178</summary>
<details>
<summary>lrc</summary>

<details>
<summary>F - Contrast</summary>

- <details><summary>comment</summary>对于任意一个元素即在a出现也在b出现，那么在a出现的次数+在b出现的次数>n的话，那一定不行，所以判断一下，直接随机大法,https://codeforces.com/blog/entry/82687，对于这题的做法很赞，线性时间就解决，贪心带修正</details>
- <details><summary>tag</summary>随机法or贪心修正</details>
- spendtime: 120
- ACtime: 2022.6.21
- ACstatus: 正解更好
- score: 7
</details>
</details>
</details>
</details>


<details>
<summary>AtCoder Beginner Contest 179</summary>
<details>
<summary>lrc</summary>

<details>
<summary>D - Leaping Tak</summary>

- <details><summary>comment</summary>因为段数很小，所以考虑多个滑动窗口模型，还算是中规中矩dp之上的一种dp</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 40
- ACtime: 2022.11.11
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>F - Simplified Reversi</summary>

- <details><summary>comment</summary>思考的出发点就是，怎么将每次的操作其实是一个线段，这样的一种结构存储起来，而且要尽量保证每次的时间，对于某一行来说，需要找到在这一行中的列的最小值，当然对于列的时候，需要找到行的最小值，那么只需维护行列的最小值即可，是一个区间，则线段树的min版本即可</details>
- <details><summary>tag</summary>min线段树</details>
- spendtime: 65
- ACtime: 2022.6.20
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 180</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Traveling Salesman among Aerial Cities</summary>

- <details><summary>comment</summary>初看这种dp考烂了，但是总隐隐觉得这种至少访问一次的方式在dp的转移上是不是可能有问题，但是依然没有，就按照常规的写吧</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 45
- ACtime: 2022.11.11
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 183</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Queen on Grid</summary>

- <details><summary>comment</summary>就是对于横竖对角前缀和一下即可</details>
- <details><summary>tag</summary>前缀和dp</details>
- spendtime: 38
- ACtime: 2022.11.10
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>F - Confluence</summary>

- <details><summary>comment</summary>复杂度估计错了，把自己吓到了，最后无奈就硬写，结果反而自己思考没问题，真的不明白思考了个什么，自己吓自己了一个多小时，真的服了，就是把小的和进大的里面即可</details>
- <details><summary>tag</summary>并查集</details>
- spendtime: 60+自己吓自己100
- ACtime: 2022.11.10
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 184</summary>
<details>
<summary>lrc</summary>

<details>
<summary>D - increment of coins</summary>

- <details><summary>comment</summary>一个简单的概率期望模型，不至于太难理解，可以归档归档，思路出的很直观</details>
- <details><summary>tag</summary>期望dp</details>
- spendtime: 30
- ACtime: 2022.11.9
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>E - Third Avenue</summary>

- <details><summary>comment</summary>一看到这种就知道非常的码农，写很多，对于字母a-z，贪心的只用处理一遍即可，所以时间复杂度可控，常见的模型加了小东西</details>
- <details><summary>tag</summary>最短路搜索</details>
- spendtime: 55
- ACtime: 2022.11.9
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>F - Programming Contest</summary>

- <details><summary>comment</summary>看到这种40这种数据，又看到是一种类似需要枚举全部的，所以直接折半，一般暴力枚举预处理排序，另一半继续暴力枚举即可，时间常数稍微有一点大，可能存在性能抖动，多交一次即可</details>
- <details><summary>tag</summary>双向搜索</details>
- spendtime: 40
- ACtime: 2022.11.9
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 185</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Sequence Matching</summary>

- <details><summary>comment</summary>直观就能想到最长公共子序列这个dp模型，所以是个二维dp，只需要改变转移时候的语义即可，算是一个中规中矩的dp</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 40
- ACtime: 2022.11.8
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 186</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Throne</summary>

- <details><summary>comment</summary>纯板子，没啥好说的，直接套板子就行</details>
- <details><summary>tag</summary>一阶线性同余板子</details>
- spendtime: 45
- ACtime: 2022.11.8
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>F - Rook on Grid</summary>

- <details><summary>comment</summary>考虑将一行当成一个区间来解，对于第一行特殊处理，对于第二行以后就一样，当第一列上有阻碍也特殊处理，设个flag，对于区间查询的话，数据结构就首选线段树了，还算比较直观好理解</details>
- <details><summary>tag</summary>线段树</details>
- spendtime: 80
- ACtime: 2022.6.20
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 187</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Through Path</summary>

- <details><summary>comment</summary>思考了很久，不是持续，但也有一个小时的思考，而且还走了很多的弯路，我觉得很不错的，面试笔试出这个应该会死一片，算法就是一遍dfs即可，主要收集两种信息，第一种，顺着dfs方向的，是一定会加上，第二种，逆着dfs方向的，在dfs的路径上的不加，其余都加的，这块还是需要好好想想的，比较的思维，代码也不是一次就能写好，记录逆序的总和，统计输出即可</details>
- <details><summary>tag</summary>dfs思维题</details>
- spendtime: 120
- ACtime: 2022.11.7
- ACstatus: 完全会
- score: 7
</details>

<details>
<summary>F - Close Group</summary>

- <details><summary>comment</summary>这复杂度也能过，真的出题太不认真了，一个3^n这，怎么能过，害我不敢写，但是图染色还是复杂度厉害，新整了个板子</details>
- <details><summary>tag</summary>dpor图染色</details>
- spendtime: 30or180
- ACtime: 2022.6.21
- ACstatus: 完全会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 188</summary>
<details>
<summary>lrc</summary>

<details>
<summary>F - +1-1x2</summary>

- <details><summary>comment</summary>为了减少状态，考虑从y向x，看了看lc的（得到目标值的最少行动次数），有点被误导，认为+，-不可能同时都出现，所以，其中加了步贪心，这样的贪心的过程还是有很高的正确度，只有两个case过不了，则考虑使用，另一种方法，当y为偶数，就可以除2，奇数的时候，+1，-1，记录visit，bfs不断搜索即可，唉，可能有点小失误，看了答案才能绕过弯</details>
- <details><summary>tag</summary>bfs+贪心</details>
- spendtime: 120
- ACtime: 2022.6.18
- ACstatus: 看提示后改正
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 189</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Rotate and Flip</summary>

- <details><summary>comment</summary>整个所有的计算的每一个中间步骤都可以使用数学的式子表现出来，这样子就直接可以快速计算，就是需要写的代码还是比较多，算是一个比较码农的题目，在比赛的阶段，也就意味着前4到题目必须40分钟写完，那还是对手速要求很高的</details>
- <details><summary>tag</summary>数学公式模拟</details>
- spendtime: 60
- ACtime: 2022.11.7
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 190</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Magical Ornament</summary>

- <details><summary>comment</summary>先求最短路，然后在上面就是TSP问题，直接用哈密尔顿状态压缩即可</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 90
- ACtime: 2022.6.18
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>F - Shift and Inversions</summary>

- <details><summary>comment</summary>观察到，每一次改变相对于上一步是非常好求的，所以，只要求出初始值，那么后续就会很简单，初始值就是标准的逆序对即可，板板正正的一道题目</details>
- <details><summary>tag</summary>逆序对+枚举</details>
- spendtime: 20
- ACtime: 2022.11.5
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 191</summary>
<details>
<summary>lrc</summary>

<details>
<summary>D - Circle Lattice Points</summary>

- <details><summary>comment</summary>主要是得好好的处理浮点，注意double.valueof可能有问题，用BigDecimal解决，向上取整向下取证对正负数稍微有区别，非常的烦人</details>
- <details><summary>tag</summary>精度处理</details>
- spendtime: 140
- ACtime: 2022.6.17
- ACstatus: 精度处理必须得学习
- score: 6
</details>

<details>
<summary>E - Come Back Quickly</summary>

- <details><summary>comment</summary>稍微改一下板子即可，没啥的，对于每个都跑一遍最短路即可</details>
- <details><summary>tag</summary>最短路</details>
- spendtime: 25
- ACtime: 2022.11.5
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 192</summary>
<details>
<summary>lrc</summary>

<details>
<summary>D - Base n</summary>

- <details><summary>comment</summary>我觉得这种题目作为面试笔试一定相当的考验心态，思路过于简单，所以人往往会大意，错了7发，wa麻木了，应该要能记录一下，本以为快速搞定，结果面向案例调试了好久，麻木</details>
- <details><summary>tag</summary>二分</details>
- spendtime: 65
- ACtime: 2022.11.4
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>F - Potion</summary>

- <details><summary>comment</summary>可以这么考虑，考虑从N个物品中拿k件，则可以外层循环枚举k，内层的状态是dp[i][j]表示已经拿了i件且和的余数为j的sum的最大值，当x%k就可以定位到dp[k][x%k]的最大值，从而可以求出在k个时，最小的时间，时间复杂度100(1^2+2^2+3^2+...)为三千多万，合理时间复杂度</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 35
- ACtime: 2022.6.17
- ACstatus: 完全会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 193</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Oversleeping</summary>

- <details><summary>comment</summary>遇到不会的知识点就得学，花了很长的时间，好吧，那真的没办法了</details>
- <details><summary>tag</summary>线性同余方程组</details>
- spendtime: 240
- ACtime: 2022.6.16
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 195</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Lucky 7 Battle</summary>

- <details><summary>comment</summary>刚开始想反了而且想复杂了，对必胜态和必败态的转换还是在刚开始没想清楚，刚开始是从前往后，其实正解是从后往前，然后对于T来说，是不断的增大获胜的可能，尽可能使得状态可达，所以是或关系，而对于A来说，是使得状态尽可能不可达，所以是与关系，状态就7个从0到6，这样就可以了</details>
- <details><summary>tag</summary>博弈论dp</details>
- spendtime: 120
- ACtime: 2022.6.15
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 196</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Filters</summary>

- <details><summary>comment</summary>函数是个分段函数，从左到右不断的递增的，就可以二分每一段的端点了</details>
- <details><summary>tag</summary>二分</details>
- spendtime: 70
- ACtime: 2022.6.13
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>D - Hanjo</summary>

- <details><summary>comment</summary>整成二分图去枚举即可，写得也太长了，刚开始都不知道怎么枚举，挺烦躁的一道题</details>
- <details><summary>tag</summary>枚举</details>
- spendtime: 90
- ACtime: 2022.11.4
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 197</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Traveler</summary>

- <details><summary>comment</summary>一道还不错的贪心题目，可以这么考虑，因为对于上一个id只能存在两种端点情况，所以就是对两种情况做个dp即可，还可以的一道贪心题目</details>
- <details><summary>tag</summary>贪心+dp</details>
- spendtime: 40
- ACtime: 2022.11.3
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>F - Construct a Palindrome</summary>

- <details><summary>comment</summary>设mark[i][j]和dis[i][j]分别表示，1->i,n->j是否可以组成回文且其长度为dis[i][j]，主要就是这个抽象点对，刚开始想到了这样的方式，但是没有很准确的抽象出（i,j）所以一直不敢下笔写，故错失良机了，看了提示想了想就直接写出来了,双向bfs大法好，多多积累</details>
- <details><summary>tag</summary>双向bfs</details>
- spendtime: 90
- ACtime: 2022.6.13
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 198</summary>
<details>
<summary>lrc</summary>

<details>
<summary>D - Send More Money</summary>

- <details><summary>comment</summary>直接枚举所有的映射即可，就是用到下一个排列的板子，没啥的，刚刚在适应键盘</details>
- <details><summary>tag</summary>枚举</details>
- spendtime: 60
- ACtime: 2022.11.2
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 199</summary>
<details>
<summary>lrc</summary>

<details>
<summary>D - RGB Coloring 2</summary>

- <details><summary>comment</summary>这道题目的代码还是可以的，就算看懂了题解，也需要看一下代码，感觉一个dfs能写出千万种花来，暴力枚举的复杂度是需要想明白，然后大胆搜素即可，讲道理，我确实代码上，可能会有问题，所以不会也是正常</details>
- <details><summary>tag</summary>暴力枚举</details>
- spendtime: 120
- ACtime: 2022.6.9
- ACstatus: 不会
- score: 7
</details>

<details>
<summary>E - Permutation</summary>

- <details><summary>comment</summary>还以为自己绝对不会的题，结果写一下给出来了，状态压缩dp了，我是分两步走，第一步先判断状态的合法性，第二步转移即可，转移也就是枚举当前所选的数转移即可，算是有惊无险，给自己点个赞</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 60
- ACtime: 2022.6.10
- ACstatus: 完全会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 200</summary>
<details>
<summary>lrc</summary>

<details>
<summary>D - Happy Birthday! 2</summary>

- <details><summary>comment</summary>写得有点久，不知道为啥，主要是哪个0的处理，还有写法的处理，错了一次才慢慢调出来的，比较僵化</details>
- <details><summary>tag</summary>路径记录dp</details>
- spendtime: 100
- ACtime: 2022.11.1
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>E - Patisserie ABC 2</summary>

- <details><summary>comment</summary>分析，如果可以快速知道对于一个sum，其中共有多少种方式，这是其一，对于已知的sum里面，又是怎么分布的，可以这么计算，考虑如果是两个，a+b=x，任意给个x就可以在O(1)时间内求出(a,b)的方案数，则这样的连续的x形成了一维数组，对于仍给sum，可以计算x+y=sum,x的区间，就可以O(1)时间求出，则对于任给sum的方案数解决，那对于在sum中，(i,j,k)按照从小到大即可，其中有些小细节，注意调一调即可，思路出的不那么快，代码写的也不快，其中有各种bug，废了些时间才最终搞好，还算不错的分析题目</details>
- <details><summary>tag</summary>分析</details>
- spendtime: 140
- ACtime: 2022.6.8
- ACstatus: 完全会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 201</summary>
<details>
<summary>lrc</summary>

<details>
<summary>D - Game in Momotetsu Worlds</summary>

- <details><summary>comment</summary>考虑无后效性，假设到某一步，这一步怎么做决定，跟曾经已经做过的决定没有关系，则dp就表示，dp[x][y]表示先手与后手分数之差最大的分数之差，不知道为啥自己刚开始写错了好多次，后来才写对，看来要调整好自己写代码时候的状态</details>
- <details><summary>tag</summary>博弈dp</details>
- spendtime: 120
- ACtime: 2022.10.31
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>E - Xor Distances</summary>

- <details><summary>comment</summary>感觉自己有点思维不够灵活了，有点愚蠢，在树中 ，对于任意的x,y的距离的异或来说，都相当于从根节点到x和从根节点到y的异或，所以一遍dfs，求出根节点到所有点的dis,然后在这个数组基础上，转化成，对于其中所有的pair，x1,x1^x2,x1^x3,x2^x3,x1^x4,x2^x4,x3^x4,...,就变成了一个记数优化问题了，直接分别对每一bit统计即可，两步转化</details>
- <details><summary>tag</summary>分析+记数优化</details>
- spendtime: 120
- ACtime: 2022.6.7
- ACstatus: 看提示后会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 202</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Count Descendants</summary>

- <details><summary>comment</summary>dfs时间戳法，拍脑袋当然想不出来，估计是一种经典方法，在dfs中记录每次进入节点的时间戳，出去的时间戳，如何快速判断某一个节点是否是某一个节点的子节点呢？满足INx<=INy< OUTx即可，则统计二分即可，新东西</details>
- <details><summary>tag</summary>dfs序</details>
- spendtime: 120
- ACtime: 2022.6.6
- ACstatus: 不会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 203</summary>
<details>
<summary>lrc</summary>

<details>
<summary>D - Pond</summary>

- <details><summary>comment</summary>刚开始不小心写了无用代码，后来迅速更改，二维前缀和+二分判定即可，判定思路直观</details>
- <details><summary>tag</summary>二分判定</details>
- spendtime: 50
- ACtime: 2022.6.3
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>E - White Pawn</summary>

- <details><summary>comment</summary>比较直观的模拟，使用set作为中间存储，将点排序，每一层慢慢向上迭代，每次将删除的点和新加的点收集起来，统一处理</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 48
- ACtime: 2022.6.3
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 204</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Rush Hour 2</summary>

- <details><summary>comment</summary>对于t+x+c/(t+x+1)如何求极值，直接基本不等式，是个对勾函数，求一下即可，没想到考成这样，然后在djstla的判断过程那一行改就行，还算思路出的比较流畅的一道题目</details>
- <details><summary>tag</summary>最短路</details>
- spendtime: 44
- ACtime: 2022.6.2
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 206</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Divide Both</summary>

- <details><summary>comment</summary>脑溢血题目，可以这么考虑，令f(k)为gcd(x,y)=k的元素对数量，此处利用容错进行计算，f(k)=(r/k-l/k)^2-f(2*k)-f(3*k)-...,这个式子很难想，实现的时候看成dp从后向前，其中减去不合法的，其中的数对中仍然包含，x=g的情况，则还需要将r/i*2-1减掉即可，好难好难，我觉我脑溢血了</details>
- <details><summary>tag</summary>数论+容斥</details>
- spendtime: 130
- ACtime: 2022.6.1
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 207</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Mod i</summary>

- <details><summary>comment</summary>又一次被dp血虐了，虽然代码足够的短，但我还是想了半天，其中转移部分的优化依然卡壳，这种分组dp一般都可以使用dp[i][j]表示前i个分j组，麻烦就在于对于每次转移的不好想，这道题的转移的思想，经常在记数优化中使用，对于前缀和可以适当分类，此题使用模余分类，然后就可以在O（1）时间求出转移，唉，又被摆了一道</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 180
- ACtime: 2022.5.31
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 210</summary>
<details>
<summary>lrc</summary>

<details>
<summary>D - National Railway</summary>

- <details><summary>comment</summary>我觉得两边dp的思路还是非常可观的，代码也简单，但却不能直观想到，想必是某种板子，而写出来的方法是一种相对能想出来的方法，无非就是对整个过程就行分析，相处能优化的点即可，总体复杂度比dp多了logn，所以也是能过的，dp确实比较好，而模拟的方法就要代码稍微复杂一点点，而且其中还要有公式的推导，所以会更难一些，学习dp方法，但要保留会优化的能力</details>
- <details><summary>tag</summary>dp或者模拟优化</details>
- spendtime: 120
- ACtime: 2022.10.29
- ACstatus: 正解更好
- score: 7
</details>

<details>
<summary>E - Ring MST</summary>

- <details><summary>comment</summary>数论推导了，窒息了吧，第一个w和v在模余n下相等，可以转化为w=v+kn,第二个w=v+k1*a+k2*b+k3*c...,可以转化为w=v+k*gcd(a,b,c,...),所以一直在找，如何判定任意的点是否连通，直接看gcd结果即可，刚开始没看数据，用并查集直接傻了，这部分数论推导还是可以的，有了这个强大的知识之后，既然gcd代表了联通块的个数，那么每一次采用新的ai时，能新加的边数直接就能求出，所以可以在nlogn时间内完成算法，我是真没想到直接数论推导，还尝试在不同的case上找规律，我也是弱爆了</details>
- <details><summary>tag</summary>数论</details>
- spendtime: 90
- ACtime: 2022.5.30
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 211</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Red Polyomino</summary>

- <details><summary>comment</summary> dfs没写好，确实非常的，无语吧，努力了很长的时间，结果方向错了。自己总是停留在dfs一种很差的误区，那样的写法拖垮了我，实际上，dfs写的特别简单，使用hashset在搜索时候去重就可以了，每次直接枚举全盘即可，判断是否与现在的盘面联通即可，还是输在了写法上，刚开始走了很多的枚举弯路，浪费了大量的时间和代码精力。题目没啥思维，主要难就难在枚举</details>
- <details><summary>tag</summary>dfs</details>
- spendtime: 360
- ACtime: 2022.5.28
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 212</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Safety Journey</summary>

- <details><summary>comment</summary>看似暴力转移不行，其实是ok的，因为边只有5000，所以先把总和算出来，减去即可，并不算绕弯子</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 50
- ACtime: 2022.10.28
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 213</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Stronger Takahashi</summary>

- <details><summary>comment</summary> 直接bfs即可，遇到障碍物的话就把它周围的8个一起+1放入优先级队列等待搜索，思路好出，代码确实也不难写，是应该快速就可以出的题目</details>
- <details><summary>tag</summary>bfs</details>
- spendtime: 30
- ACtime: 2022.10.27
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 214</summary>
<details>
<summary>lrc</summary>

<details>
<summary>D - Sum of Maximum Weights</summary>

- <details><summary>comment</summary>不断合并，不断的判断两边的size即可，直接合并即可，思路就是将点匹配变成数边即可</details>
- <details><summary>tag</summary>贪心+并查集</details>
- spendtime: 10
- ACtime: 2022.10.27
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>E - Packing Under Range Regulations</summary>

- <details><summary>comment</summary>贪心思考，其实列差分约束也是可以的，不过边有负值，还是担心有可能超时就没有写，所以就老老实实的贪心，按照左端点从小到大排序，在左端点相同的时候，按照右端点从小到大，使用一个优先级队列，维护右端点，然后不断的加入，判断，递增当前位置的指针，即可，其中的每一步是有小细节的，注意多加些判断即可</details>
- <details><summary>tag</summary>贪心</details>
- spendtime: 60
- ACtime: 2022.5.25
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>F - Substrings</summary>

- <details><summary>comment</summary>每次看到去重的dp我就害怕，因为一直不太好理解其中的转移，当然这道题目，我又失败了，之后重新思考，dp[i]仍然表示必定选择i为结尾的不同的字符串的个数，转移的话，从i-2到与i相同的字符为终止，每次想这种转移都非常的抽象，不过，看到转移却觉得非常的合理，与lc的940一样让我害怕，恐惧</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 120
- ACtime: 2022.5.26
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 215</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Chain Contestant</summary>

- <details><summary>comment</summary>从10入手，表示dp[i][j]表示已经选了i个状态且结尾是j的方案数即可，主要是半天没读懂题，读题10分钟</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 25
- ACtime: 2022.10.25
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>F - Dist Max 2</summary>

- <details><summary>comment</summary>这道题没做出来，我觉得不是很应该，一个二分的题目，而且我觉得判定的方式，我是可以想出来的，不知道为什么我没写出来，是还是反应不够灵敏吗，主要就是判定是否任给一距离，在这个数组中是否可以达到，像这种最小值的最大值，一般不都二分吗，我还以为是我没有看透模型，看来自己还是，脑子没有转过来弯，每次看到绝对值，直接就怕了，没做出来但我觉得思考是轻量级的，不应该</details>
- <details><summary>tag</summary>二分判定</details>
- spendtime: 60
- ACtime: 2022.5.24
- ACstatus: 看提示会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 216</summary>
<details>
<summary>lrc</summary>

<details>
<summary>F - Max Sum Counting</summary>

- <details><summary>comment</summary>贪心的过程主要是排序，之后子集和表示状态即可，思路出的不是很慢，很顺利就出来了</details>
- <details><summary>tag</summary>贪心+dp</details>
- spendtime: 40
- ACtime: 2022.10.24
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>G - 01Sequence</summary>

- <details><summary>comment</summary>看到这个直接就想到了，LCP32这道题目，因为很难，印象也很深刻，所以，直接就拿过来用了，不过，要让自己完整的写出来，还是很有难度的，建议把这部分的代码好好阅读，当成一种板子，因为贪心过程真的很难很难，我是不可能自创extra这种玩意的，贪心的方式最好也可以看题解，反正每次看一遍就要学一遍，后来发现，有另外一种抽象的形式，叫差分约束，使用每一个不等式建立原图中的边，然后找到所有的差分的式子，跑一个最短路即可，这种建模方式还是值得学习，贪心也确实太难</details>
- <details><summary>tag</summary>贪心or差分约束</details>
- spendtime: 60
- ACtime: 2022.5.23
- ACstatus: 之前做过
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 217</summary>
<details>
<summary>lrc</summary>

<details>
<summary>F - Make Pair</summary>

- <details><summary>comment</summary>我好难受，唉，转移的时候，想不清楚了，两个区间进行合并的时候，直接忘记排列组合的知识在里面了，直接被干翻，而且区间转移的时候，也不是很自信，痛苦啊，错失了，痛苦啊，其实我们仍然可以这么考虑，对于一个区间来说，依然是从其中的两个子区间转移而来，前一半必须符合，(left,y)为合法的，这么考虑,对于前一半来说，其方案数为dp[left+1][y-1],因为必须先合并中间的，才能合并两边的，后一半的方案为dp[y+1][right],则考虑将两个序列合并，但是每个序列也要保持跟之前一半的时候一样的次序有多少种方案，这就需要排列组合了，乘从n个中去k个。则在这里面就是，C（len/2,(y-left)/2），这样子就完成了正确的转移，刚开始把排列组合这事忘了，合并序列忘了，唉，失败，看来各种知识都有可能用到</details>
- <details><summary>tag</summary>区间dp</details>
- spendtime: 90
- ACtime: 2022.5.21
- ACstatus: 看提示后会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 218</summary>
<details>
<summary>lrc</summary>

<details>
<summary>F - Blocked Roads</summary>

- <details><summary>comment</summary>直观上来看，这题目一定一定是有个特别简单的做法我没看出来，但就是这份没有看出来，造成了我的不会，没想到看过提示之后，巨简单，我直接就傻了，对时间复杂度，在心理上有个稍微的估计，那就是n*n*n,但是，一直没有走到正轨上，其实就是，求个最短路，然后不在最短路上的一定不是，在最短路上的，在跑一遍最短路算法，我还是大意了，我好蠢啊，给个7分并不是因为这个有多难，而在于第一次没有做出来</details>
- <details><summary>tag</summary>最短路+分析</details>
- spendtime: xx纯思考了
- ACtime: 2022.5.21
- ACstatus: 看提示后会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 219</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Moat</summary>

- <details><summary>comment</summary>这一个很长的模拟题目，直接对二进制枚举即可，然后判断，是否可行，所以枚举不难，判定很难，至少把我绕进去了一会，不过我最终冲出重围，很关键的点就是，刚开始不好想，到底什么情况是违法的，不合适的，想了半天，判定分为两部，首先，1的部分必须是一个联通块，执行一下这个判断，第二，不能形成中空样子的，就比如{1,1,1},{1,0,1},{1,1,1}中间是空的，这是非法的，那么如何判断呢，只需要从边界做渗透即可，如果边界能够渗透所有的0，则成立，反之，必然存在0的联通块被包裹，是一个很好的模拟分析枚举题目</details>
- <details><summary>tag</summary>分析模拟</details>
- spendtime: 70
- ACtime: 2022.5.19
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 220</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Distance on Large Perfect Binary Tree</summary>

- <details><summary>comment</summary>写法上是不是有什么讲究，好像细节问题还挺多的，稍微的多调试了一会，不过方案数倒是比较的直观，比较模糊写的一道题目</details>
- <details><summary>tag</summary>数形结合+细节</details>
- spendtime: 80
- ACtime: 2022.10.21
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>F - Distance Sums 2</summary>

- <details><summary>comment</summary>两边dfs，先求子树信息，再求最终的，算是个代码上稍微多的dfs</details>
- <details><summary>tag</summary>双dfs</details>
- spendtime: 50
- ACtime: 2022.10.22
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 221</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - LEQ</summary>

- <details><summary>comment</summary>没想到没有直接反应上来，有点太愚蠢了，自责中，算是一个非常板子的题目都没有反应过来，还看了一眼答案，就是因为数学式子很愚蠢的化简有问题，一方面排序，排序完之后使用数状数组维护前x的区间和即可，累加即可，主要就是对1/x的处理太愚蠢了，就是都可以逆元直接求和的，还通分，真的愚蠢</details>
- <details><summary>tag</summary>除法逆元+树状数组</details>
- spendtime: 80
- ACtime: 2022.10.20
- ACstatus: 看了一下提示
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 222</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Red and Blue Tree</summary>

- <details><summary>comment</summary>先把走过的边统计一下，得到了一个边的统计数组，此时再用dp即可，刚开始觉得比较犹豫的地方就是这里，时间复杂度快10^8,但看了官方的也是说10^8，所以复杂度应该是ok的，dfs和dp的结合，应该算个不错的题目</details>
- <details><summary>tag</summary>dp+dfs</details>
- spendtime: 60
- ACtime: 2022.5.20
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>F - Expensive Expense</summary>

- <details><summary>comment</summary>先执行一遍树形dp，得到子树的解，此时还剩父节点的信息没有聚合，那就再来一遍dfs，把父节点的信息汇总即可，用dj的话，有个很重要的性质，是看答案了解到的，就是假设图里面存在一个直径，这个直径表示一个图上，最远的两个点，先将原图抽象，复制一个点，找直径的方式是，从任意点求dj，然后找到最远的点，再从这个点再做dj，然后就得到了，然后有了s,t,对于原图来说，对于任意点来说u，最大的距离就是，max（(u,s'),(u,t')),这个结论确实不知道，但知道的话，估计写的很快，dp也不错</details>
- <details><summary>tag</summary>树形dp</details>
- spendtime: 50
- ACtime: 2022.5.20
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 223</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Placing Rectangles</summary>

- <details><summary>comment</summary>其实题目初读是挺吓人的，至少读完是比较懵的状态，完全不知道怎么搞，然后死马当活马医，直接枚举可能的放置方案就可以了，没相当还真行，吓人题目</details>
- <details><summary>tag</summary>枚举</details>
- spendtime: 50
- ACtime: 2022.10.14
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>F - Parenthesis Checking</summary>

- <details><summary>comment</summary>看到区间查询，还加区间修改的这类操作，遇事不决就看看分块能不能行，当然，这道题目分块还是比较可行的，复杂度也说得过去，遂一遍过掉了，虽然编码确实得好久，然后看了看正解，是用线段数做的，那从时间复杂度上就优化很多了，核心思路是这样建模的，把()这样的符号，看成-1,1,一个区间只要区间和为0且没有前缀和是负数即可，那么区间和是好维护的，那前缀和怎么办呢，可以这么考虑，对于一个节点来说，其最小的前缀和来自于，左节点中，或者左节点区间和加右节点最小值，即node.min=min(left.min,left.sum+right.min),这就是这道题目的巧妙之处</details>
- <details><summary>tag</summary>分块or线段树</details>
- spendtime: 60
- ACtime: 2022.5.12
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 224</summary>
<details>
<summary>lrc</summary>

<details>
<summary>D - 8 Puzzle on Graph</summary>

- <details><summary>comment</summary>直接搜索即可，暴力搜索即可，没啥技巧</details>
- <details><summary>tag</summary>枚举</details>
- spendtime: 35
- ACtime: 2022.10.13
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>E - Integers on Grid</summary>

- <details><summary>comment</summary>从后向前，将每一个行列的最大值存储，每一轮都去把所有相同的值去处理了，这是细节地方，刚开始就没有处理好，所以，从大到小去迭代，这样子就可以在nlogn时间内完成，刚开始还在纠结如何建图，后来还是方向搞错了</details>
- <details><summary>tag</summary>分析+贪心</details>
- spendtime: 60
- ACtime: 2022.5.10
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>F - Problem where +s Separate Digits</summary>

- <details><summary>comment</summary>知道题目仔仔细细看了答案，发现，根本没有很增进我的理解，那么还是要重新思考，看了两篇的理解，都是不太行，然后自己再重新读题目，发现题目都读的有一点出入，所以产生了自己重新理解问题，我们可以这么思考，尝试讨论，每一位的贡献值是多少，以111为例，1+1+1,11+1,1+11,111,这四种，从后往前，可以发现，每一位的贡献是这么分布的，(1,1,1,1)(1,1,10,10)(1,1,10,100),尝试扩展1111，情况会更加清楚,(1,1,1,1,1,1,1,1)(1,1,1,1,10,10,10,10)(1,1,1,1,10,10,100,100)(1,1,1,1,10,10,100,1000),可以发现，每次改变的是一小半，逐步的下去，则找到规律就十分好求了，这就是关键点，不过我还是认为这样的分析还是很难在短时间内想到，非常的困难，题目难度虽然不是很高，但是，我真的想了看了很久</details>
- <details><summary>tag</summary>找规律</details>
- spendtime: 280
- ACtime: 2022.5.13
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 225</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - 7</summary>

- <details><summary>comment</summary>用了一个不错的套子，套了一层，突然没有看出来，但，一经提示后，还是很能快速的反应过来对应到模型上的，简单来说，就是将7的两点的斜率抽象成区间的两个端点，然后跑一遍不相交区间最大值即可，刚开始把贪心过程搞反了，害得我检查了好久，其实对于区间[l,r]来说，按照r最大，r相等时候l最大，排序，然后再从前向后即可</details>
- <details><summary>tag</summary>分析+贪心</details>
- spendtime: 70
- ACtime: 2022.5.9
- ACstatus: 看提示会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 226</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Just one</summary>

- <details><summary>comment</summary>对于环和环上单链算一个整体，其为2，乘起来即可</details>
- <details><summary>tag</summary>找环</details>
- spendtime: 70
- ACtime: 2022.10.13
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 227</summary>
<details>
<summary>lrc</summary>

<details>
<summary>D - Project Planning</summary>

- <details><summary>comment</summary>因为一个英文单词读错了，我也是谢特了，没办法，所以，我看了一下答案，那确实是不太好，隐隐约约记着lc的周赛某一次最后一个题目就是这个，但是实在是找不到了，将模型抽象出来就是，从k个不同的堆里面选择1个，这样的操作最多能有多少次，每次累计的时候sum+=min(ai,mid)即可，其中，mid为二分出来的值，代表，最多可以操作mid次，判定就是，当sum>=mid*k的时候，就证明一定是可以的，用此来判定，算是比较经典的判定方式，唉，没读懂题目吃大亏啊</details>
- <details><summary>tag</summary>二分判定</details>
- spendtime: 80？
- ACtime: 2022.5.7
- ACstatus: 没读懂题
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 228</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Integer Sequence Fair</summary>

- <details><summary>comment</summary>再一次复习了一些定理的使用范畴，如此考虑，对于a^b来说，只要b还是long范围，那么直接使用a^b的快速幂，取模依然是(res*base)%mod,但是当b非常大的时候，就不能行了，费马尔小定理告诉我们，a^(p-1)==1(mod p),当p为素数时候，则其实就变成了a^(b%(p-1)),如果b又是个嵌套的，比如这道题目中的，m^(k^n)，对于k^n快速幂的话，因为k^n取模的对象是mod-1,则外面取模mod,指数取模mod-1,这就是区别</details>
- <details><summary>tag</summary>费马尔小定理</details>
- spendtime: 60
- ACtime: 2022.10.11
- ACstatus: 看提示后会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 229</summary>
<details>
<summary>lrc</summary>

<details>
<summary>F - Make Bipartite</summary>

- <details><summary>comment</summary>我觉得就是一个贪心，但我很难抽象出一种正确解的方式，但一看题解，给的竟然是，dp？？wc？我又失败了？不过状态是很牛逼的，状态的设计还是很清楚的，可以这么去表达，dp[i][j][k]表示，i个点染成j,且1个点为k，颜色就两种，0，1，0节点直接染0即可，所以对于任意一个点的运算，只跟前一个状态有关，那为什么要有一个状态k呢，主要还是因为，在最后的1和n的交界处，需要处理，刚开始为了无后效性，所以，假设将1，n断开，最终，需要再讨论加上即可，设计状态非常不错，是一个非常好的dp题目，代码量也非常的短，好题，但我不会</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 思考60
- ACtime: 2022.5.6
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 231</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Minimal payments</summary>

- <details><summary>comment</summary>简单说下做法吧，是这样的，从最小的一位不断的向高位走，比如对于，87来说，1，10，100三种货币，对于1来说，就两种选择，要么选择为正符号，要么选择为-符号，那么看高位的需求，在这里面，高位要么需求把7拿掉，要么需要+3达到高位的需求，则对于这种情况，分别递归去做，每一层的做法都是相同的，时间复杂度一时间没有想明白，不得不说，还是牛的，基本上已经到了次次都不会的地步，关于复杂度的推论，尝试使用这个角度去理解，其中我们达到下一级或者上一级其实在模余的意义下是一样的，所以，开始状态开始了分叉其实并没有</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 思考40+看题解70
- ACtime: 2022.5.2
- ACstatus: 不会
- score: 7
</details>

<details>
<summary>F - Jealous Two</summary>

- <details><summary>comment</summary>按照第一个维度排序，从小到大，第二个维度使用数状数组维护，统计即可，语义比较清晰</details>
- <details><summary>tag</summary>树状数组</details>
- spendtime: 50
- ACtime: 2022.10.10
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 232</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Rook Path</summary>

- <details><summary>comment</summary>不知道咋回事，对于有范围的想不明白了，比如对于到a的方案数，一共走k步，允许+1，-1，但不能超过[x,y]这个范围的方案数，不会求了，如果能解决了这点，那就很容易出来了，后来发现，原来是题读错了，这就很让人窒息了，因为一次可以走任意的格子，所以其实就是个线性dp，状态即是否等于目标格子，转移很简单，记忆化搜索即可，感觉还行吧</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 120
- ACtime: 2022.10.8
- ACstatus: 不会->完全会
- score: 7->6
</details>

<details>
<summary>F - Simple Operations on Sequence</summary>

- <details><summary>comment</summary>说说不会做的感言，嗯，对于状态的表达还是欠缺，后来花了一下午的时间，好好理解抽象的含义，最终搞懂了，是怎么个回事，我们用dp[s]表示，s为已经决定那些元素的二进制压缩，那么dp[s]就表示已经决定了s状态的元素的最小值，其中还有个隐含的条件在于，len(s)为二进制中为1的个数，已经决定了s状态的元素，且它们在0-len(s)这个范围内，则循环遍历的从1-(pow(2,n))，每次对于一个状态s，我们考虑往进添加元素，并且这个新加进来的元素位于len(s)+1,则对于交换的次数，我们就可以用逆序对的方式求出，即考虑，对于已经决定的元素是都不用计算的，只用计算，未决定的但是比当前的元素的下标要小的即可，分析结束，还是一个非常不错的dp，而且代码量短，分析起来很考验思维，个人评价非常不错</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 思考60+看解释150
- ACtime: 2022.4.29
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 234</summary>
<details>
<summary>lrc</summary>

<details>
<summary>F - Reordering</summary>

- <details><summary>comment</summary>主要是状态和转移没思考清楚，觉得可能用组合数学的方式更好思考，没想到还是dp更加万能一些，dp[i][j]表示前i种字符能形成j长度的不同字符串的方案数，想明白这个自然就能想明白转移</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 100
- ACtime: 2022.10.7
- ACstatus: 看提示后会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 235</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - MST + 1</summary>

- <details><summary>comment</summary>做个标记，对于查询边，并不真正合并，对于真正边，才合入，运行一遍最小生成树算法结束，代码清晰，语义简单</details>
- <details><summary>tag</summary>贪心</details>
- spendtime: 22
- ACtime: 2022.10.6
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 236</summary>
<details>
<summary>lrc</summary>

<details>
<summary>F - Spices</summary>

- <details><summary>comment</summary>我个人觉得可能想法是一种经典的东西的衍生，但是我不会，状态想不出来，转移也出不来，后来看了解释，虽然代码量很少，但是，个人觉得，非常的怎么说，难以想到吧，不过真的是有巧妙的地方，首先我们可以直观感受到，对于选择大小是一个不可能很大的，而且，所有的子集必然可以拼凑出，不同的数，而不同的数，最多只有2^n个，所以，集合的大小就为n,则这个是比较需要注意的，好的，我们讲从中选择n个数，那么选择i个数，每次最多需要更新2^n，则算法的复杂度就局限在了2^n*n，对于已经能表示的数，可以直接用数组存储起来，然后不断更新即可，那具体算法怎么跑？按照钱数先贪心的排序一遍，然后检查是否当前的状态可以表示，不可以就得加进去，然后更新即可，此处的算法正确性，有赖于贪心的证明，关于，加进去与不加进去，对语不加进去的非常好证明，对于加进去的，需要贪心的反证，总之，还是个非常不错的贪心题目</details>
- <details><summary>tag</summary>贪心</details>
- spendtime: 60思考
- ACtime: 2022.4.28
- ACstatus: 不会
- score: 7
</details>

<details>
<summary>E - Average and Median</summary>

- <details><summary>comment</summary>这种题目的二分思路还是比较好出来的，因为，是求最大限制类，所以，转为判定思路。对于中位数的判定是好想的，我们尽可能的在这个序列中，选择更多的>=中位数的数，而对于平均数的探讨，用到了常用的技巧，那就是，假定每个数都要减去这个mid，然后线性dp求可不可能>=0即可，其中有个二分的时候，整数的二分是low=mid+1,double的二分是low=mid</details>
- <details><summary>tag</summary>二分+贪心</details>
- spendtime: 70
- ACtime: 2022.4.27
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 237</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Skiing</summary>

- <details><summary>comment</summary>初看是一道求单源最长路问题，又有正权值边，也有负权值边，自然就想到了bellman算法或者spfa算法，但是数据量很大，10^5级别，很容易就被卡掉，心中故有疑虑，关于边权的构造，与以往的随意的图也不同，即边权是有构造约束的，那么通过其定义边权的方式，能不能试图找到突破口，用一些转化，来变成nlogn算法，虽然一个有负权重的边无法转换成djstla算法来求，但并不意味者，在特殊场景中，完全无法转换，但是仔细观察，这种路径的转化带有很大胆的猜测性，任意给一种路径A->B->C可以随意举例A,B,C的大小关系，对于A>B,则A->B为0的边，B->A为A-B的边，可以举例出，A->C的最短路，则应该是在上面构造的边的最短路-（Ha-Hc）的负数，则就是原始的答案，关于这步转化和构造还是非常非常难的，个人觉得技巧的积累度很高，还是需要多积累</details>
- <details><summary>tag</summary>分析+最短路</details>
- spendtime: 150
- ACtime: 2022.4.4
- ACstatus: 看提示后会
- score: 7
</details>

<details>
<summary>F - |LIS| = 3</summary>

- <details><summary>comment</summary>看题目必然是一道dp，但难点在于对状态的精准抽象，是不是会不自觉的想到，能不能使用dp[i][j]来表示，结尾为i的且最长序列恰好为j的方案数，这是一种很直观，不带有思考的dp的表达方式，但是，这是不行的，从直观上就很难抽象出前面序列的各种情况，即我们使用的状态的维度肯定是不够的，遂换一种思路，既然维度不够，那可不可以使用一个整数，每两位表示以当前这个位所表达的索引的数的最长序列的长度，是一种状态压缩的思路，即最多有4^10的状态，故时间复杂度超了，也没办法做，发现，其实这个状态的定义是有冗余的，冗余的状态则会导致时间复杂度的提升，遂，将状态改成，dp[i][j][k]表示递增序列长度为1的最小的值为i且递增序列长度为2的最小值为j且递增序列为3的最小值为k的方案数，这样时间复杂度就是千万级别的复杂度，因为发现，当一个序列中还是最小的值会对后面起到作用，此处的叙述，还是比较混乱，关于初值如何定，最后结果的存储在那里，还是需要对状态定义有一定的理解才可以做出来，对此题目的考察还是非常推荐的，是一道dp好题目</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 120
- ACtime: 2022.4.5
- ACstatus: 完全会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 238</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Range Sums</summary>

- <details><summary>comment</summary>一道bfs抽象好题目，思考时间较长，没有一眼就看出模型，非常考验思维，也不失代码表达，底层模型简单，非常不错的题目，我想了很长时间，一直没有参悟透彻，建模比较困难，是一道非常不错的考验抽象能力的题目，线段信息从前往后，从后往前，从不对的dp思考到bfs建图，标注一下，相当不错</details>
- <details><summary>tag</summary>bfs抽象</details>
- spendtime: 180
- ACtime: 2022.10.5
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>F - Two Exams</summary>

- <details><summary>comment</summary>这种题目与lc上有些题目，有相似的关联，与俄罗斯套娃信封问题的数据有些相似，但从状态设计上，还是很有区别的，此问题可以如此思考，状态如此设计，dp[i][j][k]表示，已经检查过i个人，且已经有j个人加入中，在未选择中的k是最小的的所有方式，刚开始由于没有思考清楚题目，直接就把状态设计成，二维的，直接用dp[i][j]来表达前i个中选择了j个人的所有方案，然后考虑在转移的时候，多加些逻辑，其实没有深刻认识到，关于已选人数其实并不能完整表明状态，需要加一个维度来表明，其次，如此设计状态之后，关于转移的理解也是比较有意思，先对第一个维度进行排序，然后在第二个维度上进行dp，考虑如果将i个人加入时，需要看，是否此人的未选中的最小的要小于上个阶段中的k即可，从意思上，就是，如果此人的排名越高，则更加可能被加入，如果排名越低，则越不可能，此题目比较有意思的，还在于，当考虑如果不把i人加入时，就会对结果有影响，因为就不能更新那些，如果不加i就收到影响的状态，所以此处的更新比较难想，这就是此题目的价值所在，我评价还是很好的dp题目，转移和状态很不错</details>
- <details><summary>tag</summary>dp+贪心</details>
- spendtime: 70
- ACtime: 2022.3.9
- ACstatus: 不会
- score: 7
</details>

<details>
<summary>G - Cubic</summary>

- <details><summary>comment</summary>奇怪的知识又增加了，莫队算法还是可以的，适用场景也会变得简答，离线区间查询，我们是不是可以想办法利用查询区间的排布，来达到n^1.5的时间消耗，就是把区间的排布，按照分块的思想，排序，按照第一个关键字，第二个关键字排序，但是关键在于，一定是按照区间的编号进行排序，这部分需要好好想明白，然后，又在歧视java了，难道对于这种，又要让我整c++吗，看来，是时候，该思考思考了</details>
- <details><summary>tag</summary>莫队算法</details>
- spendtime: 170
- ACtime: 2022.3.9
- ACstatus: 不会
- score: 7

</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 239</summary>
<details>
<summary>lrc</summary>
<details>
<summary>A	Horizon</summary>

- <details><summary>comment</summary>水水就行</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 2
- ACtime: 2022.2.22
- ACstatus: 完全会
- score: 2
</details>

<details>
<summary>B	Integer Division</summary>

- <details><summary>comment</summary>写就完事了</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 2
- ACtime: 2022.2.22
- ACstatus: 完全会
- score: 3
</details>

<details>
<summary>C	Knight Fork</summary>

- <details><summary>comment</summary>如图枚举一圈即可</details>
- <details><summary>tag</summary>枚举</details>
- spendtime: 10
- ACtime: 2022.2.22
- ACstatus: 完全会
- score: 4
</details>
<details>

<summary>D	Prime Sum Game</summary>

- <details><summary>comment</summary>简单博弈问题，主要是数据太小</details>
- <details><summary>tag</summary>暴力</details>
- spendtime: 10
- ACtime: 2022.2.22
- ACstatus: 完全会
- score: 4
</details>

<details>
<summary>E	Subtree K-th Max</summary>

- <details><summary>comment</summary>看K瞬间就明白怎么做了</details>
- <details><summary>tag</summary>树形dp</details>
- spendtime: 15
- ACtime: 2022.2.22
- ACstatus: 完全会
- score: 4
</details>

<details>
<summary>F	Construct Highway</summary>

- <details><summary>comment</summary>不得不说，写起来是真的麻烦，不得不说，代码量的复杂是真的麻烦</details>
- <details><summary>tag</summary>并查集+贪心</details>
- spendtime: 75
- ACtime: 2022.2.22
- ACstatus: 结合提示
- score: 6
</details>

<details>
<summary>G - Builder Takahashi</summary>

- <details><summary>comment</summary>没办法，不仅仅要网络流，而且还要求割集，还是无比的麻烦，不过网络流的割点转化为割边这个思路还是不错的</details>
- <details><summary>tag</summary>网络流</details>
- spendtime: 150
- ACtime: 2022.2.22
- ACstatus: 结合提示
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 240</summary>
<details>
<summary>lrc</summary>

<details>
<summary>F - Sum Sum Max</summary>

- <details><summary>comment</summary>这题比较的，嗯，有点麻烦，第一次做sum的时候，形成了一个折线图，第二次的sum其实是求积分，关键就在于找>0的点，预处理一遍即可，我觉得是作为这个分数段的题目，但是其中的考虑一定不会那么直观想出来，作为面试题目，应该能死一大堆</details>
- <details><summary>tag</summary>数形结合</details>
- spendtime: 180
- ACtime: 2022.10.3
- ACstatus: 完全会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 241</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Putting Candies</summary>

- <details><summary>comment</summary>主要就是找环，然后模拟一遍，记录一下即可</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 20
- ACtime: 2022.9.28
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>F - Skate</summary>

- <details><summary>comment</summary>没读清楚题目，有点傻逼，意思是必须要撞到障碍物才能停，那理所应当的离散化，最多只有障碍物旁边的点，所以时间上是过得去，直接bfs即可，刚开始没有看清楚题目，以为是不能跨越障碍物，非常的愚蠢</details>
- <details><summary>tag</summary>bfs+离散化</details>
- spendtime: 75
- ACtime: 2022.7.20
- ACstatus: 不会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 242</summary>
<details>
<summary>lrc</summary>

<details>
<summary>D - ABC Transform</summary>

- <details><summary>comment</summary>就是不断除2判断即可，递归写好写</details>
- <details><summary>tag</summary>分析+模拟</details>
- spendtime: 30
- ACtime: 2022.9.14
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>E - (∀x∀)</summary>

- <details><summary>comment</summary>不算特别但是也比较可以，最后一步，判断是否存在由前半部分的回文的时候，应该从后往前判断即可</details>
- <details><summary>tag</summary>数位回文</details>
- spendtime: 50
- ACtime: 2022.9.15
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>G - Range Pairing Query</summary>

- <details><summary>comment</summary>就是莫队算法，刚开始写的有点小问题，调一调就可以了，非常的板</details>
- <details><summary>tag</summary>莫队算法</details>
- spendtime: 90
- ACtime: 2022.7.20
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 243</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Edge Deletion</summary>

- <details><summary>comment</summary>这种就是对于算法的一个理解度了，先用n^3的算法求出任意点之间的最短距离，然后枚举边，什么情况下这个边不需要，设这条边为(a,b)，则a->i,i->b如果还有小于等于的，就证明不需要这条边，这就是关键，很强，积累吧</details>
- <details><summary>tag</summary>floyd</details>
- spendtime: 70
- ACtime: 2022.7.20
- ACstatus: 不会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 244</summary>
<details>
<summary>lrc</summary>

<details>
<summary>F - Shortest Good Path</summary>

- <details><summary>comment</summary>关键就是抽象好，转化为图论的问题，如何转化，将dp[s][i]表示s集合且结尾为i的最短距离，又因为权重为1，则可以直接bfs，复杂度最多17*17*2^17，可以过掉，转移过程很简单，dp[s][i]->s(xor)(i的邻居)即可，是一道好题目，虽然分值不高，但卡我好几下，刚开始觉得是个暴力搜索，实在愚蠢</details>
- <details><summary>tag</summary>抽象+bfs</details>
- spendtime: 110
- ACtime: 2022.7.21
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 245</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Wrapping Chocolate</summary>

- <details><summary>comment</summary>题目还是不错的，很简单的模型，但是值得思考，先排序，按照第一维度，再按照第二个维度排序，然后就是一个常用的技巧，我们对盒子数组，从前往后，此时需要查询的就是满足条件的，那二维的肯定不好查询，所以就排序来降维处理，这样我们第一维度按照顺序，只用考虑第二个维度的事情就可以了，treemap维护一下即可</details>
- <details><summary>tag</summary>贪心</details>
- spendtime: 39
- ACtime: 2022.9.12
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>F - Endless Walk</summary>

- <details><summary>comment</summary>一眼题目写了巨久，傻了，其实就是保留环和环的入边，即可，就是拓扑排序反着来就行</details>
- <details><summary>tag</summary>反转拓扑排序</details>
- spendtime: 50
- ACtime: 2022.9.13
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 246</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Bishop 2</summary>

- <details><summary>comment</summary>不知道为啥写废掉了，直接dj的话，边数还是有些大，所以直接贪心的bfs即可，不知道为啥写得废掉了，非常的狗屎，太失败了</details>
- <details><summary>tag</summary>bfs</details>
- spendtime: 90
- ACtime: 2022.9.8
- ACstatus: 完全会
- score: 5
</details>

<details>
<summary>F - typewriter</summary>

- <details><summary>comment</summary>这道题算是容斥原理的入门级别了，二进制表示a,b,c,d,...，然后所有的之和就为,L1+L2+L3..-L1(and)L2-L2(and)L3....+L1(and)L2(and)L3+..这样的形式，比较快速就出来了</details>
- <details><summary>tag</summary>容斥原理</details>
- spendtime: 50
- ACtime: 2022.7.21
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 247</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Max Min</summary>

- <details><summary>comment</summary>就是一种计数优化，写得稍微时间长了点，非常的无奈，不过题目表达的意思是比较简单的，简单来说，就是在x和y之间的有用，别的没用break,O（n）算法即可</details>
- <details><summary>tag</summary>分析</details>
- spendtime: 50
- ACtime: 2022.9.8
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 248</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - K-colinear Line</summary>

- <details><summary>comment</summary>两点确定一条直线，当前仅当k为1时是无限，使用set去重即可</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 20
- ACtime: 2022.9.7
- ACstatus: 完全会
- score: 5
</details>

<details>
<summary>F - Keep Connect</summary>

- <details><summary>comment</summary>代码和形状结合，分析清楚转移的情况即可，算是一个根本不难出思路，但是需要把情况分析清楚的dp</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 100
- ACtime: 2022.7.23
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>


<details>
<summary>AtCoder Beginner Contest 249</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - RLE</summary>

- <details><summary>comment</summary>dp状态定义非常重要，dp[i][j]表示原始字符串长度为i，转化之后字符串长度为j的方案数，对于j来说，转移就来自四种长度，这就是关键的复杂度优化的地方，其他的没啥，对我来说，还是状态有点不太好想</details>
- <details><summary>tag</summary>dp+前缀和</details>
- spendtime: 100
- ACtime: 2022.7.25
- ACstatus: 不会
- score: 7
</details>

<details>
<summary>F - Ignore Operations</summary>

- <details><summary>comment</summary>从后往前，贪心来做，我们可以将一次1操作看成一个分水岭，存在两种，要么跳过，要么不跳过，对于不跳过，怎么讨论前面没有意义，对于跳过，就是必须得用一次跳过操作，记为cnt,对于负数维护一个优先级队列，正数不需要跳过，优先级队列的大小要<= k-cnt,则就完成了贪心的过程</details>
- <details><summary>tag</summary>贪心</details>
- spendtime: 55
- ACtime: 2022.7.23
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 250</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Prefix Equality</summary>

- <details><summary>comment</summary>出一道这样的题目还是非常不错的，要思维有思维，要难度有一点点难度，很不错的题目，这么思考，对于a中出现的所有数，只需要求在b中第一次出现的位置即可，同理，则使用map存储一下第一次出现的位置，然后再从前往后维护一个max数组即可，表明当前位置为i时需要对方的位置至少为j，则就很容易写出来了</details>
- <details><summary>tag</summary>分析</details>
- spendtime: 25
- ACtime: 2022.7.21
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 251</summary>
<details>
<summary>lrc</summary>

<details>
<summary>D - At Most 3</summary>

- <details><summary>comment</summary>式子可以写成这样，val=a*10000+b*100+c,a,b,c的取值都是1-99，所以是可行的，这个就是关键，所以跟输入已经无关了，直接1-99,100-9900,10000-990000,即可</details>
- <details><summary>tag</summary>脑筋急转弯</details>
- spendtime: 40
- ACtime: 2022.9.3
- ACstatus: 不会
- score: 7
</details>

<details>
<summary>E - Takahashi and Animals</summary>

- <details><summary>comment</summary>这种带有环形的dp的处理技巧就是算头部是一种状态，另一种状态是不算头部，状态和转移都十分的好写，没啥问题</details>
- <details><summary>tag</summary>环形dp</details>
- spendtime: 27
- ACtime: 2022.9.5
- ACstatus: 完全会
- score: 5
</details>

<details>
<summary>F - Two Spanning Trees</summary>

- <details><summary>comment</summary>dfs和bfs的另一种说明，上手画画图，发现，对于第一种生成树来说，不就是dfs的另一种解释吗，对于第二种生成树来说，不就是bfs的另一种解释吗，也属于灵机一动那种了</details>
- <details><summary>tag</summary>dfs+bfs</details>
- spendtime: 40
- ACtime: 2022.7.25
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>


<details>
<summary>AtCoder Beginner Contest 252</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Road Reduction</summary>

- <details><summary>comment</summary>一眼题目，果断看错，理所应当，最小生成树，当然不是，很容易举例，其实是最短路选边的过程，因为最短路一定是个生成树</details>
- <details><summary>tag</summary>最短路</details>
- spendtime: 30
- ACtime: 2022.9.2
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>F - Bread</summary>

- <details><summary>comment</summary>基本上已经没有太多的思维转折点了，贪心来，不断从优先级队列里面挑出最小的两个进行拼装即可</details>
- <details><summary>tag</summary>贪心</details>
- spendtime: 30
- ACtime: 2022.7.25
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 253</summary>
<details>
<summary>lrc</summary>

<details>
<summary>F - Operations on a Matrix</summary>

- <details><summary>comment</summary>两个操作分别操作了不同的维度，一个是列的区间操作，自然想到了线段树，一个是替换行的操作，两遍处理，第一遍，需要找到每一个需要print的点的最近的替换操作，然后正向计算出此时替换时，此点的累加的值是多少，记录下来，这样子，两个维度的计算就可以按照这样的方式计算，假设当时记录的值是x,替换的值是y,当查询这个点时候的值为z,则z-x+y就是真正的值，这就是关键，剩下的都是一些细节问题，不过题目还是不错的</details>
- <details><summary>tag</summary>分析+线段树</details>
- spendtime: 100
- ACtime: 2022.7.26
- ACstatus: 完全会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 254</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Small d and k</summary>

- <details><summary>comment</summary>主要是度数太小，直接可以bfs，不假思索，比较一眼</details>
- <details><summary>tag</summary>bfs</details>
- spendtime: 25
- ACtime: 2022.8.30
- ACstatus: 完全会
- score: 5
</details>

<details>
<summary>F - Rectangle GCD</summary>

- <details><summary>comment</summary>最主要的技巧就是，gcd(a+b1,a+b2,a+b3,...)可以转化为gcd(a+b1,b2-b1,b3-b2,...)直接将式子转化为跟另一个维度无关的数，就可以使用线段树来维护了，比较简单，这就是思路的关键，很数学</details>
- <details><summary>tag</summary>gcd技巧</details>
- spendtime: 150
- ACtime: 2022.7.27
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 255</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Lucky Numbers</summary>

- <details><summary>comment</summary>大概是10^7复杂度算法，使用两个map存储一下，如果确定了第一个数，那么后面的数都确定下来了，x,s[1]-x,s[2]-s[1]+x,等等，一个map存储+x，一个map存储-x,在每一个map中枚举，假设这个数是变成了x[i],然后分别统计一下即可，思路出的比较快，不绕弯子</details>
- <details><summary>tag</summary>计数优化</details>
- spendtime: 35
- ACtime: 2022.8.27
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>F - Pre-order and In-order</summary>

- <details><summary>comment</summary>有个很玄学的运行时错误，不知道为什么会有，用tryAC了，后面可以看一看，但是这算是一个mid吧？不知道为什么分能上那么高</details>
- <details><summary>tag</summary>树的构建</details>
- spendtime: 90
- ACtime: 2022.7.28
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 260</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - At Least One</summary>

- <details><summary>comment</summary>我觉得还算不错的题目，这样子考虑，一组点队，(x1,y1),(x2,y2)...,如此序列，对于任意查询来说，(x,q)这个区间至少要包含所有点对的一个点，如此考虑，是一个堆砌逻辑的二分问题，出思路的时候，也是比较缓慢，所以赛事并没有做出来，之后就做出来了，其中当然也加入了区间查询的基础算法</details>
- <details><summary>tag</summary>贪心</details>
- spendtime: 70
- ACtime: 2022.7.23
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Regular Contest 105</summary>
<details>
<summary>lrc</summary>

<details>
<summary>C - Camels and Bridge</summary>

- <details><summary>comment</summary>这道题目的思考和验证还是非常的长且逐步完成，是一道代码量很大，且思维要求很不错的题目，有两个必须提前就知道的工具，一个是区间最值查询，一个是下一个排列，使用线段树和下一个排列的板子解决，现在重新思考，对于任意一种排列，思考，如何使得这个排列的总长度最小，x1,x2,x3举例子，需要判断x2+x1,x3+x2,x3+x2+x1,每次中，从条件中寻找一个小于这个重量且长度最长的作为期待长度，与现有的长度比较，如果现有长度大，则ok，反之，需要增加距离，这块就是贪心的地方，刚开始是哪一块不够就在那一块增，后来苦苦debug，一直找不出来原因，后来才找到，需要在最后一个增加，相当于前面的序列已经是最优子结构，只能更改后来的，不能更改前面的子结构，这样子才可以最优，所以使用一个dis记录一下即可，整个逻辑就是这样，验证加思索，比较综合的一道题目</details>
- <details><summary>tag</summary>贪心+枚举+区间最值</details>
- spendtime: 150
- ACtime: 2022.7.30
- ACstatus: 完全会
- score: 7
</details>

<details>
<summary>D - Let's Play Nim</summary>

- <details><summary>comment</summary>首先我连Nim问题都不知道，Nim是经典的博弈论模型，它有个非常简单的结论，所有的数的异或值为0则后手必胜，反之先手必胜，则在这个题目中，当n为奇数时候，假设先手选择放入第一个盘子，后手可以每次选择最大的放入第一个盘子，则能保证第一个盘子的个数，大于剩余所有盘子的总数，则使得异或的结果不为零，则后手必胜了，当n为偶数时，如果异或和等于0，则后手只需要看先手每次拿什么即可，则使得异或和为0，则后手胜，反之先手胜</details>
- <details><summary>tag</summary>博弈结论</details>
- spendtime: 90
- ACtime: 2022.7.28
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Regular Contest 106</summary>
<details>
<summary>lrc</summary>

<details>
<summary>D - Powers</summary>

- <details><summary>comment</summary>真的，把式子展开即可，化简就可以了，这种数学题目，拉这么高的分，感觉没啥算法的考点，唉，无奈，就是化简式子即可，代码都懒得写了</details>
- <details><summary>tag</summary>数学化简</details>
- spendtime: 130
- ACtime: 2022.7.30
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Regular Contest 108</summary>
<details>
<summary>lrc</summary>

<details>
<summary>D - AB</summary>

- <details><summary>comment</summary>智商再次收到了歧视，这简直是碾压，我以为是dp，结果是个层层分析即可，费了很大的劲写了dp，结果一拍脑袋想错了，结果是个分析，就是这么去分析，Cab='A'怎么怎么样，='B'怎么怎么样，这些就可以了，逐渐往下分析，情况也比较好想，我真的是废物</details>
- <details><summary>tag</summary>脑筋急转弯+分析</details>
- spendtime: 110
- ACtime: 2022.8.1
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Regular Contest 111</summary>
<details>
<summary>lrc</summary>

<details>
<summary>C - Too Heavy</summary>

- <details><summary>comment</summary>果然是经典问题加贪心在里面，有两种经典问题，第一种只能交换临项元素，最少操作次数就是逆序对，第二种，可以交换任意两个，则就是总数-循环节个数，而这个题目依然是相当的，只是有个条件在里面，那就是，需要先移动背包值最大的，因为放任背包值最大的不在正确位置，只会阻碍背包的正常移动，第一次写的时候，就比较简单考虑，后来看了看，发现自己还是想少了</details>
- <details><summary>tag</summary>贪心</details>
- spendtime: 120
- ACtime: 2022.8.2
- ACstatus: 看提示会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Regular Contest 112</summary>
<details>
<summary>lrc</summary>

<details>
<summary>C - DFS Game</summary>

- <details><summary>comment</summary>博弈论的问题重在想清楚博弈的过程，可以仔细想清楚，为什么，比较困难难想的是多叉树的时候，如何处理，对于节点数是偶数的，操作一下并不影响先后手顺序，但是对于奇数个节点的数是影响的，所以，对于偶数个节点是否要选择的判断依据是，这个子树中，后手获得的比先手的多，而若先手比后手多，则取决于有多少个奇树，奇数个就是先手，偶数个就是后手，则博弈过程就结束了，当然在选择奇树的时候，依据后手-先手从大到小，保证后手获得的最多，分析到这里，加上一点大胆的猜测，题目就分析结束了，不错的博弈题目，主要还是自己想清楚了</details>
- <details><summary>tag</summary>博弈+贪心</details>
- spendtime: 120
- ACtime: 2022.8.3
- ACstatus: 完全会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Regular Contest 116</summary>
<details>
<summary>lrc</summary>

<details>
<summary>D - I Wanna Win The Game</summary>

- <details><summary>comment</summary>尝试搜了一下，发现解释的都不是很好，重新梳理一下，首先对于和为m,且异或为0的这样的序列需要大概现有个感觉，即每一个bit都是偶数，考虑dp[i][j]表示最高位可以为i位时总和为j的方案数，i的取值是13左右，j为5000，而每一个状态的转移负责度是5000，所以总的复杂度上线是13*5000*5000,但远远达不到，因为越往高位，可转移就越小，其实是13*5000*（1250+625+313+....）或者比这个更小，则最终可以过去，答案就在dp[len][m]中，转移比较简单，对于dp[i][j]来说，循环，从dp[i-1][j-val]*C(n,val/base)的求和，相当于我们有一堆bit，从0到len，分别对每一种位置的bit进行放置，使用组合数，而每一层独立，使用乘法，总共有多少种，这么计算即可，分析下来，最终结果还是比较漂亮，题非常短，代码也很短，就很好</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 120
- ACtime: 2022.8.4
- ACstatus: 完全会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Regular Contest 117</summary>
<details>
<summary>lrc</summary>

<details>
<summary>C - Tricolor Pyramid</summary>

- <details><summary>comment</summary>首先不会的就是卢卡斯定理（求余数比较小的情况），这个确实直接不会了，其次就是对于原始问题的抽象，将颜色分别看成三个数，分别是0，1，2，叠在上面的就是（-x-y）%3，不断反转，化简上去，就是杨辉三角，这下每一个对于上面的贡献就出来了，直接求就好了，还是个不错的抽象题目，数学技巧也有了</details>
- <details><summary>tag</summary>组合数学</details>
- spendtime: 90
- ACtime: 2022.8.5
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Regular Contest 120</summary>
<details>
<summary>lrc</summary>

<details>
<summary>D - Bracket Score 2</summary>

- <details><summary>comment</summary>蛮考验智商的一道题目，可以这么思考，如何得到最大值，把最大的分为一组，把最小的分为一组，变成了两组，这样子，直接匹配即可，直接用栈模拟一遍，保证在这个过程中，由栈决定左右括号，还是非常不错的题目，我还是比较肯定</details>
- <details><summary>tag</summary>贪心+模拟</details>
- spendtime: 120
- ACtime: 2022.8.8
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Regular Contest 121</summary>
<details>
<summary>lrc</summary>

<details>
<summary>C - Odd Even Sort</summary>

- <details><summary>comment</summary>再也不想写的一道题目，烦死了，恶心，什么玩意啊，烦得一批，一个一个移动即可，垃圾题目，代码量超级大，一点都没有思维的通用性学习</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 240
- ACtime: 2022.8.8
- ACstatus: 完全会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Regular Contest 122</summary>
<details>
<summary>lrc</summary>

<details>
<summary>C - Calculator</summary>

- <details><summary>comment</summary>就差一些，想到了是用斐波那契数列来做变化，而且也分析出来了，任意的整数都可以使用斐波那契数列表示，表示为01010类似这种，但是如何组装到一块，这么没有分析出来，其实就是不同的斐波那契数如何用同样的递推表示出来即可，每次需要时+1，这块可以写几个例子多看几次即可，差一些，不过还不错，是个高思维题目</details>
- <details><summary>tag</summary>脑筋急转弯+斐波那契数列应用</details>
- spendtime: 240
- ACtime: 2022.8.9
- ACstatus: 看提示后会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 257</summary>
<details>
<summary>lrc</summary>

<details>
<summary>C - Robot Takahashi</summary>

- <details><summary>comment</summary>我心态写崩了，我觉得是输在了编程习惯上，非常的狗，不过作为一个还不错的中等题目，与lc中等题目差不多，基础不牢，地动山摇</details>
- <details><summary>tag</summary>枚举+二分</details>
- spendtime: 60
- ACtime: 2022.8.9
- ACstatus: 完全会
- score: 5
</details>

<details>
<summary>D - Jumping Takahashi 2</summary>

- <details><summary>comment</summary>注意注意，注意看循环的次序，别写错了，debug了老半天</details>
- <details><summary>tag</summary>floyd</details>
- spendtime: 40
- ACtime: 2022.8.10
- ACstatus: 完全会
- score: 5
</details>

<details>
<summary>E - Addition and Multiplication 2</summary>

- <details><summary>comment</summary>一道简单题目被自己写的稀碎，先确定最大长度，然后从高位开始不断的试下去，如果并不影响最大长度，则可以加入</details>
- <details><summary>tag</summary>贪心+分析+模拟</details>
- spendtime: 40
- ACtime: 2022.8.10
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>F - Teleporter Setting</summary>

- <details><summary>comment</summary>写了一种超级复杂复杂的做法，但是过了，我先给自己点个赞，但是，是有语义更加清晰的做法，之前想到了建立超级源点，但是没想到怎么用，非常的愚蠢，执行两次，一次从1，一次从n,答案要么在d1[n],要么在d1[0]+d2[i],要么在d2[0]+d1[i]之间，这才是最优建模，我好愚蠢，看了之后，大呼愚蠢，这才是建模，又被好题目震惊到了</details>
- <details><summary>tag</summary>建模+最短路</details>
- spendtime: 150
- ACtime: 2022.8.10
- ACstatus: 完全会
- score: 7
</details>

<details>
<summary>G - Prefix Concatenation</summary>

- <details><summary>comment</summary>dp表示最少需要多少个前缀来匹配,先观察一下dp的单调性，如果存在dp[i+1]< dp[i]，则意味着i+1时候去掉一个字母，变成i的情况，去掉最后一个字母，依然是前缀，则dp[i+1]的值要么不变，要么变小，则与之前的值矛盾了，则只可能存在dp[i+1]>=dp[i],则针对这种情况的dp，对于任意的i来说，当然是i-len，中的len越长越好，则这块就用到了kmp的原始定义，kmp的自匹配中间加入无关字符，变成s+" "+t是常用的字符串的变换方法，转移方程就巨简单了</details>
- <details><summary>tag</summary>dp+kmp</details>
- spendtime: 90
- ACtime: 2022.8.11
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 258</summary>
<details>
<summary>lrc</summary>

<details>
<summary>C - Rotation</summary>

- <details><summary>comment</summary>模拟，只用记录使用了多少cnt即可，放在周赛的2题位置合适</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 11 
- ACtime: 2022.8.11
- ACstatus: 完全会
- score: 4
</details>

<details>
<summary>D - Trophy</summary>

- <details><summary>comment</summary>是个很不错的贪心题目，中等难度，代码也十分简单，枚举终止位置即可</details>
- <details><summary>tag</summary>贪心</details>
- spendtime: 13
- ACtime: 2022.8.11
- ACstatus: 完全会
- score: 5
</details>

<details>
<summary>E - Packing Potatoes</summary>

- <details><summary>comment</summary>是一个很不错的题目，逐渐分析，使用二分确定最大步长，然后连一条边到下一个节点，重复，直到到一个已经访问过的节点，则意味着找到了循环的入口，逐步分析到这里还是比较有思维挑战的，还不错，代码实现也是有一定的代码量，不过时间稍微有点超，下次得想快一点，=</details>
- <details><summary>tag</summary>二分</details>
- spendtime: 50
- ACtime: 2022.8.11
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>F - Main Street</summary>

- <details><summary>comment</summary>就是按照自己脑海里的没错，先到达最近的主路线的四个点，再到达目标路线的最近的四个点，细节很多，比如当两个点的横向距离特别近，或者纵向距离特别近，估计是个考量细节编程的题目，没有多大建模思维的价值，懒得写了，就这样吧，偷懒打死</details>
- <details><summary>tag</summary>分析+模拟</details>
- spendtime: 120
- ACtime: 2022.8.11
- ACstatus: 不会
- score: 7
</details>

<details>
<summary>G - Triangle</summary>

- <details><summary>comment</summary>实在搞不懂什么时候1e8次方的算法可以执行，bitset优化下即可，别多想，很简单，放在这个位置也是了，吓唬人</details>
- <details><summary>tag</summary>bitset</details>
- spendtime: 30
- ACtime: 2022.8.12
- ACstatus: 完全会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 264</summary>
<details>
<summary>lrc</summary>

<details>
<summary>C - Matrix Reducing</summary>

- <details><summary>comment</summary>代码写挺长的，适合lc周赛的倒数第二题，写得比较久了</details>
- <details><summary>tag</summary>二进制枚举</details>
- spendtime: 30
- ACtime: 2022.8.13
- ACstatus: 完全会
- score: 5
</details>

<details>
<summary>E - Blackout 2</summary>

- <details><summary>comment</summary>写起来不复杂，并查集的题目都是语义十分清晰，表达虽然长，但一点也不乱，题目的核心就是将删边的过程，倒着就变成了连边的过程了，直接统计数目即可</details>
- <details><summary>tag</summary>并查集</details>
- spendtime: 22
- ACtime: 2022.8.11
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>F - Monochromatic Path</summary>

- <details><summary>comment</summary>dp这东西真的是，只有自己想一遍，想清楚转移过程，才能真正的清楚，之前想到了状态应该那么表示，但是没下手去敲，官方题解写得真的废物，这种dp也算是难度非常打的了，刚开始还没看到一个条件，就是只往右和下，简单来说对于一个块来说，总共用四种状态，第一种行列均不反转，第二种行反转，第三种列反转，第四种行列均反转，就可以了，转移过程一定需要结合题意，主要就是转移，想清楚其实也比较清晰，学到一招小技巧，因为是求同色的，则就存在黑白两种情况，一种写法是加个flag，让函数明白此时处理的是啥，重复代码量巨大无比，另一种是，反转数据，这样函数就可以写成唯一的，则这样节省很大的代码空间</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 240
- ACtime: 2022.8.10
- ACstatus: 看提示后会
- score: 7
</details>

<details>
<summary>G - String Fair</summary>

- <details><summary>comment</summary>重要的还是建模的过程，将xx抽象为一个节点，每个节点有26条边,形如ab->bc,边的值为val(c)+val(bc)+val(abc)，跑一遍spfa即可判断，另外学到的小技巧就是初始值，使用个“$$”代替，还是很不错的，有诸多的好处，感觉这个分数的题目，其实也不是很难，感觉也就正正常常，建模困难，不过也不是很智商,后来拿到测试用例之后，发现是先前的边权值超过int了，模版写的是没有问题</details>
- <details><summary>tag</summary>建模+spfa</details>
- spendtime: 30
- ACtime: 2022.8.12
- ACstatus: 完全会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 265</summary>
<details>
<summary>lrc</summary>

<details>
<summary>D - Iroha and Haiku (New ABC Edition)</summary>

- <details><summary>comment</summary>写三遍前缀和即可，满足返回，三段，是个中等题目</details>
- <details><summary>tag</summary>前缀和</details>
- spendtime: 15
- ACtime: 2022.8.21
- ACstatus: 完全会
- score: 5
</details>

<details>
<summary>E - Warp</summary>

- <details><summary>comment</summary>看到了有点手无举措，速度很慢，而且思路走偏了，dp[i][x][y]表示一共走i步，A走了x步，B走了y步，C走了i-x-y步，常用的状态压缩的办法，失败啊！，不是很难，但就是没写出来，唉</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 100
- ACtime: 2022.8.21
- ACstatus: 看提示后会
- score: 7
</details>

<details>
<summary>F - Manhattan Cafe</summary>

- <details><summary>comment</summary></details>
- <details><summary>tag</summary></details>
- spendtime: 150
- ACtime: 2022.8.22
- ACstatus: 不会
- score: 7
</details>

<details>
<summary>G - 012 Inversion</summary>

- <details><summary>comment</summary>终于把2400分的题目调出来了，给自己鼓个掌，这个主要是线段树的建模问题，一个线段维护0,1,2,01,02,12,21,20,10,的信息，特别注意一件事情，这种替换映射的写法要格外注意，注意懒标记的写法，今天debug了2个小时，终于debug出来了，我们仔细思考在一个经典的计算区间和中，add[p]+=v会有这么个操作，这是因为加法遵循交换律，但是对于替换这样的，或者映射，或者把一个值改为一个，并不可以遵循交换律，即必须有上一步是啥，再映射，比如add[i]=v不可以这么写，而应该add[i]=v[add[i]]需要结合上一步映射的结果，这个debug出来，太爽了</details>
- <details><summary>tag</summary>线段树建模</details>
- spendtime: 240
- ACtime: 2022.8.24
- ACstatus: 不会
- score: 8
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 266</summary>
<details>
<summary>lrc</summary>

<details>
<summary>C - Convex Quadrilateral</summary>

- <details><summary>comment</summary>写崩了一道题目，完全崩掉，记录一下</details>
- <details><summary>tag</summary>叉积</details>
- spendtime: 45
- ACtime: 2022.8.27
- ACstatus: 完全会
- score: 5
</details>

<details>
<summary>D - Snuke Panic (1D)</summary>

- <details><summary>comment</summary>很简单的dp，抽象也简单，转移也很容易，treemap存一下就行</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 20
- ACtime: 2022.8.27
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>E - Throwing the Die</summary>

- <details><summary>comment</summary>主要代码很简单，带有期望的dp有点让人摸不到头脑，正确理解题意是第一步，状态定义很直观，关键在于怎么转移，其实就是先继续求n-1次的期望，如果点数大于n-1次的期望就终止，不大于就按照n-1的算，统一除以6即可，还不错的题目，上次碰见了一次期望题目，理解起来好多了</details>
- <details><summary>tag</summary>期望dp</details>
- spendtime: 20
- ACtime: 2022.8.27
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>F - Well-defined Path Queries on a Namori</summary>

- <details><summary>comment</summary>直接找树中的环即可，使用dfs构建出树，然后再加一条边，任何经过环的路径都存在两条，所以，使用并查集维护连通分枝，环上的不连接，不是环的连接起来即可，确实是个这个分数的题目，只不过真的没有时间做了</details>
- <details><summary>tag</summary>基环树</details>
- spendtime: 50
- ACtime: 2022.8.30
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>G - Yet Another RGB Sequence</summary>

- <details><summary>comment</summary>先将rb抽象成一个k，则重新变成r(r-k)，g(g-k),b,k,先随意排列r,b,k,再考虑插入g，不能插入在r的右边即可，则相当于将g个球放入到b+k+1个空箱子里面，允许有空箱子的方案数，如果写成通用的形式就是，n个球放入k个盒子，允许为空，则C(n+k-1,k-1)为方案数，所以题目就顺利解决了</details>
- <details><summary>tag</summary>组合数学</details>
- spendtime: 60
- ACtime: 2022.9.19
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 267</summary>
<details>
<summary>lrc</summary>

<details>
<summary>C - Index × A(Continuous ver.)</summary>

- <details><summary>comment</summary>逐步滑动即可</details>
- <details><summary>tag</summary>前缀和</details>
- spendtime: 20
- ACtime: 2022.9.3
- ACstatus: 完全会
- score: 5
</details>

<details>
<summary>D - Index × A(Not Continuous ver.)</summary>

- <details><summary>comment</summary>直接dp，比上一道更加不需要动脑子</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 20
- ACtime: 2022.9.3
- ACstatus: 完全会
- score: 5
</details>

<details>
<summary>E - Erasing Vertices 2</summary>

- <details><summary>comment</summary>读懂题目，二分最大值，看是否可以讲图上的点全部拿完即可</details>
- <details><summary>tag</summary>bfs+二分</details>
- spendtime: 35
- ACtime: 2022.9.3
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>F - Exactly K Steps</summary>

- <details><summary>comment</summary>主要还是结论记少了，这题有关树的直径，树中任意一点到直径的距离一定是最大的，又推论出树的距离k的祖先问题，如果是交互式查询的话，是nlogn处理，如果是提前就知道需要哪一个，就可以o（n），即在树dfs遍历的过程中处理，因为确实常数比较大，nlogn超时了，我老老实实写成O（n）的了</details>
- <details><summary>tag</summary>脑筋急转弯</details>
- spendtime: 240
- ACtime: 2022.9.22
- ACstatus: 不会
- score: 7
</details>

<details>
<summary>G - Increasing K Times</summary>

- <details><summary>comment</summary>一道红分题目，但是感觉这样的dp并不是多么困难，原本的模型是欧拉数，指在n个数列中有m个上升的方案总数，这道题目的状态定义与之一摸一样，不一样的在于它多了条件，允许重复，则每次需要先计算same，再去算真正的值，每次考虑插入的位置，插入在x>ai>=ai+1的位置会增1，插入ai< ai+1的和same的不变，则转移方程则很好写出</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 120
- ACtime: 2022.9.23
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 268</summary>
<details>
<summary>lrc</summary>

<details>
<summary>C - Chinese Restaurant</summary>

- <details><summary>comment</summary>统计计数即可</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 22
- ACtime: 2022.9.10
- ACstatus: 完全会
- score: 5
</details>

<details>
<summary>D - Unique Username</summary>

- <details><summary>comment</summary>题目读了半天，还是比较麻烦的，排好之后搜索就行了，还可以吧，不是很困难，构思如何写废了些时间</details>
- <details><summary>tag</summary>搜索</details>
- spendtime: 35
- ACtime: 2022.9.10
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>E - Chinese Restaurant (Three-Star Version)</summary>

- <details><summary>comment</summary>不知道如何更简单的写了，其实就是线性函数的叠加，非常的麻烦，这题目，真的，毒瘤一般的题目，写的我真的，好久没写这种毒瘤一般的模拟，非常烦，对于一个线性叠加的函数来说，比如一段区间加上x函数，那么就是初始是0，每次都加1，保留前缀和即可，挺不好写的，算是毒瘤题目</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 240
- ACtime: 2022.9.30
- ACstatus: 完全会但自己的解决方案不够好
- score: 7
</details>

<details>
<summary>F - Best Concatenation</summary>

- <details><summary>comment</summary>就是没有正确贪心，非常无奈，贪心的依据是这样，假设有一个排列，交换两项使得比原始值更大，所以统计每个字符串的x个数，和所有的位数的值之和y（此处依赖于题意）,依据y/x的值，从小到大，不等式很容易得出，不过不知道为啥有runtime error，之后有了测试用例再看一下,解决了，主要是因为在写比较器的时候，千万不能忽虑了要写返回0，要不然在比较的时候，可能会出现，不一致的情况，最好的解决方案是，直接用基本类型的compare函数即可</details>
- <details><summary>tag</summary>贪心</details>
- spendtime: 60
- ACtime: 2022.9.12
- ACstatus: 看提示后会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 269</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E - Last Rook</summary>

- <details><summary>comment</summary>对行列分别二分，因为写法的问题，所以写崩了，而且不好debug，换了个写法秒过</details>
- <details><summary>tag</summary>二分搜索</details>
- spendtime: 80
- ACtime: 2022.9.17
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 270</summary>
<details>
<summary>lrc</summary>

<details>
<summary>D - Stones</summary>

- <details><summary>comment</summary>初次也是死在了读题上，不过最终有惊无险，博弈类dp，而不是直观的贪心</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 31
- ACtime: 2022.12.29
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>E - Apple Baskets on Circle</summary>

- <details><summary>comment</summary>二分的挺直观的，没啥太好说的，直接过掉</details>
- <details><summary>tag</summary>二分</details>
- spendtime: 19
- ACtime: 2022.12.29
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>F - Transportation</summary>

- <details><summary>comment</summary>一道题整了一天，所谓还真的不错的题目，首先写了一版乱贪心版本，非常难debug，后来反映过来，应该不是正解，重新思考建模，后知后觉，然后想到了，需要引入虚拟源点，而且是两个，因为既有机场，又有码头，最让我觉得升华的点在于，接下来的骚操作，最优解一定存在于这四种组合，只用路连接，用路和机场，用路和港口，用路和机场和港口，这就是今日最佳，一直在思考一件事，细致去想，对于超级源点，每次至少也要连>1,我想的办法是统计+撤回，但上述方法更加优雅，非常厉害，被上了一课，看来建模功夫还得多看啊</details>
- <details><summary>tag</summary>建模+最小生成树</details>
- spendtime: 一天
- ACtime: 2022.12.29
- ACstatus: 完全会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 271</summary>
<details>
<summary>lrc</summary>

<details>
<summary>C - Manga</summary>

- <details><summary>comment</summary>不知道为啥，一个C写了我31分钟，中间还错了好几次，读题，注意，即使第三题坑也很大啊</details>
- <details><summary>tag</summary>模拟思维</details>
- spendtime: 31
- ACtime: 2022.12.30
- ACstatus: 完全会
- score: 5
</details>

<details>
<summary>D - Flip and Adjust</summary>

- <details><summary>comment</summary>一个dp回溯，反正这种题目就是码农，没啥的，复杂度都很直观</details>
- <details><summary>tag</summary>dp回溯</details>
- spendtime: 17
- ACtime: 2022.12.30
- ACstatus: 完全会
- score: 5
</details>

<details>
<summary>E - Subsequence Path</summary>

- <details><summary>comment</summary>读完题，题目还是比较冗长，但是我们能品出很关键的，如何保证最短且是子序列，这点值得思考，正向去思考，很容易发现，后面有可能有更优解，所以，倒叙来不断更新即可，思路出的比较快</details>
- <details><summary>tag</summary>倒叙</details>
- spendtime: 29
- ACtime: 2022.12.30
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>F - XOR on Grid Path</summary>

- <details><summary>comment</summary>一个很传统的集合dp，在于如何更好的优化，第一次从前向后，复杂度直接爆掉，如此考虑，将复杂控制在中间，从(0,0)点和(n,n)点双向探索，在中间交汇即可，能想到这点我还是可以的</details>
- <details><summary>tag</summary>折半搜索</details>
- spendtime: 80
- ACtime: 2022.12.30
- ACstatus: 完全会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 272</summary>
<details>
<summary>lrc</summary>

<details>
<summary>C - Max Even</summary>

- <details><summary>comment</summary>分为奇偶模拟吧</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 8
- ACtime: 2023.1.10
- ACstatus: 完全会
- score: 4
</details>

<details>
<summary>D - Root M Leaper</summary>

- <details><summary>comment</summary>讲究的地方就在于如何灵活变换方向，其他的没啥，从直觉上来说，平方和相加等于一个数的不会很多解，大胆写</details>
- <details><summary>tag</summary>bfs搜索</details>
- spendtime: 15
- ACtime: 2023.1.10
- ACstatus: 完全会
- score: 5
</details>

<details>
<summary>E - Add and Mex</summary>

- <details><summary>comment</summary>乱搞出来的，这个题作为一个笔试的最后一道题目还是非常不错的，看了看题解之后，觉得自己或许稍微写的有点复杂，总体是这样子的，其实也就是把0-m之间的数存储下来，总共不会超过nlogn个，调和级数决定的，想了想，确实可以写简单点，吃一堑长一智了，还是非常不错的题目的</details>
- <details><summary>tag</summary>乱搞</details>
- spendtime: 50
- ACtime: 2023.1.10
- ACstatus: 完全会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>AtCoder Beginner Contest 273</summary>
<details>
<summary>lrc</summary>

<details>
<summary>D - LRUD Instructions</summary>

- <details><summary>comment</summary>以为能够秒的一道题目，竟然错了很多次，错麻我了，下次还是要好好的读题，不可以再这么随意下去</details>
- <details><summary>tag</summary>平衡树板子</details>
- spendtime: 40
- ACtime: 2023.1.13
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>E - Notebook</summary>

- <details><summary>comment</summary>写了个大模拟，真的吐了，从第一天写到第二天，真的不好写，吐了吐了，就是死命的想要磕出来我想要的效果，当然愿望达到了，时间也被浪费了，中间踩了巨多的坑，而且为了模拟回退过程，辅助了很多的操作，比如undo操作，又比如记录save和load的treeset，代码相当的长，中间是思考过使用树去做这道题目，但是很无奈，最终还是选择了大模拟，建模使用树的方式还是要更快捷一些，而且代码优雅，嗨，失败</details>
- <details><summary>tag</summary>模拟or树</details>
- spendtime: 300
- ACtime: 2023.1.14
- ACstatus: 完全会 正解更好
- score: 7
</details>

<details>
<summary>F - Two Strings</summary>

- <details><summary>comment</summary>干掉了一道atc2200的题目，只能说太牛了，这种题目遇到的太少，以至于建模是一点也不会</details>
- <details><summary>tag</summary>后缀数组</details>
- spendtime: ???
- ACtime: 2023.1.19
- ACstatus: 不会
- score: 8
</details>
</details>
</details>
</details>