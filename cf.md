<details>
<summary>Educational Codeforces Round 144 (Rated for Div. 2)</summary>
<details>
<summary>lrc</summary>

<details>
<summary>A. Typical Interview Problem</summary>

- <details><summary>comment</summary>交替比较一下即可</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 9
- ACtime: 2023.2.28
- ACstatus: 完全会
- score: 4
</details>

<details>
<summary>B. Asterisk-Minor Template</summary>

- <details><summary>comment</summary>关键在于只要有两个相等的字符即可</details>
- <details><summary>tag</summary>分类讨论</details>
- spendtime: 32
- ACtime: 2023.2.28
- ACstatus: 完全会
- score: 5
</details>

<details>
<summary>C. Maximum Set</summary>

- <details><summary>comment</summary>思考如何最长且满足要求，可以这么考虑，最长一定是乘2，比如1 2 4 8这种序列，其中倍数关系都是2 2 2 ,但其实是可以中间乘3的，比如2 2 3，然后3的位置可以随便换，则就解了这道题目，之所以赛时没有做出来，还是因为太困了，脑子太迷乱了，其实应该是能拿下的，刚开始写不小心犯了很多小错误，后来脑子清醒的时候，很快就过去了</details>
- <details><summary>tag</summary>思维</details>
- spendtime: ？？
- ACtime: 2023.3.5
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>814 div2</summary>
<details>
<summary>lrc</summary>

<details>
<summary>A. Chip Game</summary>

- <details><summary>comment</summary>大胆猜，行列为偶数先手，奇数后手</details>
- <details><summary>tag</summary>猜结论</details>
- spendtime: 10
- ACtime: 2022.8.16
- ACstatus: 完全会
- score: 4
</details>

<details>
<summary>B. Mathematical Circus</summary>

- <details><summary>comment</summary>对各种数就0，1，2，3四种情况，讨论下即可</details>
- <details><summary>tag</summary>分析+模拟</details>
- spendtime: 40
- ACtime: 2022.8.16
- ACstatus: 完全会
- score: 5
</details>

<details>
<summary>C. Fighting Tournament</summary>

- <details><summary>comment</summary>最大的一定会被挪到前面，模拟一遍，记录一下，二分结果即可</details>
- <details><summary>tag</summary>模拟+二分</details>
- spendtime: 35
- ACtime: 2022.8.17
- ACstatus: 完全会
- score: 5
</details>

<details>
<summary>D1. Burenka and Traditions (easy version)</summary>

- <details><summary>comment</summary>代价没看出来，其实说的也就是，有两种操作，能操作1或2长度，最少需要多少操作，dp[i][j]表示对于前i个且结尾是j的最少操作数，转移即可，有点可惜且愚蠢</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 180
- ACtime: 2022.8.18
- ACstatus: 看提示后会
- score: 6
</details>

<details>
<summary>D2. Burenka and Traditions (hard version)</summary>

- <details><summary>comment</summary>在上一个问题上，把n和ai变大了，这个贪心太困难了，首先，对于我来说，如何想到这种贪心，第二，想到这种贪心如何让自己觉得靠谱，使用set记录异或前缀和，找出不相交的异或值为零的线段，每找出一个，相比之前的一个线段的值就-1，还不错，这种贪心蛮好活的</details>
- <details><summary>tag</summary>贪心</details>
- spendtime: 50
- ACtime: 2022.8.18
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>815 div2</summary>
<details>
<summary>lrc</summary>

<details>
<summary>A — Burenka Plays with Fractions</summary>

- <details><summary>comment</summary>写崩了一道题目，思路直接飞了，可能太困了吧</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 50
- ACtime: 2022.8.18
- ACstatus: 完全会
- score: 5
</details>

<details>
<summary>B — Interesting Sum</summary>

- <details><summary>comment</summary>心态写崩了，大崩，大崩，简直！就说为啥别人三分钟，我想这么长时间，大崩，大崩，直接简单思考，假设最大两个数为a,b,最小两个数为c,d,那么答案为a+b-c-d，为什么？排列组合一下，发现总能取到，我只能说，我死板了，还想着多么复杂的方法，失败，大大的失败</details>
- <details><summary>tag</summary>脑筋急转弯</details>
- spendtime: 120
- ACtime: 2022.8.19
- ACstatus: 不会
- score: 5
</details>

<details>
<summary>C. Corners</summary>

- <details><summary>comment</summary>细心观察一下就知道，扫描全图，对于一个L形来说，如果含有的1的数目小于1，则一定可以最大化是cnt(表示1的个数)，如果一个零不含，则cnt-2,只含有一个0，则cnt-1</details>
- <details><summary>tag</summary>观察</details>
- spendtime: 30
- ACtime: 2022.8.19
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>D1. Xor-Subsequence (easy version)</summary>

- <details><summary>comment</summary>dp的难点无外乎来自几个方面，状态难抽象，转移不好想，转移过程太多需要优化，这道题目入手就是让你以为转移很多，实际上只有256个至多，因为要保证出了高9位以上的一样，才能满足异或的条件，则直接转移即可，还不错，可以发现这点真的还不错，是一道很好的dp题目，吓死人</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 60
- ACtime: 2022.8.19
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>D2. Xor-Subsequence (hard version)</summary>

- <details><summary>comment</summary></details>
- <details><summary>tag</summary></details>
- spendtime: 50
- ACtime: 2022.8.18
- ACstatus: 不会
- score: 7
</details>

<details>
<summary>E. Misha and Paintings</summary>

- <details><summary>comment</summary></details>
- <details><summary>tag</summary></details>
- spendtime: 50
- ACtime: 2022.8.18
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>135 div2(Educational)</summary>
<details>
<summary>lrc</summary>

<details>
<summary>A. Colored Balls: Revisited</summary>

- <details><summary>comment</summary>直接模拟一下即可，对对碰</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 15
- ACtime: 2022.9.8
- ACstatus: 完全会
- score: 3
</details>

<details>
<summary>B. Best Permutation</summary>

- <details><summary>comment</summary>构造，就是找个规律即可，比较简单，大胆猜结论即可</details>
- <details><summary>tag</summary>脑筋急转弯</details>
- spendtime: 16
- ACtime: 2022.9.8
- ACstatus: 完全会
- score: 5
</details>

<details>
<summary>C. Digital Logarithm</summary>

- <details><summary>comment</summary>相等的先剔除，再将所有大于10的做一次变换，则分别得到9的数组，再将相等的剔除，再做一次变换即可，纯纯的感觉是对的</details>
- <details><summary>tag</summary>贪心</details>
- spendtime: 32
- ACtime: 2022.9.8
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>D. Letter Picking</summary>

- <details><summary>comment</summary>博弈论贪心问题，不需要想，区间dp即可</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 43
- ACtime: 2022.9.8
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>821 div2</summary>
<details>
<summary>lrc</summary>

<details>
<summary>A. Consecutive Sum</summary>

- <details><summary>comment</summary>分组模拟即可</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 5
- ACtime: 2022.9.19
- ACstatus: 完全会
- score: 4
</details>

<details>
<summary>B. Rule of League</summary>

- <details><summary>comment</summary>只可能0和x，且n-1需要整除x，随后模拟下即可</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 20
- ACtime: 2022.9.19
- ACstatus: 完全会
- score: 5
</details>

<details>
<summary>C. Parity Shuffle Sorting</summary>

- <details><summary>comment</summary>考虑首位即可，讨论四种情况，奇奇，奇偶，偶奇，偶偶，即可</details>
- <details><summary>tag</summary>脑筋急转弯</details>
- spendtime: 60
- ACtime: 2022.9.19
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>D1. Zero-One (Easy Version)</summary>

- <details><summary>comment</summary>没看清题目，直接做爆了，有点惨，题目有个重要的条件就是,x>=y，事情就简单很多了，如果相邻，则x和2*y的最小，不相邻，直接全用y，刚开始我用dp，但是写崩了</details>
- <details><summary>tag</summary>贪心</details>
- spendtime: 300
- ACtime: 2022.9.19
- ACstatus: 不会
- score: 6
</details>

<details>
<summary>D2. Zero-One (Hard Version)</summary>

- <details><summary>comment</summary>dp[l][r]表示这个区间内翻转好的最小值，记忆化搜索，转移，要么从dp[l+1][r-1],dp[l+2][r],dp[l][r-2],转移而来，对于r和l的反转，要么用相邻的不断迭过去，要么使用y，太失败了</details>
- <details><summary>tag</summary>区间dp</details>
- spendtime: 25
- ACtime: 2022.9.19
- ACstatus: 不会
- score: 6
</details>
</details>
</details>
</details>