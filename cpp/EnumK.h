#ifndef ENUMK_H
#define ENUMK_H

#include <vector>
using std::vector;

class EnumK
{
    vector<int> enumK(int k, int n)
    {
        vector<int> res;
        int comb = (1 << k) - 1;
        while (comb < 1 << n)
        {
            res.push_back(comb);
            int x = comb & -comb, y = comb + x;
            comb = ((comb & ~y) / x >> 1) | y;
        }
        return res;
    }
};

#endif