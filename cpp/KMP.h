#ifndef KMP_H
#define KMP_H
#include <string>

const int MAXM = 10000;

int nextTable[MAXM];

using std::string;

class KMP
{
public:
    void getNextTable(string pattern);
    int kmp(string, string);
};

void KMP::getNextTable(string pattern)
{
    int m = pattern.size();
    int j = 0;
    nextTable[j] = -1;
    int i = nextTable[j];
    while (j < m)
    {
        if (i == -1 || pattern[j] == pattern[i])
        {
            i++;
            j++;
            nextTable[j] = i;
        }
        else
        {
            i = nextTable[i];
        }
    }
}

int KMP::kmp(string text, string pattern)
{
    getNextTable(pattern);
    int n = text.size();
    int m = pattern.size();
    int i = 0;
    int j = 0;
    int number = 0;
    while (i < n)
    {
        if (j == -1 || text[i] == pattern[j])
        {
            i++;
            j++;
        }
        else
        {
            j = nextTable[j];
        }
        if (j == m)
        {
            number++;
            j = nextTable[j];
        }
    }
    return number;
}
#endif
