vector<int> graph[200000];
int d[200000];
int f[200000][31];
void bfs() {
    deque<int> q;
    q.push_back(1);
    d[1] = 1;
    while (q.size() != 0) {
        int temp = q.front();
        q.pop_front();
        for (auto item : graph[temp]) {
            if (d[item]) continue;
            d[item] = d[temp] + 1;
            f[item][0] = temp;
            for (int j = 1; j <= 28; j++) {
                f[item][j] = f[f[item][j - 1]][j - 1];
            }
            q.push_back(item);
        }
    }
}
int lca(int x, int y) {
    if (d[x] > d[y]) std::swap(x, y);
    for (int i = 28; i >= 0; i--) {
        if (d[f[y][i]] >= d[x]) y = f[y][i];
    }
    if (x == y) return x;
    for (int i = 28; i >= 0; i--) {
        if (f[x][i] != f[y][i]) {
            x = f[x][i];
            y = f[y][i];
        }
    }
    return f[x][0];
}