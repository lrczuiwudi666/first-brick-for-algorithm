#ifndef LENGTH_OF_LNDS_H
#define LENGTH_OF_LNDS_H
#include <vector>
using std::vector;
class LengthofLNDS
{
public:
    int lengthofLNDS(vector<int> &);
};

int LengthofLNDS::lengthofLNDS(vector<int> &nums)
{
    int len = 1, n = nums.size();
    if (n == 0)
    {
        return 0;
    }
    vector<int> d(n + 1);
    d[len] = nums[0];
    for (int i = 1; i < n; i++)
    {
        if (nums[i] >= d[len])
        {
            d[++len] = nums[i];
        }
        else
        {
            int l = 1, r = len, pos = 0;
            while (l <= r)
            {
                int mid = (l + r) >> 1;
                if (d[mid] <= nums[i])
                {
                    pos = mid;
                    l = mid + 1;
                }
                else
                {
                    r = mid - 1;
                }
            }
            d[pos + 1] = nums[i];
        }
    }
    return len;
}
#endif