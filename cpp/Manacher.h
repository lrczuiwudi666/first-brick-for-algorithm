int L[3000001];
string mid = "";
void make(string &s)
{
    mid += ("$#");
    for (int i = 0; i < s.length(); i++)
    {
        mid += (s[i]);
        mid += ('#');
    }
    mid += ('!');
    L[1] = 1;
    int k = 0, r = 0;
    for (int i = 2; i < mid.length() - 1; i++)
    {
        if (i < r)
        {
            L[i] = min(r - i, L[2 * k - i]);
        }
        else
        {
            L[i] = 1;
        }
        while (mid[i - L[i]] == mid[i + L[i]])
        {
            L[i]++;
        }
        if (i + L[i] > r)
        {
            r = i + L[i];
            k = i;
        }
    }
}