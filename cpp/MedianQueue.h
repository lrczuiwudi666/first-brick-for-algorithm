#ifndef MEIDIAN_QUEUE_H
#define MEIDIAN_QUEUE_H
#include <vector>
#include <queue>
#include <string>
using std::greater;
using std::less;
using std::priority_queue;
using std::vector;
using std::string;
class MedianQueue {
public:
    priority_queue<int, vector<int>, less<int>> front;
    priority_queue<int, vector<int>, greater<int>> rear;
    long long sumPre = 0, sumLast = 0;
    void add(int);
    int get() { return front.top(); }
    int getFrontSize() { return front.size(); }
    int getRearSize() { return rear.size(); }
    long long getPreSum() { return sumPre; }
    long long getLastSum() { return sumLast; }
    string toString();
};

void MedianQueue::add(int x) {
    if (front.size() == 0) {
        front.push(x);
        sumPre += x;
        return;
    }
    if (front.size() == rear.size()) {
        if (rear.top() <= x) {
            int ch = rear.top();
            rear.pop();
            sumLast -= ch;
            sumPre += ch;
            rear.push(x);
            front.push(ch);
        }
        else {
            sumPre += x;
            front.push(x);
        }
    }
    else {
        if (x > front.top()) {
            sumLast += x;
            rear.push(x);
        }
        else {
            int ch = front.top();
            front.pop();
            sumPre -= ch;
            sumPre += x;
            sumLast += ch;
            front.push(x);
            rear.push(ch);
        }
    }
}
#endif