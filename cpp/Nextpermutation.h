#ifndef NEXT_PERMUTATION_H
#define NEXT_PERMUTATION_H

#include <vector>
using std::vector;

class NextPermutation
{
public:
    void nextPermutation(vector<int> &);
    void lastPermutation(vector<int> &);

private:
    void reverse(vector<int> &, int);
    void swap(vector<int> &, int, int);
};

void NextPermutation::nextPermutation(vector<int> &nums)
{
    int i = nums.size() - 2;
    while (i >= 0 && nums[i + 1] <= nums[i])
    {
        i--;
    }
    if (i >= 0)
    {
        int j = nums.size() - 1;
        while (j >= 0 && nums[j] <= nums[i])
        {
            j--;
        }
        swap(nums, i, j);
    }
    reverse(nums, i + 1);
}

void NextPermutation::lastPermutation(vector<int> &nums)
{
    int index = 0;
    for (int i = 1; i < nums.size(); i++)
    {
        if (nums[i] < nums[i - 1])
        {
            index = i;
        }
    }
    if (index != 0)
    {
        int j = index;
        for (int i = index; i < nums.size(); i++)
        {
            if (nums[i] < nums[index - 1])
            {
                j = i;
            }
            else
                break;
        }
        swap(nums, j, index - 1);
        reverse(nums, index);
    }
    else
    {
        reverse(nums, 0);
    }
}

void NextPermutation::reverse(vector<int> &nums, int start)
{
    int i = start, j = nums.size() - 1;
    while (i < j)
    {
        swap(nums, i, j);
        i++;
        j--;
    }
}

void NextPermutation::swap(vector<int> &nums, int i, int j)
{
    int temp = nums[i];
    nums[i] = nums[j];
    nums[j] = temp;
}

#endif