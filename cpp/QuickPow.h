// https://leetcode-cn.com/problems/power-of-two/
// 快速幂没找到合适的题目，可以用这个去测试一下。
#ifndef QUICK_POW_H
#define QUICK_POW_H

class quickPow
{
public:
    int pow(int, int);
    int powWithMod(long long, long long);

private:
    int mod = 1e9 + 7;
};

int quickPow::pow(int n, int m)
{
    int res = 1;
    long long base = n;
    while (m != 0)
    {
        if ((m & 1) == 1)
        {
            res = res * base;
        }
        base = base * base;
        m = m >> 1;
    }
    return res;
}

int quickPow::powWithMod(long long n, long long m)
{
    long long res = 1;
    long long base = n % mod;
    while (m != 0)
    {
        if ((m & 1) == 1)
        {
            res = (res * base) % mod;
        }
        base = (base * base) % mod;
        m = m >> 1;
    }
    return (int)res;
}

#endif