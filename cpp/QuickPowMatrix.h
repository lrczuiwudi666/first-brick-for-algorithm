/**
 * 矩阵快速幂算法描述：适用于在动态规划当中，符合某种迭代的矩阵的时候，并且需要反复乘的情况，所以
 * 可以考虑使用矩阵快速结算，比如status数组，经过(A^n)*status(其中*表示矩阵的乘积),其中n很大的时候
 * 可以将n分解成2进制，这样，可以经过预处理完成，快速乘积，类似快速幂算法
 * @author lrc
 *
 */
class QuickPowMatrix {
    public:
        long long mod = 1e9 + 7;
        vector<vector<long long>> mult(vector<vector<long long>> &x, vector<vector<long long>> &y) {
            vector<vector<long long>> res(x.size());
            for (int i = 0; i < x.size(); i++) {
                res[i].resize(y[0].size(), 0);
            }
            for (int i = 0; i < res.size(); i++) {
                for (int j = 0; j < res[0].size(); j++) {
                    for (int j2 = 0; j2 < x[0].size(); j2++) {
                        res[i][j] += (x[i][j2] * y[j2][j]) % mod;
                        res[i][j] %= mod;
                    }
                }
            }
            return res;
        }
        vector<vector<long long>> mark(int len) {
            vector<vector<long long>> res(len);
            for (int i = 0; i < res.size(); i++) {
                res[i].resize(len, 0);
            }
            for (int i = 0; i < res.size(); i++) {
                res[i][i] = 1;
            }
            return res;
        }
        vector<vector<long long>> powM[62];
        //不一定是这种初始化方式，也可以使用已经有的矩阵来初始化
        void init(int len, int c) {
            powM[0] = make(len, c);
            for (int j = 1; j < 62; j++) {
                powM[j] = mult(powM[j - 1], powM[j - 1]);
            }
        }
        vector<vector<long long>> powPre(vector<vector<long long>> &n, long long m) {
            auto res = mark(n.size());
            int index = 0;
            while (m != 0) {
                if ((m & 1) == 1) {
                    res = mult(res, powM[index]);
                }
                m = m >> 1;
                index++;
            }
            return res;
        }
        //需要给定一种生成重复矩阵的方法，对于每一个题来说都不一样，也就是基不一样
        vector<vector<long long>> make(int len, int c) {
            vector<vector<long long>> m = {
                {c - 1, c},
                {len - c, len - 1 - c}
            };
            return m;
        }
};
