// https://leetcode-cn.com/problems/distinct-echo-substrings/
#ifndef STRING_HASH_H
#define STRING_HASH_H

#include <vector>
#include <string>

using std::string;
using std::vector;

class StringHash
{
public:
    vector<unsigned long long> make(string &);
    vector<unsigned long long> make(vector<int> &);
    int cal(int, int, vector<unsigned long long> &);

private:
    int powWithMod(int, int);
};

vector<unsigned long long> StringHash::make(string &pre)
{
    vector<unsigned long long> fix(pre.size() + 1);
    unsigned long long h = 0;
    for (int i = 0; i < pre.size(); i++)
    {
        h = 31 * h + pre[i];
        fix[i + 1] = h;
    }
    return fix;
}

vector<unsigned long long> StringHash::make(vector<int> &pre)
{
    vector<unsigned long long> fix(pre.size() + 1);
    unsigned long long h = 0;
    for (int i = 0; i < pre.size(); i++)
    {
        h = 31 * h + pre[i];
        fix[i + 1] = h;
    }
    return fix;
}

int StringHash::cal(int begin, int end, vector<unsigned long long> &fix)
{
    return fix[end + 1] - fix[begin] * powWithMod(31, end - begin + 1);
}

int StringHash::powWithMod(int n, int m)
{
    int res = 1;
    unsigned long long base = n;
    while (m != 0)
    {
        if ((m & 1) == 1)
        {
            res = (res * base);
        }
        base = (base * base);
        m = m >> 1;
    }
    return res;
}

#endif