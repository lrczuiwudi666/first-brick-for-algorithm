// https://leetcode-cn.com/problemset/all/?topicSlugs=union-find&page=1
#ifndef UF_H
#define UF_H
#include <iostream>
#include <exception>
#include <vector>
using std::out_of_range;
using std::vector;

class Uf
{
public:
    Uf(int);
    int find(int);
    int count() { return cnt; }
    int get_size(int x) {
        return size[find(x)];
    }
    void uni(int, int);
    // validata that p is a valid index
    void validate(int);

private:
    vector<int> parent;
    vector<int> rank; // rank[i]=rank of subtree rooted at i(never more than 31)
    vector<int> size;
    int cnt;          // number of components;
};

Uf::Uf(int n)
{
    if (n < 0)
        throw out_of_range("arr out of range!");
    cnt = n;
    parent.resize(n);
    rank.resize(n);
    size.resize(n);
    for (int i = 0; i < n; i++)
    {
        parent[i] = i;
        rank[i] = 0;
        size[i] = 1;
    }
}

int Uf::find(int p)
{
    validate(p);
    while (p != parent[p])
    {
        parent[p] = parent[parent[p]];
        p = parent[p];
    }
    return p;
}

void Uf::uni(int p, int q)
{
    int rootP = find(p);
    int rootQ = find(q);
    if (rootP == rootQ)
        return;
    if (rank[rootP] < rank[rootQ]) {
		parent[rootP] = rootQ;
		size[rootQ] += size[rootP];
    }
    else if (rank[rootP] > rank[rootQ]) {
		parent[rootQ] = rootP;
		size[rootP] += size[rootQ];
	}
    else {
		parent[rootQ] = rootP;
		size[rootP] += size[rootQ];
		rank[rootP]++;
	}
    cnt--;
}

void Uf::validate(int p)
{
    int n = parent.size();
    if (p < 0 || p >= n)
    {
        throw out_of_range("index out of range!");
    }
}

#endif