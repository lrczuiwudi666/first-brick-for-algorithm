// https://leetcode-cn.com/problems/range-sum-query-mutable/
#ifndef ARR_TREE_H
#define ARR_TREE_H

#include <vector>
using std::vector;

class ArrTree
{
public:
    ArrTree() = default;
    // 初始化arr为空
    ArrTree(int n) : arr(n + 1){};
    // nums为原始数组，arr为树状数组
    ArrTree(vector<int> &nums)
    {
        arr.resize(nums.size() + 1);
        for (int i = 1; i <= nums.size(); i++)
        {
            arr[i] = nums[i - 1];
            for (int j = i - 1; j > i - lowbit(i); j -= lowbit(j))
                arr[i] += arr[j];
        }
    }
    int lowbit(int i) { return i & (-i); }
    int query(int);
    void update(int, int);
    vector<int> arr;
};

int ArrTree::query(int x)
{
    int ans = 0;
    for (int i = x; i > 0; i -= lowbit(i))
    {
        ans += arr[i];
    }
    return ans;
}

void ArrTree::update(int x, int add)
{
    for (int i = x; i < arr.size(); i += lowbit(i))
    {
        arr[i] += add;
    }
}

#endif