#ifndef ARR_TREE_MAX_H
#define ARR_TREE_MAX_H
#include <vector>
using std::vector;

class ArrTreeMax
{
public:
    ArrTreeMax() = default;
    ArrTreeMax(int n) : arr(n + 1) {}
    ArrTreeMax(vector<int> &nums)
    {
        arr.resize(nums.size() + 1);
        for (int i = 1; i <= nums.size(); i++)
        {
            arr[i] = nums[i - 1];
            for (int j = i - 1; j > i - lowbit(i); j -= lowbit(j))
                arr[i] += arr[j];
        }
    }
    int lowbit(int i) { return i & (-i); }
    int query(int);
    void update(int, long long);
    vector<int> arr;
};

int ArrTreeMax::query(int x)
{
    long long ans = 0;
    for (int i = x; i > 0; i -= lowbit(i))
    {
        ans = std::max(ans, (long long)arr[i]);
    }
    return ans;
}

void ArrTreeMax::update(int x, long long add)
{
    for (int i = x; i < arr.size(); i += lowbit(i))
    {
        arr[i] = std::max((long long)arr[i], add);
    }
}

#endif