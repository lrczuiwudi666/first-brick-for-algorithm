#ifndef CMN_H
#define CMN_H

#include <vector>
using std::vector;

class Cmn
{
public:
    void init(int);
    void initWithMod(int);

private:
    vector<vector<int>> cmn;
    int mod = (int)(1e9 + 7);
    int powWithMod(long long, long long);
    // 快速幂求逆元，n>=k
    long long C(int, int);
};

void Cmn::init(int len)
{
    cmn = vector<vector<int>>(len + 1, vector<int>(len + 1));
    cmn[0][0] = 1;
    for (int i = 1; i < cmn.size(); i++)
    {
        cmn[i][0] = 1;
        for (int j = 1; j <= i; j++)
        {
            cmn[i][j] = (cmn[i - 1][j - 1] + cmn[i - 1][j]);
        }
    }
}

void Cmn::initWithMod(int len)
{
    cmn = vector<vector<int>>(len + 1, vector<int>(len + 1));
    cmn[0][0] = 1;
    for (int i = 1; i < cmn.size(); i++)
    {
        cmn[i][0] = 1;
        for (int j = 1; j <= i; j++)
        {
            cmn[i][j] = (cmn[i - 1][j - 1] + cmn[i - 1][j]) % mod;
        }
    }
}

int Cmn::powWithMod(long long n, long long m)
{
    long long res = 1;
    long long base = n % mod;
    while (m != 0)
    {
        if ((m & 1) == 1)
        {
            res = (res * base) % mod;
        }
        base = (base * base) % mod;
        m = m >> 1;
    }
    return (int)res;
}

long long Cmn::C(int n, int k)
{
    if (k == 0)
        return 1;
    int res = 1;
    for (int i = n; i >= n - k + 1; i--)
    {
        res = (int)(((long long)res * i) % mod);
    }
    int t = 1;
    for (int i = 2; i <= k; i++)
    {
        t = (int)(((long long)t * i) % mod);
    }
    t = powWithMod(t, mod - 2);
    return ((long long)res * t) % mod;
}

#endif