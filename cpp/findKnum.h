// https://leetcode-cn.com/problems/median-of-two-sorted-arrays/
#ifndef FIND_K_NUM_H
#define FIND_K_NUM_H

#include <vector>
using std::vector;
/**
 * 在两个有序数组中，历经log(n+m)的时间获得第k个数
 * 重点的划分思路是，比较对于一个组的k/2个数的大小
 * 如果a>=b那么b中的k/2个数一定不是k个最小的数
 * 子问题就可以缩小为a,和b剩余的数中找k-k/2个数
 */

class FindKNum
{
public:
    double getKthElement(const vector<int> &nums1, const vector<int> &nums2, int k);
};

double FindKNum::getKthElement(const vector<int> &nums1, const vector<int> &nums2, int k)
{
    /* 主要思路：要找到第 k (k>1) 小的元素，那么就取 pivot1 = nums1[k/2-1] 和 pivot2 = nums2[k/2-1] 进行比较
     * 这里的 "/" 表示整除
     * nums1 中小于等于 pivot1 的元素有 nums1[0 .. k/2-2] 共计 k/2-1 个
     * nums2 中小于等于 pivot2 的元素有 nums2[0 .. k/2-2] 共计 k/2-1 个
     * 取 pivot = min(pivot1, pivot2)，两个数组中小于等于 pivot 的元素共计不会超过 (k/2-1) + (k/2-1) <= k-2 个
     * 这样 pivot 本身最大也只能是第 k-1 小的元素
     * 如果 pivot = pivot1，那么 nums1[0 .. k/2-1] 都不可能是第 k 小的元素。把这些元素全部 "删除"，剩下的作为新的 nums1 数组
     * 如果 pivot = pivot2，那么 nums2[0 .. k/2-1] 都不可能是第 k 小的元素。把这些元素全部 "删除"，剩下的作为新的 nums2 数组
     * 由于我们 "删除" 了一些元素（这些元素都比第 k 小的元素要小），因此需要修改 k 的值，减去删除的数的个数
     */

    int m = nums1.size();
    int n = nums2.size();
    int index1 = 0, index2 = 0;

    while (true)
    {
        // 边界情况
        if (index1 == m)
        {
            return nums2[index2 + k - 1];
        }
        if (index2 == n)
        {
            return nums1[index1 + k - 1];
        }
        if (k == 1)
        {
            return std::min(nums1[index1], nums2[index2]);
        }

        // 正常情况
        int newIndex1 = std::min(index1 + k / 2 - 1, m - 1);
        int newIndex2 = std::min(index2 + k / 2 - 1, n - 1);
        int pivot1 = nums1[newIndex1];
        int pivot2 = nums2[newIndex2];
        if (pivot1 <= pivot2)
        {
            k -= newIndex1 - index1 + 1;
            index1 = newIndex1 + 1;
        }
        else
        {
            k -= newIndex2 - index2 + 1;
            index2 = newIndex2 + 1;
        }
    }
}

#endif