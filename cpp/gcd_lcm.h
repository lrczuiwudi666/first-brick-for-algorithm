#ifndef GCD_LCM_H
#define GCD_LCM_H

#include <vector>

using std::vector;

class GcdLcm
{
public:
    // 最大公约数
    long long gcd(long long a, long long b)
    {
        return b == 0 ? a : gcd(b, a % b);
    }
    // 最小公倍数
    long long lcm(long long a, long long b)
    {
        return a * b / gcd(a, b);
    }

    // 扩展欧几里得
    // ax[0]+bx[1]=gcd(a,b);
    // 递归版本
    long long exgcd(long long, long long, vector<long long> &);
    // 迭代版本
    long long exgcd(long long a, long long b, long long &x, long long &y);
};

long long GcdLcm::exgcd(long long a, long long b, vector<long long> &x)
{
    x.resize(2);
    if (b == 0)
    {
        x[0] = 1;
        x[1] = 0;
        return a;
    }
    long long d = exgcd(b, a % b, x);
    long long z = x[0];
    x[1] = z - x[1] * (a / b);
    return d;
}

long long GcdLcm::exgcd(long long a, long long b, long long &x, long long &y)
{
    if (a < b)
        return exgcd(b, a, y, x);
    long long m = 0, n = 1;
    x = 1;
    y = 0;
    while (b != 0)
    {
        long long d = a / b, t;
        t = m;
        m = x - d * t;
        x = t;
        t = n;
        n = y - d * t;
        y = t;
        t = a % b;
        a = b;
        b = t;
    }
    return a;
}

#endif