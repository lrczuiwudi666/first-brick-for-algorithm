#ifndef LONGEST_SUBARRAY_IN_RANGE_H
#define LONGEST_SUBARRAY_IN_RANGE_H
#include <list>
#include <vector>

using std::list;
using std::vector;

class LongestSubarrayInRange
{
    //在O(n)的时间内求出不超过range的长度下最大的连续子数组之和，是著名的单调队列算法
    /**
     *
     * @param range 连续数组长度最大值
     * @param fix 原始数组的前缀数组，其中在单调队列的算法中已经进行了转换
     * @return 在range范围内的最大值，其中对于任何一个数组来说，此函数单调
     */
public:
    int longestSubarrayInRange(int, vector<long long> &);
};

int LongestSubarrayInRange::longestSubarrayInRange(int range, vector<long long> &fix)
{
    list<int> dequeue;
    dequeue.push_back(0);
    int maxVal = INT_MIN;
    for (int i = 1; i < fix.size(); i++)
    {
        while (!dequeue.empty() && i - *dequeue.begin() > range)
            dequeue.pop_front();
        maxVal = (int)std::max((long long)maxVal, fix[i] - fix[*dequeue.begin()]);
        while (!dequeue.empty() && fix[*dequeue.rbegin()] >= fix[i])
            dequeue.pop_back();
        dequeue.push_back(i);
    }
    return maxVal;
}

#endif