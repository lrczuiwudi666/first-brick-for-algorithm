#ifndef SGEMENT_TREE_H
#define SGEMENT_TREE_H
#include <vector>
using std::vector;
class SgementTree {
public:
    vector<int> left, right, sum, data;
    SgementTree(vector<int> d, int len);
    void build(int p, int l, int r);
    void change(int, int, int);
    int ask(int, int, int);
};

SgementTree::SgementTree(vector<int> d, int len) {
    left.resize(len * 4);
    right.resize(len * 4);
    sum.resize(len * 4);
    data = d;
    this->build(1, 1, len);
}
void SgementTree::build(int p, int l, int r) {
    left[p] = l;
    right[p] = r;
    if (l == r) {
        sum[p] = data[l - 1];
        return;
    }
    int mid = (l + r) / 2;
    build(p * 2, l, mid);
    build(p * 2 + 1, mid + 1, r);
    sum[p] = sum[p * 2 + 1] + sum[p * 2];
}

void SgementTree::change(int p, int x, int v) {
    if (this->left[p] == this->right[p]) {
        data[left[p] - 1] = v;
        sum[p] = v;
        return;
    }
    int mid = (left[p] + right[p]) / 2;
    if (x <= mid) {
        change(p * 2, x, v);
    }
    else {
        change(p * 2 + 1, x, v);
    }
    sum[p] = sum[p * 2] + sum[p * 2 + 1];
}

int SgementTree::ask(int p, int l, int r) {
    if (l <= left[p] && right[p] <= r) {
        return sum[p];
    }
    int mid = (left[p] + right[p]) / 2;
    int res = 0;
    if (l <= mid) res += ask(p * 2, l, r);
    if (r > mid) res += ask(p * 2 + 1, l, r);
    return res;
}

#endif