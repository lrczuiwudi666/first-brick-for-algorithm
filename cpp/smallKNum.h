// https://leetcode-cn.com/problems/kth-smallest-product-of-two-sorted-arrays/
#ifndef SMALL_K_NUM_H
#define SMALL_K_NUM_H

#include <vector>
using std::vector;
class SmallKNum
{
    /**
     *  用于计算两个有序数组中，A【i】*B【j】的最小的第k个数
     * @param a 有序数组1，必须正数
     * @param b 有序数组2，必须正数
     * @param range 2分划分的边界
     * @return  <=range的个数
     */
    long long small(vector<int> a, vector<int> b, long long range)
    {
        long long c = 0;
        int j = b.size() - 1;
        for (int i = 0; i < a.size(); i++)
        {
            while (j >= 0 && (long long)a[i] * (long long)b[j] > range)
                j--;
            if (j >= 0)
            {
                if ((long long)a[i] * (long long)b[j] <= range)
                    c += j + 1;
            }
            else
                break;
        }
        return c;
    }
};

#endif