class Solution {
public:
	int get( vector<int> &a, vector<int> &b, vector<int> &c )
	{
		int	x	= b[0] - a[0];
		int	y	= b[1] - a[1];
		int	q	= c[0] - b[0];
		int	p	= c[1] - b[1];
		return(x * p - y * q);
	}


	vector<vector<int> > outerTrees( vector<vector<int> > &trees )
	{
		if ( trees.size() <= 3 )
			return(trees);
		int	n	= trees.size();
		int	mi	= 1e5;
		for ( auto item : trees )
		{
			mi = min( mi, item[0] );
		}
		int left = 0;
		for ( int i = 0; i < trees.size(); i++ )
		{
			if ( trees[i][0] == mi )
			{
				left = i;
			}
		}
		vector<vector<int> >	res;
		vector<bool>		mark( n, false );
		res.push_back( trees[left] );
		int p = left;
		mark[p] = true;
		do
		{
			int q = (p + 1) % n;
			for ( int i = 0; i < n; i++ )
			{
				if ( get( trees[p], trees[q], trees[i] ) < 0 )
				{
					q = i;
				}
			}
			/* cout << p << " " << q << endl; */
			for ( int i = 0; i < n; i++ )
			{
				if ( mark[i] || p == i || q == i )
				{
					continue;
				}
				if ( get( trees[p], trees[q], trees[i] ) == 0 )
				{
					res.push_back( trees[i] );
					mark[i] = true;
				}
			}
			if ( !mark[q] )
			{
				res.push_back( trees[q] );
				mark[q] = true;
			}
			p = q;
		}
		while ( p != left );
		return(res);
	}
};