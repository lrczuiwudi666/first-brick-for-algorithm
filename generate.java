import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
/**
 * 用于生成用户模版代码
 */
public class generate {
    public static void main(String[] args) throws Exception {
        String path = "java/src";
        File dir = new File(path);
        File[] files = dir.listFiles();
        File out = new File("java.json");
        FileWriter fileWritter = new FileWriter(out.getName(), true);
        fileWritter.write("{\n");
        for (var item : files) {
            var in = new BufferedReader(new InputStreamReader(new FileInputStream(item)));
            String pre = item.getName().substring(0, item.getName().length() - 5);
            StringBuffer mid = new StringBuffer();
            mid.append((char) 9);
            mid.append("\"");
            mid.append(pre);
            mid.append("\"");
            mid.append(": {\n");
            mid.append((char) 9);
            mid.append((char) 9);
            mid.append("\"prefix\": \"");
            mid.append(pre);
            mid.append("\",\n");
            mid.append((char) 9);
            mid.append((char) 9);
            mid.append("\"body\": [\n\"");
            String s = null;
            while ((s = in.readLine()) != null) {
                mid.append(s.replaceAll("\"", "\\\\" + "\""));
                mid.append("\\n");
            }
            mid.append("\"");
            mid.append((char) 9);
            mid.append((char) 9);
            mid.append("]\n");
            mid.append((char) 9);
            mid.append("}\n");
            // System.out.println(mid);
            fileWritter.write(mid.toString());
            fileWritter.flush();
        }
        fileWritter.close();
    }

}
