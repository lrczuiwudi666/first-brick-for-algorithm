public class BinaryDictionaryTree {
	public BinaryDictionaryTree left = null;
	public BinaryDictionaryTree right = null;
	public int sum = 1;

	// public boolean value;
	public BinaryDictionaryTree() {
	}

	/**
	 * @param v     要插入的数
	 * @param index 记录递归的层数，表示当前插入所在的层数；调用形式为ins(v, 0);
	 */
	public void ins(int v, int index) {
		if (index >= 31)
			return;
		if (((v >> (30 - index)) & 0x1) == 0x1) { // 如果v当前位置为1，则需要插入右子树；
			if (check(this.right)) {
				this.right.sum++;
				this.right.ins(v, index + 1);
			} else { // 如果v当前位置为0，则插入左子树；
				this.right = new BinaryDictionaryTree();
				this.right.ins(v, index + 1);
			}
		} else {
			if (check(this.left)) {
				this.left.sum++;
				this.left.ins(v, index + 1);
			} else {
				this.left = new BinaryDictionaryTree();
				this.left.ins(v, index + 1);
			}
		}
	}

	public void del(int v, int index) {
		if (index >= 31)
			return;
		if (((v >> (30 - index)) & 0x1) == 0x1) {
			this.right.sum--;
			if (!check(this.right)) {
				this.right = null;
				return;
			} else
				this.right.del(v, index + 1);
		} else {
			this.left.sum--;
			if (!check(this.left)) {
				this.left = null;
				return;
			} else
				this.left.del(v, index + 1);
		}
	}

	/**
	 * 对二叉字典树t进行合法性检验；如果当前节点为null或不为null但sum为0则不合法，其余情况合法。
	 */
	public boolean check(BinaryDictionaryTree t) {
		boolean flag = true;
		if (t != null && t.sum == 0) {
			flag = false;
		} else if (t == null) {
			flag = false;
		}
		return flag;
	}

	/**
	 * 求v与当前二叉树存储的int型整数进行异或所能取得的最大值对应的整数；
	 * 
	 * @param v     需要进行异或的数字
	 * @param index 记录递归的层数，表示当前进行比较的位数(符号位右边一位为0)
	 * 
	 */
	public int getnum(int v, int index) {
		if (index >= 31)
			return 0;

		if (((v >> (30 - index)) & 0x1) == 0x1) {
			if (check(this.left)) {
				return this.left.getnum(v, index + 1);
			} else {
				return (1 << (31 - index - 1)) + this.right.getnum(v, index + 1);
			}

		} else {
			if (check(this.right)) {
				return (1 << (31 - index - 1)) + this.right.getnum(v, index + 1);
			} else {
				return this.left.getnum(v, index + 1);
			}
		}
	}

	/*
	 * 当前字典树与v的异或值小于range的个数，范围查询
	 */
	public int getsmall(int v, int index, int range) {
		if (index >= 31)
			return 0;

		if ((((v >> (30 - index)) & 0x1) == 0x1) && (((range >> (30 - index)) & 0x1) == 0x1)) {
			return (check(this.right) ? this.right.sum : 0)
					+ (check(this.left) ? this.left.getsmall(v, index + 1, range) : 0);
		} else if ((((v >> (30 - index)) & 0x1) == 0x1) && (((range >> (30 - index)) & 0x1) != 0x1)) {
			return (check(this.right) ? this.right.getsmall(v, index + 1, range) : 0);
		} else if ((((v >> (30 - index)) & 0x1) != 0x1) && (((range >> (30 - index)) & 0x1) == 0x1)) {
			return (check(this.right) ? this.right.getsmall(v, index + 1, range) : 0)
					+ (check(this.left) ? this.left.sum : 0);
		} else {
			return (check(this.left) ? this.left.getsmall(v, index + 1, range) : 0);
		}
	}

	/*
	 * 当前字典树与v异或大于range的个数，范围查询
	 */
	public int gethigh(int v, int index, int range) {
		if (index >= 31)
			return 0;

		if ((((v >> (30 - index)) & 0x1) == 0x1) && (((range >> (30 - index)) & 0x1) == 0x1)) {
			return (check(this.left) ? this.left.gethigh(v, index + 1, range) : 0);
		} else if ((((v >> (30 - index)) & 0x1) == 0x1) && (((range >> (30 - index)) & 0x1) != 0x1)) {
			return (check(this.right) ? this.right.gethigh(v, index + 1, range) : 0)
					+ (check(this.left) ? this.left.sum : 0);
		} else if ((((v >> (30 - index)) & 0x1) != 0x1) && (((range >> (30 - index)) & 0x1) == 0x1)) {
			return (check(this.right) ? this.right.gethigh(v, index + 1, range) : 0);
		} else {
			return (check(this.left) ? this.left.gethigh(v, index + 1, range) : 0)
					+ (check(this.right) ? this.right.sum : 0);
		}
	}

	/**
	 * 按照从小到大的方式打印字典树存储的数
	 */
	public void show(int x) {
		if (check(this.left)) {
			System.out.println(x + "  left");
			this.left.show(x + 1);
		}
		if (check(this.right)) {
			System.out.println(x + "  right");
			this.right.show(x + 1);
		}
	}
}