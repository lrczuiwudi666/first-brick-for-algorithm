import java.util.ArrayList;
import java.util.Arrays;

public class BipartiteGraphMatch {
	 //寻找增广路的递归函数，从语义上来说，其实很难
     public boolean dfs(int index,ArrayList<Integer>[] group,boolean[] visit,int[] match) {
    	  for(int item:group[index]) {
    		   if(!visit[item]) {
    			   visit[item]=true;
    			   if(match[item]==-1||dfs(match[item], group, visit, match)) {
    				   match[item]=index;
    				   return true;
    			   }
    		   }
    	  }
    	  return false;
     }
     /**
      * 用于处理二分图中的最大匹配问题，假设权值都为1
      * @param group 输入邻接矩阵，是一个满足二分图基本性质的图结构
      * @param n 一半集合的值
      * @return 返回最大匹配个数
      */
     public int getmax(ArrayList<Integer>[] group,int n) {
    	  boolean[] visit=new boolean[group.length];
    	  int[] match=new int[group.length];
    	  Arrays.fill(match, -1);
    	  int ans=0;
    	  for (int i = 0; i < n; i++) {
			   Arrays.fill(visit, false);
			   if(dfs(i, group, visit, match)) ans++;
		  }
    	  return ans;
     }
     
}
