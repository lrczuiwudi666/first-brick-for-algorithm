public class BipartiteWeightGraphMathch {
	public int[][] w=null;
	public int[] la=null;
	public int[] lb=null;
	public boolean[] va=null;
	public boolean[] vb=null;
	public int[] match=null;//右部节点访问了哪一个左部节点
	public int[] upd=null;
    public BipartiteWeightGraphMathch(int[][] w) {
		this.w=w;
		this.la=new int[w.length];
		this.lb=new int[w.length];
		this.va=new boolean[w.length];
		this.vb=new boolean[w.length];
		this.match=new int[w.length];
		this.upd=new int[w.length];
		Arrays.fill(match, -1);
	}
	//寻找增广路的递归函数，从语义上来说，其实很难
    public boolean dfs(int index) {
   	    va[index]=true;
    	for (int i = 0; i <vb.length; i++) {
		if(!vb[i]) {
		    if(la[index]+lb[i]==w[index][i]) {
		         vb[i]=true;
		         if(match[i]==-1||dfs(match[i])) {
		        	 match[i]=index;
		        	 return true;
		         }
		    }else {
				upd[i]=Math.min(upd[i],la[index]+lb[i]-w[index][i]);
			}
		}
	}
   	  return false;
    }
    public int delta=Integer.MAX_VALUE;
    public int km() {
    	for (int i = 0; i < la.length; i++) {
			la[i]=Integer.MIN_VALUE;
			lb[i]=0;
			for (int j = 0; j < lb.length; j++) {
				la[i]=Math.max(la[i], w[i][j]);
			}
		}
    	for (int i = 0; i < la.length; i++) {
			boolean flag=true;
    		while(flag) {
				Arrays.fill(va, false);
				Arrays.fill(vb, false);
				Arrays.fill(upd, Integer.MAX_VALUE);
    			if(dfs(i)) {
    				flag=false;
    				continue;
    			}
    			for (int j = 0; j < vb.length; j++) {
					 if(!vb[j]) delta=Math.min(delta, upd[j]);
				}
    			for (int j = 0; j < la.length; j++) {
					if(va[j]) la[j]-=delta;
					if(vb[j]) lb[j]+=delta;
				}
			}
		}
    	int ans=0;
    	for (int i = 0; i < la.length; i++) {
			ans+=w[match[i]][i];
		}
    	return ans;
    }
}