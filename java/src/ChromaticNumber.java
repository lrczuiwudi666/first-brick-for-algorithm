/**
 * 解决图染色最优化问题，最少需要多少种颜色可以将图染色
 */
public class ChromaticNumber {
    /**
     * g是临界表的mask，比如(0,1),(0,2)表示的边集，对于邻接表来说就是，00000110
     * @param g
     * @return
     */
    int get(int[] g) {
        int n = g.length;
        if (n == 0)
            return 0;
        // randomly choose a large prime
        int modulo = 1077563119;
        int all = 1 << n;
        int[] ind = new int[all], s = new int[all];
        for (int i = 0; i < all; i++)
            s[i] = (((n - popcount(i)) & 1) > 0 ? -1 : 1);
        ind[0] = 1;
        for (int i = 1; i < all; i++) {
            int ctz = ctz(i);
            ind[i] = ind[i - (1 << ctz)] + ind[(i - (1 << ctz)) & ~g[ctz]];
            if (ind[i] >= modulo)
                ind[i] -= modulo;
        }
        // compute the chromatic number (= \sum (-1)^{n - |i|} * ind(i)^k)
        for (int k = 1; k < n; k++) {
            long sum = 0;
            for (int i = 0; i < all; i++) {
                long cur = ((s[i] * (long) ind[i]) % modulo);
                s[i] = (int) cur;
                sum += cur;
            }
            if (sum % modulo != 0)
                return k;
        }
        return n;
    }

    /**
     * 计算有多少个位是1
     * 
     * @param x 需要计算的mask
     * @return 个数
     */
    public int popcount(int x) {
        int res = 0;
        while (x > 0) {
            if ((x & 1) == 1) {
                res++;
            }
            x = (x >> 1);
        }
        return res;
    }

    public int ctz(int x) {
        for (int i = 0; i < 31; i++) {
            if ((x & (1 << i)) == (1 << i))
                return i;
        }
        return 32;
    }
}