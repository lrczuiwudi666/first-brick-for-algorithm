public class Cmn {
	private int[][] cmn = null;

	// 没有mod的
	public void init(int len) {
		this.cmn = new int[len + 1][len + 1];
		cmn[0][0] = 1;
		for (int i = 1; i < cmn.length; i++) {
			cmn[i][0] = 1;
			for (int j = 1; j <= i; j++) {
				cmn[i][j] = (cmn[i - 1][j - 1] + cmn[i - 1][j]);
			}
		}
	}

	// 有mod的
	private static int mod = (int) (1e9 + 7);

	public void initwithmod(int len) {
		this.cmn = new int[len + 1][len + 1];
		cmn[0][0] = 1;
		for (int i = 1; i < cmn.length; i++) {
			cmn[i][0] = 1;
			for (int j = 1; j <= i; j++) {
				cmn[i][j] = (cmn[i - 1][j - 1] + cmn[i - 1][j]) % mod;
			}
		}
	}

	private static int powWithmod(long n, long m) {
		long res = 1;
		long base = n % mod;
		while (m != 0) {
			if ((m & 1) == 1) {
				res = (res * base) % mod;
			}
			base = (base * base) % mod;
			m = m >> 1;
		}
		return (int) res;
	}

	/**
	 * 计算阶乘，res[0]为jc，res[1]为jc的逆元
	 * 
	 * @param len 需要产生的长度
	 * @return
	 */
	static long[][] get_jc_inv(int len) {
		long[][] res = new long[2][len];
		res[0][0] = 1;
		res[1][0] = 1;
		for (int i = 1; i < len; i++) {
			res[0][i] = (res[0][i - 1] * i) % mod;
			// 求其逆元
			res[1][i] = powWithmod(res[0][i], mod - 2);
		}
		return res;
	}

	// 快速幂逆元求法，n>=k
	static long C(long[] jc, long[] jc_inv, int n, int k) {
		if(k>n) return 0;
		if (k == 0)
			return 1;
		return (((jc[n] * jc_inv[k]) % mod) * jc_inv[n - k]) % mod;
	}

	
	/**
	 * 卢卡斯定理,求p较小的时候 
	 * @param n n>=m
	 * @param m 
	 * @param p 较小的素数
	 * @return
	 */
	static long lucas(long n,long m,long[][] jc){
		if(m==0) return 1;
		return (lucas(n/mod, m/mod, jc)*C(jc[0], jc[1], (int)(n%mod), (int)(m%mod)))%mod;
	}

}
