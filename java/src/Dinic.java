/**
 * 网络流模版，真实速度较快,一般可以处理，10^4-10^5级别的网络
 */
class Dinic {
    /**
     * 根据需要的最大值进行调整
     */
    final long inf = (long) 1 << 50;

    final int N = 40010, M = 200010;
    int[] head = new int[N];
    int[] ver = new int[M];
    long[] edge = new long[M];
    int[] Next = new int[M];
    int[] d = new int[N];

    int s = 0, t = 0, tot = 0;
    long maxflow = 0;
    LinkedList<Integer> q = null;

    void add(int x, int y, long z) {
        tot++;
        ver[tot] = y;
        edge[tot] = z;
        Next[tot] = head[x];
        head[x] = tot;
        tot++;
        ver[tot] = x;
        edge[tot] = 0;
        Next[tot] = head[y];
        head[y] = tot;
    }

    boolean bfs() {
        Arrays.fill(d, 0);
        while (!q.isEmpty())
            q.removeFirst();
        q.push(s);
        d[s] = 1;
        while (!q.isEmpty()) {
            int x = q.removeFirst();
            for (int i = head[x]; i > 0; i = Next[i]) {
                if (edge[i] > 0 && d[ver[i]] == 0) {
                    q.addLast(ver[i]);
                    d[ver[i]] = d[x] + 1;
                    if (ver[i] == t)
                        return true;
                }
            }
        }
        return false;
    }

    /**
     * 构造初始网络
     * 
     * @param n    点的个数
     * @param m    边的个数
     * @param s    源点
     * @param t    汇点
     * @param data 边的信息，{x,y,z}
     */
    public Dinic(int s, int t, ArrayList<long[]> data) {
        this.s = s;
        this.t = t;
        tot = 1;
        for (var item : data) {
            add((int) item[0], (int) item[1], item[2]);
        }
    }

    public long dinic(int x, Long flow) {
        if (x == t)
            return flow;
        long rest = flow;
        long k = 0;
        for (int i = head[x]; i > 0 && rest > 0; i = Next[i]) {
            if (edge[i] > 0 && d[ver[i]] == d[x] + 1) {
                k = dinic(ver[i], Math.min(rest, edge[i]));
                if (k == 0)
                    d[ver[i]] = 0;// 此点已经不可能再增加了，直接改变层数，剪枝
                edge[i] -= k;
                edge[i ^ 1] += k;
                rest -= k;
            }
        }
        return flow - rest;
    }

    /**
     * 注意：一般考虑到此算法是求割点转化为割边，所以此函数求出，割掉了那些点，因为语意的不清楚，有可能造成后来看不懂
     * 
     * @param begin 从那个点开始
     * @param end   到那个点结束
     * @param n     原来子图的大小，因为实际的点会被拆成两个点，i，i+n
     * @return 被拆掉的点
     */
    public ArrayList<Integer> show(int begin, int end, int n) {
        HashSet<Integer> mid = new HashSet<>();
        /** 
         * 再跑一遍bfs，将能到达的点收集即可
        */
        Arrays.fill(d, 0);
        while (!q.isEmpty())
            q.removeFirst();
        q.push(s);
        d[s] = 1;
        while (!q.isEmpty()) {
            int x = q.removeFirst();
            for (int i = head[x]; i > 0; i = Next[i]) {
                if (edge[i] > 0 && d[ver[i]] == 0) {
                    q.addLast(ver[i]);
                    mid.add(ver[i]);
                    d[ver[i]] = d[x] + 1;
                }
            }
        }
        ArrayList<Integer> res = new ArrayList<>();
        for (int i = begin; i <= end; i++) {
            if (mid.contains(i) && !mid.contains(i + n)) {
                res.add(i);
            }
        }
        return res;
    }

    public long getAns() {
        long flow = 0;
        this.q = new LinkedList<>();
        while (bfs()) {
            while ((flow = dinic(s, inf)) != 0) {
                maxflow += flow;
            }
        }
        return maxflow;
    }
}
