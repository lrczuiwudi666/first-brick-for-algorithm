public class EnumK {
	public ArrayList<Integer> EnumK(int k,int n)/*求出总共n个状态中，有k个状态为1的所有情况*/
	{
	    ArrayList<Integer> res=new ArrayList<>();
		int comb=(1<<k)-1;
	    while(comb<1<<n)//comb>=2^n退出
	    {
	        res.add(comb);
            //针对组合的处理
	        int x=comb&-comb,y=comb+x;
	        comb=((comb&~y)/x>>1)|y;
	    }
	    return res;
	}
}
