public class FindDiameter {
    /**
     * 找到一棵树的直径，返回两个节点
     * @param graph 必须是一个树
     * @return 直径的两个端点
     */
    public static int[] findLR(ArrayList<Integer>[] graph) {
        var l = dfs(graph, 0, new boolean[graph.length]);
        var r = dfs(graph, l[0], new boolean[graph.length]);
        return new int[] { l[0], r[0] };
    }

    private static int[] dfs(ArrayList<Integer>[] graph, int index, boolean[] mark) {
        if (graph[index].size() == 1 && mark[graph[index].get(0)])
            return new int[] { index, 1 };
        mark[index] = true;
        int[] res = null;
        for (var item : graph[index]) {
            if (!mark[item]) {
                var mid = dfs(graph, item, mark);
                if (res == null) {
                    res = mid;
                } else {
                    if (res[1] < mid[1])
                        res = mid;
                }
            }
        }
        res[1]++;
        return res;
    }
}