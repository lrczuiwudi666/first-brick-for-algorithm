/**
 * 其实主要是算法，板子非常简单，图的存储行式有很多，这里使用一种
 * 简单的示例，更加好的算法流程描述，可以参考进阶指南409页,特别要注意下此欧拉回路的求法，是让边只走一次，点可以重复走
 */
public class Hierholzer {
    /**
     * 
     * @param index 对当前节点的抽象，此节点可以是个数字，也可以是个字符串，只要是能表示节点的都可以
     * @param data  存储图的一种方式
     * @param last  记录，一条欧拉回路
     */
    public static void dfs(int index, HashMap<Integer, LinkedList<Integer>> data, ArrayList<int[]> last) {
        while (data.containsKey(index) && !data.get(index).isEmpty()) {
            int v = data.get(index).removeFirst();
            dfs(v, data, last);
            last.add(new int[] { index, v });
        }
    }

    public static void main(String[] args) {
        int begin = 0;
        // 需要先找一个起点
        /**
         * begin=?
         */
        HashMap<Integer, LinkedList<Integer>> data = new HashMap<>();
        // 需要初始化图
        ArrayList<int[]> last = new ArrayList<>();

        dfs(begin, data, last);
        // 最后需要逆序输出
        Collections.reverse(last);
    }
}
