public class KMP {
	//next数组的意义在于最大的前缀等于后缀的长度，比如next[j]=1;意味着最后一个字符于第一个字符相等,next[j]=0,意味着没有后缀与前缀相等
	//属于修正之前的数组
	public int[] getNext(String str){
	       int j=-1,i=0;
	      int arr[]=new int[str.length()+1];
	      arr[0]=-1;
	         while(i<str.length()) {
	          if(j==-1||str.charAt(i)==str.charAt(j)){
	              j++;
	              i++;
	              arr[i]=j;
	          }
	              else
	              j=arr[j];
	          }
	        return arr;
	  }
}
