class LevelAncestor {
    ArrayList<Integer>[] graph = null;
    int[][] la = null;
    int[] pow = { 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096, 8192, 16384, 32768, 65536, 131072 };
    int[] deepth = null;

    public LevelAncestor(ArrayList<Integer>[] graph, int root) {
        this.graph = graph;
        int len = 0;
        for (int i = 0; i < pow.length; i++) {
            if (pow[i] > graph.length) {
                len = i;
                break;
            }
        }
        this.la = new int[graph.length][len + 1];
        for (var item : la)
            Arrays.fill(item, -1);
        boolean[] mark = new boolean[graph.length];
        int[] dis = new int[graph.length + 1];
        Arrays.fill(dis, -1);
        this.deepth = new int[graph.length];
        dfs(mark, dis, root, 0);
    }

    /**
     * 预处理la树，每个节点都存储了距离它1,2,4,,,距离的祖先
     * 
     * @param mark  标记防止重复访问
     * @param dis   dfs时候记录从根到当前节点
     * @param index 当前节点
     * @param deep  深度
     */
    private void dfs(boolean[] mark, int[] dis, int index, int deep) {
        mark[index] = true;
        dis[deep] = index;
        deepth[index] = deep;
        for (int i = 0; i < la[index].length; i++) {
            if (pow[i] <= deep) {
                la[index][i] = dis[deep - pow[i]];
            } else {
                break;
            }
        }
        for (var item : graph[index]) {
            if (!mark[item])
                dfs(mark, dis, item, deep + 1);
        }
    }

    /**
     * 查询距离节点x距离为deep的祖先
     * 
     * @param x    节点x
     * @param deep 距离
     * @return 返回祖先节点，如果是-1意味着没有距离为deep的祖先
     */
    public int la(int x, int deep) {
        if (deep == 0)
            return x;
        if (deep > deepth[x])
            return -1;
        int res = x;
        while (deep != 0) {
            int len = 0;
            for (int i = 0; i < la[x].length; i++) {
                if (pow[i] <= deep) {
                    len = i;
                } else {
                    break;
                }
            }
            deep -= pow[len];
            res = la[res][len];
        }
        return res;
    }
}