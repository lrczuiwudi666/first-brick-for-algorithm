public class Manacher {
    /**
     * 存储以当前index为中心的最大回文半径的长度，主要就是需要使用这个数据
     */
    int[] L = null;

    public Manacher(String s) {
        StringBuffer mid = new StringBuffer();
        mid.append("$#");
        for (int i = 0; i < s.length(); i++) {
            mid.append(s.charAt(i));
            mid.append('#');
        }
        mid.append('!');
        L = new int[mid.length()];
        String data = mid.toString();
        L[1] = 1;
        int k = 0, r = 0;
        for (int i = 2; i < L.length - 1; i++) {
            if (i < r) {
                L[i] = Math.min(r - i, L[2 * k - i]);
            } else {
                L[i] = 1;
            }
            while (data.charAt(i - L[i]) == data.charAt(i + L[i])) {
                L[i]++;
            }
            if (i + L[i] > r) {
                r = i + L[i];
                k = i;
            }
        }
    }
}