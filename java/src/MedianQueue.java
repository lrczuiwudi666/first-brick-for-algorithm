/**
 * 获取一个动态添加的数组的中位数
 */
public class MedianQueue{
    PriorityQueue<Integer> pre=new PriorityQueue<>((x,y)->y-x);
    PriorityQueue<Integer> last=new PriorityQueue<>((x,y)->x-y);
    long sumpre=0;
    long sumlast=0;
    public void add(int x){
        if(pre.size()==0) {
            pre.add(x);
            sumpre+=x;
            return ;
        }
        if(pre.size()==last.size()){
             if(last.peek()<=x){
                int ch=last.poll();
                sumlast-=ch;
                sumpre+=ch;
                sumlast+=x;
                last.add(x);
                pre.add(ch);
             }else{
                sumpre+=x;
                pre.add(x);
             }
        }else{
            if(x>pre.peek()){
               sumlast+=x;
               last.add(x); 
            }else{
               int ch=pre.poll();
               sumpre-=ch;
               sumpre+=x;
               sumlast+=ch;
               pre.add(x);
               last.add(ch);
            }
        }
    }
    public int get(){
        return pre.peek();
    }
    public int getpresize(){
        return pre.size();
    }
    public int getlastsize(){
        return last.size();
    }
    public long getpresum(){
        return sumpre;
    }
    public long getlastsum(){
        return sumlast;
    }
     public String toString(){
        return pre.toString()+"  "+last.toString()+"  "+get()+"  "+getpresum()+"  "+getlastsum();
    }
}
