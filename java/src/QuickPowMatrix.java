/**
 * 矩阵快速幂算法描述：适用于在动态规划当中，符合某种迭代的矩阵的时候，并且需要反复乘的情况，所以
 * 可以考虑使用矩阵快速结算，比如status数组，经过(A^n)*status(其中*表示矩阵的乘积),其中n很大的时候
 * 可以将n分解成2进制，这样，可以经过预处理完成，快速乘积，类似快速幂算法
 * @author lrc
 *
 */
public class QuickPowMatrix {
	public long mod = (long) (1e9+7);
	public long[][] mult(long[][] x,long[][] y) {
    	long[][] res=new long[x.length][y[0].length];
    	for (int i = 0; i < res.length; i++) {
			for (int j = 0; j < res[0].length; j++) {
				 for (int j2 = 0; j2 <x[0].length; j2++) {
						res[i][j]+=(x[i][j2]*y[j2][j])%mod;
						res[i][j]%=mod;
				 }			 			
			}
		}
    	return res;
    }
    public long[][] mark(int len){
    	long[][] res=new long[len][len];
    	for (int i = 0; i < res.length; i++) {
			res[i][i]=1;
		}
    	return res;
    }
    public static ArrayList<long[][][]> data=null;
    private void init(int len) {
    	this.data=new ArrayList<>();
    	for (int i = 1; i <=len; i++) {
    	long[][][] mid=new long[31][i][i];
        mid[0]=make(i);
    	for (int j = 1; j < mid.length; j++) {
			mid[j]=mult(mid[j-1], mid[j-1]);
		}
    	data.add(mid);
    	}
    }
    private  long[][] powPre(long[][] n, int m) {
    	long[][] res=mark(n.length);                              
	    int index=0;
	    while(m != 0) {
	        if ((m&1) == 1) {            
	            res=mult(res,data.get(n.length-1)[index]);        
	        }                                     
	        m = m >> 1;
    	    index++;
	    }
	    return res;                      
	}
    //需要给定一种生成重复矩阵的方法，对于每一个题来说都不一样，也就是基不一样
    public long[][] make(int len){
    	return null;
    }
}
