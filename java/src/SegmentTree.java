//此为单点修改区间求和
 public class SegmentTree {
	     int[] left=null;
	     int[] right=null;
	     int[] sum=null;
	     int[] data=null;
	     
	     //int mod=(int) (1e9+7);
	     public SegmentTree(int[] data,int len) {
	    	 this.left=new int[len*4];
	    	 this.right=new int[len*4];
	    	 this.sum=new int[4*len];
	    	 this.data=data;
	    	 this.build(1, 1, len);
	     }
	     public void build(int p,int l,int r) {
	         left[p]=l;
	         right[p]=r;
	         if(l==r) {
	        	 this.sum[p]=data[l-1];
	        	 return ;
	         }
	         int mid=(l+r)/2;
	         build(p*2, l, mid);
	         build(p*2+1, mid+1, r);
	         this.sum[p]=this.sum[p*2+1]+this.sum[p*2];
	     }
	     public void change(int p,int x,int v) {
	    	 if(this.left[p]==this.right[p]) {
	    		 this.data[this.left[p]-1]=v;
	    	 	 this.sum[p]=v;
	    	 	 return ;
	    	 }
	    	 int mid=(this.left[p]+this.right[p])/2;
	         if(x<=mid) {
	        	 change(p*2, x, v);
	         }else {
	        	 change(p*2+1, x, v);
	         }
	    	 this.sum[p]=this.sum[p*2]+this.sum[p*2+1];
	     }
	     public int ask(int p ,int l,int r) {
	    	 if(l<=this.left[p]&&this.right[p]<=r) {
	    		 return this.sum[p];
	    	 }
	    	 int mid=(this.left[p]+this.right[p])/2;
	    	 int res=0;
	    	 if(l<=mid) res+=ask(p*2,l,r);
	    	 if(r>mid)  res+=ask(p*2+1, l, r);
	    	 return res;
	     }
	     
	}
