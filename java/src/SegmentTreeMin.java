public class SegmentTreeMin {
    int[] left = null;
    int[] right = null;
    int[] add = null;
    int range = 0;
    /**
     * 构造的时候需要注意，range表示初始值，也可以是某个上限
     * @param len 长度
     * @param range 上限
     */
    public SegmentTreeMin(int len, int range) {
        this.left = new int[len * 4];
        this.right = new int[len * 4];
        this.add = new int[4 * len];
        Arrays.fill(add, range);
        this.range = range;
        this.build(1, 1, len);
    }

    public void build(int p, int l, int r) {
        left[p] = l;
        right[p] = r;
        if (l == r) {
            return;
        }
        int mid = (l + r) / 2;
        build(p * 2, l, mid);
        build(p * 2 + 1, mid + 1, r);
    }

    public void spread(int p) {
        if (add[p] != range) {
            add[p * 2] = Math.min(add[p], add[p * 2]);
            add[p * 2 + 1] = Math.min(add[p], add[p * 2 + 1]);
            add[p] = range;
        }
    }

    public void change(int p, int l, int r, int v) {
        if (left[p] >= l && right[p] <= r) {
            add[p] = Math.min(add[p], v);
            return;
        }
        spread(p);
        int mid = (left[p] + right[p]) / 2;
        if (l <= mid)
            change(p * 2, l, r, v);
        if (r > mid)
            change(p * 2 + 1, l, r, v);
    }

    public int ask(int p, int l, int r) {
        if (l <= this.left[p] && this.right[p] <= r) {
            return add[p];
        }
        spread(p);
        int mid = (this.left[p] + this.right[p]) / 2;
        int res = range;
        if (l <= mid) {
            res = Math.min(ask(p * 2, l, r), res);
        }
        if (r > mid) {
            res = Math.min(ask(p * 2 + 1, l, r), res);
        }
        return res;
    }
}
