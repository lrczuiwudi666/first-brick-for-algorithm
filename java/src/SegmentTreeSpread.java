public class SegmentTreeSpread {
		 int[] left=null;
	     int[] right=null;
	     int[] sum=null;
	     int[] add=null;
	     int[] data=null;
	     int mod=(int) (1e9+7);
	     public SegmentTreeSpread(int[] data,int len) {
	    	 this.left=new int[len*4];
	    	 this.right=new int[len*4];
	    	 this.sum=new int[4*len];
	    	 this.add=new int[4*len];
	    	 this.data=data;
	    	 this.build(1, 1, len);
	     }
	     public void build(int p,int l,int r) {
	         left[p]=l;
	         right[p]=r;
	         if(l==r) {
	        	 this.sum[p]=data[l-1];
	        	 return ;
	         }
	         int mid=(l+r)/2;
	         build(p*2, l, mid);
	         build(p*2+1, mid+1, r);
	         this.sum[p]=this.sum[p*2+1]+this.sum[p*2];
	     }
	     public void spread(int p) {
	    	 if(add[p]!=0) {
	    		 sum[p*2]+=((long)add[p]*(right[p*2]-left[p*2]+1))%mod;
	    		 sum[p*2]%=mod;
	    		 sum[p*2+1]+=((long)add[p]*(right[p*2+1]-left[p*2+1]+1))%mod;
	    		 sum[p*2+1]%=mod;
	    	     add[p*2]+=add[p];
	    	     add[p*2]%=mod;
	    	     add[p*2+1]+=add[p];
	    	     add[2*p+1]%=mod;
	    	     add[p]=0;
	    	 }
	     }
	     public void change(int p,int l,int r,int v) {
	    	 if(left[p]>=l&&right[p]<=r) {
	    		 sum[p]+=((long)v*(right[p]-left[p]+1))%mod;
	    		 sum[p]%=mod;
	    		 add[p]+=v;
	    		 add[p]%=mod;
	    		 return ;
	    	 }
	    	 spread(p);
	    	 int mid=(left[p]+right[p])/2;
	    	 if(l<=mid) change(p*2, l, r, v);
	    	 if(r>mid) change(p*2+1, l, r, v);
	    	 this.sum[p]=this.sum[p*2]+this.sum[p*2+1];
	    	 this.sum[p]%=mod;
	     }
	     public int ask(int p ,int l,int r) {
	    	 if(l<=this.left[p]&&this.right[p]<=r) {
	    		 return this.sum[p];
	    	 }
	    	 spread(p);
	    	 int mid=(this.left[p]+this.right[p])/2;
	    	 int res=0;
	    	 if(l<=mid) {
	    		 res+=ask(p*2,l,r);
	    		 res%=mod;
	    	 }
	    	 if(r>mid)  {
	    		 res+=ask(p*2+1, l, r);
	    		 res%=mod;
	    	 }
	    	 return res;
	     }
	}
