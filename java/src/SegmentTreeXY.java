/**
 * 这个数据结构的解释，建议结合题目来理解，其中一些钉死的参数比如-1000000,这种应该随着题目的改变而改变，写起来非常的复杂，既要离散化，还要反向记录
 * https://atcoder.jp/contests/abc167/tasks/abc167_f
 */
public class SegmentTreeXY {
    class SegmentTreeMax {
        int[] left = null;
        int[] right = null;
        int[] max = null;
        int[] data = null;

        // int mod=(int) (1e9+7);
        public SegmentTreeMax(int[] data, int len) {
            this.left = new int[len * 4];
            this.right = new int[len * 4];
            this.max = new int[4 * len];
            Arrays.fill(max, -1000000);
            this.data = data;
            this.build(1, 1, len);
        }

        public void build(int p, int l, int r) {
            left[p] = l;
            right[p] = r;
            if (l == r) {
                this.max[p] = data[l - 1];
                return;
            }
            int mid = (l + r) / 2;
            build(p * 2, l, mid);
            build(p * 2 + 1, mid + 1, r);
            this.max[p] = Math.max(this.max[p * 2 + 1], this.max[p * 2]);
        }

        public void change(int p, int x, int v) {
            if (this.left[p] == this.right[p]) {
                this.data[this.left[p] - 1] = v;
                this.max[p] = v;
                return;
            }
            int mid = (this.left[p] + this.right[p]) / 2;
            if (x <= mid) {
                change(p * 2, x, v);
            } else {
                change(p * 2 + 1, x, v);
            }
            this.max[p] = Math.max(this.max[p * 2], this.max[p * 2 + 1]);
        }

        public int ask(int p, int l, int r) {
            if (l <= this.left[p] && this.right[p] <= r) {
                return this.max[p];
            }
            int mid = (this.left[p] + this.right[p]) / 2;
            int res = -1000000;
            if (l <= mid)
                res = Math.max(ask(p * 2, l, r), res);
            if (r > mid)
                res = Math.max(ask(p * 2 + 1, l, r), res);
            return res;
        }
    }

    HashMap<Integer, LinkedList<Integer>> map = null;
    TreeMap<Integer, Integer> tree = null;
    SegmentTreeMax max = null;
    HashMap<Integer, TreeMap<Integer, Integer>> rev = null;
    int limit = 0;

    public SegmentTreeXY(ArrayList<int[]> d, int limit) {
        this.limit = limit;
        d.sort((x, y) -> x[0] == y[0] ? y[1] - x[1] : x[0] - y[0]);
        map = new HashMap<>();
        tree = new TreeMap<>();
        rev = new HashMap<>();
        int index = 0;

        for (int i = 0; i < d.size(); i++) {
            // stem.out.println(d.get(i)[0]+"------"+d.get(i)[1]);
            if (map.containsKey(d.get(i)[0])) {
                map.get(d.get(i)[0]).addLast(d.get(i)[1]);
                if (rev.containsKey(d.get(i)[1])) {
                    rev.get(d.get(i)[1]).put(d.get(i)[0], rev.get(d.get(i)[1]).getOrDefault(d.get(i)[0], 0) + 1);
                } else {
                    TreeMap<Integer, Integer> ms = new TreeMap<>();
                    ms.put(d.get(i)[0], 1);
                    rev.put(d.get(i)[1], ms);
                }
            } else {
                index++;
                var mid = new LinkedList<Integer>();
                mid.add(d.get(i)[1]);
                map.put(d.get(i)[0], mid);
                tree.put(d.get(i)[0], index);
                if (rev.containsKey(d.get(i)[1])) {
                    rev.get(d.get(i)[1]).put(d.get(i)[0], rev.get(d.get(i)[1]).getOrDefault(d.get(i)[0], 0) + 1);
                } else {
                    TreeMap<Integer, Integer> ms = new TreeMap<>();
                    ms.put(d.get(i)[0], 1);
                    rev.put(d.get(i)[1], ms);
                }
                // stem.out.println(tree);
            }
        }
        int[] mmid = new int[index];
        Arrays.fill(mmid, limit);
        // System.out.println(index);
        max = new SegmentTreeMax(mmid, index);
        for (var item : map.keySet()) {
            max.change(1, tree.get(item), map.get(item).getFirst());
        }
    }

    public void add(int x, int val) {
        if (map.containsKey(x)) {
            map.get(x).addLast(val);
            if (rev.containsKey(val)) {
                rev.get(val).put(x, rev.get(val).getOrDefault(x, 0) + 1);
            } else {
                TreeMap<Integer, Integer> ms = new TreeMap<>();
                ms.put(x, 1);
                rev.put(val, ms);
            }
        } else {
            var mid = new LinkedList<Integer>();
            mid.add(val);
            map.put(x, mid);
            if (rev.containsKey(val)) {
                rev.get(val).put(x, rev.get(val).getOrDefault(x, 0) + 1);
            } else {
                TreeMap<Integer, Integer> ms = new TreeMap<>();
                ms.put(x, 1);
                rev.put(val, ms);
            }
            // stem.out.println(tree);
        }
        max.change(1, tree.get(x), map.get(x).getFirst());
    }

    public int query(int range) {
        var index = tree.floorEntry(range);
        // System.out.println(map);
        if (index == null)
            return limit;
        int ask = max.ask(1, 1, index.getValue());
        return ask;
    }

    public void remove(int x, int range) {
        var t = rev.get(x);
        var data = t.floorEntry(range);
        int index = tree.get(data.getKey());
        map.get(data.getKey()).removeFirst();
        var has = map.get(data.getKey()).isEmpty() ? null : map.get(data.getKey()).getFirst();
        max.change(1, index, (has == null ? limit : has));
        if (data.getValue() == 1) {
            t.remove(data.getKey());
        } else {
            t.put(data.getKey(), data.getValue() - 1);
        }
    }
}
