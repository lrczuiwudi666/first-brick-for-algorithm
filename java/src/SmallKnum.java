public class SmallKnum {
	/**
	 *  用于计算两个有序数组中，A【i】*B【j】的最小的第k个数
	 * @param a 有序数组1，必须正数
	 * @param b 有序数组2，必须正数
	 * @param range 2分划分的边界
	 * @return  <=range的个数
	 */
	public long small(ArrayList<Integer> a,ArrayList<Integer> b,long range) {
		long c=0;
	    int j=b.size()-1;
	    for(int i=0;i<a.size();i++)
	    {
	        while(j>=0&&(long)a.get(i)*b.get(j)>range) j--;
            if(j>=0)
	        if((long)a.get(i)*b.get(j)<=range) c+=j+1;
            else break;
	    }
	    return c;
	}
}
