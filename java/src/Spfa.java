public class Spfa {
    public boolean spfaLongest(ArrayList<long[]>[] graph, boolean[] mark, double[] dis, LinkedList<Integer> q,
            int[] cnt) {
        while (!q.isEmpty()) {
            var temp = q.removeFirst();
            mark[temp] = false;
            for (var item : graph[temp]) {
                if (dis[(int) item[0]] < dis[temp] + item[1]) {
                    dis[(int) item[0]] = dis[temp] + item[1];
                    cnt[(int) item[0]] = cnt[temp] + 1;
                    if (cnt[(int) item[0]] >= graph.length) {
                        return true;
                    }
                    if (!mark[(int) item[0]]) {
                        q.addLast((int) item[0]);
                        mark[(int) item[0]] = true;
                    }
                }
            }
        }
        return false;
    }

    public boolean spfaShortest(ArrayList<long[]>[] graph, boolean[] mark, double[] dis, LinkedList<Integer> q,
            int[] cnt) {
        while (!q.isEmpty()) {
            var temp = q.removeFirst();
            mark[temp] = false;
            for (var item : graph[temp]) {
                if (dis[(int) item[0]] > dis[temp] + item[1]) {
                    dis[(int) item[0]] = dis[temp] + item[1];
                    cnt[(int) item[0]] = cnt[temp] + 1;
                    if (cnt[(int) item[0]] >= graph.length) {
                        return true;
                    }
                    if (!mark[(int) item[0]]) {
                        q.addLast((int) item[0]);
                        mark[(int) item[0]] = true;
                    }
                }
            }
        }
        return false;
    }

    public void spfa(ArrayList<long[]>[] graph, int b) {
        boolean[] mark = new boolean[graph.length];
        double[] dis = new double[graph.length];
        int[] cnt = new int[graph.length];
        // 取决于到底是最长路还是最短路
        Arrays.fill(dis, -1e18);
        dis[b] = 0;
        LinkedList<Integer> q = new LinkedList<>();
        q.add(b);
        mark[b] = true;
        if (spfaLongest(graph, mark, dis, q, cnt)) {
            System.out.println("Infinity");
        } else {
            long max = -(long) 1e18;
            for (int i = 1; i < dis.length; i++) {
                max = Math.max(max, (long) dis[i]);
            }
            System.out.println(max);
        }
    }

}