public class StringHash {
	public int[] make(char[] pre) {
		 int[] fix=new int[pre.length+1];
	     int h=0;
	     for (int i = 0; i < pre.length; i++) {
	         h = 31 * h + pre[i];
	         fix[i+1]=h;
	     }
	     return fix;
	 }
	 public int[] make(int[] pre) {
		 int[] fix=new int[pre.length+1];
	     int h=0;
	     for (int i = 0; i < pre.length; i++) {
	         h = 31 * h + pre[i];
	         fix[i+1]=h;
	     }
	     return fix;
	 }
	 public int cal(int begin,int end,int[] fix) {
			return fix[end+1]-fix[begin]*powWithmod(31,end-begin+1);
     }
	private  int powWithmod(int n, int m) {
		    int res = 1;                     
		    int base = n;                    
		    while(m != 0) {                  
		        if ((m&1) == 1) {            
		            res = (res * base);        
		        }                            
		        base = (base * base);          
		        m = m >> 1;                  
		    }                                
		    return res;                      
	 }
}
