public class Tarjan {
 private int[] dfn=null;
 private int[] low=null;
 private int num=1;
 private ArrayList<int[]>[] group=null;
 private HashSet<int[]> treepath=null;
 public void dfs(int x) {
	 if(dfn[x]!=0) return ;
	 dfn[x]=low[x]=num;
	 this.num++;
	 for(int[] temp:group[x]) {
		 int y=temp[0]==x?temp[1]:temp[0];
		 if(dfn[y]==0) {
			 treepath.add(temp);
			 dfs(y);
			 low[x]=Math.min(low[x],low[y]);
		 }else if(!treepath.contains(temp)){
			 low[x]=Math.min(low[x],dfn[y]);
		 }
	 }
 }
 private ArrayList<int[]> res=null;
 public void check() {
	 this.res=new ArrayList<>();
	 for(int[] item:treepath) {
		 if(dfn[item[0]]<low[item[1]]||dfn[item[1]]<low[item[0]]) {
			 res.add(item);
		 }
	 }
 }
 public void init(int n, List<List<Integer>> connections) {
	 this.dfn=new int[n];
	 this.low=new int[n];
	 this.group=new ArrayList[n];
	 for (int i = 0; i < n; i++) {
		this.group[i]=new ArrayList<>();
	 }
	 this.treepath=new HashSet<>();
	 for(List<Integer> item:connections) {
		  int[] data=new int[2];
		  data[0]=item.get(0);
		  data[1]=item.get(1);
		  group[data[0]].add(data);
		  group[data[1]].add(data);
	 }
 }
}
