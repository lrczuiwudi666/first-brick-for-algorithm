
public class arrTree {
    // 并不包含对索引0的操作，会产生死循环
    int[] arr;
    int n;

    /**
     * 注意，在零处的索引，是不可以使用的，所以，n的取值是 >=（需要的长度+1）
     * 
     * @param n 需要创建的区间长度
     */
    public arrTree(int n) {
        this.n = n;
        arr = new int[n];
    }

    public int lowbit(int i) {
        return i & (-i);
    }

    // 小于等于x的个数
    public int query(int x) {
        int ans = 0;
        for (int i = x; i > 0; i -= lowbit(i)) {
            ans += arr[i];
        }
        return ans;
    }

    // 加操作,同样也可以减
    public void update(int x, int add) {
        for (int i = x; i < n; i += lowbit(i)) {
            arr[i] += add;
        }
    }
}
