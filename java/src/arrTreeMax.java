
public class arrTreeMax {
    // 并不包含对索引0的操作，会产生死循环
    long[] arr;
    int n;

    public arrTreeMax(int n) {
        this.n = n;
        arr = new long[n];
    }

    public int lowbit(int i) {
        return i & (-i);
    }

    // 小于x索引的的arr[1]-arr[x]的最大值
    public long query(int x) {
        long ans = 0;
        for (int i = x; i > 0; i -= lowbit(i)) {
            ans = Math.max(ans, arr[i]);
        }
        return ans;
    }

    // 只可添加不可删除
    public void update(int x, long add) {
        for (int i = x; i < n; i += lowbit(i)) {
            arr[i] = Math.max(arr[i], add);
        }
    }
}
