/**
 * 树状数组+差分的结合，关键部分需要数学推导
 */
public class arrTreeRange {
    // 并不包含对索引0的操作，会产生死循环
    int[] s1;
    int[] s2;
    int n;

    /**
     * 注意，在零处的索引，是不可以使用的，所以，n的取值是 >=（需要的长度+1）
     * 
     * @param n 需要创建的区间长度
     */
    public arrTreeRange(int n) {
        this.n = n;
        this.s1 = new int[n];
        this.s2 = new int[n];
    }

    public int lowbit(int i) {
        return i & (-i);
    }

    // 小于等于x的个数
    public int query(int x) {
        int ans = 0;
        for (int i = x; i > 0; i -= lowbit(i)) {
            ans += (x + 1) * s1[i] - s2[i];
        }
        return ans;
    }

    // 加操作,同样也可以减
    public void update(int x, int add) {
        for (int i = x; i < n; i += lowbit(i)) {
            s1[i] += add;
            s2[i] += x * add;
        }
    }

    // 区间更新操作
    public void range_update(int l, int r, int add) {
        update(l, add);
        update(r + 1, add * (-1));
    }

    // 区间查询操作
    public int range_ask(int l, int r) {
        return query(r) - query(l - 1);
    }
}
