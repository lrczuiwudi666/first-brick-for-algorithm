public class djstla {
	/**
	 * 稠密图
	 * @param road 权重路径
	 * @param n 有多少个节点
	 * @param b 源点
	 * @param MX 初始规定的最大值
	 * @return 返回距离矩阵
	 */
	public static int[] djstla(int[][] road, int n, int b,int MX) {
		boolean[] visited = new boolean[n];
        int[] distance = new int[n];
        for (int i = 0; i < n; i++) {   //维护两个数组，每次找下一个节点只在没有访问过的点中寻找
            visited[i] = false;
            distance[i] = MX;
        }
        visited[b] = true;
        distance[b] = 0;
        int cur = b;  //current point
        int nextP =-1;
        int times =0;
        while (times<(n-1)) {    //超过目标点数还没有找到就不用找了
            int minL =Integer.MAX_VALUE;
            for (int j = 0; j < n ; j++) {   //寻找下一个节点
                if (visited[j]) continue;    //每次找下一个节点只在没有访问过的点中寻找
                if (distance[cur] + road[cur][j] < distance[j]) {
                    distance[j] = distance[cur] + road[cur][j];
                }
                if (distance[j]<minL){
                    minL =distance[j];
                    nextP =j;
                }
            }
            visited[nextP] = true;
            cur =nextP;
            times++;
        }
        return distance;
    }
	/**
	 * 
	 * @param graph int[] 0->edgeid,1->next,2->weight
	 * @param n 节点数
	 * @param b 源点
	 * @return
	 */
	public static double[] djstla(ArrayList<int[]>[] graph,int n,int b) {
		double[] d=new double[n];
		boolean[] v=new boolean[n];
		Arrays.fill(d,Double.MAX_VALUE);
		d[b]=0;
		PriorityQueue<double[]> pq=new PriorityQueue<>((x,y)->x[0]>y[0]?1:-1);
		double[] begin= {0,b};
		pq.add(begin);
		while(!pq.isEmpty()) {
			double[] temp=pq.poll();
			if(v[(int) temp[1]]) continue;
			v[(int) temp[1]]=true;
			for(int[] item:graph[(int) temp[1]]) {
				if(d[item[1]]>d[(int) temp[1]]+item[2]) {
					d[item[1]]=d[(int) temp[1]]+item[2];
					double[] next= {d[item[1]],item[1]};
					pq.add(next);
				}
			}
		}
		return d;
	}	
}
