/**
 * 在两个有序数组中，历经log(n+m)的时间获得第k个数
 * 重点的划分思路是，比较对于一个组的k/2个数的大小
 * 如果a>=b那么b中的k/2个数一定不是k个最小的数
 * 子问题就可以缩小为a,和b剩余的数中找k-k/2个数
 */
public class findKnum {
    public double findKnum(int[] nums1, int[] nums2,int index1,int index2,int k){
        if(index1==nums1.length){
        return nums2[index2+k-1];
        }else if(index2==nums2.length){
        return nums1[index1+k-1];
        }
        if(k==1) {
            if(index1<nums1.length&&index2<nums2.length)
            return Math.min(nums1[index1], nums2[index2]);
            else
               return -1;
        }else{
           int a=Math.min(nums1.length-index1,k/2-1);
           int b=Math.min(nums2.length-index2,k/2-1);
           if(nums1[a+index1-1]>=nums2[b+index2-1]){
               return findKnum(nums1, nums2, index1, b+index2,k-b);
           }else{
               return findKnum(nums1, nums2, index1+a,index2,k-a); 
           }
        }
    }
}
