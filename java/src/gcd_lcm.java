public class gcd_lcm {
	// 求两个数的最大公约数
	public long gcd(long a, long b) {
		return b == 0 ? a : gcd(b, a % b);
	}

	// 求两个数最小公倍数
	public long lcm(long a, long b) {
		return a * b / gcd(a, b);
	}

	// 扩展欧几里得
	// ax[0]+bx[1]=gcd(a,b)
	public long exgcd(long a, long b, long[] x) {
		if (b == 0) {
			x[0] = 1;
			x[1] = 0;
			return a;
		}
		long d = exgcd(b, a % b, x);
		long z = x[0];
		x[0] = x[1];
		x[1] = z - x[1] * (a / b);
		return d;
	}

	/**
	 * 解一个一次线性同余方程 a*x=b (mod m) ，返回x最小解
	 * 
	 * @param a
	 * @param b
	 * @param m
	 * @return x的最小解，如果返回-1则表示无解
	 */
	public long mod_equation(long a, long b, long m) {
		long[] xy = new long[2];
		long g = exgcd(a, m, xy);
		if (b % g != 0)
			return -1;// b要能整除g
		long mid = m / g;
		return ((xy[0] * b / g) % mid + mid) % mid;
	}

	/**
	 * 线性同余方程组，主要解一组线性同余方程 x=r[i](mod m[i])意义下，求最小的x
	 * 
	 * @param r 等式右边的值
	 * @param m 模余意义下的模数
	 * @return 解和最小公倍数
	 */
	public long[] mod_system(long[] r, long[] m) {
		long[] res = new long[2];
		res[1] = 1;
		for (int i = 0; i < r.length; i++) {
			// m=lcm(m1,m2,m3,...),则之前的通解是x+t*m,t为自由变量
			// x+t*m=r[i]（mod m[i]）则 t*m=r[i]-x(mod m[i])为线性同余方程
			long mid = r[i] - res[0];
			// 有可能为负数需要处理
			if (mid < 0) {
				mid += Math.abs(mid) / m[i] * m[i] + m[i];
				mid %= m[i];
			}
			long t = mod_equation(res[1], mid, m[i]);
			if (t == -1)
				return new long[] { 0, 0 };
			res[0] = res[0] + t * res[1];
			res[1] = lcm(res[1], m[i]);
		}
		return res;
	}
}
