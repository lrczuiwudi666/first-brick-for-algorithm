public class lcp {
	 public int cal(int begin,int end,int[] fix) {
			return fix[end+1]-fix[begin]*powWithmod(31,end-begin+1);
  }
	private  int powWithmod(int n, int m) {
		    int res = 1;                     
		    int base = n;                    
		    while(m != 0) {                  
		        if ((m&1) == 1) {            
		            res = (res * base);        
		        }                            
		        base = (base * base);          
		        m = m >> 1;                  
		    }                                
		    return res;                      
	 }
	public int[] make(char[] pre) {
		 int[] fix=new int[pre.length+1];
	     int h=0;
	     for (int i = 0; i < pre.length; i++) {
	         h = 31 * h + pre[i];
	         fix[i+1]=h;
	     }
	     return fix;
	 }
	//得到的直接是排序结果，是后缀数组的排序结果，假设一个字符串形如"cba",那么其后缀数组是"cba","ba","a",
	//对应的后缀数组按字符串排序的结果之后，"a","ba","cba",所以返回的数组是{2,1,0},表示排序之后的位置索引
	//此方法并没有求出最长公共前缀，而是求出后缀数组的排序，因为在lc的5837求公共前缀也只是为了在O(1)的时间内
	//比较大小而已
    public int[] lcpWithHash(String x) {
    	int[] fix=make(x.toCharArray());
    	int[][] wait=new int[x.length()][1];
    	for (int i = 0; i < wait.length; i++) {
			wait[i][0]=i;
		}
    	Arrays.sort(wait,(a,b)->compare(a[0], b[0], fix, x));
    	int[] res=new int[x.length()];
    	for (int i = 0; i < wait.length; i++) {
			res[wait[i][0]]=i;
		}
    	return res;
    }
	public int compare(int x,int y,int[] fix,String s) {
	    int low=1;
	    int high=Math.min(fix.length-1-y,fix.length-1-x);
	    int res=0;
	    while(low<=high) {
	    	int mid=(low+high)>>1;
	        int temp1=cal(x,x+mid-1,fix);
	        int temp2=cal(y,y+mid-1,fix);
	        if(temp1==temp2) {
	        	low=mid+1;
	        	res=mid;
	        }else {
	        	high=mid-1;
	        }
	    }
	    if(res+x<s.length()&&res+y<s.length()) {
	    	return s.charAt(x+res)-s.charAt(y+res);
	    }else if(res+x==s.length()){
	    	return -1;
	    }else {
	    	return 1;
	    }
	}
	//dp[i][j]表示从字符串x[i],y[i]，起始的话，最长公共前缀的长度
	public int[][] lcp(String x) {
    	int[][] dp=new int[x.length()+1][x.length()+1]; 
	    for(int[] item:dp) {
	    	Arrays.fill(item, -1);
	    }
	    for (int i = 0; i < dp.length; i++) {
			dp[x.length()][i]=0;
		}
	    for (int i = 0; i < dp.length; i++) {
	    	dp[i][x.length()]=0;
		}
	    for (int i = 0; i < dp.length; i++) {
			for (int j = 0; j < dp.length; j++) {
				get(dp, i, j, x);
			}
		}
	    return dp;
    }
	public int get(int[][] dp,int x,int y,String s) {
		if(dp[x][y]!=-1) return dp[x][y];
		int res=0;
		if(s.charAt(x)==s.charAt(y)) {
            dp[x][y]=1+get(dp, x+1, y+1, s);
			return dp[x][y];
		}else {
			dp[x][y]=res;
		}
		return res;
	}
    public int[] lcpWithDp(String x) {
		int[][] dp=lcp(x);
		int[][] wait=new int[x.length()][1];
    	for (int i = 0; i < wait.length; i++) {
			wait[i][0]=i;
		}
    	Arrays.sort(wait,(a,b)->compare(a[0], b[0],x,dp));
    	int[] res=new int[x.length()];
    	for (int i = 0; i < wait.length; i++) {
			res[wait[i][0]]=i;
		}
    	return res;
	}
	public int compare(int x,int y,String s,int[][] lcp) {
		if(x+lcp[x][y]<s.length()&&y+lcp[x][y]<s.length()) {
			return s.charAt(x+lcp[x][y])-s.charAt(y+lcp[x][y]);
		}else if(x+lcp[x][y]==s.length()) {
			return -1;
		}else {
			return 1;
		}
	}
}
