public class longestSubarrayInRange {
	//在O(n)的时间内求出不超过range的长度下最大的连续子数组之和，是著名的单调队列算法
	/**
	 * 
	 * @param range 连续数组长度最大值
	 * @param fix 原始数组的前缀数组，其中在单调队列的算法中已经进行了转换
	 * @return 在range范围内的最大值，其中对于任何一个数组来说，此函数单调
	 */
	public int longestSubarrayInRange(int range, long[] fix) {
		LinkedList<Integer> dequeue=new LinkedList<>();
		dequeue.add(0);
		int max=Integer.MIN_VALUE;
        for (int i = 1; i < fix.length; i++) {
			while(!dequeue.isEmpty()&&i-dequeue.peekFirst()>range) dequeue.poll();
			max=(int) Math.max(max,fix[i]-fix[dequeue.peekFirst()]);
			while(!dequeue.isEmpty()&&fix[dequeue.peekLast()]>=fix[i]) dequeue.pollLast();
			dequeue.addLast(i);
		}		
		return max;
	}
}
