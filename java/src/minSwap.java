public class minSwap {
    /**
     * 可以任意交换两个元素
     * 
     * @param arr 从1-n的一个排列
     * @return 返回排列成单调递增的需要的最少交换次数
     */
    public int minimumSwaps(int[] arr) {
        int swap = 0;
        boolean visited[] = new boolean[arr.length];

        for (int i = 0; i < arr.length; i++) {
            int j = i, cycle = 0;

            while (!visited[j]) {
                visited[j] = true;
                j = arr[j] - 1;
                cycle++;
            }

            if (cycle != 0)
                swap += cycle - 1;
        }
        return swap;
    }
}