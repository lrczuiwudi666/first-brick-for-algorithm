public class quickPow {
	private static int pow(int n, int m) {
    int res = 1;                     
    int base = n;                    
    while(m != 0) {                  
        if ((m&1) == 1) {            
            res = res * base;        
        }                            
        base = base * base;          
        m = m >> 1;                  
    }                                
    return res;                      
	}
	private int mod=(int) (1e9+7);
	private static int powWithmod(long n, long m) {
	    long res = 1;                     
	    long base = n%mod;                    
	    while(m != 0) {
	        if ((m&1) == 1) {            
	            res = (res * base)%mod;        
	        }                            
	        base = (base * base)%mod;          
	        m = m >> 1;                  
	    }
	    return (int) res;                      
	}

}
