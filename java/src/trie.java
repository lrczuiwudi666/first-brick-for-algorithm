public class trie {
		 public char data='a';
		 public HashMap<Character,trie> map=null;
		 public boolean flag=false;
		 public trie(char data,boolean flag) {
			 this.map=new HashMap<>();
			 this.data=data;
			 this.flag=flag;
		 }
		 public void add(String s,int index,trie root) {
			 if(index>=s.length()) {
				 root.flag=true;
				 return ;
			 }
			 if(root.map.containsKey(s.charAt(index))) {
				 root.add(s, index+1, root.map.get(s.charAt(index)));
			 }else {
				 trie a=new trie(s.charAt(index), false);
				 root.map.put(s.charAt(index),a);
				 root.add(s, index+1, root.map.get(s.charAt(index)));
			 }
		 }
	       public void show() {
			 System.out.println(this.data+"   "+this.flag);
			 for(char item:this.map.keySet()) {
				 this.map.get(item).show();
			 }
		 }
	     public void add(String s) {
	    	 this.add(s,0,this);
	     }
}
