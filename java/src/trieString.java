public class trieString {
	public class trie {
		 public String data=null;
		 public HashMap<String,trie> map=null;
		 public ArrayList<String> ss=null;
		 public boolean flag=false;
		 public trie(String data,boolean flag) {
			 this.map=new HashMap<>();
			 this.data=data;
			 this.flag=flag;
			 this.ss=new ArrayList<>();
		 }
		 public void add(String[] s,int index,trie root) {
			 if(index>=s.length) {
				 root.flag=true;
				 return ;
			 }
			 if(root.map.containsKey(s[index])) {
				 root.add(s, index+1, root.map.get(s[index]));
			 }else {
				 trie a=new trie(s[index], false);
				 root.map.put(s[index],a);
				 root.ss.add(s[index]);
				 root.add(s, index+1, root.map.get(s[index]));
			 }
		 }
	       public void show() {
			 System.out.println(this.data+"   "+this.flag);
			 for(String item:this.map.keySet()) {
				 this.map.get(item).show();
			 }
		  }
	     public void add(String[] s) {
	    	 this.add(s,0,this);
	     }
      @Override
	     public String toString() {
	    	 return this.data;
	     }
}
}
