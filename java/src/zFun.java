    public class zFun {
    /**
     * 计算s串的后缀与s的最长公共前缀，返回数组代表i起始的后缀与s的最长公共前缀
     * 此算法还是比较难理解，在思维中的模拟过程还是比较困难，此算法绝不可能自己发明，只能https://oi-wiki.org/string/z-func/
     * 
     * @param s 需要计算的字符串
     * @return eg. "aaaaa"->[5,4,3,2,1], "aba"->[3,0,1]
     */
    public int[] z_function(String s) {
        int n = s.length();
        int[] res = new int[s.length()];
        int l = 0, r = 0;
        for (int i = 1; i < res.length; i++) {
            if (i <= r && res[i - l] < r - i + 1) {
                res[i] = res[i - l];
            } else {
                res[i] = Math.max(0, r - i + 1);
                while (i + res[i] < n && s.charAt(res[i]) == s.charAt(i + res[i]))
                    res[i]++;
            }
            if (i + res[i] - 1 > r) {
                l = i;
                r = i + res[i] - 1;
            }
        }
        res[0] = n;
        return res;
    }
}
