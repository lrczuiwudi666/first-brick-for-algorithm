<details>
<summary>371场周赛</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>再次进行康复训练，题目还可以，复习复习手速，重温经典数据结构二叉字典树，用于从一堆数字里面找到当前数字最大的异或结果的数字，靠记忆还是能想的起来的</details>
- <details><summary>tag</summary>中等场</details>
- spendtime: 80
- ACtime: 2024.11.24
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>117场双周赛</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>手速应该是巨慢的，然后第三题目这个容斥原理，或者dp也不是很会，看来还是欠收拾了，老年人垂死挣扎</details>
- <details><summary>tag</summary>中等场</details>
- spendtime: 75
- ACtime: 2024.9.16
- ACstatus: 不会
- score: 6
</details>
</details>

<details>
<summary>370场周赛</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>时隔六个月的康复训练，还是时不时要打一打的，场次应该算是比较简单的场次，手感还在还行，一百多名的水平，好久没写树形dp了，三题还是卡了一下，四题是树状数组的衍生版max版本，一个板子题目，就是调整一个long long即可，其他的都很简单，还行吧这种场次</details>
- <details><summary>tag</summary>手速场</details>
- spendtime: 70
- ACtime: 2024.5.4
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>369场周赛</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>补了一场周赛的题目，水平倒也没有自己想象的下滑那么厉害，其中最耐人寻味的其实是第三题，这个dp的转移方程好久没仔细思考过了，还是比较有意思的，看了一眼题解区，我觉得没人能把真的状态的定义的转移想清楚，可以这么考虑，定义dp[i][0/1]表示合法的长度到i最后一个选与不选，考虑dp[i][0]状态只能从dp[i - 1][1]和dp[i - 2][1]转移而来，dp[i][1]能从dp[i - 1][0]和dp[i - 1][1], dp[i - 2][1], dp[i - 3][1]转移而来，这里面最大的坑点在于发现只能对于dp[i - 1][0]的0转移而来，这点就是为了满足，对于任意的长度为3的数组的设定，这点还是非常坑的，至少我wa了好久才反应过来，还是好久没有做过dp的题目了，转移方程还是需要仔细想合理性的</details>
- <details><summary>tag</summary>质量场</details>
- spendtime: 100
- ACtime: 2023.11.2
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>367场周赛</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>整体的节奏都是前缀后缀存储这种套路，直接搞就可以了，依然写的很慢，但还行吧，只能说，手速场依然是无敌的手速怪们拿高分，很多天没打了，手速场依然稳定上分</details>
- <details><summary>tag</summary>手速场</details>
- spendtime: 33
- ACtime: 2023.10.15
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>366场周赛</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>不知道是我好久没有做题目了，手还是太生疏了，第二题读错了题目，还以为很复杂，第三题一个区间dp半天没看出来，第四题一个思维题目，看了好久才写出来，所以整体题目还是可以的，思维，dp，模拟都有了</details>
- <details><summary>tag</summary>质量场</details>
- spendtime: 90
- ACtime: 2023.10.8
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>365场周赛</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>修正了并查集的板子，周赛的题目倒是还行，第三题没有很快的反应过来，其实非常的简单</details>
- <details><summary>tag</summary>手速场</details>
- spendtime: 45
- ACtime: 2023.10.1
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>114场双周赛</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>第三题还是有点意思的，一个脑筋急转弯，《将数组分割成最多数目的子数组》与的性质需要好好的去猜，还是蛮不错的脑筋急转弯题目</details>
- <details><summary>tag</summary>手速场</details>
- spendtime: 37
- ACtime: 2023.9.30
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>统计树中的合法路径数目</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>来自364场周赛，就是质朴的树上dp，考虑子树的影响，考虑父节点的影响，时间写的确实久了，好久没写过类似的东西了，感觉手上的功夫确实还是慢了一些，好几次把抽象都能想错，确实还是稍微有些慢的，总结就是太慢了，想debug想了很久，卡我很久，本来又能再展前百雄风</details>
- <details><summary>tag</summary>中等质量场</details>
- spendtime: ？？
- ACtime: 2023.9.24
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>113场双周赛</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>T2这个题目还是可以的，wa了几发尝试了好几次，其实在于将相等的数值看成一个整体，分别和前后处理，诡异的贪心吧，其他的题目中规中矩</details>
- <details><summary>tag</summary>手速场</details>
- spendtime: ？？
- ACtime: 2023.9.17
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>完全子集的最大元素和</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>来自363场周赛，这一场还是很不错的质量场的，T2还是很不错的脑筋急转弯，这道题稍微的小卡我一点，有点愚蠢了属于是，改变一下枚举的方法即可，还是挺好玩的一个题目</details>
- <details><summary>tag</summary>小技巧</details>
- spendtime: ？？
- ACtime: 2023.9.17
- ACstatus: 不会
- score: 6
</details>
</details>

<details>
<summary>字符串转换</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>来自362场质量场周赛，这一场T2，T3也有很多坑，边角条件有点多，坑死我了，这是一个很好的题目，是一个组合拼接的题目，知识点有kmp或者z函数，矩阵快速幂，dp是知识点高浓度题目，考虑一个字符串中有几次出现，再考虑操作的转移，难度很大，非常难的题目</details>
- <details><summary>tag</summary>kmp+矩阵快速幂+dp</details>
- spendtime: ？？
- ACtime: 2023.9.10
- ACstatus: 不会
- score: 7
</details>
</details>

<details>
<summary>统计一个字符串的 k 子序列美丽值最大的数目</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>我写复杂了，用了集合dp，思路反反复复出错了好多次，其实用个数学办法就可以了，可以参考灵神的写法，还是可以的，太菜啦，来自112场周赛，vp了一把，输的一塌糊涂</details>
- <details><summary>tag</summary>数学ordp</details>
- spendtime: 45
- ACtime: 2023.9.4
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>边权重均等查询</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>强制我补了一下板子的一次，终于把lca在线算法补上了，来自361场质量场，题目整体还是不错的一场</details>
- <details><summary>tag</summary>lca</details>
- spendtime: ？？
- ACtime: 2023.9.3
- ACstatus: 不会
- score: 7
</details>
</details>

<details>
<summary>360场周赛</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>T3稍微想的有点久了，好久没写这样的贪心题目了，还是非常不错的，值得记录，2835. 使子序列的和等于目标的最少操作次数，这个题目，好贪心题目，第一次想错很多次，还是非常的尴尬的，本质上是大数拆小，小数合并这样的思路，不是很好写，wa好多发，T4应该是个我不太会的点，想着很麻烦，感觉应该用倍增的思路，倍增的想法一直都在脑海中，但是正式的实现才是这次，还是比较尴尬的，树上倍增，图上倍增，赛场上没往这方面想，找环一直是个麻烦的思路，迟迟未写才是对的，2836. 在传球游戏中最大化函数值，两个比较好的题目，还是不错的</details>
- <details><summary>tag</summary>质量场</details>
- spendtime: ？？
- ACtime: 2023.8.27
- ACstatus: 不会
- score: 7
</details>
</details>

<details>
<summary>111场双周赛</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>T4数位dp，感觉还是需要花时间写的,赛后虚拟了一把，感觉名次依然特别拉垮</details>
- <details><summary>tag</summary>手速场</details>
- spendtime: 39
- ACtime: 2023.8.20
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>359场周赛</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>别人手速好快啊，非常的离谱啊，真的太快了，自己写的好慢啊</details>
- <details><summary>tag</summary>手速场</details>
- spendtime: 39
- ACtime: 2023.8.20
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>358场周赛</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>感觉十分的码农，T4就是单调栈，快速幂，质因数分解，贪心的混杂题目，十分的长，而且单调栈的例子应该需要多一点，wa了一发才整明白，还行吧，就是手速太慢了</details>
- <details><summary>tag</summary>码农场</details>
- spendtime: 60
- ACtime: 2023.8.13
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>357场周赛</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>其实T3这种题目在lc上挺常见的，最近做少了，速度直接就下来了，赛时竟然没有写最优的，事后补了补过掉，一个多源最短路，都没一眼分析出来，然后在求最大方案，bfs写的也不够好，T4一眼不会，感觉是贪心去替换一下，但是没找到优雅的替换的基准，其实感觉是自己好久没写这样的优先级队列的了，其实很简单，感觉就是正确性在初次写的时候感觉很怪，不是特别有自信吧，先按照第一个纬度排序，然后考虑使得不同的种类增加的可能，只考虑可能种类数的，优先级维护一下第一纬度最小的即可，感觉这个思路蛮直观，就是在脑海里的正确性，比较玄学</details>
- <details><summary>tag</summary>质量场</details>
- spendtime: ？？
- ACtime: 2023.8.6
- ACstatus: 不会
- score: 7
</details>
</details>

<details>
<summary>110场双周赛</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>T4还是挺难的，想了蛮久的，不太会，按照第二个纬度生序排列确实没好好想，第二个是，没想到能用总体减去最大可能的值，第三个，就是没想到动态规划，我愿意称这个为最近遇到最好的动态规划题目，还是非常不错的</details>
- <details><summary>tag</summary>质量场</details>
- spendtime: ？？
- ACtime: 2023.8.5
- ACstatus: 不会
- score: 7
</details>
</details>

<details>
<summary>356场周赛</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>T4的数位dp写的时间太长了，但是整体的质量比较高，一遍过掉，感觉别人写得怎么那么快，可能是因为自己好久没有写数位dp这种东西了，像T3这种，原题的数据是比较长的，直接KMP吧，我看也是有一些人没有过掉的，总体来说，手稍微有点慢</details>
- <details><summary>tag</summary>中等质量场</details>
- spendtime: 60
- ACtime: 2023.7.30
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>355场周赛</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>T4搜了搜，给了一些灵感，简单来说就是，dfs记录路径即可，然后再枚举一个一个，前缀和看看结果就行，注意要枚举偶数情况和奇数情况，T3直接个不会，T2的技巧是从后往前，看了一下，有人确实不是能直观的想起来的，最近10场最难的一场，幸运的是T4出了，所以排名可观,T3真的思维了，排序之后，从前向后枚举即可，如果不够就合并入下一个，继续判断，确实思维，相当的思维</details>
- <details><summary>tag</summary>难度场</details>
- spendtime: 55
- ACtime: 2023.7.22
- ACstatus: 不完全会
- score: 7
</details>
</details>

<details>
<summary>109场双周赛</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>可能是在高铁上，手速场还是慢了的，很卡的，题目写得太慢了，尴尬的</details>
- <details><summary>tag</summary>手速场</details>
- spendtime: 38
- ACtime: 2023.7.22
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>354场周赛</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>wa了一个multiset清除的小点，十分窒息的，其余的还行，非常快速就出了，都没啥记忆点</details>
- <details><summary>tag</summary>手速场</details>
- spendtime: 31
- ACtime: 2023.7.16
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>353周赛</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>WA了一发之后，还是比较可惜的，不知道能不能上分上去，手速场真的是，没上去还的再经过几周，题目的话应该都没有啥太多的记忆点，很正常的题目</details>
- <details><summary>tag</summary>手速场</details>
- spendtime: 28
- ACtime: 2023.7.9
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>108场双周赛</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>依然写的很慢的一场手速场，不理解了，自己好慢啊</details>
- <details><summary>tag</summary>手速场</details>
- spendtime: 37
- ACtime: 2023.7.8
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>352场周赛</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>应该是很典的一场，没有什么太多好遗憾的，速度的话，感觉正常的典题场，只能这个速度了，快不上去了，很烦</details>
- <details><summary>tag</summary>典题场</details>
- spendtime: 33
- ACtime: 2023.7.2
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>351场周赛</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>在机场做了一下，感受了一下，题目要比昨天的T2好多了，T4也比昨天的好，所以如果打的话名次还是可以的，不会太丢人，感觉现在T2全玩这种思维题，div2的C题这种，以后看来是个很难搞的局面了，变得难打多了，注意__builtin_popcountll这个事情，其他的没啥</details>
- <details><summary>tag</summary>中规中矩场</details>
- spendtime: 50
- ACtime: 2023.6.25
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>107场双周赛</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这么多场以来，最惨败的一场，一个T2导致整场节奏垮掉，T4没有直接写最优时间复杂度算法，走偏了两次，直接tle，写得我没有脾气了，又回过头去写T2，写得最麻烦的算法，然后一个没注意，直接又T一次，这场本来是上分场，搞得是最近这么多场以来最痛的教训，T2其实是一种脑筋急转弯，不过我直到现在也不是很能特别get这种，用搜索去解了，注意时间复杂度即可，实际上还是得脑筋急转弯，太失败了</details>
- <details><summary>tag</summary>惨败场</details>
- spendtime: 120
- ACtime: 2023.6.25
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>350场周赛</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这场记忆点挺低的，vp了一把，54名，还行吧，不知道掉分不掉分，总体来说，就是第四题写的慢了点，还是想了想，这种题跟数据量很耦合的关系，就明白应该把什么作为状态了，还可以，我觉得打了肯定还是能再上分的，没打到也没啥可惜的</details>
- <details><summary>tag</summary>手速场</details>
- spendtime: 44
- ACtime: 2023.6.18
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>消除游戏</summary>

#### platform leetcode
#### id 390

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>要知道如何模拟是关键，确实打败我了，难搞啊，挺好的一个中等题目</details>
- <details><summary>tag</summary>脑筋急转弯模拟</details>
- spendtime: ？？
- ACtime: 2023.6.12
- ACstatus: 不会
- score: 5
</details>
</details>

<details>
<summary>349周赛</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>T2，T3，T4都不太好写，还是很不错的，值得点赞的一场，个人反思就是，板子不顺手，而且写的慢，反应也慢，T3没想清楚就直接写了，复杂度直接爆掉，还是应该好好思考的，T2wa了很多人，但我很强，没有wa，直接过</details>
- <details><summary>tag</summary>高质量场</details>
- spendtime: 73
- ACtime: 2023.6.10
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>106场双周赛</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>T3大胆写，T4大胆猜，庆祝我第一次第一页，祝贺祝贺，再次破纪录了</details>
- <details><summary>tag</summary>低质量场</details>
- spendtime: 30
- ACtime: 2023.6.10
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>至少有 K 个重复字符的最长子串</summary>

#### platform leetcode
#### id 395

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>写了个假算法先过了，分治递归的做法确实没有观察出来，失败的，滑动窗口的思路不太直观，个人还是首推分治递归，不错的中等</details>
- <details><summary>tag</summary>递归or滑窗</details>
- spendtime: ??
- ACtime: 2023.6.9
- ACstatus: 不会
- score: 5
</details>
</details>

<details>
<summary>348场周赛</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>vp了一把周赛，最后一道题目写得时间有点长，拆成最小表示的dp就行，前三个还是秒的，最后一个慢慢慢</details>
- <details><summary>tag</summary>中等质量场</details>
- spendtime: 60
- ACtime: 2023.6.6
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>根据身高重建队列</summary>

#### platform leetcode
#### id 406

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>还是可以的一个构造题目，非常的思维，还是让我想了一段时间，重点是如何暴力</details>
- <details><summary>tag</summary>思维</details>
- spendtime: 27
- ACtime: 2023.6.5
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>最小操作次数使数组元素相等</summary>

#### platform leetcode
#### id 453

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>最后结束的瞬间把公式推出来了，非常的卡我，卡了很长时间，公式能推出来实在是运气，没想到一个中等题目，竟然这么吃思维，最小值增加的数就是最终的数，列等式即可，(small + x) * n - sum = (n - 1) * x解即可</details>
- <details><summary>tag</summary>思维</details>
- spendtime: 60
- ACtime: 2023.5.29
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>347场周赛</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>总体觉得还是可以的，T3还是非常可以的，至少写了很长的时间，T4直观但是代码也是很长的</details>
- <details><summary>tag</summary>中等质量场</details>
- spendtime: 49
- ACtime: 2023.5.28
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>105场双周赛</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>最后一个写的慢了，前三个都很快，只可惜啊，没办法还是慢了一步，不过确实不知到wa在了哪里</details>
- <details><summary>tag</summary>手速场</details>
- spendtime: 34
- ACtime: 2023.5.27
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>我能赢吗</summary>

#### platform leetcode
#### id 464

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这个博弈的状态定义我是非常之疑惑的，不明白为啥当前的状态之和可以被去掉，不过能去掉也有合理的空间，因为是顺着搜索空间在搜，但是就是写的时候还是非常的质疑正确性的，没想明白，需要多看看</details>
- <details><summary>tag</summary>博弈</details>
- spendtime: ？？
- ACtime: 2023.5.25
- ACstatus: 不会
- score: 6
</details>
</details>

<details>
<summary>验证IP地址</summary>

#### platform leetcode
#### id 468

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>非常傻逼的题目，碰见了就哇哇哇吧，出成面试，整个人就崩溃了</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 30
- ACtime: 2023.5.23
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>修改图中的边权</summary>

#### platform leetcode
#### id 2699

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>首次完全不会了，直接愣住了，太难了吧，两次dj的解法非常的优雅，直接冲了，我觉得还是非常难的一道题目，如此题目还是应该多学多看，来自周赛346场，前三题手速狗已经赢麻了，我真垃圾</details>
- <details><summary>tag</summary>两次dj</details>
- spendtime: ？？
- ACtime: 2023.5.21
- ACstatus: 不会
- score: 7
</details>
</details>

<details>
<summary>最大或值</summary>

#### platform leetcode
#### id 2680

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>确实没想到的一个大离谱的题目，完全没想到，dp竟然是错的，震惊了，第二次被hack了，人生还是震惊的，没想到就应该贪心，还是非常容易走弯路的一个题目，十分的赞，竟然是贪心震惊</details>
- <details><summary>tag</summary>贪心</details>
- spendtime: 10
- ACtime: 2023.5.13
- ACstatus: 完全会-愣-完全会
- score: 6
</details>
</details>

<details>
<summary>不同岛屿的数量2</summary>

#### platform leetcode
#### id 711

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>来自启林的一场笔试题目，就是一个leetcode的会员题目，实在检查不出来哪里有问题了，debug不成喽，遂放弃，采用set存的形状</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: ？？
- ACtime: 2023.4.28
- ACstatus: 不知道哪里错了
- score: 7
</details>
</details>

<details>
<summary>345场周赛</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>更加纯纯手速了，手太慢啦</details>
- <details><summary>tag</summary>手速场</details>
- spendtime: 30
- ACtime: 2023.5.14
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>104场双周赛</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>纯纯手速场，脑子转得比较慢吧，手速应该能更快的，T4公式推一下就行，T3暴力dp即可</details>
- <details><summary>tag</summary>手速场</details>
- spendtime: 37
- ACtime: 2023.5.13
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>2023春季lcp个人赛</summary>
<details>
<summary>lrc</summary>

<details>
<summary>LCP 72. 补给马车</summary>

- <details><summary>comment</summary>签到题目</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 8
- ACtime: 2023.4.22
- ACstatus: 完全会
- score: 4
</details>

<details>
<summary>LCP 73. 探险营地</summary>

- <details><summary>comment</summary>麻烦一点的模拟</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 16
- ACtime: 2023.4.22
- ACstatus: 完全会
- score: 5
</details>

<details>
<summary>LCP 74. 最强祝福力场</summary>

- <details><summary>comment</summary>初次看，确实还是挺不好做的，但是好在数据量小，离散化一下就可以做了，也确实比较麻烦</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 40
- ACtime: 2023.4.22
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>LCP 75. 传送卷轴</summary>

- <details><summary>comment</summary>wa了好几发，不知道为什么数据一大我就老少1，实在是不是特别清楚，仔仔细细思考后，还是无法理解，可能就是玄学的题目吧，为什么并查集的结果要比答案少1呢？实在是不理解了，太难受了。</details>
- <details><summary>tag</summary>贪心+最短路</details>
- spendtime: 48
- ACtime: 2023.4.22
- ACstatus: 完全会
- score: 7
</details>

</details>
</details>
</details>

<details>
<summary>优美的排列</summary>

#### platform leetcode
#### id 526

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>评价一下，还是挺难的一个中等题目，dp的转移，其实挺不直观的，我觉得是我最近遇到的最难的一个中等题目，虽然我想到了用dp来做，但是状态转移一直是个问号，dp[i]表示i个被选的数放进了前num(i)个数里面的方案数，挺绕,回溯的方案我觉得更不太好，选数很绕</details>
- <details><summary>tag</summary>dp</details>
- spendtime: ？？
- ACtime: 2023.5.13
- ACstatus: 不会
- score: 6
</details>
</details>

<details>
<summary>344场周赛</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>还是比较手速的一场，后两道题目的手速稍微是有点慢的，所以名次不太好看，总体难度不大，中规中矩</details>
- <details><summary>tag</summary>手速场</details>
- spendtime: 35
- ACtime: 2023.5.7
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>移除盒子</summary>

#### platform leetcode
#### id 546

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>难度比较大的dp，看了下记录，2年前不会，现在依然不会，当然已经敏锐的察觉出来2维肯定做不了，这种情况一般都是需要加一个纬度，但是依然没有想出来状态怎么表示，dp[i][j][k]这是状态表示，需要好好解释，它的表示是对于区间i-j,当区间右边有k个和区间最右值相等的最大得分，这是很难的，转移建议看原生题解</details>
- <details><summary>tag</summary>dp</details>
- spendtime: ？？
- ACtime: 2023.5.6
- ACstatus: 不会
- score: 7
</details>
</details>

<details>
<summary>数青蛙</summary>

#### platform leetcode
#### id 1419

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接模拟计数即可，看了看2年前的提交记录，不禁感慨到，以前做了1个半小时，现在只用11分钟，那我还是进步很多了，题目还是很不错的题目，中等里面算是易错的题目了</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 11
- ACtime: 2023.5.6
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>343场周赛</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>第一题的翻译就是个鬼，傻逼翻译，后两道题就还可以，最后一个贪心，据说来自华为的机试，还是挺难挺不错的贪心题目，T3是一个也很不错的最短路，看数据范围，直接bfs就可以了，抽象注意细节就可以了，反正还是很麻烦的，整体来说，作为康复赛是一个非常好的选择</details>
- <details><summary>tag</summary>质量原题场</details>
- spendtime: 80
- ACtime: 2023.4.30
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>103场双周赛</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>再次突破名次记录，手速场上去了，不过感觉也就是今天人少而已，不过也还行，嫩牛的，前三题数据都在摆烂，第四题数据还ok，难度适中吧，想一下就出来了，只需要在脑中模拟一遍，优化一下即可,后来看了眼题解区，发现自己的做法异常出色，脑筋急转弯还是可以的</details>
- <details><summary>tag</summary>手速场</details>
- spendtime: 24
- ACtime: 2023.4.29
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>最优除法</summary>

#### platform leetcode
#### id 553

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>挺有意思的一个题目，数学解法很惊艳，其实也就是x/（y / z / ...）最大了，dp也行，也很清晰，数据范围放在这里，我就丑陋dp了，这种中等题目还是很有思维含量的</details>
- <details><summary>tag</summary>dpor数学</details>
- spendtime: 40
- ACtime: 2023.4.29
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>分割连接字符串</summary>

#### platform leetcode
#### id 555

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>没仔细看题，被wa麻木的一道题目，其实难度上还是挺可以的，需要静静思考4分钟-6分钟，以为是一个中等能秒出，还是大意了，简单来说，对于每一个字符内部都需要枚举分割点，正向和逆向都需要，所以整体上还是很可以的一个题目</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 20
- ACtime: 2023.4.29
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>出界的路径数</summary>

#### platform leetcode
#### id 576

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>脑子糊涂了，一个三维dp都没有看出来，真的愚蠢</details>
- <details><summary>tag</summary>dp</details>
- spendtime: ？？
- ACtime: 2023.4.24
- ACstatus: 看提示后会
- score: 6
</details>
</details>

<details>
<summary>342场周赛</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>翻车的一次手速，实打实的失败，彻彻底底的败了，最佳上分错过了，心态没稳住</details>
- <details><summary>tag</summary>手速场</details>
- spendtime: 41
- ACtime: 2023.4.23
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>341场周赛</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>最后一道题目我倒是写得有点慢，很码农，不过思路很清晰，先计算每个节点的访问次数，然后进行树上dp即可</details>
- <details><summary>tag</summary>手速场</details>
- spendtime: 38
- ACtime: 2023.4.16
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>102场双周赛</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>没有一道题不是手速，直接全部手速结束，没啥难的，全部暴力</details>
- <details><summary>tag</summary>手速场</details>
- spendtime: 26
- ACtime: 2023.4.15
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>段式回文</summary>

#### platform leetcode
#### id 1147

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>注意下状态的定义即可，不小心犯了个小错误，调了老半天，挺直观的一个题目，还行，就是状态只用定义长度即可，还比较简单</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 40
- ACtime: 2023.4.13
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>不含连续1的非负整数</summary>

#### platform leetcode
#### id 600

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>看了题解，跟斐波那契一样，表示疑问的巧合，另一方面，我觉得用折半dp做，还是可以的，dp分成两半即可，写不太好写，来自远古周赛我觉得缺失还可以的题目，也不是很难</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 50
- ACtime: 2023.4.11
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>标签验证器</summary>

#### platform leetcode
#### id 591

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>毒瘤一般的题目，真毒瘤中的毒瘤，泄愤，不明白，远古的高手们竟然有耐心写完，佩服佩服的，称之为超级无敌之巨烦琐大大大模拟，怎么会有这么复杂的题目，最最最复杂模拟，写了一半我就放弃了</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: ？？
- ACtime: 2023.4.10
- ACstatus: 不会
- score: 7
</details>
</details>

<details>
<summary>根据二叉树创建字符串</summary>

#### platform leetcode
#### id 606

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>像屎一样的题目描述，必须记下来泄愤，垃圾描述</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: ？？
- ACtime: 2023.4.10
- ACstatus: 不会
- score: 5
</details>
</details>

<details>
<summary>网格图中最少访问的格子数</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>遇事不对直接上线段树板子，幸好我的java还是有板子的，不过确实仔细一想，bfs+set也能写，来自340场周赛的最后一场的思路也行，评价一下340场周赛，我觉得题目整体还是可以的，是个质量场，四道题目都需要码力，还是可以的</details>
- <details><summary>tag</summary>线段树orbfs+set</details>
- spendtime: 37
- ACtime: 2023.4.9
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>任务调度器</summary>

#### platform leetcode
#### id 621

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>我最后采用了模拟的思路，也就是根据题意不断的去模拟，找最近的即可，但其实有个很关键的贪心过程我没贪出来，但是一开始的思路还比较对，也就是分两种情况，如果种类数小于n+1，那就是最大的长度*最大长度的次数+最大长度的次数，这个贪心在我一开始思考的时候，就出来了，但如果大于n+1,则直接就是任务的长度，这个是另一个贪心过程，这点我刚开始没有想清楚，造成了时间浪费很多，困扰很多，总体来说，这是一个贪心非常好的题目，在中等里面算top级别了。</details>
- <details><summary>tag</summary>模拟or贪心</details>
- spendtime: 150
- ACtime: 2023.4.8
- ACstatus: 写法不是最优
- score: 6
</details>
</details>

<details>
<summary>最少翻转操作数</summary>

#### platform leetcode
#### id 2612

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>我觉得还是非常难写的一个bfs+大模拟，这样的一个下标是非常容易写吐的，不知道为什么了，比赛时写出也不太可能，看了一下题解的写法，或许比较清晰明了吧，不过我写的常数项非常的大，不管了，就这样吧，反正就是下次遇到铁跪，来自339场周赛，前三题的手速还是可以的，就是最后一个就不太能赛时写出来了，只能这样了，题目我觉得还是个非常难写的东西的</details>
- <details><summary>tag</summary>模拟+bfs</details>
- spendtime: ??
- ACtime: 2023.4.5
- ACstatus: 写法不是最优
- score: 7
</details>
</details>

<details>
<summary>求解方程</summary>

#### platform leetcode
#### id 640

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>之后看了看题解区，觉得比较值得学习的技巧还是，把-变成+-，然后按照+切割处理，这样是最方便的，大模拟应该都去死</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 33
- ACtime: 2023.4.2
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>大礼包</summary>

#### platform leetcode
#### id 638

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>主要是时间复杂度我觉得会超标，但没想到它数据给的其实挺弱的，记忆化搜索是可以过去的，而且学了一招，vector是有重写比较符号的，这样使得vector可以用map存储，但它没有实现hash所以，不能被unordered_map存储，记忆化的方法比较套路，还算有收获的一道题目吧</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 40
- ACtime: 2023.4.2
- ACstatus: 不想写了
- score: 6
</details>
</details>

<details>
<summary>使子数组元素和相等</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>没想到T3难住我了，确实没有想到，百思不得其解啊，为啥很多人秒过，但是我卡了很久很久，后来看了一眼提示之后，后知后觉，其实算是一种推导式子，a[0]-a[k]-a[2*k]在一组，如果有循环的情况，就继续，使用个数组标记就可以，我失败了啊，果然最近中等题目做不出来了，来自101场双周赛，这场还是挺质量的，我瞬时排名瞬间前一百，还挺好的，但是因为我的不争气的第三题，失败了</details>
- <details><summary>tag</summary>分组</details>
- spendtime: ??
- ACtime: 2023.4.1
- ACstatus: 不会
- score: 6
</details>
</details>


<details>
<summary>使子数组元素和相等</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>没想到T3难住我了，确实没有想到，百思不得其解啊，为啥很多人秒过，但是我卡了很久很久，后来看了一眼提示之后，后知后觉，其实算是一种推导式子，a[0]-a[k]-a[2*k]在一组，如果有循环的情况，就继续，使用个数组标记就可以，我失败了啊，果然最近中等题目做不出来了，来自101场双周赛，这场还是挺质量的，我瞬时排名瞬间前一百，还挺好的，但是因为我的不争气的第三题，失败了</details>
- <details><summary>tag</summary>分组</details>
- spendtime: ??
- ACtime: 2023.4.1
- ACstatus: 不会
- score: 6
</details>
</details>

<details>
<summary>分割数组为连续子序列</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>没想到还是存在我不会的中等题目，题目的大意理解错了好几次，题面应该复述一遍，一个数组，从中整理出很多子序列，每个子序列要求不能小于3，并且是连续的，比较直观的就是用hash表和最小堆维护，最后检查所有的是否>=3即可，而贪心的策略是，如果之前有就加入之前的即可，而区别在于，当创建新的时候，必须确保后三个一定有，否则就是false，注意遍历的顺序，还是从原始数组的的从前向后，但是总结来说，还是hash+堆的方式更加可取</details>
- <details><summary>tag</summary>贪心</details>
- spendtime: 40
- ACtime: 2023.3.29
- ACstatus: 不会
- score: 6
</details>
</details>

<details>
<summary>收集树中金币</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>两遍拓扑排序，非常思维，来自338场周赛，应该是近10场来的最好的一道题目了，先根据叶子把不需要到达的叶子去掉，这是一遍拓扑，第二遍，去掉两层计算总长，一条边的代价是2，所以也决定了为什么从哪个根都是一样的，所以衍生为，其实k不只可以是2，其他的也可以，好题，因为我首次坐牢,（2023.9.21）再次写的时候，好多了，就是思路出的稍微慢点，但是好歹也是正解，终究是没有坐牢</details>
- <details><summary>tag</summary>思维</details>
- spendtime: 120
- ACtime: 2023.3.26
- ACstatus: 看提示后会
- score: 7
</details>
</details>

<details>
<summary>周赛337</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>无伤过四题，手速这场发挥是真不错，而且名次也打得非常好，还是可以的，初次写的代码还是十分ok的，感觉还可以，难得手速场发挥不错</details>
- <details><summary>tag</summary>手速场</details>
- spendtime: 28
- ACtime: 2023.3.19
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>双周赛100</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>双周赛100场，纯纯的手速场，第一题简直太困难了吧，什么sb题目，其他的写完就没印象了</details>
- <details><summary>tag</summary>手速场</details>
- spendtime: 35
- ACtime: 2023.3.18
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>骑士在棋盘上的概率</summary>

#### platform leetcode
#### id 688

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>是一道概率dp，一时间竟然没有反应出来，还想着大随机数模拟呢，结果十分可想而知，绕了一下终于反应过来，原来是一道概率dp，差点被绕过去了，作为中等题还是非常可以的</details>
- <details><summary>tag</summary>概率dp</details>
- spendtime: 40
- ACtime: 2023.3.10
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>分割数组使乘积互质</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>主要是复杂度高的不敢写，第一次写了个简单版本，果然t掉，后来加了都是质数的判断，结果逻辑没写对，后来修正了逻辑之后，过掉了，这是本场最麻烦的题目吧，非常的狗血，怎么手速那么快啊，来自335场手速场，T1，T2，T4都非常的典，一眼题目，T3还是写崩掉了</details>
- <details><summary>tag</summary>质数+模拟技巧</details>
- spendtime: 40
- ACtime: 2023.3.5
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>统计将重叠区间合并成组的方案数</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>贪心之后合并区间，然后取2次幂即可，来自99场双周赛，感觉还是很质量的啊，怎么写那么快</details>
- <details><summary>tag</summary>合并区间</details>
- spendtime: 17
- ACtime: 2023.3.4
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>统计可能的树根数目</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>还是挺不好想的，需要各种小细节，不明白为啥大家过的那么快啊，双周赛质量挺高的啊，不明白啊，很奇怪啊，难道是我写太慢了？</details>
- <details><summary>tag</summary>换根dp</details>
- spendtime: 37
- ACtime: 2023.3.4
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>买卖股票的最佳时机 II</summary>

#### platform leetcode
#### id 122

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>与714是相同的题目，就是差个条件</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 5
- ACtime: 2023.3.4
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>买卖股票的最佳时机含手续费</summary>

#### platform leetcode
#### id 714

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>不知道为啥dp没有反应过来，正解中贪心的思路也行，不过复用性不好</details>
- <details><summary>tag</summary>dp</details>
- spendtime: ？？
- ACtime: 2023.3.4
- ACstatus: 看提示后会
- score: 6
</details>
</details>

<details>
<summary>在网格图中访问一个格子的最少时间</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>就是优先级队列，或者叫成最短路也行，就是自己写的与别人的速度差好多，也许是vector的原因？不过从算法上来说，应该是对的，没啥太大的问题，还是可以的，算是比较中等的hard</details>
- <details><summary>tag</summary>最短路or优先级队列</details>
- spendtime: 25
- ACtime: 2023.2.26
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>求出最多标记下标</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>大坑题目，非常容易走到贪心的坑里面去，真的太坑了，本来觉得是一眼题目，结果wa之后仔细思考贪心的漏洞，非常的坑，非常的cf，我觉得这道题目确实不错的不错，很强，正确做法就是二分结果，然后再判定结果是否可行，这是最正确的做法，太坑了，面试中的杀手题目，来自334场周赛</details>
- <details><summary>tag</summary>思维+二分</details>
- spendtime: 22
- ACtime: 2023.2.26
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>最长重复子数组</summary>

#### platform leetcode
#### id 718

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>最长前缀dp，没反应过来，用字符串hash会被卡常数，所以第一次为了过掉偷懒，直接切java过掉，之后看了dp的做法是OK的，滑动窗口的思路不是很好，并不建议采用，dp还是思路清晰，题目在6-5分之间</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 35
- ACtime: 2023.2.23
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>删除注释</summary>

#### platform leetcode
#### id 722

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>走坑了，真的被走坑了，想直接对数组处理，不仅仅要记录很多的状态，条件，还有非常难想的case，看了许久恍然大悟，核心算法就是转成一行，拼接在一起，这样相当的好处理</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 60
- ACtime: 2023.2.2
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>333场周赛</summary>

#### platform leetcode
#### id 

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>后三道题目都非常质量可以，T2每次可以加一个2次幂，减一个二次幂，问把某一个数变为0需要最少的操作数，我用的动态规划直接解的，有些冗长，看了题解之后，可以使用dfs来考虑，对最小的位数进行考虑，要么加上去，要么减去，然后搜索，显然更加优雅，T3题目的动态规划能简单点，当然也是wa了好几下，将所有的质因子枚举，用二进制表示状态即可，注意1这个细节，题面是求有多少个子集满足质因子的指数只能小于等于1，所以dp直接做，T4同样非常的思维，一看觉得有些难，但是我直接上手写，发现贪心是可以过得去的，这么考虑，先初始为类似aaaa这样的序列，然后不断检查，不断调整，优先调整后面的字母，然后输出即可，就是特殊情况有些多，需要多几步人为的检查</details>
- <details><summary>tag</summary>质量场</details>
- spendtime: 84
- ACtime: 2023.2.19
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>98场双周赛</summary>

#### platform leetcode
#### id 

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>T4就是一个板子题目，c++一看不行，直接就切到java，还好规定时间内出来了，T2非常的思维，T3也非常的思维，题面分别是，得分的定义是最大距离和最小距离的求和，可以改变两个元素，求最小，T3是一堆数or，求最小不能or出来的数，还是可以的</details>
- <details><summary>tag</summary>手速场</details>
- spendtime: 53
- ACtime: 2023.2.18
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>打开转盘锁</summary>

#### platform leetcode
#### id 752

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接搜索算法即可，不是很困难，稍微写点代码的问题</details>
- <details><summary>tag</summary>bfs</details>
- spendtime: 25
- ACtime: 2023.2.14
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>金字塔转换矩阵</summary>

#### platform leetcode
#### id 756

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>其实算是被绕进去了，而且数据范围也就这样，也就是6，从写法上还是得相当的讲究，后来仔细想了想，状态根本没有想的那么的多，这么思考，直接把某一层看成是一个整数，其实也就10^6级别的数据，其实也还行，仔细设计下状态的转移应该不是件多难的事情，不过也算是中等里面虽然数据量非常的小，但是却能成功让我写这么长时间的题目了，搜索加了状态存储，快了很多，放在面试我就跪了</details>
- <details><summary>tag</summary>搜索</details>
- spendtime: 70
- ACtime: 2023.2.13
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>基本计算器 IV</summary>

#### platform leetcode
#### id 770

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>超级无敌大模拟，在lc上，算是top3级别的麻烦题目，超级无敌麻烦，在远古的周赛竟然有人能在那么短的时间内写出来，真的超级厉害，真的是无敌大模拟，一定跟它刚到底，不服输，这个题目直接可以改写为lab，真的是宇宙级别的麻烦，干！同样也是非常的无脑，也不需要太多的思维，特别是优先级的拆解，非常的麻烦</details>
- <details><summary>tag</summary>超级无敌大模拟</details>
- spendtime: 480
- ACtime: 2023.2.12
- ACstatus: 完全会
- score: 8
</details>
</details>

<details>
<summary>332场周赛</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>题目的质量和思维还是可以的，T2排个序会更好做，但是赛时并没有思考清楚，直接就数状数组了，所以耗费了时间，加上语言应用的并不习惯，所以速度慢了，T3枚举的方式也有问题，非要按照长度，那是真的没有必要，T4也非常简单，直观二分就可以，贪心的搞个前缀即可，整体来说，T2，T3，T4都还是可以的</details>
- <details><summary>tag</summary>质量场</details>
- spendtime: 120
- ACtime: 2023.2.12
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>331场周赛</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>T3本来算是一种二维dp，但是数据量来看，不能用直观的二维直接做，遂考虑二分，打家劫舍系列的不知道第几个了，使用二分里面再做dp即可，T4一道思维题，大概做了40分钟，卡得比较久，题面是交换两个数组的元素使得数组相等，代价是交换元素的最小值，先统计一判断一下可不可能，再将需要交换的元素统计出来，并且排序，现在一个数组被分成了两部分，一部分是不需要交换的，一部分是需要交换的，贪心去思考，用最小和最大的一起，可以使得代价最小，所以分别比较两个数组的最小值，谁小就用谁的，第二个要注意的点非常的关键，也可以使用非交换区的交换，不过代价*2，来达到交换任意两个的效果，所以后面的就不用看了，直接剩余的代价数乘最小可用来间接交换的数即可，这点才是重中之重，还是可以的题目</details>
- <details><summary>tag</summary>质量场</details>
- spendtime: 73
- ACtime: 2023.2.5
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>97场双周赛</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这场直接全盘wa飞了，直接全飞，真的窒息了，wa出了天际，创了wa的最高纪录，7次，直接跪，最后一道题目的坑点是非常多的，从并查集的应用到，左和下属于一起，右和上属于一起，再到8个方向的判定，边界，深深的让我感受到受挫，真的wa飞我，卡了一会还wa飞我，跪，写完一看就60个人不到，以为又可以前100，直接跪，还有T3，两个线段问题，忘了一个端点时候的最大了，T2是不能超过，忘记加等号了，所以wa得飞起来了,之后看了他们的hack点，用并查集需要修正的，坑点，超级大坑，巨大坑，大大大坑，真的，直接就被hack掉了，大坑，无敌大坑，太生气了，先需要判断是否可达，大坑，并查集也过不去，还是并查集写坑了，主要的原因在于，只能向右和向下，所以，用轮廓法还是更好，参考灵老,补了一下注意细节就好</details>
- <details><summary>tag</summary>思维（轮廓法）+贪心dp+模拟+模拟</details>
- spendtime: 65
- ACtime: 2023.2.4
- ACstatus: 不完全会
- score: 7
</details>
</details>

<details>
<summary>找到最终的安全状态</summary>

#### platform leetcode
#### id 802

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>刚开始被题意还是糊弄了一下，环就一定不是，还是拉胯了，后来调整了一下，再用了标记法去解决环的问题，所以，写的时间是长了点，不过最终还好，主要用拓扑排序也是优雅的</details>
- <details><summary>tag</summary>标记深搜or拓扑排序</details>
- spendtime: 45
- ACtime: 2023.2.4
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>删除被覆盖区间</summary>

#### platform leetcode
#### id 1288

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>如果数据量再大点就铁贪心了，这么小n^2也能过掉，当然就是不断学习，不断的强化的过程</details>
- <details><summary>tag</summary>贪心</details>
- spendtime: 15
- ACtime: 2023.2.4
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>丑数 II</summary>

#### platform leetcode
#### id 264

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>小题目还是折麽我很久，算是吃一堑长一智，觉得思路冲破的关键点就是dp，dp没冲上去，走了个偏路，好在数据量的问题，但是思考了一下也没办法出大，所以自己的搜索的方法也是ok的，dp还是可以的，算是小hard，远古时候的经典题还是要快速点，我看了下记录，lh就用25分钟，窒息了，我怎么这么菜啊</details>
- <details><summary>tag</summary>搜索ordpor最小堆</details>
- spendtime: 45
- ACtime: 2023.2.3
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>交替打印字符串</summary>

#### platform leetcode
#### id 1195

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>重新写了一遍多线程的东西，感觉这次理解好了，尝试用信号量的方式重写了一下，效率还是蛮高的，但是多线程的坑点依然存在，加强理解，加强判断，作为面试题目还是不错的</details>
- <details><summary>tag</summary>多线程</details>
- spendtime: 40
- ACtime: 2023.2.2
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>寻找重复的子树</summary>

#### platform leetcode
#### id 652

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>之前写过一遍，总体来说还是进步了，速度提升了一倍左右，当然仍然还是踩了一些坑的，写错了一次，写了个大dp，序列化方式也好，树的唯一表示也好O（n）的方法，这个进阶可能会问到，预期时间内写出来了就是成功</details>
- <details><summary>tag</summary>模拟ordpor序列化</details>
- spendtime: 35
- ACtime: 2023.1.31
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>适龄的朋友</summary>

#### platform leetcode
#### id 825

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这道题目作为远古时候的一场周赛题目，质量还是可以的，如果将这道题目作为面试题目，那我可能就要掉分，稍微有点卡壳我写的，还算有价值的中等题目</details>
- <details><summary>tag</summary>模拟+双指针</details>
- spendtime: 40
- ACtime: 2023.1.31
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>统计上升四元组</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>第一次写看错题目了，第二次才读对，就是一种枚举上需要花点技巧，没有那么容易一次编码写对，使用了数状数组维护，所以会有个小常数的多，但是语义还是清晰的，当然看正解是二维前缀和，感觉写起来和想起来比较难，故也不用太了解，优化的时候可以看看，330周赛的第二不错的题目</details>
- <details><summary>tag</summary>技巧枚举</details>
- spendtime: 45
- ACtime: 2023.1.30
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>将珠子放入背包中</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这一道题目直接决定思维的重要性，脑子直接被绕过去了，致最强思维题，绝密杀手题目，太厉害了，可以这么思考，首尾的值一定包含其中，则差值为零，分成k段需要k-1刀，每一刀可以产生w[i]+w[i+1]的收益，则将所有的值算出来，排序，选择最小的k-1和最大的k-1个即可，超强思维，dp思维直接被绕飞，330周赛中的最难题目</details>
- <details><summary>tag</summary>思维</details>
- spendtime: 30
- ACtime: 2023.1.30
- ACstatus: 不会
- score: 7
</details>
</details>

<details>
<summary>除法求值</summary>

#### platform leetcode
#### id 399

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>等式关系具有传递性，建图然后dfs去做即可，但稍微注意一下代码的写法，像我还是wa了一发才懂得，看了题解和大家的讨论之后，觉得这是最像hard的medium，我觉得确实能赶上一个小hard</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 30
- ACtime: 2023.1.29
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>打印零与奇偶数</summary>

#### platform leetcode
#### id 1116

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>感觉还是信号量的方式更加的优雅一些，当然也是可以加锁的，无锁结构需要不断的沉睡，面试出出来，估计得调试半天</details>
- <details><summary>tag</summary>多线程</details>
- spendtime: 50
- ACtime: 2023.1.27
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>用 Rand7() 实现 Rand10()</summary>

#### platform leetcode
#### id 470

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>面试杀手题目，稍微的有点不知所措了，规定时间内还是能写出来的，能骗样例，但是拒绝采样大法还是应该好好用起来,概率题目果然非常的思维</details>
- <details><summary>tag</summary>思维题目</details>
- spendtime: 30
- ACtime: 2023.1.26
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>下一个数</summary>

#### platform leetcode
#### id 面试题 05.04

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>面试杀手题目，真的太过分了吧，这直接完蛋，也不是说不会，而是wa太多，写的太麻烦，真的是面试杀手题目，思路在刚开始被引偏了好几次，值得被记忆的题目,一道中等题目还是应该给予困难的评分，主要是看所花时间</details>
- <details><summary>tag</summary>思维题目</details>
- spendtime: 60
- ACtime: 2023.1.25
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>329场周赛</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>T3成了全场最佳，刚开始看错了，以为两个都是异或，后来发现一个是异或，一个是或，直接搞错，结论就是，不要有一个全是0就可以，一个几行的思维题，被绕来绕去，感叹，还是思维慢慢啊，真的就离谱了，发挥得一坨屎啊，如何才能提高思维的敏锐性呢？</details>
- <details><summary>tag</summary>手速场</details>
- spendtime: 45
- ACtime: 2023.1.22
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>96场双周赛</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>虽然是手速场，但是绕绕我，我很生气，这发挥也太差劲了吧，T4一个数学结论，真的是把我写崩溃了，绕了好几发，T3也是不错的题目，T2真的弱智，T1咋数据范围这么大，边看手机边抢红包真的不行啊，又错过我的上分双周赛了</details>
- <details><summary>tag</summary>手速场</details>
- spendtime: 60
- ACtime: 2023.1.22
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>统计异或值在范围内的数对有多少</summary>

#### platform leetcode
#### id 1803

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>还是板子准备少了啊，直接现场写板子，这怎么能快呢？好久没有写字典树的东西了，对于区间异或的理解还是不到位，重新温习了一下，感觉还是不错的，是一个困难的题目，如果有板子的话，大概就是2分种的题目,正所谓有板子5分，无板子7分，后来仔细想了想hash的做法，觉得hash真的太牛了，代码风格极其简单，理解上稍微有些绕弯子但是总体上还是一种非常优秀的写法，值得反复学习</details>
- <details><summary>tag</summary>字典树</details>
- spendtime: 60
- ACtime: 2023.1.16
- ACstatus: 完全会
- score: 7
</details>
</details>

<details>
<summary>328场周赛</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这场t2挺裸的，但是二维差分确实很久没写了，太生疏了，所以搜了下，T3应该是全场最佳了，想的时间最久没有之一，理由还是自己对双指针的写法有点生疏了，努力挣扎了一下成功做出，就是T3把我坑了，本来是能进前百，结果只能前两百，T4虽然写的没有那么快，但是思路出的比较顺畅还算比较好，不过确实还是挺难的，综合评价，败笔是T3，生疏T2，中规中矩T4，难度分布还是可以的，这场还是质量场的</details>
- <details><summary>tag</summary>质量场</details>
- spendtime: 63
- ACtime: 2023.1.15
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>计数二进制子串</summary>

#### platform leetcode
#### id 696

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>稍微写错了一小下，觉得算是一道蛮有意思的简单题目，还是不错的，适合作为面试时候的小思维题</details>
- <details><summary>tag</summary>思维</details>
- spendtime: 10
- ACtime: 2023.1.9
- ACstatus: 完全会
- score: 4
</details>
</details>

<details>
<summary>最小栈</summary>

#### platform leetcode
#### id 155

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>非常适合作为面试题目，基本的容器的使用，包含思维上的快速实现，觉得还是值得记录一下的，很容易就想到了别的实现方法</details>
- <details><summary>tag</summary>思维</details>
- spendtime: 8
- ACtime: 2023.1.9
- ACstatus: 完全会
- score: 4
</details>
</details>

<details>
<summary>327场周赛</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>T4这个模拟我也是醉了，好久没有碰到这么难写的模拟了，确实，相当的难写，T3题其实也不错，但跟T4就远不能比了，这个大模拟，我只能说很牛，四个堆的倒来倒去，真的麻烦，好久没在规定时间之外写出来了，看来水平还是有所下降啊</details>
- <details><summary>tag</summary>质量场</details>
- spendtime: 150
- ACtime: 2023.1.8
- ACstatus: 完全会
- score: 7
</details>
</details>

<details>
<summary>重复叠加字符串匹配</summary>

#### platform leetcode
#### id 686

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这题是真的让我难受，直接就吐血了好吗，肯定是我哪里不优雅了，非常的烦人，觉得应该算是一个hard，虽然知识点是知道的，但是也没有好好用好，比较可惜，应该好好看一看的,再次写一遍的时候，依然花了半个小时，不过还是非常有进步的，一用string的find，还是有进步，以为自己写的相当不优雅，结果一看，都不优雅，自己的其实还好</details>
- <details><summary>tag</summary>kmp or hash</details>
- spendtime: ？？
- ACtime: 2023.1.7 - 2023.3.10
- ACstatus: 写得过于复杂 完全会
- score: 6
</details>
</details>

<details>
<summary>95场双周赛</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>我觉得我写的确实比较慢，感觉还是被绕了好久，T4不错，T3也不错，甚至T2和T1都没有读完题秒出，一次全过了，但是T4肯定还是有优化的余地的，log方还是不优雅，能排个130名。</details>
- <details><summary>tag</summary>质量场</details>
- spendtime: 45
- ACtime: 2023.1.7
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>搜索旋转排序数组 II</summary>

#### platform leetcode
#### id 81

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>二分去写的话，还是非常的麻烦的，不是很会写，觉得这种就直接循环，挺好的</details>
- <details><summary>tag</summary>二分</details>
- spendtime: 5
- ACtime: 2023.1.5
- ACstatus: 二分不会
- score: 5
</details>
</details>

<details>
<summary>下一个更大元素 II</summary>

#### platform leetcode
#### id 503

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>简单单调栈的示意题目，这么思考，对于循环的解决方案，通常都是再加一段，本题的方法类似的，再加一段即可</details>
- <details><summary>tag</summary>单调栈</details>
- spendtime: 18
- ACtime: 2023.1.4
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>递增的三元子序列</summary>

#### platform leetcode
#### id 334

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>好好关注下进阶的要求，直接上贪心，维护两个数就行，第一个和第二个，使得前两个数尽可能的小即可，直观在这道题上确实还行啊，速度</details>
- <details><summary>tag</summary>贪心</details>
- spendtime: 13
- ACtime: 2023.1.4
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>326场周赛</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这手速场简直了，非常离谱啊，当然有些小小坑，比如经过群友提示的T4，对于1的处理等等，这也太手速了吧</details>
- <details><summary>tag</summary>究极手速场</details>
- spendtime: 23
- ACtime: 2023.1.1
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>重建序列</summary>

#### platform leetcode
#### id 剑指 Offer II 115

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>有拓扑排序的思想在里面，但不一定要写成拓扑排序吧，三种判断的叠加即可，如果按照拓扑排序是不是还有点麻烦了？</details>
- <details><summary>tag</summary>模拟or拓扑排序</details>
- spendtime: 27
- ACtime: 2022.12.26
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>好分区的数目</summary>

#### platform leetcode
#### id contest 325

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>dp最近思考少了，像这种正难反易的dp，有点被绕糊涂，还是已经常看常练啊，我觉得这场其实质量还可以啊，都让我写蛮长时间的，是我慢了么？总体而言，T3可以，T2写了我很长，我觉得这场还是可以的</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 50
- ACtime: 2022.12.24
- ACstatus: 看提示后会
- score: 6
</details>
</details>

<details>
<summary>最小化两个数组中的最大值</summary>

#### platform leetcode
#### id biweekly contest 94

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>有点小崩溃，这道题目竟然写了那么久，直接导致了全局最慢，思路没有瞬间出来，首先是被读题害了一次，T411分钟就写完了，真没想到这个题花了这么久的时间，真没想到，嗨，真的是，要记录一下,T1,T2,T4都比较简单，T2读题是非常麻烦的，T1也一样麻烦，天杀的T3</details>
- <details><summary>tag</summary>二分</details>
- spendtime: 40
- ACtime: 2022.12.24
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>到达终点数字</summary>

#### platform leetcode
#### id 754

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>据说是大魔王级别的简单题目，来源于某一场周赛，确实很魔王，确实，非常数学，尝试了一些贪心想法，结果当然是有问题的，没办法，正儿八级数学最重要，再次做的时候，没有非常的卡壳我，应该20分钟以内做出来了，贪心去猜结论，只有相差为偶数的时候，可以使得某一个取反</details>
- <details><summary>tag</summary>数学or贪心</details>
- spendtime: 40 -> 20
- ACtime: 2022.12.24
- ACstatus: 不会 -> 会
- score: 5
</details>
</details>

<details>
<summary>和最小的 k 个数对</summary>

#### platform leetcode
#### id 剑指 Offer II 061

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>我是使用二分直接做的，总体来说，虽然代码量大一点，但是思路比较清晰，优先队列的做法，对思维的要求还是比较高的，特别是初始化加入的方法，看了之后还是觉得比较有意思的</details>
- <details><summary>tag</summary>优先队列or二分</details>
- spendtime: 30
- ACtime: 2022.12.22
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>字符串中的所有变位词</summary>

#### platform leetcode
#### id 剑指 Offer II 015

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>主要还是没有一眼看出更加精准的做法，而直接采用不态优的做法，温故而知新，要时刻记得基础要牢靠</details>
- <details><summary>tag</summary>双指针</details>
- spendtime: 17
- ACtime: 2022.12.21
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>全局倒置与局部倒置</summary>

#### platform leetcode
#### id 775

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>我很笨，所以直接数据结构了，其实是可以O（n）的，有点意思啊</details>
- <details><summary>tag</summary>思维？结论？数据结构</details>
- spendtime: 15
- ACtime: 2022.12.21
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>324场周赛</summary>

#### platform leetcode
#### id

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>稍微写的有点小慢，不过整体还可以的，可能一百多名快两百名吧，没啥特别注意的，就是T2还wa了一发，4的质因数和还是4，wa了一发，很生气</details>
- <details><summary>tag</summary>手速场</details>
- spendtime: 44
- ACtime: 2022.12.18
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>优美的排列 II</summary>

#### platform leetcode
#### id 667

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>构造题，就是看，想到了就能行，想不到就jj，不涉及任何的数据结构，适合面试的时候来一场小练</details>
- <details><summary>tag</summary>思维题</details>
- spendtime: 20
- ACtime: 2022.12.14
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>剑指 Offer 14- I. 剪绳子</summary>

#### platform leetcode
#### id 

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>dp,模拟，数学，做法还是很丰富的，感觉还是需要记记结论，非常的强大这道题目，考难了都得死掉</details>
- <details><summary>tag</summary>思维</details>
- spendtime: 25
- ACtime: 2022.12.13
- ACstatus: 正解更好
- score: 5
</details>
</details>

<details>
<summary>翻转卡片游戏</summary>

#### platform leetcode
#### id 

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>如果这道题目的数值范围变成10^5那我可能会被卡住，能写下去还不是因为数据范围比较小吗，主要还是一个思维，题目还是比较有意思的，看着正解还是愣了半天</details>
- <details><summary>tag</summary>思维</details>
- spendtime: 20
- ACtime: 2022.12.13
- ACstatus: 正解更好
- score: 5
</details>
</details>

<details>
<summary>让数组不相等的最小总代价</summary>

#### platform leetcode
#### id 

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这道题目算是lc上贪心在近半年左右遇到最难的了，灵老的代码很不错，风格非常的简洁，这道题目还是相当可以的，值得被反复这么，贪心过程的思考非常的难</details>
- <details><summary>tag</summary>贪心</details>
- spendtime: 90
- ACtime: 2022.12.11
- ACstatus: 不会
- score: 7
</details>
</details>

<details>
<summary>多数元素</summary>

#### platform leetcode
#### id 

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>收录一道简单题目，但确实对应典型的算法，对于寻找众数，直观上就想到hash，或者随机判断法，但是像这样的投票算法，还是非常的新鲜，记录一下，算法本身不直观，正确性的证明难以直观想到，不过总体上还行，值得知识储备下。</details>
- <details><summary>tag</summary>Boyer-Moore投票算法</details>
- spendtime: 0
- ACtime: 2022.12.11
- ACstatus: 不会
- score: 7
</details>
</details>

<details>
<summary>323场周赛</summary>

#### platform leetcode
#### id 

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>感觉自己的速度维持在了稳定的一个速度，很多次手速场基本就是30多分钟这个速度，不知道如何提高了，题目质量还行的一场，虽然手速，至少T4花了19分钟，还是有点长了，其他的总共18分钟，还是应该再快一点</details>
- <details><summary>tag</summary>手速场</details>
- spendtime: 37
- ACtime: 2022.12.11
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>93场双周赛</summary>

#### platform leetcode
#### id 

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这场真的可以啊，很久没见的双周赛又来了，T2,T3,还不错，T4我直接坐牢了，坐牢了70分钟？</details>
- <details><summary>tag</summary>难度场</details>
- spendtime: 150
- ACtime: 2022.12.11
- ACstatus: 不会
- score: 7
</details>
</details>

<details>
<summary>808</summary>

#### platform leetcode
#### id 

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>非常有杀伤力的一道题目，相当的不错，一个中等题目，竟然有cf的味道，竟然让我看得都愣住了，发现好题目，其实提示就是让我更加相信了二维dp的做法，还有尽可能的推导出来n越大是什么情况，这点还是比较不错的，没想到在尘封的题库里面，竟然有这么一道不错的题目，记录一下，中等最难了</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 60
- ACtime: 2022.12.10
- ACstatus: 看提示会
- score: 7
</details>
</details>

<details>
<summary>322场周赛</summary>

#### platform leetcode
#### id 

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>t4写的有点慢，t3刚开始读题读的有问题，整场发挥不太好，如果交的话，估计100多名吧，反正总体不佳，堪忧堪忧</details>
- <details><summary>tag</summary>质量场</details>
- spendtime: 68
- ACtime: 2022.12.4
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>321场周赛</summary>

#### platform leetcode
#### id 

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>T4不小心写慢了啊，我觉得T4难度还可以啊，怎么就这么多人过</details>
- <details><summary>tag</summary>手速场</details>
- spendtime: 36
- ACtime: 2022.11.27
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>92场双周赛</summary>

#### platform leetcode
#### id 

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>我看了我的时间进度，其实感觉还行，就是T1wa的太愚蠢了，看到别人齐刷刷的wa了，自己看了看无动于衷，真的太愚蠢了</details>
- <details><summary>tag</summary>手速场</details>
- spendtime: 33
- ACtime: 2022.11.26
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>320周赛</summary>

#### platform leetcode
#### id 

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>应该是还可以的一场，T4让我给糊涂了，自己写出来的已经不知道为啥正确了，主要就是一个前缀和优化的dp，总体上写的稍微有点绕，其他还行，细节我也是没控制好，wa了两发，T3据说也很坑，不过我没啥感觉，一遍而且很快就过了,最后又重新看了一下t4一些hack的数据，加了一些判断，总体来说，还是挺坑的，是一道恶心题目</details>
- <details><summary>tag</summary>质量场</details>
- spendtime: 45
- ACtime: 2022.11.20
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>319周赛</summary>

#### platform leetcode
#### id 

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>抄了个板子所以手速了一下，其余主要就是中规中矩吧，没有双周赛那么码农</details>
- <details><summary>tag</summary>手速场</details>
- spendtime: 31
- ACtime: 2022.11.13
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>91场双周赛</summary>

#### platform leetcode
#### id 

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>总体来说，T3和T4还是可以的，T4稍微有一点脑筋急转弯，不过线性判断即可，T3纯纯模拟，写了我巨长，因为在地铁上，所以没交到也正常，不过确实还是难度可以的一场</details>
- <details><summary>tag</summary>码农十分厚重</details>
- spendtime: 63
- ACtime: 2022.11.13
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>318周赛</summary>

#### platform leetcode
#### id 

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>t4难度还可以，我是wa了一发，幡然醒悟，真的是，太尴了，但是看大家做的情况不是很好，所以证明是一个难度还可以的题目，dp确实比较讲究，其中有很细微的地方第一次不一定能想得通</details>
- <details><summary>tag</summary>质量还可以</details>
- spendtime: 55
- ACtime: 2022.11.6
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>317周赛</summary>

#### platform leetcode
#### id 

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>t4出的很慢，只不过我觉得我的算法是不错的，不过实现起来确实有一些麻烦，算是一道不错的题目，t3也是写的比较傻，一个类似于数位dp的，还不错，t3,t4很不错呀</details>
- <details><summary>tag</summary>质量还可以</details>
- spendtime: 50
- ACtime: 2022.10.30
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>90双周赛</summary>

#### platform leetcode
#### id 6217

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>用分块走偏了，排个序其实就行嘛，手速场没发挥呀</details>
- <details><summary>tag</summary>手速场</details>
- spendtime: 38
- ACtime: 2022.10.23
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>316场周赛</summary>

#### platform leetcode
#### id 6217

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>手速场我反正写的比较慢，也不知道为啥，就是菜啦</details>
- <details><summary>tag</summary>手速场</details>
- spendtime: 40
- ACtime: 2022.10.23
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>221021天池-04. 意外惊喜</summary>

#### platform leetcode
#### id https://leetcode.cn/contest/tianchi2022/problems/tRZfIV/

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>非常难了，很难，但是题目代码非常简洁，第一次写分治dp，觉得也算是蛮有意思，首先要草出来的重要定理是这样的，如果一个选一部分，那么别的必然全选完或不选，这个结论是由数组是单调递增的得出来的，就相当于，对于一个做前缀dp，其余的是01背包，则有大量的重复计算，则使用分治法，分左右，左01背包，右计算，再右01背包，左计算，lc上难得的非常不错的题目</details>
- <details><summary>tag</summary>分治dp</details>
- spendtime: 120
- ACtime: 2022.10.22
- ACstatus: 不会
- score: 7
</details>
</details>

<details>
<summary>221021天池-03. 整理书架</summary>

#### platform leetcode
#### id (https://leetcode.cn/contest/tianchi2022/problems/ev2bru/)

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>不知道为啥没贪出来，感觉稍微有点质疑自己的做法？非常奇怪，就是贪心即可，挺深刻的，做了这么久的中等题目，记下来，很牛</details>
- <details><summary>tag</summary>贪心</details>
- spendtime: 90
- ACtime: 2022.10.22
- ACstatus: 不明白赛时啥问题
- score: 6
</details>
</details>

<details>
<summary>315场周赛</summary>

#### platform leetcode
#### id 6202

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接手速，挺快的，没啥印象深刻的题目</details>
- <details><summary>tag</summary>手速场</details>
- spendtime: 20
- ACtime: 2022.10.16
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>89场双周赛</summary>

#### platform leetcode
#### id 6202

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>t1也不错，正向模拟，t2一吓唬就容易使用前缀和+逆元，完全没有必要，t3使用二分即可，t4确实不错，至少让我非常的谨慎，虽然书写不需要多少时间，考虑到数据范围，枚举联通域内的大小即可，然后dfs判断一下即可，没交比较可惜，要不然2500以上了</details>
- <details><summary>tag</summary>题目质量不错</details>
- spendtime: 42
- ACtime: 2022.10.15
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>使用机器人打印字典序最小的字符串</summary>

#### platform leetcode
#### id 6202

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>比较不错的贪心，至少我觉得相当好，因为给我使绊子了，考虑这样子，写法也是经过了推敲的来的，先统计一下每种字符最后的结尾，然后从前往后，对于当前字符，先判断wait里面是否还有比起<= 的字符，加入，因为后面加入的一定是>= 当前字符的，指针向后移动即可，更新已经处理过的指针，非常不错的题目，值得反复回味，是314场周赛的T3</details>
- <details><summary>tag</summary>贪心</details>
- spendtime: 20
- ACtime: 2022.10.9
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>313场周赛</summary>

#### platform leetcode
#### id 6195

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>最后一题，稍微就是使用个n^2的dp预处理，而我使用了字符串hash，这点上不一样，其他对于状态的拆解，转移都是可以的</details>
- <details><summary>tag</summary>手速场</details>
- spendtime: 37
- ACtime: 2022.10.2
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>88双周赛</summary>

#### platform leetcode
#### id 6198

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>基本上没啥值得说的了，手速吧，第一道题成为这场最佳，想不错还真得注意写法</details>
- <details><summary>tag</summary>手速场</details>
- spendtime: 35
- ACtime: 2022.10.1
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>舒适的湿度</summary>

#### platform leetcode
#### id lcp65

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>很厉害的一道dp，dp[i][j]定义为前i个数中，与最低点的距离为j时，最大最小之差，转移不算难，但前提是要绝对的抽象模型，这种类型的dp非常的厉害，我是不会的，这种状态定义还是太妙了</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 180
- ACtime: 2022.9.29
- ACstatus: 不会
- score: 7
</details>
</details>

<details>
<summary>二叉树灯饰</summary>

#### platform leetcode
#### id lcp64

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>主要是状态定义先麻了一下，有四种，我刚开始写成了三种，全亮，全暗，只根节点亮，只根节点灭，转移也没好好想清楚，对于改变当前节点状态的有4种操作，1，2，3，123，对于不改变当前节点值的，12，23，13，什么都不做，即可，我觉得还是一道非常不错的dp</details>
- <details><summary>tag</summary>树形dp</details>
- spendtime: 240
- ACtime: 2022.9.26
- ACstatus: 不会
- score: 7
</details>
</details>

<details>
<summary>寻找重复的子树</summary>

#### platform leetcode
#### id 652

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>题目还不错的，一道中等题目胜过很多的hard题目，让我能写废，真的不简单，树的序列化，刚开始还是时间复杂度估计的有问题，后来，特别是优化的部分，非常的不错，是一道好题目</details>
- <details><summary>tag</summary>树的序列化</details>
- spendtime: 60
- ACtime: 2022.9.25
- ACstatus: 不会
- score: 6
</details>
</details>

<details>
<summary>周赛312</summary>

#### platform leetcode
#### id 6127

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>久违没有进前100了，还好，今日思路出的不是很卡可，并查集结合一下，T3题目真的是看题，看错了题就没办法，真的仔细读题</details>
- <details><summary>tag</summary>t4贪心来</details>
- spendtime: 39
- ACtime: 2022.9.25
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>周赛311</summary>

#### platform leetcode
#### id 6127

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>纯纯手速场，24min竟然排这名次，真的很难绷得住</details>
- <details><summary>tag</summary>手速场</details>
- spendtime: 24
- ACtime: 2022.9.18
- ACstatus:  完全会
- score: 6
</details>
</details>

<details>
<summary>双周赛87</summary>

#### platform leetcode
#### id 6127

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这场的最后一题可以，第一个这么麻烦也可以，是个很好的思维典范，很多大佬直接翻车，其实也就是这么考虑，对于val小于0的放入a数组，大于等于0的放入b，a求和的绝对值加上b中的最大值即可，然后再枚举a数组，找出剔除一个且最大的，脑筋急转弯啊啊啊啊啊，写得满了啊啊啊啊</details>
- <details><summary>tag</summary>脑筋急转弯</details>
- spendtime: 75
- ACtime: 2022.9.17
- ACstatus:  完全会
- score: 6
</details>
</details>

<details>
<summary>中国银联专场竞赛（2023届校园招聘专场）</summary>

#### platform leetcode
#### id 6127

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>前三题速度还可以，保持很好，最后一题直接写崩了，对于优先级队列的for循环遍历并不符合有序，我忘了，还是要不断的pop出来然后再add进去，写崩了，有点无奈，这么简单没写好，我能怎么办，过量的紧张和前面心情不好，直接恶化了表现力，算了吧，还是应该调整好心态，好心态才是竞技的必备</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 95
- ACtime: 2022.9.16
- ACstatus:  完全会
- score: 6
</details>
</details>

<details>
<summary>周赛310</summary>

#### platform leetcode
#### id 6127

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>板子没准备好啊，真的是，直接完蛋，浪费20分钟，傻逼，我真的太傻逼了，t3让我竟然想了一会，还不错，是个很合理的中等题目，想明白贪心过程即可</details>
- <details><summary>tag</summary>模拟+treemap贪心+区间最值查询（线段树）</details>
- spendtime: 43
- ACtime: 2022.9.11
- ACstatus:  完全会
- score: 6
</details>
</details>

<details>
<summary>周赛309</summary>

#### platform leetcode
#### id 6127

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>质量还不错，都需要好好想想，wa两发纯属不小心，看了看大家情况，看来wa是难免的，t4有个小细节，就是推迟的时候也要选择编号最小的，这个刚开始忘记了所以wa了</details>
- <details><summary>tag</summary>数论+脑筋急转弯+模拟</details>
- spendtime: 42
- ACtime: 2022.9.4
- ACstatus:  完全会
- score: 6
</details>
</details>

<details>
<summary>双周赛86</summary>

#### platform leetcode
#### id 6127

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>还行吧，就是wa了，四道题目也确实没啥太多记忆点，就是t4刚开始写了个二分交了，比较超时，后来又补充了一个双指针做法，才算满意，这也算是策略了</details>
- <details><summary>tag</summary>t4题意难读</details>
- spendtime: 35
- ACtime: 2022.9.3
- ACstatus:  完全会
- score: 6
</details>
</details>

<details>
<summary>周赛308</summary>

#### platform leetcode
#### id 6127

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>又打废了，好难过，毕竟wa了，非常的傻逼，妈的，题目怎么给重边呢？是真的傻逼，傻逼中的傻逼，瞬间窒息，之前的努力又白费了，好吧，又从开始慢慢来吧</details>
- <details><summary>tag</summary>t4拓扑序</details>
- spendtime: 120
- ACtime: 2022.8.22
- ACstatus:  完全会
- score: 6
</details>
</details>

<details>
<summary>2022九坤投资专场竞赛</summary>

#### platform leetcode
#### id 6127

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>第一道题是一个计数优化，hashmap存一下就行，然后忘记取模了，第二道题是一个dfs即可，第三道题作为中等题目难度还可以，首先求一下最大公约数，然后除过最大公约数，剩余的一定是2^x*3^y，如果不是则-1.如果是则取所有x,y最大的即可，刚开始错了一发，失败，第四题不会，之后看了日神写的，发现很牛逼，是一个非常困难的dp，我觉得不太好想，而且加入期望之后，不是那么好理解，按照我现在理解是这样的，先考虑状态怎么定义，直接用数组的值表示状态，dp[s]表示s到目标状态的期望值，当然dp[目标]=0，考虑转移，因为所有取出的筹码概率均等，而每走一步，需要多抽一次，所以每一轮，多kind的代价，除有效的局数就是答案，写了一版，直接tle，主要还是因为每次排序过于耗时间，所以日神写的真没错，每次对于相同值的操作是一样的，优化就是这点，很厉害，这道题目加入数学期望之后，竟然有点懵，所以还是可以的，值得记录，但是我还是没有看懂样例在讲什么</details>
- <details><summary>tag</summary>t4没看懂题目</details>
- spendtime: 120
- ACtime: 2022.8.22
- ACstatus: 不会
- score: 7
</details>
</details>

<details>
<summary>周赛307</summary>

#### platform leetcode
#### id 6127

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>倒开，最后一题，感觉不会，跳了过去，写前面三题，写得也不是多么快，但是，第二题把我错麻木了，想了好久不知道自己哪里错了，记录一下这个题目，《最大回文数字》，非常的坑比，非常的坑比，就是因为最后少个判断，把自己搭进去了，第三题的错误也是离谱，数组范围没开够，水逆啊，三道题直接1千名开外，太太失败，看了看最后一题，发现也比较简单，就是个维护最小堆，其中对于负数来说，可以加，也可以不加，对于正数来说，可以取，也可以不取，所以直接所有取绝对值排序就好了，从小到大，这是个技巧，另一个技巧我主观认为，第一次写了一个nklongk的复杂度，不过，又改了一下，将比较过程都改成归并排序，发现还是不过，再优化，改成数组，并且就使用两个数组，不分配，发现终于过了，但是，看了看题解，发现还是复杂了，又看，发现，其实取完绝对值排序之后，当新加入的数无法再更新最小的k个时候，直接就可以break，所以，时间复杂度就很小了，就是nlogn+klogk，非常的烦人，记录一下吧，《找出数组的第 K 大和》，据说面试原题</details>
- <details><summary>tag</summary>难度场</details>
- spendtime: 120
- ACtime: 2022.8.21
- ACstatus: 完全会
- score: 7
</details>
</details>

<details>
<summary>双周赛85</summary>

#### platform leetcode
#### id 6127

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>写崩了，错了两发，哎哎哎，就从上分场变成了掉分场，很难崩得住啊，唉，说是一个板子题，我得想想，线段树怎么用，什么线段树，命名逆序并查集就可以了，我真的傻逼</details>
- <details><summary>tag</summary>手速场</details>
- spendtime: 50
- ACtime: 2022.8.20
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>周赛306</summary>

#### platform leetcode
#### id 6127

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>我对于原题是真的无语，好端端的数位dp，搞什么原题呢？正常的数位dp不得调一会，真得烦，没有意义</details>
- <details><summary>tag</summary>原题场</details>
- spendtime: 50
- ACtime: 2022.8.14
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>周赛305</summary>

#### platform leetcode
#### id 6127

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>太手速了，最后两个题目都是dp，特别是T3，稍微想了一会，其中还理解错了一次题意，其实就是个线性dp</details>
- <details><summary>tag</summary>手速场</details>
- spendtime: 30
- ACtime: 2022.8.7
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>双周赛84</summary>

#### platform leetcode
#### id 6127

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>非常基础的前三道题目，最后一个题目是个脑筋急转弯，我想了十几分钟，有点慢，方案就是从后往前，range一定是逐渐变小，然后再考虑当一个数大于range时候，该怎么办，如果可以整除，则直接除，如果不可以，那么range的变化也非常直观，直接就x/(x/range+1)即可，太慢所以就不交了，就这样，这几次的周赛一直都不太好呀，没有很快，虽然也没啥不会</details>
- <details><summary>tag</summary>手速场+脑筋急转弯</details>
- spendtime: 40
- ACtime: 2022.8.6
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>疯狂游戏专场笔试</summary>

#### platform leetcode
#### id 6127

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接就是只能敲，完全不能复制粘贴，写得我太难受了，反正题再也找不到了，也无所谓了，都是模拟，很难绷得住</details>
- <details><summary>tag</summary>纯模拟</details>
- spendtime: 80
- ACtime: 2022.8.6
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>周赛304</summary>

#### platform leetcode
#### id 6127

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>挺扯的一场周赛，反正没交，事后交了一下，三个题都wa了，真的很难绷得住，好吧，还有第二题哪个，什么鬼玩意，妈的，烦得一批，什么周赛，比手速啊啊？？</details>
- <details><summary>tag</summary>手速场</details>
- spendtime: 50
- ACtime: 2022.7.31
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>周赛303</summary>

#### platform leetcode
#### id 6127

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这里只评价一下T4,非常的无奈，罚坐了五十分钟，最后终于写了出来，脑筋急转弯啊，应该尝试写一两个case去试试的，结果发现了惊人的结论，假设有两个数，nums1,nums2,其二进制位数为x+y,则其and和or之和就为x+y,我是真的傻逼了，无奈，还想着用什么优化技巧呢</details>
- <details><summary>tag</summary>脑筋急转弯</details>
- spendtime: 70
- ACtime: 2022.7.24
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>双周赛83</summary>

#### platform leetcode
#### id 6131

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>草，整个题目最值得评价的就是，脑筋急转弯，草，没有很快速的想出来，唉，与前排失之交臂啊，做法也比较简单，就是想要形成各种不同的子序列，需要在原始数组上形成[1-k][1-k]...这样的，我刚开始以为[1-k]中的顺序也比较重要，后来发现，内部的顺序无关，直接hashset直接存，脑筋急转弯，啊啊啊啊</details>
- <details><summary>tag</summary>模拟+计数+数据结构+脑筋急转弯</details>
- spendtime: 45
- ACtime: 2022.7.23
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>周赛302</summary>

#### platform leetcode
#### id 无

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>比较简单，每个题几乎没有思考时间，直接就出来了，所以整个集合，具体题目不配出现</details>
- <details><summary>tag</summary>模拟+计数+模拟+分析</details>
- spendtime: 27
- ACtime: 2022.7.17
- ACstatus: 完全会
- score: 5
</details>
</details>


<details>
<summary>统计理想数组的数目</summary>

#### platform leetcode
#### id 6115

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>为什要分解质因数，因为每次增2以上的倍数才会有变化，所以要分解质因数，那么计算的是什么，计算的是f(i)表示长度为i且最大值为i的方案数，一个数可以表示为(p1,x1)(p2,x2)(...)对于一种质因子来说，要把x1个数放到n个盒子里面，隔板法, C(n-1+x1,x1)即可，而每一种因数的放置方法是独立的，所以乘起来即可，是一个数学问题的衍生，还不错，没想到受了数论那么多轮的暴虐，还是依然失败在数论上了，唉，太菜</details>
- <details><summary>tag</summary>分解质因数+组合数学</details>
- spendtime: 55
- ACtime: 2022.7.10
- ACstatus: 不会
- score: 6
</details>
</details>

<details>
<summary>移动片段得到字符串</summary>

#### platform leetcode
#### id 6114

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>一个线性的模拟，这种模拟我的评价都是还不错的题目，简单，考察一点点思维，有一点点门槛，特别是面试的时候，比较紧张的情况下</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 10
- ACtime: 2022.7.10
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>无限集中的最小数字</summary>

#### platform leetcode
#### id 6113

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接内置数据结构了</details>
- <details><summary>tag</summary>内置数据结构</details>
- spendtime: 5
- ACtime: 2022.7.10
- ACstatus: 完全会
- score: 3
</details>
</details>

<details>
<summary>装满杯子需要的最短总时长</summary>

#### platform leetcode
#### id 6112

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接模拟挺好，数据量反正小</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 5
- ACtime: 2022.7.10
- ACstatus: 完全会
- score: 3
</details>
</details>

<details>
<summary>坐上公交的最晚时间</summary>

#### platform leetcode
#### id 6117

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>虽然在我心里，只要是我会的题目，不会轻易的整到6分这个难度，但是看了评论发现这是这场难度最大的，所以还是勉强标个6吧，这题一遍就过，感觉问题一点都不大，思路就是，先预处理，原本的情况，再考虑加入，加入时切记判断不可重复，最晚的时间点一定出现在所有的buses的时间点上，或者乘客的前后两个位置，所以后来写写也挺丝滑的</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 50
- ACtime: 2022.7.10
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>元素值大于变化阈值的子数组</summary>

#### platform leetcode
#### id 6119

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>有点被晃过去的感觉，非常的不爽，好久没在lc平台上遇到不会的题目了，这题确实是我有点不懂脑子，并查集这么板子的题目没有看出来，当然单调栈也是可以的，做法就是看每个元素的影响力，算出对于一个i来说，[i-x,i+y]这个区间内部，最小值是nums[i]，然后一一判断，不过从时间上来说，就是比并查集要慢，其中得益于贪心的概率性高效</details>
- <details><summary>tag</summary>并查集</details>
- spendtime: 100
- ACtime: 2022.7.10
- ACstatus: 不会
- score: 6
</details>
</details>

<details>
<summary>计算布尔二叉树的值</summary>

#### platform leetcode
#### id 6116

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>按照题目的意思写出来即可</details>
- <details><summary>tag</summary>递归</details>
- spendtime: 7
- ACtime: 2022.7.10
- ACstatus: 完全会
- score: 3
</details>
</details>

<details>
<summary>最小差值平方和</summary>

#### platform leetcode
#### id 6118

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>好久没写这种题目了，非常的麻烦，写得我都不想写，就这样吧，反正就是二分出来，一个可行的下界，然后从后往前，一个减一</details>
- <details><summary>tag</summary>二分</details>
- spendtime: 47
- ACtime: 2022.7.10
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>2022 年度杭州未来科技城数字经济人才编程大赛</summary>

#### platform leetcode
#### id zj-future04. 门店商品调配 zj-future03. 快递中转站选址 zj-future02. 黑白棋游戏 zj-future01. 信号接收

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这场做的比较扯，最后一道题用随机大法，T3写了个n^3的做法，非常的愚蠢，T1和T2是简单的题目，怎么做都可以，这里补充一下T3的正解，因为x，y轴是独立的，所以直接分别求中位数即可，我还是有点愚蠢了，第三题dp的做法吧，怎么说呢，时间上没有随机大法好，写起来也不是很容易，不过思想可以学一学，就是将子集之和为0这个条件，不断的使用，比如当一个集合s==0，对于s的所有子集来说，i，s^i,为0时，就可以化为更小的，所以，转移清楚了，对于一个s==0来说，dp[s]=｜s｜-1，这个才是关键，其他的没啥</details>
- <details><summary>tag</summary>dp 中位数 前缀和 模拟</details>
- spendtime: 60
- ACtime: 2022.7.7
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>解密消息</summary>

#### platform leetcode
#### id 6111

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>模拟好讨厌</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 8
- ACtime: 2022.7.3
- ACstatus: 完全会
- score: 4
</details>
</details>

<details>
<summary>螺旋矩阵 IV</summary>

#### platform leetcode
#### id 6111

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>模拟吧</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 13
- ACtime: 2022.7.3
- ACstatus: 完全会
- score: 4
</details>
</details>

<details>
<summary>知道秘密的人数</summary>

#### platform leetcode
#### id 6109

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>可以滑窗优化也可以直接暴力</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 13
- ACtime: 2022.7.3
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>网格图中递增路径的数目</summary>

#### platform leetcode
#### id 6110

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>整个图是个DAG，直接dp，从根出发即可</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 14
- ACtime: 2022.7.3
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>判断矩阵是否是一个 X 矩阵</summary>

#### platform leetcode
#### id 6101

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>感觉这没啥好说的，直接模拟吧</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 7
- ACtime: 2022.6.26
- ACstatus: 完全会
- score: 3
</details>
</details>

<details>
<summary>统计放置房子的方式数</summary>

#### platform leetcode
#### id 6100

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>相当于在n个数中取k个不相邻的数，相当于在n-k+1中取k个数，又小小的学到一招，其实放在这个位置我还是比较诧异的，这样的组合数学知识，已经变成了t2位置的题目了吗？</details>
- <details><summary>tag</summary>组合数学</details>
- spendtime: 10
- ACtime: 2022.6.26
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>拼接数组的最大分数</summary>

#### platform leetcode
#### id 6103

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>可以这么考虑，定义状态为dp[i][0/1]表示，长度为i时，只是用nums1和使用nums2替换的最大值，则转移就很容易的出来了，所以形如ABA这种，从后往前枚举即可，我觉得还算一个很不错的轻量级dp</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 10
- ACtime: 2022.6.26
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>从树中删除边的最小分数</summary>

#### platform leetcode
#### id 6103

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>写的比较长，比较费时间，也不知道为啥别人写那么快，枚举删除的边对即可，事先用dfs将子数的异或结果进行存储，特别注意一种特殊情况，当要删除的边对是一种包含关系，即到x有个path，到y有个path，x的path包含y的path，这种情况删除的时候需要注意</details>
- <details><summary>tag</summary>枚举</details>
- spendtime: 45
- ACtime: 2022.6.26
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>统计星号</summary>

#### platform leetcode
#### id 6104

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>看清楚题目，直接模拟即可</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 8
- ACtime: 2022.6.25
- ACstatus: 完全会
- score: 3
</details>
</details>

<details>
<summary>统计无向图中无法互相到达点对数</summary>

#### platform leetcode
#### id 6106

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>并查集的板子已经这么不值钱了吗</details>
- <details><summary>tag</summary>并查集</details>
- spendtime: 10
- ACtime: 2022.6.25
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>操作后的最大异或和</summary>

#### platform leetcode
#### id 6107

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>把题审清楚，a&(a^x)的效果就是可以使得a的任意位清零，所以直接统计所有的数位，有1即可，算是一个需要想清楚的题目</details>
- <details><summary>tag</summary>化简</details>
- spendtime: 15
- ACtime: 2022.6.25
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>不同骰子序列的数目</summary>

#### platform leetcode
#### id 6107

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>dp[i][j]表示最以(i,j)结尾的方案数，转移即可，想了一会，好失败啊</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 25
- ACtime: 2022.6.25
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>二叉搜索树染色</summary>

#### platform leetcode
#### id lcp52

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>逆着来，用个treeset直接删除，要么就改改线段树，使用线段min模版，线段树实现区间赋值也不难，不过当时是真的没想到</details>
- <details><summary>tag</summary>脑筋急转弯or线段树</details>
- spendtime: 70（赛时）or 10（赛后）
- ACtime: 2022.6.22
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>2022顺丰竞赛</summary>

#### platform leetcode
#### id xxxx

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>挺垃圾的，一言难尽了</details>
- <details><summary>tag</summary>模拟+并查集+射线法判断点收否在多边形内部+拓扑排序</details>
- spendtime: 60
- ACtime: 2022.6.19
- ACstatus: 完全会
- score: 4
</details>
</details>

<details>
<summary>兼具大小写的最好英文字母</summary>

#### platform leetcode
#### id 5242

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>模拟即可</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 5
- ACtime: 2022.6.19
- ACstatus: 完全会
- score: 3
</details>
</details>

<details>
<summary>个位数字为 K 的整数之和</summary>

#### platform leetcode
#### id 5218

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>wa了一次，程序bug啊，错一次才明白</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 20
- ACtime: 2022.6.19
- ACstatus: 完全会
- score: 4
</details>
</details>

<details>
<summary>小于等于 K 的最长二进制子序列</summary>

#### platform leetcode
#### id 6099

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>虽然作为一个中等题目，但是打眼一看就没想出算法，但是T4打眼一看，思路就出了，可能其中的中间细节没有，但是至少有个思路，这题一看不会，跳过了，把其他题看过之后才做，重新回过头看的时候，瞬间就反应过来了，原来是一道dp，从后往前，记录长度为i的子序列能达到的最小值即可，进行迭代</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 20
- ACtime: 2022.6.19
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>卖木头块</summary>

#### platform leetcode
#### id 5254

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>不小心写错了，真的崩不住了，很惊险，原因就是分割成两部分，其实也是两个子问题，刚开始没有想清楚，导致wa一次，而且吓了自己一下，怎么就没想清楚呢，敲敲自己的脑袋</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 50
- ACtime: 2022.6.19
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>计算应缴税款总额</summary>

#### platform leetcode
#### id 5259

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>模拟即可</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 6
- ACtime: 2022.6.12
- ACstatus: 完全会
- score: 3
</details>
</details>


<details>
<summary>网格中的最小路径代价</summary>

#### platform leetcode
#### id 5270

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>dp很简单</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 15
- ACtime: 2022.6.12
- ACstatus: 完全会
- score: 4
</details>
</details>

<details>
<summary>公平分发饼干</summary>

#### platform leetcode
#### id 5289

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这道题目，和1723相同，我看了记录，竟然没有，怪不得想不起来了，然后这道题目因为数据范围很小，所以，枚举下排列顺序再判断也可做，当然正解还是应该枚举子集的子集，转移起来更好</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 32
- ACtime: 2022.6.12
- ACstatus: 正解更好
- score: 6
</details>
</details>

<details>
<summary>公司命名</summary>

#### platform leetcode
#### id 6094

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>稍微想的时间久了一点，主要纠结于怎么样子加速计算，经过几次想法验证的失败之后，才顺利的想出来，这么考虑，对于首字母来说，要保证交换之后没有，举例，(a+x,f+y)来说，要求(f+x,a+y)不存在，则可以依照这样的掩码规则分组，对于dp[x][y]表示x为真且不含y的数目，转移就很顺利的写出</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 32
- ACtime: 2022.6.12
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>强密码检验器 II</summary>

#### platform leetcode
#### id 6095

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>麻烦点稳过就行</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 7
- ACtime: 2022.6.11
- ACstatus: 完全会
- score: 3
</details>
</details>

<details>
<summary>咒语和药水的成功对数</summary>

#### platform leetcode
#### id 6096

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这周二分的天下？</details>
- <details><summary>tag</summary>二分</details>
- spendtime: 4
- ACtime: 2022.6.11
- ACstatus: 完全会
- score: 4
</details>
</details>

<details>
<summary>替换字符后匹配</summary>

#### platform leetcode
#### id 6097

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>逆着匹配即可</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 10
- ACtime: 2022.6.11
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>统计得分小于 K 的子数组数目</summary>

#### platform leetcode
#### id 6098

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>作为T4也真的是太水了吧</details>
- <details><summary>tag</summary>二分</details>
- spendtime: 10
- ACtime: 2022.6.11
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>极大极小游戏</summary>

#### platform leetcode
#### id 6090

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>模拟模拟</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 3
- ACtime: 2022.6.5
- ACstatus: 完全会
- score: 3
</details>
</details>

<details>
<summary>划分数组使最大差为 K</summary>

#### platform leetcode
#### id 6091

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接排序搞</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 5
- ACtime: 2022.6.5
- ACstatus: 完全会
- score: 4
</details>
</details>

<details>
<summary>替换数组中的元素</summary>

#### platform leetcode
#### id 6092

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接hash</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 7
- ACtime: 2022.6.5
- ACstatus: 完全会
- score: 4
</details>
</details>

<details>
<summary>设计一个文本编辑器</summary>

#### platform leetcode
#### id 6093

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>两个双向链表</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 13
- ACtime: 2022.6.5
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>以组为单位订音乐会的门票</summary>

#### platform leetcode
#### id 10011

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>分块吧，我不想想别的，代码量大，不需要想</details>
- <details><summary>tag</summary>分块</details>
- spendtime: 120
- ACtime: 2022.5.29
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>重排字符形成目标字符串</summary>

#### platform leetcode
#### id 6078

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接模拟吧</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 4
- ACtime: 2022.5.29
- ACstatus: 完全会
- score: 3
</details>
</details>

<details>
<summary>价格减免</summary>

#### platform leetcode
#### id 6079

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接模拟</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 15
- ACtime: 2022.5.29
- ACstatus: 完全会
- score: 4
</details>
</details>

<details>
<summary>到达角落需要移除障碍物的最小数目</summary>

#### platform leetcode
#### id 6081

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接建图即可，没啥难度</details>
- <details><summary>tag</summary>建图+最短路</details>
- spendtime: 10
- ACtime: 2022.5.29
- ACstatus: 完全会
- score: 5
</details>
</details>


<details>
<summary>使数组按非递减顺序排列</summary>

#### platform leetcode
#### id 6080

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>t3比t4难的一次，要记录一下，这个单调栈很神奇，虽然反应过来是单调栈，但是无奈功底还是不够，思考，维护栈中向后删除的最大元素的个数即可，清晰的转移即可，思维很高的题目，记录一下</details>
- <details><summary>tag</summary>单调栈</details>
- spendtime: 70
- ACtime: 2022.5.29
- ACstatus: 不会
- score: 6
</details>
</details>

<details>
<summary>巫师的总力量和</summary>

#### platform leetcode
#### id 6077

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>lc终于出了一个我认为很难的题目，主要就是，中间的数学化简技巧很可以，第二方面是，关于管辖范围左闭又开这个细节，当然对于双求和，那块的数学技巧也是可以的，不好调试，非常的考验心态，我承认我可能自己无法独立完成，这道题目将单调栈和前缀和的前缀和混合来考，我挂了，挂的明明白白，单调栈我又抛之于脑后，我很不爽，又在尝试用treemap在哪里乱拼凑，总之失败还是挺大的，好题目，真的是中等技巧叠加的好题目，举一反三</details>
- <details><summary>tag</summary>单调栈+前缀和的前缀和</details>
- spendtime: 180
- ACtime: 2022.5.22
- ACstatus: 不会
- score: 7
</details>
</details>

<details>
<summary>毯子覆盖的最多白色砖块数</summary>

#### platform leetcode
#### id 2271

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>存个前缀和，再来个treemap即可</details>
- <details><summary>tag</summary>二分</details>
- spendtime: 17
- ACtime: 2022.5.16
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>不含特殊楼层的最大连续楼层数</summary>

#### platform leetcode
#### id 2274

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>把所有区间都求一遍即可</details>
- <details><summary>tag</summary>枚举</details>
- spendtime: 8
- ACtime: 2022.5.16
- ACstatus: 完全会
- score: 4
</details>
</details>

<details>
<summary>按位与结果大于零的最长组合</summary>

#### platform leetcode
#### id 2275

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>枚举那一位即可</details>
- <details><summary>tag</summary>枚举</details>
- spendtime: 8
- ACtime: 2022.5.16
- ACstatus: 完全会
- score: 4
</details>
</details>

<details>
<summary>统计区间中的整数数目</summary>

#### platform leetcode
#### id 6066

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>就是每次看到区间合并之类的就不想动了，这次写的应该算是个很漂亮的代码了，虽然确实我左想右想不想写复杂代码，好在一遍就过掉了，还是不错的，区间合并还是得仔细想想，倒也不难就是，可能从逻辑到代码之间的鸿沟还是有的，稍微写久了，存储区间可以用左端点，后来还是觉得把所有端点都放在平衡树上会带来代码的复杂度，所以就放弃了，然后在这个基础上，操作，就会容易很多</details>
- <details><summary>tag</summary>区间合并</details>
- spendtime: 70
- ACtime: 2022.5.16
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>最大波动的子字符串</summary>

#### platform leetcode
#### id 6069

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>枚举所有的字母对即可，对于(x,y)来说，x看成1,y看成0，则得到了一个01的数组，则需要求出对于任意的端点来说，(i-j+1-(a[j]-a[i-1])-(a[j]-a[i-1]))的绝对值的最大，其中还要在每一个子区间内保证01都的出现，则dp[i][0]表示包含0的最小值，dp[i][1]包含1的最大值，dp[i][2],dp[i][3]同理，转移的时候比较注意，想完全点，我还是太菜，没有把逻辑理通顺</details>
- <details><summary>tag</summary>dp</details>
- spendtime: ?? 可能有点长
- ACtime: 2022.5.15
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>字符串中最大的 3 位相同数字</summary>

#### platform leetcode
#### id 6056

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>模拟了，签到了</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 5
- ACtime: 2022.5.8
- ACstatus: 完全会
- score: 3
</details>
</details>

<details>
<summary>统计值等于子树平均值的节点数</summary>

#### platform leetcode
#### id 6057

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>dfs即可</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 5
- ACtime: 2022.5.8
- ACstatus: 完全会
- score: 4
</details>
</details>

<details>
<summary>统计打字方案数</summary>

#### platform leetcode
#### id 6058

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>线性dp即可，逐渐累加</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 10
- ACtime: 2022.5.8
- ACstatus: 完全会
- score: 4
</details>
</details>

<details>
<summary>检查是否有合法括号字符串路径</summary>

#### platform leetcode
#### id 6059

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>记录一个左括号数即可，然后慢慢迭代，dp[i][j][k],三维dp就够了</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 16
- ACtime: 2022.5.8
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>字符串的总引力</summary>

#### platform leetcode
#### id 6050

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>记录一下，依然翻车，已经不知道该说什么好了,罚自己很久的抑郁，好像自己每次一整这种重复字串或者，类似的就会状态想不清楚，推导不了了，其实是个很容易想明白的事情，但陷入的思维漩涡，想走出来，还真的不是很容易，我们可以使用dp[i]表示，以i结尾的所有子字符串吸引力，则dp[i]和dp[i-1]之间的递推为，若之前没有相同的，则直接相当于所有的加1，即加i+1，若之前有形如,..a....a...,则与dp[i-1]的关系是，直到第一个相同的之间每个加1，即加i-上一个a的索引，即可</details>
- <details><summary>tag</summary>脑筋急转弯</details>
- spendtime: 90（被绕进去）
- ACtime: 2022.5.1
- ACstatus: 不会
- score: 6
</details>
</details>

<details>
<summary>统计是给定字符串前缀的字符串数目</summary>

#### platform leetcode
#### id 6051

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接枚举模拟即可</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 4
- ACtime: 2022.4.30
- ACstatus: 完全会
- score: 3
</details>
</details>

<details>
<summary>最小平均差</summary>

#### platform leetcode
#### id 6052

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接枚举模拟即可</details>
- <details><summary>tag</summary>枚举</details>
- spendtime: 8
- ACtime: 2022.4.30
- ACstatus: 完全会
- score: 4
</details>
</details>

<details>
<summary>统计网格图中没有被保卫的格子数</summary>

#### platform leetcode
#### id 6053

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接按行列存储即可，使用一个treemap，然后，四个方向判断一下即可</details>
- <details><summary>tag</summary>枚举+判定</details>
- spendtime: 15
- ACtime: 2022.4.30
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>逃离火灾</summary>

#### platform leetcode
#### id 6054

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>翻车了，题目没有读清楚，翻车了，无话可说了，掉大分，行吧，真的是，都怪自己吧，一个bfs+二分的题目没好好写，怎么说都是无语，行，可以这么思考，因为火烧到每一个砖块的时间是固定的，所以二分达到最终点的时间，然后往回探索，如果可以回去，则提高时间，如果不可以，则降低时间，然后特判一下极端情况</details>
- <details><summary>tag</summary>二分+bfs</details>
- spendtime: 60
- ACtime: 2022.5.1
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>多个数组求交集</summary>

#### platform leetcode
#### id 6041

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>模拟即可</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 4
- ACtime: 2022.4.24
- ACstatus: 完全会
- score: 3
</details>
</details>

<details>
<summary>统计圆内格点数目</summary>

#### platform leetcode
#### id 6042

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>对区域进行枚举即可</details>
- <details><summary>tag</summary>枚举</details>
- spendtime: 6
- ACtime: 2022.4.24
- ACstatus: 完全会
- score: 4
</details>
</details>

<details>
<summary>统计包含每个点的矩形数目</summary>

#### platform leetcode
#### id 6043

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>注意数据的不对称，压缩下维度即可</details>
- <details><summary>tag</summary>二分+分析</details>
- spendtime: 12
- ACtime: 2022.4.24
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>花期内花的数目</summary>

#### platform leetcode
#### id 6044

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>不明白华为场出这种题目，有点重复，虽然不妨碍我倒开</details>
- <details><summary>tag</summary>差分+离散化</details>
- spendtime: 13
- ACtime: 2022.4.24
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>守护太空城</summary>

#### platform leetcode
#### id lcp53

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>确实是一道，非常非常麻烦的dp题目，使用3进制状态压缩，只关心，自己这一层和前一层的情况，所以，使用0,1,2，表示这个位置要么不开，要么开，要么开联合，在状态转移的时候，还是需要仔细思考，此处非常的难写，且难想，debug了很长的时间，而且增加了很多预处理，第一次实现的版本是，n*3^(2*m)的复杂度，但是我看到其实可能可以更加的优化到n*3^m的复杂度，不过确实非常的考验状态的设计了，思维难度还是太大，遂自己想出来的才是最好的，用别人的状态定义可以看看</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 150
- ACtime: 2022.4.18
- ACstatus: 完全会
- score: 7
</details>
</details>

<details>
<summary>字典序排数</summary>

#### platform leetcode
#### id 386

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>初看我是没什么思路的，不过主要是，只要想出来前一位和后一位的关系即可，对于任意一个字典序元素来说，恰好比它大的，就是在后面增加0，即*10，那什么时候，加零不行了呢，当>n的时候，就不行了，那么需要判断一下，最后一位是不是9，如果是9的话，需要跳回到前一位，或者如果这一个数+1>n，则也跳回上一位数，直到都不满足这两个约束条件，才跳出循环，将此数+1，这块个人觉得还是非常非常难想的，看一个知识点，就跟着学，学到了学到了</details>
- <details><summary>tag</summary>数学</details>
- spendtime: xx
- ACtime: 2022.4.18
- ACstatus: 不会
- score: 6
</details>
</details>

<details>
<summary>相邻字符不同的最长路径</summary>

#### platform leetcode
#### id 6073

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接在dfs里面做决策即可，有点像树中最长距离，只不过加个if而已</details>
- <details><summary>tag</summary>分析</details>
- spendtime: 17
- ACtime: 2022.4.18
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>转角路径的乘积中最多能有几个尾随零</summary>

#### platform leetcode
#### id 6072

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>一个wa了两次的题目，考虑清楚情况，可以将拐角想清楚，拐角分为四种情况，如果把拐角，看成有方向的，那么总共就四种方向，分别与四个象限对应，情况考虑清楚就没问题了</details>
- <details><summary>tag</summary>分析</details>
- spendtime: 25
- ACtime: 2022.4.18
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>完成所有任务需要的最少轮数</summary>

#### platform leetcode
#### id 6071

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>不要想情况是什么，而转为枚举，才是好的，这样可以把我们从繁琐的情况考虑中解脱出来</details>
- <details><summary>tag</summary>贪心</details>
- spendtime: 14
- ACtime: 2022.4.18
- ACstatus: 完全会
- score: 4
</details>
</details>

<details>
<summary>计算字符串的数字和</summary>

#### platform leetcode
#### id 6070

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>一个递归，判断，生成</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 4
- ACtime: 2022.4.18
- ACstatus: 完全会
- score: 3
</details>
</details>

<details>
<summary>节点序列的最大得分</summary>

#### platform leetcode
#### id 6063

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>先建图，然后枚举边，以这个边分别枚举最大的节点，刚开始罚坐是因为没看清题，然后后来是思维绕进去了，然后再后来，突然醒悟，我是傻逼</details>
- <details><summary>tag</summary>枚举</details>
- spendtime: xx（思考绕弯路，所以本来是2分钟的东西，增长了很多）
- ACtime: 2022.4.17
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>设计一个 ATM 机器</summary>

#### platform leetcode
#### id 6062

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>好好读题，可能有理解的歧义</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 16
- ACtime: 2022.4.17
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>买钢笔和铅笔的方案数</summary>

#### platform leetcode
#### id 6061

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接从前向后枚举</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 4
- ACtime: 2022.4.17
- ACstatus: 完全会
- score: 3
</details>
</details>

<details>
<summary>找到最接近 0 的数字</summary>

#### platform leetcode
#### id 6060

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接模拟</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 3
- ACtime: 2022.4.17
- ACstatus: 完全会
- score: 3
</details>
</details>

<details>
<summary>商店促销活动</summary>

#### platform leetcode
#### id 招商银行-04

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接不会了，看来还是自己没有仔细思考啊，两次dp，第一次主要求，不打折情况，第二次，强行所有都打折即可，当然，选择b的时候，做一步贪心，对B从小到大排序，状态清晰，转移清晰，我是傻逼</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 40
- ACtime: 2022.4.10
- ACstatus: 不会
- score: 7
</details>
</details>

<details>
<summary>点燃木棒</summary>

#### platform leetcode
#### id 招商银行-03

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>最麻烦建图题目，竟然把难度转移到了，建立图上，我是真没想到，非常非常麻烦，还要判断点的连通情况，实在是非常非常麻烦，反正是把我写吐了，不过还好，一遍过掉了，给这么多分，还是因为编码麻烦</details>
- <details><summary>tag</summary>考验编码</details>
- spendtime: 40
- ACtime: 2022.4.10
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>公园规划</summary>

#### platform leetcode
#### id 招商银行-02

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>求最大的度+1即可，大胆分析而来</details>
- <details><summary>tag</summary>脑筋急转弯</details>
- spendtime: 8
- ACtime: 2022.4.10
- ACstatus: 完全会
- score: 4
</details>
</details>

<details>
<summary>文本编辑程序设计</summary>

#### platform leetcode
#### id 招商银行-01

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>注意细节，模拟即可</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 6
- ACtime: 2022.4.10
- ACstatus: 完全会
- score: 4
</details>
</details>

<details>
<summary>花园的最大总美丽值</summary>

#### platform leetcode
#### id 6040

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这个题目，编码量和细节还都是有的，如何评价呢，写错个小东西，就会卡半天，思路很快就出来，但是代码上特别的不好写啊，debug了好久，先排序，其次，从前向后枚举，自从i点以后的，都要变成target的，i点之前，计算最大的可能的达到的数，这块使用个二分，即可，所以，最后复杂度是，n*logn*logn,但是细节需要好好考虑，还不错的一道编程题目</details>
- <details><summary>tag</summary>枚举+二分</details>
- spendtime: 60
- ACtime: 2022.4.10
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>K 次增加后的最大乘积</summary>

#### platform leetcode
#### id 6039

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>贪心啦，递增最小的数</details>
- <details><summary>tag</summary>贪心</details>
- spendtime: 7
- ACtime: 2022.4.10
- ACstatus: 完全会
- score: 4
</details>
</details>

<details>
<summary>向表达式添加括号后的最小结果</summary>

#### platform leetcode
#### id 6038

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>暴力模拟吧</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 13
- ACtime: 2022.4.10
- ACstatus: 完全会
- score: 4
</details>
</details>

<details>
<summary>按奇偶性交换后的最大数字</summary>

#### platform leetcode
#### id 6037

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>暴力模拟吧</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 7
- ACtime: 2022.4.10
- ACstatus: 完全会
- score: 3
</details>
</details>

<details>
<summary>加密解密字符串</summary>

#### platform leetcode
#### id 5302

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>阅读理解一下，直接计数即可，但是但是，此处因为自己的问题，被hack，估计要掉到一千多名之外了，非常痛之痛，本来要2400的段位了，但是，现在没办法了，只能在2300多呆一会了，特此记录一下惨痛的教训</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 15
- ACtime: 2022.4.3
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>每个小孩最多能分到多少糖果</summary>

#### platform leetcode
#### id 5219

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>很裸的二分</details>
- <details><summary>tag</summary>二分</details>
- spendtime: 6
- ACtime: 2022.4.3
- ACstatus: 完全会
- score: 4
</details>
</details>

<details>
<summary>找出输掉零场或一场比赛的玩家</summary>

#### platform leetcode
#### id 5235

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>模拟了没啥好说</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 6
- ACtime: 2022.4.3
- ACstatus: 完全会
- score: 4
</details>
</details>

<details>
<summary>转化时间需要的最少操作数</summary>

#### platform leetcode
#### id 6055

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>慢慢除即可</details>
- <details><summary>tag</summary>贪心</details>
- spendtime: 6
- ACtime: 2022.4.3
- ACstatus: 完全会
- score: 3
</details>
</details>

<details>
<summary>构造字符串的总得分和</summary>

#### platform leetcode
#### id 6036

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>90%的字符串题目都可以字符串hash，当然，线性的做法，还得看z函数的板子，非常的裸</details>
- <details><summary>tag</summary>z函数or字符串hash</details>
- spendtime: 15
- ACtime: 2022.4.2
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>选择建筑的方案数</summary>

#### platform leetcode
#### id 6035

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>就两种情况，简单处理</details>
- <details><summary>tag</summary>前缀和</details>
- spendtime: 5
- ACtime: 2022.4.2
- ACstatus: 完全会
- score: 4
</details>
</details>

<details>
<summary>数组的三角和</summary>

#### platform leetcode
#### id 6034

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接模拟不废话</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 5
- ACtime: 2022.4.2
- ACstatus: 完全会
- score: 3
</details>
</details>

<details>
<summary>转换数字的最少位翻转次数</summary>

#### platform leetcode
#### id 6033

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接异或完了，统计即可</details>
- <details><summary>tag</summary>异或</details>
- spendtime: 2
- ACtime: 2022.4.2
- ACstatus: 完全会
- score: 2
</details>
</details>

<details>
<summary>从栈中取出 K 个硬币的最大面值和</summary>

#### platform leetcode
#### id 5269

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>比较简单，直接线性dp，Math.max(sum(piles[i].length)*k,n*k)的复杂度</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 6
- ACtime: 2022.3.27
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>找到指定长度的回文数</summary>

#### platform leetcode
#### id 5253

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>被骗了一下，我已经有点无语</details>
- <details><summary>tag</summary>分析</details>
- spendtime: xx
- ACtime: 2022.3.27
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>美化数组的最少删除数</summary>

#### platform leetcode
#### id 5236

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>从前到后判断即可</details>
- <details><summary>tag</summary>贪心</details>
- spendtime: 8
- ACtime: 2022.3.27
- ACstatus: 完全会
- score: 4
</details>
</details>

<details>
<summary>找出两数组的不同</summary>

#### platform leetcode
#### id 5268

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>hash+hash+hash</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 3
- ACtime: 2022.3.27
- ACstatus: 完全会
- score: 3
</details>
</details>

<details>
<summary>由单个字符重复的最长子字符串</summary>

#### platform leetcode
#### id 6030

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>稍微有点不太敏感，用treemap当然也是可以的，但是会有很复杂的逻辑，这次为了语义的好理解性，我还是坚持用线段树重新写了一遍，并且还是有很高的参考度的，非常支持线段树去解决这类问题，语义非常清晰</details>
- <details><summary>tag</summary>线段树</details>
- spendtime: 120
- ACtime: 2022.3.20
- ACstatus: 完全会
- score: 7
</details>
</details>

<details>
<summary>射箭比赛中的最大得分</summary>

#### platform leetcode
#### id 6029

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>枚举加判断即可</details>
- <details><summary>tag</summary>枚举</details>
- spendtime: 12
- ACtime: 2022.3.20
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>统计道路上的碰撞次数</summary>

#### platform leetcode
#### id 6028

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>栈模拟一下即可，经过上次的教训，已经形成肌肉记忆</details>
- <details><summary>tag</summary>栈</details>
- spendtime: 12
- ACtime: 2022.3.20
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>统计数组中峰和谷的数量</summary>

#### platform leetcode
#### id 6027

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>去重之后统计即可</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 5
- ACtime: 2022.3.20
- ACstatus: 完全会
- score: 3
</details>
</details>

<details>
<summary>统计数组中峰和谷的数量</summary>

#### platform leetcode
#### id 6027

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>去重之后统计即可</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 5
- ACtime: 2022.3.20
- ACstatus: 完全会
- score: 3
</details>
</details>

<details>
<summary>用地毯覆盖后的最少白色砖块</summary>

#### platform leetcode
#### id 6023

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>状态直观，转移直观</details>
- <details><summary>tag</summary>dp+前缀和</details>
- spendtime: 14
- ACtime: 2022.3.19
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>将数组和减半的最少操作次数</summary>

#### platform leetcode
#### id 6022

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>不断除2即可</details>
- <details><summary>tag</summary>贪心</details>
- spendtime: 8
- ACtime: 2022.3.19
- ACstatus: 完全会
- score: 4
</details>
</details>

<details>
<summary>字符串中最多数目的子字符串</summary>

#### platform leetcode
#### id 6021

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>就两种情况，直接从前往后，一个从后往前，计数就可以了</details>
- <details><summary>tag</summary>分析</details>
- spendtime: 8
- ACtime: 2022.3.19
- ACstatus: 完全会
- score: 4
</details>
</details>

<details>
<summary>将数组划分成相等数对</summary>

#### platform leetcode
#### id 6020

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>统计判断</details>
- <details><summary>tag</summary>计数</details>
- spendtime: 2
- ACtime: 2022.3.19
- ACstatus: 完全会
- score: 2
</details>
</details>

<details>
<summary>路径总和 III</summary>

#### platform leetcode
#### id 437

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>还是要想一下，线性的时间复杂度</details>
- <details><summary>tag</summary>前缀和+dfs</details>
- spendtime: 20
- ACtime: 2022.3.14
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>合作开发</summary>

#### platform leetcode
#### id 银联-04

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>主要是枚举子集即可，使用hash表来存储，使用那种hash映射的方式都可以，我是比较省事，直接字符串，然后需要从小到大按照每个人的技能的长度排序一下</details>
- <details><summary>tag</summary>hash计数</details>
- spendtime: 45
- ACtime: 2022.3.13
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>理财产品</summary>

#### platform leetcode
#### id 银联-03

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这道题目巧妙利用price的数据范围即可，结合hash能够很短的表达，但是据说也是可以二分枚举price的</details>
- <details><summary>tag</summary>贪心</details>
- spendtime: 15
- ACtime: 2022.3.13
- ACstatus: 完全会
- score: 4
</details>
</details>

<details>
<summary>优惠活动系统</summary>

#### platform leetcode
#### id 银联-02

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>我感觉我写的有个细节稍微稍微有一点点问题，但是无法排查出来了，看了好久好久，感觉不是很应该，但没办法，这种历史问题只能石沉大海</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 40
- ACtime: 2022.3.13
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>回文链表</summary>

#### platform leetcode
#### id 银联-01

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这么简单一道题目，一上来，贪心确实把我给干蒙了，确实，确实，反思一下，败笔确实是在这个地方，失败</details>
- <details><summary>tag</summary>贪心</details>
- spendtime: 40
- ACtime: 2022.3.13
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>找出数组中的所有 K 近邻下标</summary>

#### platform leetcode
#### id 6031

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>模拟即可，数据太小啦</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 3
- ACtime: 2022.3.13
- ACstatus: 完全会
- score: 3
</details>
</details>

<details>
<summary>统计可以提取的工件</summary>

#### platform leetcode
#### id 5203

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接写吧，不费事</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 10
- ACtime: 2022.3.13
- ACstatus: 完全会
- score: 4
</details>
</details>

<details>
<summary>得到要求路径的最小带权子图</summary>

#### platform leetcode
#### id 6032

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>三次最短路算法即可，没啥好想的</details>
- <details><summary>tag</summary>最短路</details>
- spendtime: 18
- ACtime: 2022.3.13
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>K 次操作后最大化顶端元素</summary>

#### platform leetcode
#### id 5227

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>翻译也太傻逼了，误导我，简单的一个题目就直接拖了好久，看来还的看英文</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: xx
- ACtime: 2022.3.13
- ACstatus: 完全会
- score: 4
</details>
</details>

<details>
<summary>O(1) 时间插入、删除和获取随机元素</summary>

#### platform leetcode
#### id 380 && 剑指 Offer II 030

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>经典结合题目，hash来增，删，随机栈来随机</details>
- <details><summary>tag</summary>随机栈+hash</details>
- spendtime: 10
- ACtime: 2022.3.12
- ACstatus: 完全会
- score: 4
</details>

<details>
<summary>lh</summary>

- <details><summary>comment</summary>数组加hash，用hash存位置即可，比较经典</details>
- spendtime: 15
- ACtime: 2022.3.19
- ACstatus: 完全会
- score: 4
</details>

</details>

<details>
<summary>保持城市天际线</summary>

#### platform leetcode
#### id 807

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>分析即可，发现限制的在于什么，行列的最大值的最小值就是顶了</details>
- <details><summary>tag</summary>分析</details>
- spendtime: 10
- ACtime: 2022.3.12
- ACstatus: 完全会
- score: 4
</details>
</details>

<details>
<summary>单词的压缩编码</summary>

#### platform leetcode
#### id 820 && 剑指 Offer II 065

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>当然可以字典树，但完全没有必要，还是直接循环来的简单，当然，当数据量大的时候，还是老老实实比较好</details>
- <details><summary>tag</summary>字典树or枚举</details>
- spendtime: 看题就知
- ACtime: 2022.3.12
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>面试题 16.10. 生存人数</summary>

#### platform leetcode
#### id xx

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>不用质疑，直接上手</details>
- <details><summary>tag</summary>差分</details>
- spendtime: 看题就知
- ACtime: 2022.3.12
- ACstatus: 完全会
- score: 4
</details>
</details>

<details>
<summary>层数最深叶子节点的和</summary>

#### platform leetcode
#### id 1302

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>太过于裸了</details>
- <details><summary>tag</summary>dfs</details>
- spendtime: 看题就知
- ACtime: 2022.3.12
- ACstatus: 完全会
- score: 4
</details>
</details>

<details>
<summary>乘积小于K的子数组</summary>

#### platform leetcode
#### id 713 && 剑指 Offer II 009

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>滑动窗口的计数问题，在对问题的反映上，神经反射还是慢了半拍，虽然能够顺利的想出来，但是还是不够快</details>
- <details><summary>tag</summary>滑动窗口</details>
- spendtime: 20
- ACtime: 2022.3.12
- ACstatus: 完全会
- score: 4
</details>
</details>

<details>
<summary>Excel 表中某个范围内的单元格</summary>

#### platform leetcode
#### id 6016

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>就是麻烦一点，没啥的</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 3
- ACtime: 2022.3.6
- ACstatus: 完全会
- score: 3
</details>
</details>

<details>
<summary>替换数组中的非互质数</summary>

#### platform leetcode
#### id 6019

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>一看到临项会合并成一个点，立马想到栈，别再想着链表不链表了，失败，适当的提升一下分数，虽然很简单很简答，给自己一份提醒吧</details>
- <details><summary>tag</summary>栈</details>
- spendtime: 15
- ACtime: 2022.3.6
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>根据描述创建二叉树</summary>

#### platform leetcode
#### id 6018

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>我写麻烦了，不过没有什么思维难点</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 15
- ACtime: 2022.3.6
- ACstatus: 完全会
- score: 4
</details>
</details>

<details>
<summary>向数组中追加 K 个整数</summary>

#### platform leetcode
#### id 6017

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这个题还是wa了多次的，还是值得注意一下，最好先去重最好了</details>
- <details><summary>tag</summary>二分+去重</details>
- spendtime: 30
- ACtime: 2022.3.6
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>得到回文串的最少操作次数</summary>

#### platform leetcode
#### id 5237

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>贪心好题，能贪心就会做，不贪就不会做，如果遇到的是偶数字符，将其分别移到最左和最右，利用树状数组挖空进行，遇到的是奇数字符且只剩一的时候，此字符是要移动到最中间的，注意，此题目的精华部分来了，这个字符，不应该在树状数组中挖空，因为它放在中间位置，是需要占据交换次数的，其他的常态处理</details>
- <details><summary>tag</summary>贪心+树状数组</details>
- spendtime: 60
- ACtime: 2022.3.6
- ACstatus: 不会
- score: 7
</details>
</details>

<details>
<summary>有向无环图中一个节点的所有祖先</summary>

#### platform leetcode
#### id 5300

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接暴力吧数据范围小</details>
- <details><summary>tag</summary>dfs</details>
- spendtime: 9
- ACtime: 2022.3.5
- ACstatus: 完全会
- score: 4
</details>
</details>

<details>
<summary>将杂乱无章的数字排序</summary>

#### platform leetcode
#### id 5217

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>转字符串，再转回来即可</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 5
- ACtime: 2022.3.5
- ACstatus: 完全会
- score: 4
</details>
</details>

<details>
<summary>数组中紧跟 key 之后出现最频繁的数字</summary>

#### platform leetcode
#### id 6024

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接hash计数即可</details>
- <details><summary>tag</summary>计数</details>
- spendtime: 3
- ACtime: 2022.3.5
- ACstatus: 完全会
- score: 3
</details>
</details>

<details>
<summary>数组中特殊等间距元素的和</summary>

#### platform leetcode
#### id 1714

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>最佳方案肯定是n^1.5的方案，不过为了空间考虑还是，稍微在这块微调一下，不知道为啥是个动态规划的标签，一直思考，分块肯定是能过掉的，还以为有更好的解法，发现还是分块靠谱</details>
- <details><summary>tag</summary>分块思想</details>
- spendtime: 40
- ACtime: 2022.3.4
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>只出现一次的数字 III</summary>

#### platform leetcode
#### id 260

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>主要是分类，因为两个不同的数字的异或结果的lowbit位必然是1，所以可以依次将原有的nums分类，分成，只出现一次的数字问题的子问题，然后求出即可，真不好想啊，怎么都想不到系列</details>
- <details><summary>tag</summary>脑筋急转弯</details>
- spendtime: xx
- ACtime: 2022.3.4
- ACstatus: 看提示后会
- score: 3
</details>
</details>

<details>
<summary>只出现一次的数字 II</summary>

#### platform leetcode
#### id 137

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>按位模与即可，最终拼出答案，依然不好想系列</details>
- <details><summary>tag</summary>脑筋急转弯</details>
- spendtime: xx
- ACtime: 2022.3.4
- ACstatus: 看提示后会
- score: 3
</details>
</details>

<details>
<summary>只出现一次的数字</summary>

#### platform leetcode
#### id 136

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>异或，之怎么也想不到的简单题系列</details>
- <details><summary>tag</summary>脑筋急转弯</details>
- spendtime: xx
- ACtime: 2022.3.4
- ACstatus: 看提示后会
- score: 3
</details>
</details>

<details>
<summary>计算分配糖果的不同方式</summary>

#### platform leetcode
#### id 1692

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>我竟然思考了好一会，为什么我没有直接写出来？？？</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 30
- ACtime: 2022.3.3
- ACstatus: 看提示后会
- score: 5
</details>
</details>

<details>
<summary>感染 K 种病毒所需的最短时间</summary>

#### platform leetcode
#### id 1956

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>主要就是一个坐标变换，比较有技巧性，然后直接扫描线开整，不过，比较难搞的地方在于，变换过去与之前的不对等,所以需要仔细处理这块，其他的倒没啥，还是用一下别的方法会好一点，要是能想办法使用非整数的扫描线也行</details>
- <details><summary>tag</summary>二分or其他方法</details>
- spendtime: 110
- ACtime: 2022.3.1
- ACstatus: 看提示后会
- score: 7
</details>
</details>

<details>
<summary>包含全部黑色像素的最小矩形</summary>

#### platform leetcode
#### id 302

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这题多多少少有点垃垮</details>
- <details><summary>tag</summary>二分</details>
- spendtime: 10
- ACtime: 2022.2.28
- ACstatus: 完全会
- score: 4
</details>
</details>

<details>
<summary>统计包含给定前缀的字符串</summary>

#### platform leetcode
#### id 6008

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>模拟吧别思考了</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 1
- ACtime: 2022.2.27
- ACstatus: 完全会
- score: 2
</details>
</details>

<details>
<summary>使两字符串互为字母异位词的最少步骤数</summary>

#### platform leetcode
#### id 6009

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接计算，cnt的绝对值差即可</details>
- <details><summary>tag</summary>模拟计数</details>
- spendtime: 3
- ACtime: 2022.2.27
- ACstatus: 完全会
- score: 3
</details>
</details>

<details>
<summary>完成旅途的最少时间</summary>

#### platform leetcode
#### id 6010

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>注意数据随时可能爆掉</details>
- <details><summary>tag</summary>二分</details>
- spendtime: 4
- ACtime: 2022.2.27
- ACstatus: 完全会
- score: 4
</details>
</details>

<details>
<summary>完成比赛的最少时间</summary>

#### platform leetcode
#### id 6011

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>在转移的时候，设坑了，有点意思，值得纪念的dp</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 45
- ACtime: 2022.2.27
- ACstatus: 看提示后会
- score: 7
</details>
</details>

<details>
<summary>守卫城堡</summary>

#### platform leetcode
#### id lcp 38

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>问题抽象才是无敌的，建议好好看看题解，不过终于在今天终于将网络流的板子补上了，这点还是很不错的，题目在此文件应该还有一个评论，如果lc杯都是这种水准，看来lc杯这种比赛终究不是事</details>
- <details><summary>tag</summary>网络流</details>
- spendtime: 70
- ACtime: 2022.2.26
- ACstatus: 看提示后会
- score: 7
</details>
</details>

<details>
<summary>最小化去加油站的最大距离</summary>

#### platform leetcode
#### id 774

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>判定我又行了，直观得很</details>
- <details><summary>tag</summary>二分</details>
- spendtime: 30
- ACtime: 2022.2.22
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>子数组最大平均数 II</summary>

#### platform leetcode
#### id 644

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>判定又把我搞住了，我好无奈</details>
- <details><summary>tag</summary>二分</details>
- spendtime: 45
- ACtime: 2022.2.22
- ACstatus: 看提示后会
- score: 7
</details>
</details>

<details>
<summary>统计各位数字之和为偶数的整数个数</summary>

#### platform leetcode
#### id 6012

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接模拟吧</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 3
- ACtime: 2022.2.20
- ACstatus: 完全会
- score: 2
</details>
</details>

<details>
<summary>合并零之间的节点</summary>

#### platform leetcode
#### id 6013

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>已经麻木的list操作</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 7 
- ACtime: 2022.2.20
- ACstatus: 完全会
- score: 3
</details>
</details>

<details>
<summary>构造限制重复的字符串</summary>

#### platform leetcode
#### id 6014

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>wa了一发，我好烦啊，如果超过了固定的长度，那就补一个即可</details>
- <details><summary>tag</summary>贪心模拟</details>
- spendtime: 20 
- ACtime: 2022.2.20
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>统计可以被 K 整除的下标对数目</summary>

#### platform leetcode
#### id 6015

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>简单数论，追求不高，n^1.5算法即可</details>
- <details><summary>tag</summary>数论</details>
- spendtime: 20 
- ACtime: 2022.2.20
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>统计数组中好三元组数目</summary>

#### platform leetcode
#### id 5999

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>只要想清楚，如何求两个的情况即可，树状数组维护区间信息即可，比如，两个数组，用map维护第二个数组的映射关系，从前往后遍历，当在第一个数组的i位置时，这个数在数组的j位置，那就查询，在区间[0,j]的区间和，就是i这个位置，左侧可以组成的个数</details>
- <details><summary>tag</summary>树状数组</details>
- spendtime: 40 
- ACtime: 2022.2.20
- ACstatus: 模糊（处于会与不会不分明状态）
- score: 6
</details>
</details>

<details>
<summary>拆分成最多数目的偶整数之和</summary>

#### platform leetcode
#### id 5998

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>从2开始枚举即可，开根号n的复杂度，边角处判断是否满足大于两倍即可</details>
- <details><summary>tag</summary>贪心</details>
- spendtime: 10
- ACtime: 2022.2.20
- ACstatus: 完全会
- score: 4
</details>
</details>

<details>
<summary>找到和为给定整数的三个连续整数</summary>

#### platform leetcode
#### id 5997

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接除结束</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 3
- ACtime: 2022.2.20
- ACstatus: 完全会
- score: 2
</details>
</details>

<details>
<summary>统计数组中相等且可以被整除的数对</summary>

#### platform leetcode
#### id 5996

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>读完题写代码</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 3
- ACtime: 2022.2.20
- ACstatus: 完全会
- score: 2
</details>
</details>

<details>
<summary>金币路径</summary>

#### platform leetcode
#### id 656

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>好久没写两步走的题目了，dp是简单的dp，回溯也是简单的回溯，如果将题目本身，改为求所有的路径，这样比较好想，而直接求字典序最小的，在思考的过程中，还是碰了几次壁，最终才写出来，算是比较周折，不过题目的意思是是简单的，只是我可能比较生疏了</details>
- <details><summary>tag</summary>dp+回溯</details>
- spendtime: 60
- ACtime: 2022.2.19
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>最大化花园的美观度</summary>

#### platform leetcode
#### id 1788

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>从前向后，线性时间复杂度，比较简单</details>
- <details><summary>tag</summary>贪心+hash</details>
- spendtime: 12
- ACtime: 2022.2.18
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>最大休假天数</summary>

#### platform leetcode
#### id 568

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>并查集模版了，太裸题了</details>
- <details><summary>tag</summary>岛屿数量 II</details>
- spendtime: 6
- ACtime: 2022.2.18
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>最大休假天数</summary>

#### platform leetcode
#### id 568

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>二维dp，转移和定义都非常直观</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 15
- ACtime: 2022.2.18
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>单词缩写</summary>

#### platform leetcode
#### id 527

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>我个人还是推荐排序，代码量短，当然在找公共前缀这一目标上，字典树还是最优的</details>
- <details><summary>tag</summary>排序or字典树</details>
- spendtime: 50
- ACtime: 2022.2.17
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>将数组分割成和相等的子数组</summary>

#### platform leetcode
#### id 548

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>可以两步走，先用hashset数组，data[i]存一遍，存储[0,i]中能被分割成两个的可行方案，然后，再遍历一遍即可</details>
- <details><summary>tag</summary>hash</details>
- spendtime: xx
- ACtime: 2022.2.16
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>离建筑物最近的距离</summary>

#### platform leetcode
#### id 317

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>看过题之后，觉得太水了，就没做，就是直接从建筑出发，来bfs空地最短路径，然后再判断空地是否满足所有建筑可达即可，可能看评论坑就在于枚举建筑，不要枚举建筑，不过理论上复杂度上限都是一样的</details>
- <details><summary>tag</summary>bfs</details>
- spendtime: xx
- ACtime: 2022.2.16
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>K 距离间隔重排字符串</summary>

#### platform leetcode
#### id 358

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>每次考虑最大的频率，然后枚举，用dis存储字母i距离当前最近的距离，如果在某一步无法操作，即不可能</details>
- <details><summary>tag</summary>贪心+堆or排序</details>
- spendtime: 35
- ACtime: 2022.2.15
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>粉刷房子 II</summary>

#### platform leetcode
#### id 265

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>跟280场周赛的第2题比较一致，需要在一个二元组集合中找到最小值，且不能相等</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 20
- ACtime: 2022.2.15
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>编码最短长度的字符串</summary>

#### platform leetcode
#### id 471

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>dp[begin][end]表示区间可以表示的更短的字符串，转移就是不断枚举分界点即可，比较直观</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 40
- ACtime: 2022.2.14
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>数组的最大与和</summary>

#### platform leetcode
#### id 6007

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>使用xx表示一个数字槽，直接上状态压缩，转移清晰，状态设计清晰，dp[i][s]表示当处理完了i个数字，且数字槽位s状态的时候的最大值</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 22
- ACtime: 2022.2.13
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>拿出最少数目的魔法豆</summary>

#### platform leetcode
#### id 6006

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>枚举变成的数即可，小于的全部为零，大于的全部变成这个数即可</details>
- <details><summary>tag</summary>贪心+前缀和</details>
- spendtime: 13
- ACtime: 2022.2.13
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>使数组变成交替数组的最少操作数</summary>

#### platform leetcode
#### id 6005

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>蛮有意思的一个贪心题目，很不错，据说群里的大佬们卡了此题目，我觉得还是不错，本质上就是分别用hash统计两个数组的信息，记录第二个数组的次大，和最大的值和个数，枚举第一个hash表即可，细节就是注意判断一下，如果最大的和当前选择的值冲突，那就换为次大值</details>
- <details><summary>tag</summary>贪心+hash</details>
- spendtime: 13
- ACtime: 2022.2.13
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>得到 0 的操作数</summary>

#### platform leetcode
#### id 6004

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接模拟吧，速度速度</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 2
- ACtime: 2022.2.13
- ACstatus: 完全会
- score: 2
</details>
</details>

<details>
<summary>完美矩形</summary>

#### platform leetcode
#### id 391

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>首先可以任意选择从左向右扫描还是从上到下，思路来源于普林斯顿算法中，讲解矩形重合的部分，将一个矩形的左边界看成区间加1，将右边界看成区间减1，随时查询集合的大小是否为大矩形的高即可，其中还用到了树状数组的高阶应用，区间修改，区间求和，本质上还是利用差分在做运算，数学推倒也比较直观，顺便补充了树状数组+差分的模版，虽然线段树是通吃的</details>
- <details><summary>tag</summary>扫描线+离散化+区间修改</details>
- spendtime: 45
- ACtime: 2022.2.12
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>寻找旋转排序数组中的最小值 II</summary>

#### platform leetcode
#### id 154

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>特殊性质特殊分析，花式二分</details>
- <details><summary>tag</summary>二分</details>
- spendtime: 15
- ACtime: 2022.2.12
- ACstatus: 会
- score: 5
</details>
</details>

<details>
<summary>按要求补齐数组</summary>

#### platform leetcode
#### id 330

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这题主要是思维难度，编码很短很短，分析问题并解决问题，还蛮有意思的一个题目，从思维走势上来说，并不能很好的分析这种问题，还是很考验大脑的</details>
- <details><summary>tag</summary>分析</details>
- spendtime: 15
- ACtime: 2022.2.12
- ACstatus: 不会
- score: 7
</details>
</details>

<details>
<summary>移掉 K 位数字</summary>

#### platform leetcode
#### id 402

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>单调栈作为一个很好的演示题目，而且可以作为321的子问题，还是很不错的，没想到再次写的时候，连个单调栈的题目都看不出来了，失败</details>
- <details><summary>tag</summary>单调栈</details>
- spendtime: 15 - 60
- ACtime: 2022.2.11 - 2023.6.7
- ACstatus: 完全会 - 不会
- score: 5
</details>
</details>

<details>
<summary>拼接最大数</summary>

#### platform leetcode
#### id 321

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>我觉得题目很不错，是个结合度高，而且有一定考察度的题目，参考402作为这道题目的子问题，不过最无奈的就是，没有给出数据范围，这很无奈，盲猜，这题数据范围是100左右，402作为单调栈经典题目，而这道题目的merge合并操作，也很有意思，它不是简单的归并排序中的合并，而是跟后面有关的。所以大概是n^3的复杂度</details>
- <details><summary>tag</summary>单调栈+贪心</details>
- spendtime: 80
- ACtime: 2022.2.11
- ACstatus: 看提示之后会
- score: 7
</details>
</details>

<details>
<summary>压缩字符串 II</summary>

#### platform leetcode
#### id 1531

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>两种状态设计的想法，第一种，使用dp[i][j][k][z]表示前i个中使用了k次，且结尾为z个k的最小长度，这种状态好理解，而且非常的肌肉记忆，不需要太多思考，就可以上转移方程，但是对于这种规模的数据，过不了，只要但凡小一点点都可以过掉，笔试实在想不出来别的方法就这种吧，第二种，很考验对状态设计的精准描述，dp[i][j]表示从i起始保留j个字符的最小长度，转移虽然简单，但是并不是最好理解的，而且思维的跨度较大，不容易形成很好的复用性，是一道难度比较大的dp题目，很有价值，希望日后好好保留，今天花了大量时间来思考实践</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 180
- ACtime: 2022.2.10
- ACstatus: 看提示之后会
- score: 7
</details>
</details>

<details>
<summary>切披萨的方案数</summary>

#### platform leetcode
#### id 1444

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>状态设计清晰，三维dp，枚举分割，快速判断用二维前缀和，记忆中是某一次周赛，当时没做出来，时隔很久之后，还算清楚快速的写出来</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 35
- ACtime: 2022.2.8
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>恢复数组</summary>

#### platform leetcode
#### id 1416

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接枚举位数即可，但其实写成dp的动态规划版本也比较清晰，都可以，在动笔写之前或许踌躇了很久，不过，思路走的还是很快很清晰</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 40
- ACtime: 2022.2.8
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>单词拆分 II</summary>

#### platform leetcode
#### id 140

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>用dp表示，划分方案的集合，采用记忆化搜索即可，也不需要太多思考，肌肉记忆直接就敲出来</details>
- <details><summary>tag</summary>集合类dp</details>
- spendtime: 25-30
- ACtime: 2022.2.7
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>扰乱字符串</summary>

#### platform leetcode
#### id 87

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>简单思考，记忆化写法简单理解，就是不断转化为规模更小的问题，四维dp表示状态，虽然可以三维表示，其实我觉得是一种可行的空间优化思路，不过，这种快题还是怎么快怎么来的好，几乎没有思考难度</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 25
- ACtime: 2022.2.7
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>矩形区域不超过 K 的最大数值和</summary>

#### platform leetcode
#### id 363

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>只要把某个维度的n优化到logn即可过掉，常见的想法就是，维度压缩</details>
- <details><summary>tag</summary>维度压缩</details>
- spendtime: 25
- ACtime: 2022.2.7
- ACstatus: 看提示后会
- score: 6
</details>
</details>

<details>
<summary>编辑距离</summary>

#### platform leetcode
#### id 72

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>猛的一下让我做，我突然不会，但经过很冷静的思考，最终顺利输出，来源于面试题目，证明dp在短时面试中的热度居高不下</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 28
- ACtime: 2022.2.6
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>对奇偶下标分别排序</summary>

#### platform leetcode
#### id 6000

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接写，无套路</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 2
- ACtime: 2022.2.6
- ACstatus: 完全会
- score: 2
</details>
</details>

<details>
<summary>重排数字的最小值</summary>

#### platform leetcode
#### id 6001

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>分情况即可，纯模拟了</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 7
- ACtime: 2022.2.6
- ACstatus: 完全会
- score: 4
</details>
</details>

<details>
<summary>设计位集</summary>

#### platform leetcode
#### id 6002

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>用个set和一个flag记录一下，此时set里面的含义就行</details>
- <details><summary>tag</summary>脑筋急转弯</details>
- spendtime: 13
- ACtime: 2022.2.6
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>移除所有载有违禁货物车厢所需的最少时间</summary>

#### platform leetcode
#### id 6003

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>甩手就是个二分，参考某一个题目的中间过程，主要转化为求波谷的过程，把dp抛之脑后，我觉得我又被戏耍了，人已麻木，所以，我啊啊啊？我的脑子啊</details>
- <details><summary>tag</summary>线性dp</details>
- spendtime: 20
- ACtime: 2022.2.6
- ACstatus: 不会
- score: 6
</details>
</details>

<details>
<summary>Range 模块</summary>

#### platform leetcode
#### id 715

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>考验编码的时刻到了，n2的算法，本质上就是对一堆区间的操作，当然动态开点的线段树也是可以的，不过我个人还是更喜欢直接对一堆区间操作，还是太考验编码的细节了，属于强编码题目, 再次写，如同狗屎一般的区间题目，简直去死</details>
- <details><summary>tag</summary>区间操作or动态线段树</details>
- spendtime: 75
- ACtime: 2022.2.6
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>拆分数位后四位数字的最小和</summary>

#### platform leetcode
#### id 5984

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接模拟即可</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 3
- ACtime: 2022.2.5
- ACstatus: 完全会
- score: 2
</details>
</details>

<details>
<summary>拆分数位后四位数字的最小和</summary>

#### platform leetcode
#### id 5984

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接模拟即可</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 3
- ACtime: 2022.2.5
- ACstatus: 完全会
- score: 2
</details>
</details>

<details>
<summary>根据给定数字划分数组</summary>

#### platform leetcode
#### id 5985

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>排序即可</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 7
- ACtime: 2022.2.5
- ACstatus: 完全会
- score: 4
</details>
</details>

<details>
<summary>删除元素后和的最小差值</summary>

#### platform leetcode
#### id 5987

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>我觉得我自己被戏耍了，一个如此裸露竟然没有思路顺利的出来，有点搞笑，下一次遇见，定不负曾经相遇</details>
- <details><summary>tag</summary>优先级队列</details>
- spendtime: xx
- ACtime: 2022.2.6
- ACstatus: 不会
- score: 6
</details>
</details>

<details>
<summary>设置时间的最少代价</summary>

#### platform leetcode
#### id 5986

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>注意细节，要不然就错</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 25
- ACtime: 2022.2.6
- ACstatus: 完全会
- score: 5
</details>
</details>
<details>
<summary>数字1的个数</summary>

#### platform leetcode
#### id 233

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>代码写的已经特别简单凝练了，代码量不是很长，一个递归简单解决，思路就是，小于的时候直接枚举所有相乘，只精确递归恰好沿着的</details>
- <details><summary>tag</summary>数学</details>
- spendtime: 18
- ACtime: 2022.2.5
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>二叉树中的最大路径和</summary>

#### platform leetcode
#### id 124

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>主要的时间不小心多花在了debug上，稍微长了一点，讲道理应该是10分钟的题目，甚至更短，树上dp非常直观，也不需要想</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 21
- ACtime: 2022.2.3
- ACstatus: 完全会
- score: 6
</details>
</details>
<details>
<summary>最大矩形</summary>

#### platform leetcode
#### id 85

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>是对84题的复用，不过这种技巧，时间很长了，我也没有想起来，但是通过二分去做了，也能过，看来有些正解确实不太容易取得记忆点，而是真正实用的才可以</details>
- <details><summary>tag</summary>技巧+单调栈</details>
- spendtime: 30
- ACtime: 2022.2.3
- ACstatus: 完全会+正解更好
- score: 6
</details>
</details>

<details>
<summary>缺失的第一个正数</summary>

#### platform leetcode
#### id 41

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>原地hash，直接利用提供的内存，不过个人觉得，在有那种读入读出的题目中，可能无用武之地，只是个小技巧</details>
- <details><summary>tag</summary>hash</details>
- spendtime: 10
- ACtime: 2022.2.3
- ACstatus: 不会
- score: 5
</details>
</details>

<details>
<summary>柱状图中最大的矩形</summary>

#### platform leetcode
#### id 84

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>再次重做的时候，虽然知道这是一个单调栈的题目，但是还是觉得难以复现，不过这么久了，我不可能对这种东西一点办法也没有，使用好理解的语意，二分找边界的方式来更好理解，（不过此处的编码细节debug了一会），并且使用了一个久违的区间最值查询的方法，使用动态规划求区间最值问题</details>
- <details><summary>tag</summary>单调栈or二分</details>
- spendtime: 50
- ACtime: 2022.2.2
- ACstatus: 完全会
- score: 6
</details>
</details>
<details>
<summary>分发糖果</summary>

#### platform leetcode
#### id 135

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>一种经典套路，熟知一遍之后，第二遍将会很快</details>
- <details><summary>tag</summary>套路</details>
- spendtime: 10
- ACtime: 2022.2.2
- ACstatus: 完全会
- score: 6
</details>
</details>
<details>
<summary>矩阵中的最长递增路径</summary>

#### platform leetcode
#### id 329

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>用个小顶堆贪心一下，然后从小到大，去不断的更新最终的结果，向四个方向探索，然后不断max即可，然后走思路大概没用50s，挺直观简单</details>
- <details><summary>tag</summary>贪心+记忆化</details>
- spendtime: 15
- ACtime: 2022.2.1
- ACstatus: 完全会
- score: 6
</details>
</details>
<details>
<summary>天际线问题</summary>

#### platform leetcode
#### id 218

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这部分的编码，还是比较繁琐，debug了好几次，不过，总体还可以，至少思路直观且简单，直接写，也不需要思考和推导</details>
- <details><summary>tag</summary>线段树</details>
- spendtime: 35
- ACtime: 2022.2.1
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>不同的子序列</summary>

#### platform leetcode
#### id 115

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>滚动dp，n*m算法，数组[i]表示以i结尾的有多少种，很直观，代码也非常短</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 15
- ACtime: 2022.1.31
- ACstatus: 完全会
- score: 6
</details>
</details>
<details>
<summary>买卖股票的最佳时机 III</summary>

#### platform leetcode
#### id 123

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>当思考过动态规划版本的k次交易之后，理论上，可以直接套用，但是对于这种只用两次的，其实也可以不用过分思考状态的定义，而好好思考，如何用别的算法来解决，那就是从前往后，从后往前，维护一个单调递增，单调递减的队列即可，线性时间复杂度</details>
- <details><summary>tag</summary>dp+单调队列</details>
- spendtime: 40
- ACtime: 2022.1.31
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>分割回文串 II</summary>

#### platform leetcode
#### id 132

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>几乎没有思考时间，直接上来就开始写，就是n2的算法，不过不过，在开始求切割方案的时候，不知道脑子是怎么了，还是状态的问题，我竟然始终陷入了一个逻辑的闭环陷阱而没有走出来，而看了一眼提示之后，瞬间写出来，对于这种我知道完全没有东西的题目，突然的一个下陷确实让我措手不及，我也实在没有想明白为啥会这个样子</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 30
- ACtime: 2022.1.31
- ACstatus: 状态不明
- score: 6
</details>
</details>

<details>
<summary>字符串分组</summary>

#### platform leetcode
#### id 5995

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>连通用并查集，关联的快速查询使用状态压缩</details>
- <details><summary>tag</summary>并查集+状态压缩</details>
- spendtime: 25
- ACtime: 2022.1.30
- ACstatus: 现在完全会
- score: 6
</details>
</details>

<details>
<summary>查找给定哈希值的子串</summary>

#### platform leetcode
#### id 5994

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这道题属于反直觉字符串hash，我被搞到了记录一下，我因为这题没做出来，无法问鼎，特此批评，一定要知道p和mod之间要互质才可以字符串hash理论</details>
- <details><summary>tag</summary>反字符串hash</details>
- spendtime: 20
- ACtime: 2022.1.30
- ACstatus: 当时傻了+现在完全会
- score: 5
</details>
</details>

<details>
<summary>分组得分最高的所有下标</summary>

#### platform leetcode
#### id 5981

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>前缀和枚举即可</details>
- <details><summary>tag</summary>前缀和</details>
- spendtime: 8
- ACtime: 2022.1.30
- ACstatus: 完全会
- score: 2
</details>
</details>

<details>
<summary>将找到的值乘以 2</summary>

#### platform leetcode
#### id 5993

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接模拟</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 1
- ACtime: 2022.1.30
- ACstatus: 完全会
- score: 2
</details>
</details>

<details>
<summary>买卖股票的最佳时机IV</summary>

#### platform leetcode
#### id 188

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>之所以给这道题目了高分，理由是我直接上来就忽略了状态定义的优雅性，使用了高时间复杂度的做法，之所以那么做，是我思考的不周到，对于买和卖这个状态之间是互相有耦合关系的，所以最后修改了状态的设计，好好分析转移，最终达到了正解的程度，这道题目很主观的就跟一种题目联系起来，必入区间分组，求差值最小的方法，所以很容易使得自己对状态没有深入思考，dp的状态定义还是需要好好思考，反复练习</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 20
- ACtime: 2022.1.29
- ACstatus: 完全会+正解更好
- score: 7
</details>
</details>

<details>
<summary>地下城游戏</summary>

#### platform leetcode
#### id 174

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>我直观一看，二分，加dp，但确实逆序dp也不错，作为一道很简答的dp，怎么做都可以</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 25
- ACtime: 2022.1.29
- ACstatus: 完全会
- score: 6
</details>
</details>
<details>
<summary>最大间距</summary>

#### platform leetcode
#### id 164

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这题啥么，这题，简直了，基数排序怎么就是线性，桶排序不就是分块吗，这题还是分块好理解，还以为单调栈这种东西的优化，看起来还是想多了，让我再写一遍我都不会去写，要么直接就sort</details>
- <details><summary>tag</summary>分块</details>
- spendtime: 30
- ACtime: 2022.1.28
- ACstatus: 线性不会+这题不太重要
- score: 6
</details>
</details>
<details>
<summary>最短回文串</summary>

#### platform leetcode
#### id 214

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>建议作为字符串hash的模版题目，太裸题了</details>
- <details><summary>tag</summary>字符串hash</details>
- spendtime: 15
- ACtime: 2022.1.28
- ACstatus: 完全会
- score: 5
</details>
</details>
<details>
<summary>路径交叉</summary>

#### platform leetcode
#### id 335

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这应该是大胆分析中太过于典范的存在了，是真的很难很难，这题无关乎算法，真的就是直接强上分析，我觉得这种类型的题目，应该属于反算法题，不按常理来出牌，这怎么办，真的真的让我难以言喻，又成功收录了lc平台上，学了也不会做系列的题目</details>
- <details><summary>tag</summary>数学+脑筋急转弯</details>
- spendtime: 90
- ACtime: 2022.1.28
- ACstatus: 不会
- score: 7
</details>
</details>
<details>
<summary>计算右侧小于当前元素的个数</summary>

#### platform leetcode
#### id 315

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直观，不需要太多解释，这应该属于裸题了</details>
- <details><summary>tag</summary>离散化+树状数组</details>
- spendtime: 15
- ACtime: 2022.1.28
- ACstatus: 完全会
- score: 5
</details>
</details>
<details>
<summary>区间和的个数</summary>

#### platform leetcode
#### id 327

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>nlogn写法，不知道为啥时间上感觉还是稍微有一点大，不过在于好想，可能是多了一些常数的复杂度，不过还是顺利作出这种题，没啥思维上的难点，很直观，比较绕的地方就在于实际区间转为离散化的映射是怎么做的</details>
- <details><summary>tag</summary>离散化+树状数组+前缀和</details>
- spendtime: 25
- ACtime: 2022.1.27
- ACstatus: 完全会
- score: 6
</details>
</details>

<details>
<summary>俄罗斯套娃信封问题</summary>

#### platform leetcode
#### id 354

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这部分的贪心还是可以的，从根本上保证剔除等于的情况，我还在纳闷，怎么做的问题，确实，如此一看的话，确实，可以保证，所以还是学到了东西的，虽然简单但是有用</details>
- <details><summary>tag</summary>LIS+贪心</details>
- spendtime: 20
- ACtime: 2022.1.27
- ACstatus: 不会nlogn写法
- score: 7
</details>
</details>

<details>
<summary>将数据流变为多个不相交区间</summary>

#### platform leetcode
#### id 352

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>怎么写都可以，就是个合并，和简单，维护一下端点的映射就可以了</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 20
- ACtime: 2022.1.27
- ACstatus: 完全会
- score: 5
</details>
</details>

<details>
<summary>回文对</summary>

#### platform leetcode
#### id 336

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这道题把我给不小心写吐了，个人觉得，是脑子可能写了一天的转换不太清楚，所以盲目走了很多的弯路，随着越写越深入，以至于心态也慢慢的发慌，但这道题目总体还是不错的，是非常值得肯定的，我们可以这么去思考，第一次反复的思考时间复杂度，到底有问题在哪里，反复想，结果花了大量的时间优化在了无意义的事情上，导致浪费了很多的时间，原来是把300^2算成了9000，所以我始终想不同时间复杂度到底出现在了哪里，后来使用线性的方法，把这块变成了300C,时间直接就优化了下来，所以这题就是个hash再加字符串hash的题，挺搞我</details>
- <details><summary>tag</summary>字符串hash+hash</details>
- spendtime: 180
- ACtime: 2022.1.27
- ACstatus: 完全会
- score: 7
</details>
</details>
<details>
<summary>采购方案</summary>

#### platform leetcode
#### id lcp 28

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>简单题出成这样也是可以</details>
- <details><summary>tag</summary>单点修改区间查询</details>
- spendtime: 10.0
- ACtime: 2021.11.8
- ACstatus: 完全会
- score: 4.0
</details>
</details>
<details>
<summary>乐团站位</summary>

#### platform leetcode
#### id lcp 29

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>数学的绕来绕去非常花时间考虑细节</details>
- <details><summary>tag</summary>数学乱乱绕</details>
- spendtime: 30.0
- ACtime: 2021.11.8
- ACstatus: 完全会
- score: 4.0
</details>
</details>
<details>
<summary>魔塔游戏</summary>

#### platform leetcode
#### id lcp 30

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>贪心就完了延迟调整</details>
- <details><summary>tag</summary>贪心</details>
- spendtime: 30.0
- ACtime: 2021.11.8
- ACstatus: 完全会
- score: 4.0
</details>
</details>
<details>
<summary>石子游戏 IX</summary>

#### platform leetcode
#### id 2029.0

<details>
<summary>gx</summary>

- <details><summary>comment</summary>博弈论啊第一次做</details>
- <details><summary>tag</summary>博弈论</details>
- spendtime: 30.0
- ACtime: 2021.11.8
- ACstatus: 不会
- score: 
</details>
</details>
<details>
<summary> 找出给定方程的正整数解</summary>

#### platform leetcode
#### id 1237.0

<details>
<summary>lh</summary>

- <details><summary>comment</summary>标签是双指针，但其实随便过</details>
- <details><summary>tag</summary>双指针</details>
- spendtime: 10.0
- ACtime: 2021.11.8
- ACstatus: 完全会
- score: 1或2
</details>
</details>
<details>
<summary>有效三角形的个数 </summary>

#### platform leetcode
#### id 611.0

<details>
<summary>lh</summary>

- <details><summary>comment</summary>二分</details>
- <details><summary>tag</summary>二分</details>
- spendtime: 30.0
- ACtime: 2021.11.8
- ACstatus: 会但磕磕绊绊
- score: 3.0
</details>
</details>
<details>
<summary>批量处理任务</summary>

#### platform leetcode
#### id lcp 32

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>离散化之后，就有点无所举措了，直观的去理解是去先找到最大的重合点然后再去动态的更新区间的信息，在此处就会产生问题第一是，离散化之后，只是记录了端点的信息，而其中被隐藏的片段，需要更多的逻辑去处理，当某一个任务的工作时间已经满足了此工作时间之后，正确的应该是需要删除这个权重，在区间上的影响，此处又会涉及到新的问题，虽然不太会，但是预估此题的难度不会超过7，理由是，模型是简单的，只是处理的技巧上，会有所区别</details>
- <details><summary>tag</summary>贪心</details>
- spendtime: 180.0
- ACtime: 2021.11.9
- ACstatus: 不会
- score: 7.0
</details>
</details>
<details>
<summary>有序数组中的单一元素</summary>

#### platform leetcode
#### id 540.0

<details>
<summary>lh</summary>

- <details><summary>comment</summary>二分</details>
- <details><summary>tag</summary>二分</details>
- spendtime: 15.0
- ACtime: 2021.11.9
- ACstatus: 只会一半剩下的得看题解
- score: 2或3
</details>
</details>
<details>
<summary>考试的最大困扰度</summary>

#### platform leetcode
#### id 2024.0

<details>
<summary>gx</summary>

- <details><summary>comment</summary>用dp想了很久，用滑动窗口解</details>
- <details><summary>tag</summary>滑动窗口</details>
- spendtime: 20.0
- ACtime: 2021.11.9
- ACstatus: 只会一半剩下的得看题解
- score: 3.0
</details>
</details>
<details>
<summary>判断单词是否能放入填字游戏内</summary>

#### platform leetcode
#### id 2018.0

<details>
<summary>gx</summary>

- <details><summary>comment</summary>看穿十分简单</details>
- <details><summary>tag</summary>前缀和</details>
- spendtime: 20.0
- ACtime: 2021.11.10
- ACstatus: 只会一半剩下的得看题解
- score: 3.0
</details>
</details>
<details>
<summary>寻找右区间</summary>

#### platform leetcode
#### id 436.0

<details>
<summary>lh</summary>

- <details><summary>comment</summary>稍微麻烦一点的二分</details>
- <details><summary>tag</summary>二分</details>
- spendtime: 10.0
- ACtime: 2021.11.10
- ACstatus: 完全会，第一次一遍过
- score: 2或3
</details>
</details>
<details>
<summary>变换的迷宫</summary>

#### platform leetcode
#### id lcp 31

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>首先，题目是难的，抽象困难理解困难，各种困难，尝试了多种解决方案，并且每一次的解决方案，都会适当的比前一次更加优秀，不过非常无奈的是，如果按照bfs来，无法处理数据量的膨胀，尝试了很多的方法，去在过程中放缩，可还是不行，在一开始就尝试了，直接枚举锁死位置，除非打表最后一个，否则就过不了，算是我绞尽脑汁地一道题目而且战斗了很久很久，真的算是lcp系列地牛逼题，就是在麻烦程度上，远超平常的周赛水准</details>
- <details><summary>tag</summary>dp或bfs</details>
- spendtime: 360.0
- ACtime: 2021.11.10
- ACstatus: 只会一半剩下的得看题解
- score: 7.0
</details>
</details>
<details>
<summary>蓄水</summary>

#### platform leetcode
#### id lcp 33

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>好唬人啊</details>
- <details><summary>tag</summary>模拟即可</details>
- spendtime: 30.0
- ACtime: 2021.11.11
- ACstatus: 完全会
- score: 4.0
</details>
</details>
<details>
<summary>二叉树染色</summary>

#### platform leetcode
#### id lcp 34

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>列出状态定义转移即可</details>
- <details><summary>tag</summary>树上dp</details>
- spendtime: 30.0
- ACtime: 2021.11.11
- ACstatus: 完全会
- score: 4.0
</details>
</details>
<details>
<summary>将x减到0的最小操作数</summary>

#### platform leetcode
#### id 1658.0

<details>
<summary>lh</summary>

- <details><summary>comment</summary>标签是双指针，二分做多了用二分做出来的</details>
- <details><summary>tag</summary>滑动窗口/二分</details>
- spendtime: 20.0
- ACtime: 2021.11.11
- ACstatus: 思考了一下会
- score: 3.0
</details>
</details>
<details>
<summary>判断单词是否能放入填字游戏内</summary>

#### platform leetcode
#### id 2018.0

<details>
<summary>gx</summary>

- <details><summary>comment</summary>指针的操作</details>
- <details><summary>tag</summary>指针</details>
- spendtime: 30.0
- ACtime: 2021.11.11
- ACstatus: 会但编码磕磕绊绊
- score: 3.0
</details>
</details>
<details>
<summary>数组美丽值求和</summary>

#### platform leetcode
#### id 2012.0

<details>
<summary>gx</summary>

- <details><summary>comment</summary>前缀和</details>
- <details><summary>tag</summary>前缀和</details>
- spendtime: 15.0
- ACtime: 2021.11.12
- ACstatus: 完全会
- score: 2.0
</details>
</details>
<details>
<summary>反转字符</summary>

#### platform leetcode
#### id 92.0

<details>
<summary>lh</summary>

- <details><summary>comment</summary>动态规划，和上次做的那道dp有点像，所以列出来状态就做出来了</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 10.0
- ACtime: 2021.11.12
- ACstatus: 完全会
- score: 2.0
</details>
</details>
<details>
<summary>数的平方等于两数乘积的方法数</summary>

#### platform leetcode
#### id 1577.0

<details>
<summary>lh</summary>

- <details><summary>comment</summary>哈希，但是学到了一些哈希的写法，会比以前的写法更好一些</details>
- <details><summary>tag</summary>哈希</details>
- spendtime: 15.0
- ACtime: 2021.11.12
- ACstatus: 只会一般剩下的得看题解
- score: 3.0
</details>
</details>
<details>
<summary>前k个高频元素</summary>

#### platform leetcode
#### id 347.0

<details>
<summary>lh</summary>

- <details><summary>comment</summary>哈希和堆排序，这题本身纯哈希就可以了，要求把复杂度降到nlogn以下，加上堆排序就可以了，还是比较明显的，不过堆排序一直没写过，还是看了题解的写法，但是思路上没什么问题。</details>
- <details><summary>tag</summary>哈希，堆排序</details>
- spendtime: 10.0
- ACtime: 2021.11.15
- ACstatus: 只会一半剩下的得看题解
- score: 2.0
</details>
</details>
<details>
<summary>电动车游城市</summary>

#### platform leetcode
#### id lcp35

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>关于动态规划和最短之间就是存在模糊的界限，最短路的一个节点也是可以看成动态规划中的状态，此题又是一个很难直观的看出来转移的，所以，动态规划的解的思考一直受阻，而且比如说，状态更新的bfs这种，又一直不敢动手，理由是，始终对复杂度的估计保持着警惕，最后，还是决定冒险试一试，因为觉得复杂度还是可以的，使用cost[i][j]表示，i个城市，剩余j油量，的最短时间，然后使用优先级队列不断更新别的可达状态，保证，每个点不会重复判断和更新，最后直接一遍过了，就是稍微思考久了一点，不过，作为一个最短路的题目，质量还是可以的</details>
- <details><summary>tag</summary>最短路</details>
- spendtime: 70.0
- ACtime: 2021.11.15
- ACstatus: 思考了一下会
- score: 6.0
</details>
</details>
<details>
<summary>处理含限制条件的好友请求</summary>

#### platform leetcode
#### id 5929.0

<details>
<summary>gx</summary>

- <details><summary>comment</summary>并查集的题目，开始思考了一下如何保存并查集不能成为朋友的边，发现并查集的根节点不固定，这样不好做。换了一种思路，每次查询重新遍历restriction数组，如果发现并查集有连线，则取false。否则取true</details>
- <details><summary>tag</summary>并查集</details>
- spendtime: 40.0
- ACtime: 2021.11.15
- ACstatus: 只会一半剩下的得看题解
- score: 4.0
</details>
</details>
<details>
<summary>至少有K个重复字符的最长子串</summary>

#### platform leetcode
#### id 395.0

<details>
<summary>lh</summary>

- <details><summary>comment</summary>递归的想法，每次根据不符合条件的节点分成两块</details>
- <details><summary>tag</summary>递归，哈希</details>
- spendtime: 10.0
- ACtime: 2021.11.16
- ACstatus: 完全会
- score: 3.0
</details>
</details>
<details>
<summary>从双倍数组中还原原数组</summary>

#### platform leetcode
#### id 2007.0

<details>
<summary>gx</summary>

- <details><summary>comment</summary>很好想，但是实现起来有很多方法，排序是一定的，但是接下来的数据结构选择有很多种，这里选择了一个队列实现</details>
- <details><summary>tag</summary>排序，队列</details>
- spendtime: 30.0
- ACtime: 2021.11.16
- ACstatus: 只会一半剩下的得看题解
- score: 3.0
</details>
</details>
<details>
<summary>出租车的最大盈利</summary>

#### platform leetcode
#### id 2008.0

<details>
<summary>gx</summary>

- <details><summary>comment</summary>开始想的是和周赛做的一道题类似，两个不重叠的活动，用排序加优先队列，但是这道题为dp，dp[i]表示到达i点获得的最大盈利</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 30.0
- ACtime: 2021.11.17
- ACstatus: 只会一半剩下的得看题解
- score: 3.0
</details>
</details>
<details>
<summary>和相同的二元子数组</summary>

#### platform leetcode
#### id 930.0

<details>
<summary>lh</summary>

- <details><summary>comment</summary>标签害人呀，不一定要按标签的想法去做</details>
- <details><summary>tag</summary>算是滑动窗口吧</details>
- spendtime: 40.0
- ACtime: 2021.11.17
- ACstatus: 完全会
- score: 3或4
</details>
</details>
<details>
<summary>可互换矩形的组数</summary>

#### platform leetcode
#### id 2001.0

<details>
<summary>gx</summary>

- <details><summary>comment</summary>被数据类型卡了</details>
- <details><summary>tag</summary>排序，哈希</details>
- spendtime: 40.0
- ACtime: 2021.11.18
- ACstatus: 完全会
- score: 3.0
</details>
</details>
<details>
<summary>预测赢家</summary>

#### platform leetcode
#### id 486.0

<details>
<summary>lh</summary>

- <details><summary>comment</summary>完全没思路</details>
- <details><summary>tag</summary>动态规划</details>
- spendtime: 25.0
- ACtime: 2021.11.18
- ACstatus: 不会
- score: 4.0
</details>
</details>
<details>
<summary>石子游戏</summary>

#### platform leetcode
#### id 877.0

<details>
<summary>lh</summary>

- <details><summary>comment</summary>和预测赢家一模一样</details>
- <details><summary>tag</summary>动态规划</details>
- spendtime: 10.0
- ACtime: 2021.11.19
- ACstatus: 完全会
- score: 4.0
</details>
</details>
<details>
<summary>石子游戏3</summary>

#### platform leetcode
#### id 1406.0

<details>
<summary>lh</summary>

- <details><summary>comment</summary>上题的变式</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 35.0
- ACtime: 2021.11.19
- ACstatus: 完全会
- score: 4.0
</details>
</details>
<details>
<summary>使数组连续的最少操作数</summary>

#### platform leetcode
#### id 2009.0

<details>
<summary>gx</summary>

- <details><summary>comment</summary>求最小的改变数量，即为保留最大数量的元素，卡在了这一点上面，用排序+去重+滑动窗口可解</details>
- <details><summary>tag</summary>排序、去重、滑动窗口</details>
- spendtime: 50.0
- ACtime: 2021.11.19
- ACstatus: 只会一半剩下的得看题解
- score: 5.0
</details>
</details>
<details>
<summary>表现良好的最长时间段</summary>

#### platform leetcode
#### id 1124.0

<details>
<summary>lh</summary>

- <details><summary>comment</summary>前缀和加单调栈</details>
- <details><summary>tag</summary>前缀和</details>
- spendtime: 40.0
- ACtime: 2021.11.19
- ACstatus: 不会
- score: 4或5
</details>
</details>
<details>
<summary>两个回文子序列长度的最大乘积</summary>

#### platform leetcode
#### id 2002

<details>
<summary>gx</summary>

- <details><summary>comment</summary>看到数据范围n<=14，可能要遍历所有情况，遂选择dfs</details>
- <details><summary>tag</summary>dfs</details>
- spendtime: 40.0
- ACtime: 2021.11.20
- ACstatus: 完全会
- score: 3.0
</details>
</details>
<details>
<summary>区间内查询数字的频率</summary>

#### platform leetcode
#### id 5186.0

<details>
<summary>gx</summary>

- <details><summary>comment</summary>用TreeMap写出来的，代码很简短，学习了ceilingEntry、higherEntry、floorEntry、lowerEntry的用法</details>
- <details><summary>tag</summary>二分</details>
- spendtime: 30.0
- ACtime: 2021.11.21
- ACstatus: 完全会
- score: 3.0
</details>
</details>
<details>
<summary>01矩阵</summary>

#### platform leetcode
#### id 542.0

<details>
<summary>lh</summary>

- <details><summary>comment</summary>这题有几道类似的，想起来了做法</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 20.0
- ACtime: 2021.11.22
- ACstatus: 完全会
- score: 3.0
</details>
</details>
<details>
<summary>游戏中弱角色的数量</summary>

#### platform leetcode
#### id 1996.0

<details>
<summary>gx</summary>

- <details><summary>comment</summary>前缀和+排序，若第一个元素不同的情况下，要注意第二个元素其排序</details>
- <details><summary>tag</summary>前缀和+排序</details>
- spendtime: 20.0
- ACtime: 2021.11.23
- ACstatus: 完全会
- score: 3.0
</details>
</details>
<details>
<summary>连续子数组和</summary>

#### platform leetcode
#### id 523.0

<details>
<summary>lh</summary>

- <details><summary>comment</summary>想到了前缀和但是没想到用哈希表，超时没解决得了</details>
- <details><summary>tag</summary>前缀和+哈希表</details>
- spendtime: 40.0
- ACtime: 2021.11.23
- ACstatus: 只会一半剩下的得看题解
- score: 4.0
</details>
</details>
<details>
<summary>反转每对括号间的子串</summary>

#### platform leetcode
#### id 1190.0

<details>
<summary>lh</summary>

- <details><summary>comment</summary>就是个简单的栈，删除括号的时候注意一下就可以，二题位置吧</details>
- <details><summary>tag</summary>栈</details>
- spendtime: 20.0
- ACtime: 2021.11.24
- ACstatus: 完全会
- score: 2或3
</details>
</details>
<details>
<summary>零钱兑换</summary>

#### platform leetcode
#### id 322.0

<details>
<summary>lh</summary>

- <details><summary>comment</summary>背包吧，三月份考研做出来过</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 20.0
- ACtime: 2021.11.24
- ACstatus: 完全会
- score: 3.0
</details>
</details>
<details>
<summary>灯泡开关2</summary>

#### platform leetcode
#### id 672.0

<details>
<summary>lh</summary>

- <details><summary>comment</summary>数学，兄弟们可以看看</details>
- <details><summary>tag</summary>数学</details>
- spendtime: 25.0
- ACtime: 2021.11.24
- ACstatus: 不会啊
- score: 4.0
</details>
</details>
<details>
<summary>访问完所有房间的第一天</summary>

#### platform leetcode
#### id 1997.0

<details>
<summary>gx</summary>

- <details><summary>comment</summary>动态规划，状态转移没想到，利用好定义的性质，从next[i-1]到i-1的时间可以用dp[i-1]-dp[next[i-1]来代替，也就是与他们第一次到达的时间差相同</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 40.0
- ACtime: 2021.11.24
- ACstatus: 不会
- score: 4.0
</details>
</details>
<details>
<summary>找到所有的农场组</summary>

#### platform leetcode
#### id 1992

<details>
<summary>gx</summary>

- <details><summary>comment</summary>遍历或者dfs即可</details>
- <details><summary>tag</summary>dfs</details>
- spendtime: 30.0
- ACtime: 2021.11.25
- ACstatus: 完全会
- score: 3.0
</details>
</details>
<details>
<summary>树上的操作</summary>

#### platform leetcode
#### id 1993.0

<details>
<summary>gx</summary>

- <details><summary>comment</summary>维护好数据结构即可</details>
- <details><summary>tag</summary>树</details>
- spendtime: 40.0
- ACtime: 2021.11.25
- ACstatus: 完全会
- score: 3.0
</details>
</details>
<details>
<summary>将字符串反转至递增</summary>

#### platform leetcode
#### id 926.0

<details>
<summary>lh</summary>

- <details><summary>comment</summary>这题做过类似的，dp可以做，答案是前缀和，做出来了就没有仔细去研究</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 10.0
- ACtime: 2021.11.25
- ACstatus: 完全会
- score: 3.0
</details>
</details>
<details>
<summary>划分字母区间</summary>

#### platform leetcode
#### id 763.0

<details>
<summary>lh</summary>

- <details><summary>comment</summary>答案说是贪心，我是做了个哈希表去判断最大最小位置</details>
- <details><summary>tag</summary>贪心，哈希</details>
- spendtime: 25-30
- ACtime: 2021.11.25
- ACstatus: 完全会
- score: 3.0
</details>
</details>
<details>
<summary>找出数组中的第 K 大整数</summary>

#### platform leetcode
#### id 1985.

<details>
<summary>gx</summary>

- <details><summary>comment</summary>自定义规则排序</details>
- <details><summary>tag</summary>排序</details>
- spendtime: 10.0
- ACtime: 2021.11.26
- ACstatus: 完全会
- score: 2.0
</details>
</details>
<details>
<summary>最长字符串链</summary>

#### platform leetcode
#### id 1048.0

<details>
<summary>lh</summary>

- <details><summary>comment</summary>dp  找转移状态</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 20.0
- ACtime: 2021.11.26
- ACstatus: 完全会
- score: 3.0
</details>
</details>
<details>
<summary>完成任务的最少工作时间段</summary>

#### platform leetcode
#### id 1986.0

<details>
<summary>gx</summary>

- <details><summary>comment</summary>状压dp</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 30.0
- ACtime: 2021.11.27
- ACstatus: 完全会
- score: 3.0
</details>
</details>
<details>
<summary>最多牌组数</summary>

#### platform leetcode
#### id lcp 36

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>数据放缩的dp,此技巧强无敌，应该属于很牛逼的放缩技巧，不亏是CF的题，随便一个就能在lc里面称大爷，牛逼</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 180.0
- ACtime: 2021.11.29
- ACstatus: 不会
- score: 7.0
</details>
</details>
<details>
<summary>数组中最大数对和的最小值</summary>

#### platform leetcode
#### id 1877.0

<details>
<summary>lh</summary>

- <details><summary>comment</summary>一题位置</details>
- <details><summary>tag</summary>没啥</details>
- spendtime: 10.0
- ACtime: 2021.11.29
- ACstatus: 完全会
- score: 2.0
</details>
</details>
<details>
<summary>子串能表示从1到N数字的二进制串</summary>

#### platform leetcode
#### id 1016.0

<details>
<summary>lh</summary>

- <details><summary>comment</summary>这题我比较疑惑</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 17.0
- ACtime: 2021.11.29
- ACstatus: 完全会
- score: 2或3
</details>
</details>
<details>
<summary>找出最长的超赞子字符串</summary>

#### platform leetcode
#### id 1542.0

<details>
<summary>lh</summary>

- <details><summary>comment</summary>前缀和，哈希</details>
- <details><summary>tag</summary>前缀和哈希</details>
- spendtime: 50.0
- ACtime: 2021.11.29
- ACstatus: 不会
- score: 5.0
</details>
</details>
<details>
<summary>最小跳跃次数</summary>

#### platform leetcode
#### id lcp 9

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>某种基础模型的灵活应用，兄弟们可以做一下，我是菜菜的做完</details>
- <details><summary>tag</summary>搜索</details>
- spendtime: 110.0
- ACtime: 2021.11.30
- ACstatus: 完全会
- score: 6.0
</details>
</details>
<details>
<summary>好子集的数目</summary>

#### platform leetcode
#### id 1994.0

<details>
<summary>gx</summary>

- <details><summary>comment</summary>dfs 状压 外加一点数学</details>
- <details><summary>tag</summary>dfs+状压</details>
- spendtime: 120.0
- ACtime: 2021.11.30
- ACstatus: 不会
- score: 5.0
</details>
</details>
<details>
<summary>N</summary>

#### platform leetcode
#### id 957.0

<details>
<summary>lh</summary>

- <details><summary>comment</summary>找到循环就行了</details>
- <details><summary>tag</summary>循环</details>
- spendtime: 40.0
- ACtime: 2021.11.30
- ACstatus: 不会
- score: 3.0
</details>
</details>
<details>
<summary>丑数2</summary>

#### platform leetcode
#### id 264.0

<details>
<summary>lh</summary>

- <details><summary>comment</summary>广搜</details>
- <details><summary>tag</summary>广搜加优先级队列</details>
- spendtime: 25.0
- ACtime: 2021.12.1
- ACstatus: 完全会
- score: 3.0
</details>
</details>
<details>
<summary>字符串的号分割数目</summary>

#### platform leetcode
#### id 1525.0

<details>
<summary>lh</summary>

- <details><summary>comment</summary>前缀和</details>
- <details><summary>tag</summary>前缀和</details>
- spendtime: 15.0
- ACtime: 2021.12.1
- ACstatus: 完全会
- score: 2或3
</details>
</details>
<details>
<summary>最小矩形面积</summary>

#### platform leetcode
#### id lcp 37

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>做法多样，但是此题目，真的用一种基础算法就可以，但是转化难度很大，很考验思维，我评价这道题，真的非常不错，非常好，牛</details>
- <details><summary>tag</summary>数学</details>
- spendtime: 110.0
- ACtime: 2021.12.1
- ACstatus: 不会
- score: 7.0
</details>
</details>
<details>
<summary>学生出勤记录2</summary>

#### platform leetcode
#### id 552.0

<details>
<summary>lh</summary>

- <details><summary>comment</summary>dp,转移状态真不好想</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 40.0
- ACtime: 2021.12.1
- ACstatus: 不会
- score: 5.0
</details>
</details>
<details>
<summary>守卫城堡</summary>

#### platform leetcode
#### id lcp 38

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这应该是目前为止，遇到的第一道网络流的题目，不仅没有写出代码，而且对于答案的知识点是非常陌生的，网络流一直是放在我的未完成知识里面没有去碰，据说，楼教主也是错了两次，所以，此题的难度应该是很大的，当然，数据量出成这个级别，应该是让我们去dp的，但dp的定义和转移的难理解程度，并不亚于对网络流的理解，后续必须找时间把这个知识点补上，对于这样的知识点的价值，其实是小于一个比较好理解的知识点的，因为，毕竟做了最后一道压轴题，难度远超周赛第4题，至少高了两个度，要想在大型竞赛中，过掉此题，看来任重而道远</details>
- <details><summary>tag</summary>网络流or魔幻dp</details>
- spendtime: 纯思考120分钟
- ACtime: 2021.12.2
- ACstatus: 不会
- score: 8.0
</details>
</details>
<details>
<summary>不同的子序列 II</summary>

#### platform leetcode
#### id 940.0

<details>
<summary>gx</summary>

- <details><summary>comment</summary>dp吧，枚举所有子集肯定超时，将dp状态设为最后一位结尾的不重复的字串的长度，算法复杂度为n2，若将状态定义为包括i以及不包括i的，时间复杂度则为O(n)</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 90.0
- ACtime: 2021.12.2
- ACstatus: 不会
- score: 4.0
</details>
</details>
<details>
<summary>航班预订统计</summary>

#### platform leetcode
#### id 1109.0

<details>
<summary>lh</summary>

- <details><summary>comment</summary>新知识，差分</details>
- <details><summary>tag</summary>前缀和，差分</details>
- spendtime: 25.0
- ACtime: 2021.12.2
- ACstatus: 不会
- score: 3或4
</details>
</details>
<details>
<summary> 不同的好子序列数目</summary>

#### platform leetcode
#### id 1987

<details>
<summary>gx</summary>

- <details><summary>comment</summary>和940可以说是相关，解法又毫不相关。</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 40.0
- ACtime: 2021.12.3
- ACstatus: 不会
- score: 4.0
</details>
</details>
<details>
<summary>使数组中所有元素相等的最小操作数</summary>

#### platform leetcode
#### id 1551.0

<details>
<summary>lh</summary>

- <details><summary>comment</summary>想到是中位数就行</details>
- <details><summary>tag</summary>数学</details>
- spendtime: 15.0
- ACtime: 2021.12.3
- ACstatus: 完全会
- score: 3.0
</details>
</details>
<details>
<summary>火柴拼正方形</summary>

#### platform leetcode
#### id 473.0

<details>
<summary>lh</summary>

- <details><summary>comment</summary>这题递归回溯不错</details>
- <details><summary>tag</summary>递归回溯</details>
- spendtime: 40-50
- ACtime: 2021.12.3
- ACstatus: 不会
- score: 4.0
</details>
</details>
<details>
<summary>找出不同的二进制字符串</summary>

#### platform leetcode
#### id 1980.0

<details>
<summary>gx</summary>

- <details><summary>comment</summary>这题遍历所有可能的二进制串即可</details>
- <details><summary>tag</summary>二进制转换</details>
- spendtime: 15.0
- ACtime: 2021.12.4
- ACstatus: 完全会
- score: 2.0
</details>
</details>
<details>
<summary>最小化目标值与所选元素的差</summary>

#### platform leetcode
#### id 1981.0

<details>
<summary>gx</summary>

- <details><summary>comment</summary>分组背包问题</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 50.0
- ACtime: 2021.12.4
- ACstatus: 只会一半剩下的得看题解
- score: 3.0
</details>
</details>
<details>
<summary>重新安排行程</summary>

#### platform leetcode
#### id 322.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>欧拉路</details>
- <details><summary>tag</summary>欧拉路+贪心</details>
- spendtime: 不详
- ACtime: 2021.12.6
- ACstatus: 不会
- score: 6.0
</details>
</details>
<details>
<summary>破解保险箱</summary>

#### platform leetcode
#### id 753.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>欧拉路，再次写的时候，从走坑发现不太对，再到意识到是欧拉回路问题，再到抽象对图的时候，还是走了不少坑，注意欧拉回路算法的定义，边走一次，点可以重复走，学过知识还是有长进的</details>
- <details><summary>tag</summary>欧拉路+判定</details>
- spendtime: 不详
- ACtime: 2021.12.6
- ACstatus: 不会 -> 会
- score: 6.0
</details>
</details>
<details>
<summary>合法重新排列数对</summary>

#### platform leetcode
#### id 2097.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>欧拉路</details>
- <details><summary>tag</summary>欧拉路+找起点</details>
- spendtime: 不详
- ACtime: 2021.12.6
- ACstatus: 不会
- score: 6.0
</details>
</details>
<details>
<summary>入场安检</summary>

#### platform leetcode
#### id LCP 47

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这是上次战队赛，在赛场上，你们并没有完成的题目，现在重新回顾，发现经过分析题目还是比较简单的，代码量也简单，你们可以做一做，看看当时你们不会的东西，到现在有没有思维上的长进</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 50.0
- ACtime: 2021.12.7
- ACstatus: 完全会
- score: 6.0
</details>
</details>
<details>
<summary>到达目的地的方案数</summary>

#### platform leetcode
#### id 1976.0

<details>
<summary>gx</summary>

- <details><summary>comment</summary>最短路，dijstra 算法，这道题手速题，然而第一遍还是倒在了数据范围上，数组d应该用long 类型的</details>
- <details><summary>tag</summary>最短路</details>
- spendtime: 50.0
- ACtime: 2021.12.7
- ACstatus: 完全会
- score: 3.0
</details>
</details>
<details>
<summary>环形闯关游戏</summary>

#### platform leetcode
#### id lcp 49

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这题对于目前的我，还是太难，楼教主yyds,这种类型的题目，不仅仅是对于题意的理解，而在于多年来对于这种问题处理的手段的积累，还是非常非常非常难，编码这次并不复杂，但是判定的方式，非常的牛，时至今日终于，看完了所有lcp系列的所有问题，基本上压轴题，倒数第二题是不会的，其他的努力的话还是可以的</details>
- <details><summary>tag</summary>枚举+判定</details>
- spendtime: 110.0
- ACtime: 2021.12.8
- ACstatus: 不会
- score: 7.0
</details>
</details>
<details>
<summary>括号生成</summary>

#### platform leetcode
#### id 22.0

<details>
<summary>gx</summary>

- <details><summary>comment</summary>递归枚举即可</details>
- <details><summary>tag</summary>递归</details>
- spendtime: 10.0
- ACtime: 2021.12.8
- ACstatus: 完全会
- score: 2.0
</details>
</details>
<details>
<summary>最长有效括号</summary>

#### platform leetcode
#### id 32.0

<details>
<summary>gx</summary>

- <details><summary>comment</summary>dp，这题有用栈做的，不过dp才是正解</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 40.0
- ACtime: 2021.12.8
- ACstatus: 不会
- score: 3.0
</details>
</details>
<details>
<summary>赛车</summary>

#### platform leetcode
#### id 818.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>读懂题意了之后直接上手写，注意标记</details>
- <details><summary>tag</summary>最短路ordp</details>
- spendtime: 50.0
- ACtime: 2021.12.9
- ACstatus: 完全会
- score: 6.0
</details>
</details>
<details>
<summary>公交路线</summary>

#### platform leetcode
#### id 815.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>定好节点，模拟走就行，发现这两题比较类似，而且，都是dp也可以做，虽然dp的速度会高一些，但是，觉得，差距大概十几倍的样子，还行还行</details>
- <details><summary>tag</summary>最短路ordp</details>
- spendtime: 50.0
- ACtime: 2021.12.9
- ACstatus: 完全会
- score: 6.0
</details>
</details>
<details>
<summary>划分数字的方案数</summary>

#### platform leetcode
#### id 1977

<details>
<summary>gx</summary>

- <details><summary>comment</summary>这道题做了我很久。非常考验思维，而且编程实现起来细节非常的多，很容易错。在dp的基础上，为了在O(1)时间内比较俩个字符串的大小，用到了lcp数组（最长公共前缀），并且不线性相加解，用到了前缀和。不得不说一道题结合dp,lcp,前缀和。很锻炼能力</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 120.0
- ACtime: 2021.12.9
- ACstatus: 不会
- score: 5.0
</details>
</details>
<details>
<summary>最大人工岛</summary>

#### platform leetcode
#### id 827.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>集合大小维护，注意一点点细节就行</details>
- <details><summary>tag</summary>并查集</details>
- spendtime: 19.0
- ACtime: 2021.12.10
- ACstatus: 完全会
- score: 5.0
</details>
</details>
<details>
<summary>连续整数求和</summary>

#### platform leetcode
#### id 829.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>代码量超级短，数学分析即可，下次一定要更快更快，我觉得分析不需要超过10分钟，应该是个20分钟的题目</details>
- <details><summary>tag</summary>数学</details>
- spendtime: 30.0
- ACtime: 2021.12.10
- ACstatus: 完全会
- score: 5.0
</details>
</details>
<details>
<summary>黑板异或游戏</summary>

#### platform leetcode
#### id 810.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>数学分析牛逼，分析之集大成者，此类题目真的是非常非常非常考验，大胆猜测的能力，和某一周的周赛的第三题那种十分类似，看来，博弈问题，完全可以通过分析，然后用几行代码表达出来，牛逼，之所以，正常的博弈问题的求解无法工作，是因为不好表示此时的局面，也不好表示下一步的决策，会爆掉时间空间的</details>
- <details><summary>tag</summary>数学</details>
- spendtime: 30.0
- ACtime: 2021.12.10
- ACstatus: 不会
- score: 7.0
</details>
</details>
<details>
<summary>二进制矩阵中的最短路径</summary>

#### platform leetcode
#### id 1091.0

<details>
<summary>lh</summary>

- <details><summary>comment</summary>bfs模板</details>
- <details><summary>tag</summary>bfs</details>
- spendtime: 20.0
- ACtime: 2021.12.10
- ACstatus: 完全会
- score: 3.0
</details>
</details>
<details>
<summary>从子集的和还原数组</summary>

#### platform leetcode
#### id 1982.0

<details>
<summary>gx</summary>

- <details><summary>comment</summary>这道题是数学题，看了题解才知道，是在经典题目的改编，原本是集合的数都是正数。这道题将所有的数加上-min，转化为一个新问题</details>
- <details><summary>tag</summary>数学</details>
- spendtime: 70.0
- ACtime: 2021.12.10
- ACstatus: 不会
- score: 6.0
</details>
</details>
<details>
<summary>零矩阵</summary>

#### platform leetcode
#### id xxx

<details>
<summary>lh</summary>

- <details><summary>comment</summary>哈希</details>
- <details><summary>tag</summary>哈希</details>
- spendtime: 10.0
- ACtime: 2021.12.10
- ACstatus: 完全会
- score: 2或3
</details>
</details>
<details>
<summary>数组的均值分割</summary>

#### platform leetcode
#### id 805.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这道题目简直太好了，真的太好了，建议都去做做，这就是不优雅解法和优雅解法直接在性能上就有所体现，真的转化分析，层层推进，太好了，这题真的太好了</details>
- <details><summary>tag</summary>搜索技巧</details>
- spendtime: 25.0
- ACtime: 2021.12.13
- ACstatus: 完全会+正解更好
- score: 6.0
</details>
</details>
<details>
<summary>打砖块</summary>

#### platform leetcode
#### id 803.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>好题好题，这题的编码，长，而且很考验对每一步的语义抽象，非常好，太好了，要把每一个细节思考的清清楚楚，这题作为并查集的一个示例是相当好的，非常不错，值得推荐大家可以都做一下</details>
- <details><summary>tag</summary>分析+并查集</details>
- spendtime: 70.0
- ACtime: 2021.12.13
- ACstatus: 完全会
- score: 6.0
</details>
</details>
<details>
<summary>序列顺序查询</summary>

#### platform leetcode
#### id 5937.0

<details>
<summary>gx</summary>

- <details><summary>comment</summary>调整两个堆的大小，小顶堆堆存储整堆数中大的部分，大顶堆存储小的部分。查询以及调整的过程中不断调整堆。数据流从小堆进入，从大堆出。其本质是模拟一个二叉树</details>
- <details><summary>tag</summary>堆</details>
- spendtime: 40.0
- ACtime: 2021.12.13
- ACstatus: 完全会
- score: 4.0
</details>
</details>
<details>
<summary>数据流的中位数</summary>

#### platform leetcode
#### id 295.0

<details>
<summary>gx</summary>

- <details><summary>comment</summary>和上一题是相同的，好题</details>
- <details><summary>tag</summary>堆</details>
- spendtime: 20.0
- ACtime: 2021.12.13
- ACstatus: 完全会
- score: 4.0
</details>
</details>
<details>
<summary>强整数</summary>

#### platform leetcode
#### id 970.0

<details>
<summary>lh</summary>

- <details><summary>comment</summary>模拟 有点简单没啥意思</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 15.0
- ACtime: 2021.12.13
- ACstatus: 完全会
- score: 2.0
</details>
</details>
<details>
<summary>从子集的和还原数组</summary>

#### platform leetcode
#### id 1982.0

<details>
<summary>lh</summary>

- <details><summary>comment</summary>数学？</details>
- <details><summary>tag</summary>数学</details>
- spendtime: 30.0
- ACtime: 2021.12.13
- ACstatus: 不会
- score: 5.0
</details>
</details>
<details>
<summary>使序列递增的最小交换次数</summary>

#### platform leetcode
#### id 801.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接dp吧</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 20.0
- ACtime: 2021.12.14
- ACstatus: 完全会
- score: 5.0
</details>
</details>
<details>
<summary>得分最高的最小轮调</summary>

#### platform leetcode
#### id 798.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这题怎么说，就是，会有两种转化，第一种，直接使用转移的方式，可以求出，k-1的前半段，和k-n的后半段，的转移，这种思维考察难度很大，第二种，考虑每个数值生效的区间，转变为区间更新问题，会容易很多，我一时间竟然选择了比较一般的思维难度较大的方式，所以，会在编码细节上，浪费大量的时间，虽然两种，代码一样很短，但是还是推荐，第二种</details>
- <details><summary>tag</summary>转移or区间更新</details>
- spendtime: 60.0
- ACtime: 2021.12.14
- ACstatus: 完全会
- score: 6.0
</details>
</details>
<details>
<summary>构造元素不等于两相邻元素平</summary>

#### platform leetcode
#### id 1968

<details>
<summary>gx</summary>

- <details><summary>comment</summary>直接构造吧</details>
- <details><summary>tag</summary>数学</details>
- spendtime: 10.0
- ACtime: 2021.12.14
- ACstatus: 完全会
- score: 2.0
</details>
</details>
<details>
<summary> 数组元素的最小非零乘积</summary>

#### platform leetcode
#### id 1969

<details>
<summary>gx</summary>

- <details><summary>comment</summary>数学，首先需要构造解，但错了好几个点，int 左移不能超过32位，其次，% 的优先级导致很多错</details>
- <details><summary>tag</summary>数学</details>
- spendtime: 30.0
- ACtime: 2021.12.14
- ACstatus: 完全会
- score: 3.0
</details>
</details>
<details>
<summary>保证文件名唯一</summary>

#### platform leetcode
#### id 1487.0

<details>
<summary>lh</summary>

- <details><summary>comment</summary>哈希</details>
- <details><summary>tag</summary>哈希</details>
- spendtime: 30.0
- ACtime: 2021.12.14
- ACstatus: 完全会
- score: 3.0
</details>
</details>
<details>
<summary>实现一个魔法字典</summary>

#### platform leetcode
#### id 676.0

<details>
<summary>lh</summary>

- <details><summary>comment</summary>哈希,从新做一次，确实是有惯性的，但是其实比较一下，看是否不同1个即可，这样才是快速的</details>
- <details><summary>tag</summary>哈希</details>
- spendtime: 15.0
- ACtime: 2021.12.14
- ACstatus: 完全会
- score: 3.0
</details>
</details>
<details>
<summary>阶乘函数后 K 个零</summary>

#### platform leetcode
#### id 793.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>时间这么长，主要是，有一定的原因，作为人来说，并不是能在一开始，就能找到最优解的方法的，需要不断的试错，最开始需要分析题目的题意，并且尝试在纸上找找规律，发现，其实也就是将每个数分解，找出跟5^x有关的即可，而且发现重要的性质，最终的答案，只有5或者0，方法一，能不能，直接用一个set，将所有离散出现的点值直接，存起来，这样，可以直接通过查询就判定出来，根据数据量，可想而知，直接超出内存，故放弃，第二种，不存储，直接动态查询吧，每隔5就算一次，然后累加，直接超时间，故反复思考，方法三，如何能将计算5的幂次这种，求得快一点，那既然每一步长，都是5，那其实可以，直接增1即可，所以求得会更快，但，还是超出了时间，卡在了最后一个，样例上，那再去优化，方法四，换一种想法，假设定出x，能否快速求出，其中得5的幂次和，答案是可以的，先静态存储5的1，2，3，4，5，，，，，次幂的值，然后直接除，然后累加数值，对于一个x,从1-x的所有5的幂次和是，x/5+x/25+x/125x/625+...，在log5k的级别就能求出，这下简直太好了，可以转判定了，此题很不错</details>
- <details><summary>tag</summary>数学</details>
- spendtime: 90.0
- ACtime: 2021.12.15
- ACstatus: 完全会
- score: 6.0
</details>
</details>
<details>
<summary>太平洋大西洋水流问题</summary>

#### platform leetcode
#### id 417.0

<details>
<summary>lh</summary>

- <details><summary>comment</summary>写不太对啊，dfs</details>
- <details><summary>tag</summary>dfs</details>
- spendtime: 50.0
- ACtime: 2021.12.15
- ACstatus: 不会·
- score: 4.0
</details>
</details>
<details>
<summary>修剪二叉搜索树</summary>

#### platform leetcode
#### id 669.0

<details>
<summary>lh</summary>

- <details><summary>comment</summary>递归</details>
- <details><summary>tag</summary>递归</details>
- spendtime: 30.0
- ACtime: 2021.12.15
- ACstatus: 完全会
- score: 3.0
</details>
</details>
<details>
<summary>第 K 个最小的素数分数</summary>

#### platform leetcode
#### id 786.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>被我做水了？？？？？虽然是一种方法，但对于更大场景下，如果数据，变成，10^4呢，相信还是用不了的，所以，转判定是合适，二分一下，转为，如果，给定一个数，那么如何求比它小的素分数的个数，使用双指针来完成即可</details>
- <details><summary>tag</summary>排序</details>
- spendtime: 10.0
- ACtime: 2021.12.16
- ACstatus: 完全会+正解更好
- score: 4（不用正解）
+2（用正解）
</details>
</details>
<details>
<summary>变为棋盘</summary>

#### platform leetcode
#### id 782.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>觉得自己还是角度出发的有问题了，优化了很久，没有很顺畅的找出来，具体解题的角度，还是需要思考，放到明日再来</details>
- <details><summary>tag</summary>分析</details>
- spendtime: 120.0
- ACtime: 2021.12.16
- ACstatus: 尝试过很久之后不会
- score: 7.0
</details>
</details>
<details>
<summary>你能穿过矩阵的最后一天</summary>

#### platform leetcode
#### id 1970.0

<details>
<summary>gx</summary>

- <details><summary>comment</summary>错了一次，第一次内存爆了，将所有棋盘事先存放进去了。后来仔细计算发现，不提前存好棋盘，在dfs前构造出棋盘，因为构造出棋盘的时间复杂度和构造的时间复杂度差不太多。</details>
- <details><summary>tag</summary>bfs 二分</details>
- spendtime: 60.0
- ACtime: 2021.12.16
- ACstatus: 完全会
- score: 4.0
</details>
</details>
<details>
<summary>移除石子使总数最小</summary>

#### platform leetcode
#### id 1962.0

<details>
<summary>gx</summary>

- <details><summary>comment</summary>堆</details>
- <details><summary>tag</summary>堆</details>
- spendtime: 10.0
- ACtime: 2021.12.16
- ACstatus: 完全会
- score: 2.0
</details>
</details>
<details>
<summary>使字符串平衡的最小交换次数</summary>

#### platform leetcode
#### id 1963.0

<details>
<summary>gx</summary>

- <details><summary>comment</summary>误以为dp了，看数据知道直接可以构造解。</details>
- <details><summary>tag</summary>遍历</details>
- spendtime: 20.0
- ACtime: 2021.12.16
- ACstatus: 不会
- score: 3.0
</details>
</details>
<details>
<summary>到达终点</summary>

#### platform leetcode
#### id 780.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>还不错的数学小题目，题目简单，模型简单，代码也简单，是个不错的练脑题，从正序思考之后，改成逆序思考会很容易</details>
- <details><summary>tag</summary>数学</details>
- spendtime: 45.0
- ACtime: 2021.12.17
- ACstatus: 完全会
- score: 6.0
</details>
</details>
<details>
<summary>找出到每个位置为止最长的有效障碍赛跑路线</summary>

#### platform leetcode
#### id 1964.0

<details>
<summary>gx</summary>

- <details><summary>comment</summary>最长上升子序列问题，优化到lnlogn复杂度，倒在二分上</details>
- <details><summary>tag</summary>dp+二分</details>
- spendtime: 40.0
- ACtime: 2021.12.17
- ACstatus: 不会
- score: 4.0
</details>
</details>
<details>
<summary>滑动谜题</summary>

#### platform leetcode
#### id 773.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>搜索问题，有很多种可选得方案，不过这题怎么简单怎么来，最好的工程实践的封装还是得去看普林斯顿算法，当时觉得此题特别特别好不过这次我直接搜索了，数据量小的话还是直接搜索比较好Programming Assignment 4: Slider Puzzle (princeton.edu)</details>
- <details><summary>tag</summary>搜索</details>
- spendtime: 50.0
- ACtime: 2021.12.18
- ACstatus: 完全会
- score: 6.0
</details>
</details>
<details>
<summary>水位上升的泳池中游泳</summary>

#### platform leetcode
#### id 778.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>dp的游走，不过官方给出的解决方案，还是很多的，很不错的，老生常谈，的问题</details>
- <details><summary>tag</summary>bfsor并查集or最短路or二分</details>
- spendtime: 20.0
- ACtime: 2021.12.18
- ACstatus: 完全会
- score: 6.0
</details>
</details>
<details>
<summary>从英文中重建数字</summary>

#### platform leetcode
#### id 423.0

<details>
<summary>lh</summary>

- <details><summary>comment</summary>没啥新意，找到关系就可以了</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 20.0
- ACtime: 2021.12.18
- ACstatus: 完全会
- score: 3.0
</details>
</details>
<details>
<summary>检查一个字符串是否</summary>

#### platform leetcode
#### id 1461.0

<details>
<summary>lh</summary>

- <details><summary>comment</summary>纯暴力的map是可以过的，但是滑动窗口的时间更好一些，他的数据在扩大一些就不好说了</details>
- <details><summary>tag</summary>滑动窗口</details>
- spendtime: 20.0
- ACtime: 2021.12.18
- ACstatus: 完全会
- score: 3.0
</details>
</details>
<details>
<summary>情侣牵手</summary>

#### platform leetcode
#### id 765.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>真没想出来！！！，转换无敌，怎么就不会了呢？？这题以后还是得再来一次，据说是华为第一题，什么？第一题我都不会！！！窒息！！真的是痛痛痛啊！给高分的目的就是让我记住</details>
- <details><summary>tag</summary>并查集</details>
- spendtime: 60.0
- ACtime: 2021.12.20
- ACstatus: 不会
- score: 7.0
</details>
</details>
<details>
<summary>区间子数组的个数</summary>

#### platform leetcode
#### id 795.0

<details>
<summary>lh</summary>

- <details><summary>comment</summary>和上周周赛第三题比较像，用dp的话确实好想一点</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 20.0
- ACtime: 2021.12.20
- ACstatus: 完全会·
- score: 3.0
</details>
</details>
<details>
<summary>你完成的完整对局数</summary>

#### platform leetcode
#### id 1904.0

<details>
<summary>lh</summary>

- <details><summary>comment</summary>模拟，里面几种特殊情况要想一想</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 20.0
- ACtime: 2021.12.20
- ACstatus: 完全会
- score: 3.0
</details>
</details>
<details>
<summary>找到所有好字符串</summary>

#### platform leetcode
#### id 1397

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>刚开始觉得是个编码复杂，细节居多的计数优化问题，做好了长时间的消耗的准备，但是没想到，写完之后，发下了重大的问题，就是，会重复减，这下，竟然卡了我许久，不知如何解决,后来再次查看这题的时候，发现，这是，182场周赛的题目，当时我应该是放着这道题目，等着以后来做，而且翻看了以前的记录表，发现，当时应该还是没有样成良好的习惯，对这道题目没有记录，唉，一方面是，无奈自己遇到这种超高难度的仍然没有长进，另一方面是，又浪费了很多时间思考了我不会的东西，总之就是又无奈又可惜</details>
- <details><summary>tag</summary>数位dp+kmp</details>
- spendtime: 120+
100
- ACtime: 2021.12.21
- ACstatus: 不会
- score: 8.0
</details>
</details>
<details>
<summary>两个回文子字符串长度的最大乘积</summary>

#### platform leetcode
#### id 1960.0

<details>
<summary>gx</summary>

- <details><summary>comment</summary>马拉车算法，学到的新算法。O(n)时间计算最长回文串。</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 120.0
- ACtime: 2021.12.21
- ACstatus: 不会
- score: 5.0
</details>
</details>
<details>
<summary>检查操作是否合法</summary>

#### platform leetcode
#### id 1958.0

<details>
<summary>gx</summary>

- <details><summary>comment</summary>遍历即可</details>
- <details><summary>tag</summary>遍历</details>
- spendtime: 30.0
- ACtime: 2021.12.22
- ACstatus: 完全会
- score: 2.0
</details>
</details>
<details>
<summary>子数组和排序后的区间和</summary>

#### platform leetcode
#### id 1508.0

<details>
<summary>lh</summary>

- <details><summary>comment</summary>其实这题是道不错的题，只是可以暴力通过而已，她后面说的前缀和挺好的</details>
- <details><summary>tag</summary>暴力或者前缀和</details>
- spendtime: 20.0
- ACtime: 2021.12.22
- ACstatus: 只会一半
- score: 3.0
</details>
</details>
<details>
<summary>减小和重新排列数组后的最大值</summary>

#### platform leetcode
#### id 1846.0

<details>
<summary>lh</summary>

- <details><summary>comment</summary>这题是个贪心，好好看一看他的数组关系就可以了</details>
- <details><summary>tag</summary>贪心</details>
- spendtime: 20.0
- ACtime: 2021.12.22
- ACstatus: 完全会
- score: 3.0
</details>
</details>
<details>
<summary>设计跳表</summary>

#### platform leetcode
#### id 1206.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>编码很复杂，遂写了一半懒得写了，感觉适合作为一个lab出出来，作为一道题目的话，还是太消耗时间了，并不适合，不过数据结构还是好的，可以重点看看时空复杂度分析，能更好的理解，以后这个数据结构完全可以作为板子，但其实仔细想了想，用处不一定很大，只是一个在增删查上面logn，还是没有，treemap实用，毕竟可以范围查询，所以其实感觉就是个编码的训练。</details>
- <details><summary>tag</summary>数据结构</details>
- spendtime: 100.0
- ACtime: 2021.12.23
- ACstatus: 会但没有写完全
- score: 6.0
</details>
</details>
<details>
<summary>K 次调整数组大小浪费的最小总空间</summary>

#### platform leetcode
#### id 1959

<details>
<summary>gx</summary>

- <details><summary>comment</summary>没看出来是区间dp，陷入到题意中。一直在想如何达到最优解</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 50.0
- ACtime: 2021.12.23
- ACstatus: 不会
- score: 3.0
</details>
</details>
<details>
<summary>最多能完成排序的块 II</summary>

#### platform leetcode
#### id 768.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>速度稍微慢点，主要是想了想别的东西，题意好理解，线性算法就过了，没有太大的难点，脑筋急转弯吗，是一个比较适合做的困难，其他人也可以做做</details>
- <details><summary>tag</summary>分析+遍历</details>
- spendtime: 35.0
- ACtime: 2021.12.24
- ACstatus: 完全会
- score: 6.0
</details>
</details>
<details>
<summary>特殊的二进制序列</summary>

#### platform leetcode
#### id 761

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>我直接搜索了，好像正解只能朝着更大的方向进行搜索，？？？好像没有太想明白为啥，简答来说，就是，只会慢慢增大，直接去除交换可能变小的值即可，使用一个优先级队列就可以了？但我觉得这块？？？是不是递归的理解，会更好一些，好像确实，再次重做的时候，速度是变快了，但是，办法依然不优雅,就是因为数据量太小了，所以能这么写</details>
- <details><summary>tag</summary>搜索or分治</details>
- spendtime: 80.0
- ACtime: 2021.12.24
- ACstatus: 完全会+正解更好
- score: 6.0
</details>
</details>
<details>
<summary>有序数组中差绝对值之和</summary>

#### platform leetcode
#### id 1685.0

<details>
<summary>lh</summary>

- <details><summary>comment</summary>从数学的角度去考虑这个问题就比较简单的</details>
- <details><summary>tag</summary>前缀和</details>
- spendtime: 20.0
- ACtime: 2021.12.24
- ACstatus: 完全会
- score: 3.0
</details>
</details>
<details>
<summary>随机数索引</summary>

#### platform leetcode
#### id 398.0

<details>
<summary>lh</summary>

- <details><summary>comment</summary>什么是蓄水池抽样，有没有题号</details>
- <details><summary>tag</summary></details>
- spendtime: 20.0
- ACtime: 2021.12.24
- ACstatus: 只会暴力
- score: 3.0
</details>
</details>
<details>
<summary>设置交集大小至少为2</summary>

#### platform leetcode
#### id 757.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>求对于一组区间来说，每个区间至少有两个数在新的数组中，要求数组最短，这道题目，是lcp32的最简单版本，那道题是带权重的，这道题只是，区间覆盖的一种变形，所以，使用lcp32的代码可以直接解，但贪心的算法也很好写, 时隔很久再次重写，再次重写贪心算法，确实还可以，比较简单就可以实现</details>
- <details><summary>tag</summary>贪心</details>
- spendtime: 20.0
- ACtime: 2021.12.27
- ACstatus: 完全会+一般情况更难
- score: 6.0
</details>
</details>
<details>
<summary>隔离病毒</summary>

#### platform leetcode
#### id 749.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>考验编码耐心和能力时候到啦，这道题真的非常考验能不能正常把过程使用代码描写出来噢，细节和考虑还是挺多的，所以，确实很考验写代码的能力，虽然题面的理解就一小会，但是编码确实特别花时间，可以作为某一次，超长耐力的训练题目,再次写了一次，果然还是十分的细节，超级大模拟，依然没有在规定时间内写完，太菜啦</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 90.0
- ACtime: 2021.12.28
- ACstatus: 完全会
- score: 6.0
</details>
</details>
<details>
<summary>摘樱桃</summary>

#### platform leetcode
#### id 741

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>题面是这个意思，走两遍，从左上角到右下角，但是只能取一次问最大，这道题，做飞了，上手的时候，转换是没有问题的，但是，在设计状态的时候，还是没有完全想清楚，依稀记得以前搞过类似的，但无奈没有了印象，对于这种处理应该如何处理，真的是非常，失败，唉，无奈啊，无奈,维度可压缩，其实最简单dp[x1][y1][x2][y2],当然可以变成三维的，直接就可以做了，唉, 再次的时候，首选就是dp和网络流，不过网络流的板子太复杂，不如直接dp快，然后40多分钟，直接就过掉了，方法虽然没有直接出，但是会查会看，还是可以的，全球名次就还不错</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 120.0 -> 45
- ACtime: 2021.12.29 - 2023.2.17
- ACstatus: 不会 - 会
- score: 7.0
</details>
</details>
<details>
<summary>统计不同回文子序列</summary>

#### platform leetcode
#### id 730.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>不得不说，虽然我不会，但这确实是一个很好的题目，真的很好，状态设计，或许是比较好出来的，但是！状态的转移，算是非常难的，难讨论，也同样比较难想，我觉得算是一个非常好非常不错的，关于去重部分的思考，很容易让我想到以前的不重复子序列，这种，题目，都是难难题，是可以被以后留下来，的东西,再来一遍还是不会，dp状态想不来，简单来说，就是字符集上下功夫，可以这么考虑，用dp[z][i][j]表示首尾是z的回文子序列在i-j中有多少个,但是如果对于任意字符集怎么办，二维动态规划更好，关键在于当s[i]=s[j]时，在区间找s[low]=s[high]，减去dp[low + 1][high - 1]，虽然字符串的话撑死不超过26个</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 60.0
- ACtime: 2021.12.30 - 2023.2.20
- ACstatus: 不会 -> 不会
- score: 7.0
</details>
</details>
<details>
<summary>我的日程安排表 III</summary>

#### platform leetcode
#### id 732.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>不太配为困难，觉得应该降为一个中等就不错了</details>
- <details><summary>tag</summary>区间更新</details>
- spendtime: 25.0
- ACtime: 2021.12.31
- ACstatus: 完全会
- score: 5.0
</details>
</details>
<details>
<summary>前缀和后缀搜索</summary>

#### platform leetcode
#### id 745.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>看了一下，211题的做法，然后非常朴素的，生成一下符合211查询的String，然后复用211思路和部分代码,再次做的时候，用前后缀的首字母作为分类+结果记忆化就过掉了，编码37分钟，感觉还好？写字典树的这样的做法反正再来一遍的时候，依然没有想起来，但是依然能够搞出来，再次增强一下记忆</details>
- <details><summary>tag</summary>字典树</details>
- spendtime: 75.0 -> 37
- ACtime: 2022.1.1 - 2023.2.16
- ACstatus: 看了提示 能搞出来
- score: 6.0
</details>
</details>
<details>
<summary>添加与搜索单词 - 数据结构设计</summary>

#### platform leetcode
#### id 211.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>查询应该还是很高效的，但不知道分叉对时间复杂度的影响</details>
- <details><summary>tag</summary>字典树</details>
- spendtime: 20.0
- ACtime: 2022.1.1
- ACstatus: 完全会
- score: 5.0
</details>
</details>
<details>
<summary>最小区间</summary>

#### platform leetcode
#### id 632

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>我总结一句就是手生，真的好久没有，做相关的东西了，一个非常大胆的猜测，就是双指针一定包含最优解，直接走就可以了，我不知道我在干啥，刚开始还在尝试二分，不明白，真的是浪费时间愚蠢到家，所以，采用正解的没有花很多，时间在这上面，而是走弯路太耗时间了，愚蠢题目，而且没啥难度的题目，单纯就是愚蠢</details>
- <details><summary>tag</summary>双指针</details>
- spendtime: 80.0
- ACtime: 2022.1.3
- ACstatus: 完全会
- score: 6.0
</details>
</details>
<details>
<summary>超级洗衣机</summary>

#### platform leetcode
#### id 517.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>记录一下，贪心的最强，我不配，贪心，我的老天爷，贪心，过不去的坎，难，一般都是代码简单，但思维太难想了，谁能想到打包算法，把前面的看成一体，将整个数组抽象成两个值，这是真的，无法想到，可能是由于传递性，这种隐含的条件推出来的，但，这也太难想了，我现在还是懵懵懂懂的，比较难搞，难, 重新再来一次的时候，依然时间上比较超，但是在疯狂wa之后，终于贪心正确了，代码很短，可以这么考虑，对于一个给予方，它至少要给予x-junzhi, 对于一个得到方，不断的被给予，至少是abs(前缀和)，前缀和是负数的情况下，依然很难，不过也能看出来我出题的能力确实是变高了，加油再接再厉</details>
- <details><summary>tag</summary>贪心</details>
- spendtime: 50.0
- ACtime: 2022.1.4 - 2023.5.13
- ACstatus: 不会 -会
- score: 7.0 - 7
</details>
</details>
<details>
<summary>原子的数量</summary>

#### platform leetcode
#### id 726

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>硬性编码，考虑清楚，我一遍过，就是稳扎稳打，没有别的技巧，模拟人的思考过程，好好的写出来就完事了，感觉考察的方向是比较适合考验心态的一次测试,时间竟然和一年前差不多，速度怎么没有提高啊？</details>
- <details><summary>tag</summary>体力</details>
- spendtime: 50.0
- ACtime: 2022.1.5
- ACstatus: 完全会
- score: 6.0
</details>
</details>
<details>
<summary>奇怪的打印机</summary>

#### platform leetcode
#### id 664

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>给加分的原因是，本题倒没有那么复杂，但是，因为自身的原因，可能在转移的过程中，没有想到最优的方式，把自己绕进去了，一直在想，怎么分段，能够枚举所有的子段，这个真的是很愚蠢，真的是很愚蠢，所以要记录一下,再次做的时候，又把自己绕进去了，没想到时隔一年，竟然对这种分段dp又一次的崩溃了，尝试了很多种dp的方式，就是没想道这种，失败中的失败，真的太太失败，时隔一年又没长进</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 90.0
- ACtime: 2022.1.6 - 2023.3.20
- ACstatus: 不会+但一提就知道了 - 依然不会
- score: 7.0
</details>
</details>
<details>
<summary>可怜的小猪</summary>

#### platform leetcode
#### id 458.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>我觉得，这种东西，做一遍，应该就能印象深刻很多，属于，不做，绝对不能快速写出，动态规划的推导绝对不是很直观的一种转移，我初次反应，就觉得这绝对是个信息论的东西，我觉得直觉还是可以的，再说设计方案，之所以，只测一次的方案好想是因为，设计方案可以直接与二进制编码联系起来，可以隐含着说，设测试数T，对于x个来说，最多能表示的状态数(T+1)^x这个式子的理解可以多多思考</details>
- <details><summary>tag</summary>信息论</details>
- spendtime: 60.0
- ACtime: 2022.1.7
- ACstatus: 不会
- score: 7.0
</details>
</details>
<details>
<summary>课程表 III</summary>

#### platform leetcode
#### id 630.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>贪心，加优先级队列，不过我确实看了看提示，才逐渐反应过来贪心的方式，不过，贪心的思考，真的是大道至简很看运气,再次写的时候，依然被贪心爆杀，我愿称之为难难难，爆杀我，不知道写几遍就杀我几遍</details>
- <details><summary>tag</summary>贪心</details>
- spendtime: 40.0
- ACtime: 2022.1.10 - 2023.4.6
- ACstatus: 会？- 不会
- score: 6.0 - 7
</details>
</details>
<details>
<summary>课程表</summary>

#### platform leetcode
#### id 207.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>裸的拓扑排序</details>
- <details><summary>tag</summary>拓扑</details>
- spendtime: 10.0
- ACtime: 2022.1.10
- ACstatus: 完全会
- score: 4.0
</details>
</details>
<details>
<summary>课程表 II</summary>

#### platform leetcode
#### id 210.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接如上</details>
- <details><summary>tag</summary>拓扑</details>
- spendtime: 5.0
- ACtime: 2022.1.10
- ACstatus: 完全会
- score: 4.0
</details>
</details>
<details>
<summary>安装栅栏</summary>

#### platform leetcode
#### id 587.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>凸包，与之前不一样的，是，可以考虑一下，如何处理，当三点以上位于同样的直线这个情况，反正使用朴素做法即可，但我写出来，是不是稍微麻烦了，一点点，时间上，稍稍有一点点多，为什么呢？留到以后, 重新温习n^2算法，凸包每次还是得看啊，没办法的，没办法捏</details>
- <details><summary>tag</summary>数学</details>
- spendtime: 80.0
- ACtime: 2022.1.11
- ACstatus: 完全会
- score: 6.0
</details>
</details>
<details>
<summary>黑名单中的随机数</summary>

#### platform leetcode
#### id 710.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>想好映射即可，没啥的，感觉算是比较简单的，现在看起来，随机栈的调整方式应该更有记忆点一样，或许在题中的实现稍微有一点概率不太，相等，但真正意义上的等概率也是好实现的，只需要，分成两个部分就可以，也比较简单</details>
- <details><summary>tag</summary>数学</details>
- spendtime: 30.0
- ACtime: 2022.1.12
- ACstatus: 完全会
- score: 5.0
</details>
</details>
<details>
<summary>K个逆序对数组</summary>

#### platform leetcode
#### id 629.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>初看较难，但经过冷静思考之后，发现，推导可以再精简一点，然后再次精简，精简到更新的划分，直到完全明白，有个过程，然后过程中，也会调了一会bug，然后就过了，dp好好思考，这个二维动态规划还是不错的</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 60.0
- ACtime: 2022.1.13
- ACstatus: 完全会
- score: 6.0
</details>
</details>
<details>
<summary>为高尔夫比赛砍树</summary>

#### platform leetcode
#### id 675.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>就是稍微写久了一点，懒懒散散的，难度倒没有多少,确实是这样，就是写的久而已</details>
- <details><summary>tag</summary>bfs</details>
- spendtime: 45.0
- ACtime: 2022.1.14
- ACstatus: 完全会
- score: 5.0
</details>
</details>
<details>
<summary>24点游戏</summary>

#### platform leetcode
#### id 679.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>暴力枚举的方式，没有多少难度，再次写了一下时间上应该是差不多的，但我觉得给分没有这么低吧，也应该是个困难题目吧，而且花了40多分钟去写的，速度上也没那么快速？看了一下当年的做法，发现自己现在竟然做蠢了，状态压缩的做法依然优雅，这场比赛其他的题目的速度是非常快的，还可以</details>
- <details><summary>tag</summary>枚举</details>
- spendtime: 40.0
- ACtime: 2022.1.15
- ACstatus: 完全会
- score: 5.0
</details>
</details>
<details>
<summary>找出第 k 小的距离对</summary>

#### platform leetcode
#### id 719.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>不假思索，直接二分转判定</details>
- <details><summary>tag</summary>二分</details>
- spendtime: 30.0
- ACtime: 2022.1.15
- ACstatus: 完全会
- score: 6.0
</details>
</details>
<details>
<summary>掉落的方块</summary>

#### platform leetcode
#### id 699.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>不小心线段树改错了，还debug了半天，以后还是要细心细心，因为这道题数据规模的问题，所以不用线段树，也是ok的，但正解一定是</details>
- <details><summary>tag</summary>区间更新</details>
- spendtime: 45.0
- ACtime: 2022.1.17
- ACstatus: 完全会
- score: 6.0
</details>
</details>
<details>
<summary>乘法表中第k小的数</summary>

#### platform leetcode
#### id 668.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这也算困难？？？？？</details>
- <details><summary>tag</summary>二分</details>
- spendtime: 25.0
- ACtime: 2022.1.17
- ACstatus: 完全会
- score: 5.0
</details>
</details>
<details>
<summary>自由之路</summary>

#### platform leetcode
#### id 514.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>感觉很直观的dp，没啥说的，直接整，这还有啥疑问的，直接搞就行，感觉怎么推都可以，使用一个优先级队列感觉也没啥提速，只要dp的状态定义十分正确，感觉时间上，都不会有什么不对的，所以直接大胆的去写就行</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 40.0
- ACtime: 2022.1.17
- ACstatus: 完全会
- score: 6.0
</details>
</details>
<details>
<summary>最大回文数乘积</summary>

#### platform leetcode
#### id 479.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>不太会，想了想，虽然可以打表，但是仍然不太会，如何枚举呢？后来看了，看，觉得这题还是有点，找规律加，一点点小处理，不太具有模型的可移植性，我觉得算一个不太好的题，也没必要再次看</details>
- <details><summary>tag</summary>找规律</details>
- spendtime: 60.0
- ACtime: 2022.1.18
- ACstatus: 不会
- score: 7.0
</details>
</details>
<details>
<summary>翻转对</summary>

#### platform leetcode
#### id 493.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>不假思索的直接出思路，思路是秒出，但编码需要细致一点，对于小于零和大于零需要单独处理，但总体而言也并不复杂，总的来说，就是使用一下树状数组，就可以很快的完成</details>
- <details><summary>tag</summary>数据结构</details>
- spendtime: 40.0
- ACtime: 2022.1.18
- ACstatus: 完全会
- score: 6.0
</details>
</details>
<details>
<summary>祖玛游戏</summary>

#### platform leetcode
#### id 488.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>以为是道简单的题目，没想到，竟然还挺麻烦的，这种带剪枝的搜索，与我而言还是不太上道，但你要说，这道题目有多大的价值，但其实也就一般，反正我觉得做做看，没啥复用的东西就是一道不知道怎么评价的题目，难确实是难，但除了难，也就那样，确实再次重写的时候，确实还是遇到了很大的磕绊的，没办法，就是这样的，难题，看了下官方题解的时间，也很长，剪枝剪了个寂寞，不如打表一下</details>
- <details><summary>tag</summary>剪枝+搜索</details>
- spendtime: 65.0 - ??
- ACtime: 2022.1.18 - 2023.5.19
- ACstatus: 不太会+打了一个表 - 确实有一个打表了
- score: 7.0 - 7
</details>
</details>
<details>
<summary>滑动窗口中位数</summary>

#### platform leetcode
#### id 480.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>不知不觉，中，编码的时间就长了起来，但因为对这种类型的，比如动态中位数，或者，动态的n位数，这种，都可以使用两个数据结构，对顶的存储起来，就可以解决，可能需要的就是编码量会大一些</details>
- <details><summary>tag</summary>数据结构重组</details>
- spendtime: 60.0
- ACtime: 2022.1.19
- ACstatus: 完全会
- score: 6.0
</details>
</details>
<details>
<summary>最小好进制</summary>

#### platform leetcode
#### id 483.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>思路上没有太难，而是非常直观，因为枚举进制是指数衰减的，在有限个长度下，即可完成，再加上二分的加速，基本上，很快就求得解了</details>
- <details><summary>tag</summary>数学</details>
- spendtime: 35.0
- ACtime: 2022.1.19
- ACstatus: 完全会
- score: 6.0
</details>
</details>
<details>
<summary>连接词</summary>

#### platform leetcode
#### id 472.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>还不错的题目，回顾一下思路，我首先先无脑hash了，超时是件很正常的事情，初步判断是因为，计算子串的hash带来的超时，遂更改成，线性计算hash的方式，但仍然超时，这时候想到更加快速的查找数据结构，字典树，但并没有马上更改自己的代码，因为我反复思考，换一个数据结构能够解决问题？发现其实并不能，因为虽然我使用改版的hash的速度是达到了字典树的效果，所以冷静思考，原来是在搜索的时候出现了问题，哎哎哎，没有记忆化搜索，这才是核心的原因，下次一定，注意对于这种搜索，常用的方法就是记忆化</details>
- <details><summary>tag</summary>标签居多hash字典树深搜dp</details>
- spendtime: 60.0
- ACtime: 2022.1.19
- ACstatus: 看了一下提示会
- score: 7.0
</details>
</details>
<details>
<summary>统计重复个数</summary>

#### platform leetcode
#### id 466.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>想到一种取巧办法，我们给定下标的增长的阈值，然后把增长看成线性的方式，想法很简单，代码也好写，但这个也是一个需要调精度的，取得增长的下标的长度越大，就越精准，就能过掉了，不过，正解的循环节，也是比较精准的方法，速度快很多，重新再做的时候，连骗题都不会了，重点还是找循环节的方法，这个我还是稍微没想清楚，还是很好的题目</details>
- <details><summary>tag</summary>循环节</details>
- spendtime: 70.0
- ACtime: 2022.1.20 - 2023.5.24
- ACstatus: 正解更好+偏解可过
- score: 7.0
</details>
</details>
<details>
<summary>IPO</summary>

#### platform leetcode
#### id 502.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>感觉是一个中等题，很直观，没啥需要考虑的，直接读懂就写就行</details>
- <details><summary>tag</summary>贪心</details>
- spendtime: 25.0
- ACtime: 2022.1.20
- ACstatus: 完全会
- score: 5.0
</details>
</details>
<details>
<summary>LFU 缓存</summary>

#### platform leetcode
#### id 460.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>因为确实有一些编码细节，需要仔细考虑，所以，稍微写慢一点，倒也没啥重要的是，自己的思路，是一语中破题目的意思，丝毫没有走弯路就是双向链表，然后使用hashmap和数组和双向链表结合起来，就可以了，此题据说作为很多家公司的面试题目，还是需要重视一下</details>
- <details><summary>tag</summary>数据结构</details>
- spendtime: 60.0
- ACtime: 2022.1.20
- ACstatus: 完全会
- score: 6.0
</details>
</details>
<details>
<summary>三个无重叠子数组的最大和</summary>

#### platform leetcode
#### id 689.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接写，无算法类题目，不需要太多的多余思考，直接就可以写，我觉得是个中等题都不为过，直接被曾经的自己干翻，可能也是写了25分钟左右，但是口气没有这么狂啊</details>
- <details><summary>tag</summary>模拟 or dp</details>
- spendtime: 25.0
- ACtime: 2022.1.21 - 2023.3.10
- ACstatus: 完全会
- score: 5.0
</details>
</details>
<details>
<summary>冗余连接 II</summary>

#### platform leetcode
#### id 685.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>写的时间长啦，长啦，没办法，稍微调了一会bug，太不应该了，虽然把下标写错是常有的事情，不过蠢事还是应该少干，主要的思路就是破环，不过看了看用并查集的做法，觉得思路还是比我要清晰很多，应该学习！</details>
- <details><summary>tag</summary>模拟</details>
- spendtime: 50.0
- ACtime: 2022.1.21
- ACstatus: 完全会+正解更好
- score: 6.0
</details>
</details>
<details>
<summary>冗余连接</summary>

#### platform leetcode
#### id 684.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接从后往前即可，遇到第一个成环的就是了</details>
- <details><summary>tag</summary>并查集</details>
- spendtime: 8.0
- ACtime: 2022.1.21
- ACstatus: 完全会
- score: 4.0
</details>
</details>
<details>
<summary>等差数列划分 II - 子序列</summary>

#### platform leetcode
#### id 446.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>主要的时间花在了想上面，虽然这是个很简单的dp，但是真真切切的想明白，是个很花时间的事情，是真的不错，要仔细思考，这个dp还是可以的，很费脑子,再一次写的时候，状态设计冗余了，但还是能过，唉</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 60.0
- ACtime: 2022.1.21 - 2023.5.29
- ACstatus: 完全会
- score: 6.0
</details>
</details>
<details>
<summary>分割数组的最大值</summary>

#### platform leetcode
#### id 410.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>属于经典重做了，动态规划和二分都可以解决</details>
- <details><summary>tag</summary>dp二分</details>
- spendtime: 25.0
- ACtime: 2022.1.21
- ACstatus: 完全会
- score: 6.0
</details>
</details>
<details>
<summary>字典序的第K小数字</summary>

#### platform leetcode
#### id 440.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>千万别小瞧这种看似简单的题目，真的是，写的时间巨长巨久，写的我人都要麻了，代码又长又丑，逻辑复现程度，级差，真的无语，没办法，自己写的也太垃圾了，但时间上击败了100%，不知道为啥，所以，坚持写下去，噢力给, 再次写的时候，已经不想爆怼码力了，看了最优雅的解法，感慨道，还是好解法好，好的解法，不仅仅思维抽象清晰，并且代码极其干净整洁，抽象成树，先算节点数，再逐渐顺延下去，很不错的好题目。估计以前疯狂怼了</details>
- <details><summary>tag</summary>搜索</details>
- spendtime: 90.0
- ACtime: 2022.1.22 - 2023.5.31
- ACstatus: 完全会
- score: 6.0
</details>
</details>
<details>
<summary>全 O(1) 的数据结构</summary>

#### platform leetcode
#### id 432.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这题是真的没意思，就是堆代码，有时间做，真的无聊，还是LFU好那个更好，这个是真的无聊，代码写的又臭又长，不写，这就是黑名单题目</details>
- <details><summary>tag</summary>XXX</details>
- spendtime: XXX
- ACtime: 2022.1.22
- ACstatus: 就算不写也会系列
- score: 6.0
</details>
</details>
<details>
<summary>贴纸拼词</summary>

#### platform leetcode
#### id 691.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>状态清晰可见，直接定义，个人觉得此处要是整一些花里胡哨的技巧，很有可能使得自己的大脑负荷太大，所以我觉得裸裸的dp直接上即可，再次写一遍，竟然写麻烦了，其实直接对原始的序列进行dp就好，我是写麻烦了，还转过来转过去的，失败</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 35.0
- ACtime: 2022.1.22
- ACstatus: 完全会
- score: 6.0
</details>
</details>
<details>
<summary>青蛙过河</summary>

#### platform leetcode
#### id 403.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>我觉得确实，直接是bfs加记忆化，是最好理解的，直接这样挺好的，被绕了，按照顺序逐渐向后即可，也就是bfs，只不过是分层的bfs，不知道以前怎么这么快就写出来了</details>
- <details><summary>tag</summary>bfs+记忆化</details>
- spendtime: 25.0 - 80
- ACtime: 2022.1.23 - 2023.6.7
- ACstatus: 完全会
- score: 6.0
</details>
</details>
<details>
<summary>O(1) 时间插入、删除和获取随机元素 - 允许重复</summary>

#### platform leetcode
#### id 381.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>神似普林斯顿随机栈了，就是加了删除这个操作，需要点小编码有一点点搞我，但是还行，也不是很难，就是需要小心处理一下</details>
- <details><summary>tag</summary>hash</details>
- spendtime: 40.0
- ACtime: 2022.1.23
- ACstatus: 完全会
- score: 6.0
</details>
</details>
<details>
<summary>解码方法 II</summary>

#### platform leetcode
#### id 639.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>主要是wa了几次，非常尴尬，线性dp，太直观了，不需要多余的解释</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 35.0
- ACtime: 2022.1.24
- ACstatus: 完全会
- score: 6.0
</details>
</details>
<details>
<summary>寻找最近的回文数</summary>

#### platform leetcode
#### id 564.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>写的也太丑了，可读性太差了，无语子，不过情况非常直观，就4种搜索的方向，从最中间，不变，加一减一，然后加以耐心调bug，属于那种操蛋题了, 再次写的时候，依然没有在规定时间内写完，跟上次记录的时间是差不多的，但是还是比较操蛋，wa了很多次，有很多细节没有写好</details>
- <details><summary>tag</summary>枚举</details>
- spendtime: 65.0
- ACtime: 2022.1.24 - 2023.4.27
- ACstatus: 完全会
- score: 6.0
</details>
</details>
<details>
<summary>强密码检验器</summary>

#### platform leetcode
#### id 420.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>确实难，确实难，没办法，贪心类的分析，还是太难，这题属于，看了之后仍然不太会做系列，谁会啃这个硬骨头呢？</details>
- <details><summary>tag</summary>贪心</details>
- spendtime: 120.0
- ACtime: 2022.1.25
- ACstatus: 完全会
- score: 7.0
</details>
</details>
<details>
<summary>接雨水</summary>

#### platform leetcode
#### id 42.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>就不用单调栈你咬我啊，dp吧简单</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 20.0
- ACtime: 2022.1.25
- ACstatus: 完全会
- score: 5.0
</details>
</details>
<details>
<summary>接雨水 II</summary>

#### platform leetcode
#### id 407.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>经过此题的折磨之后，我更加看清了我自己，原来，我的模拟水平，在一次一次中得到了提高，然而我的算法水平提高了个卵，简直有趣，不过正解还是可以的，推荐正解, 优先级队列正解终于ok了，还是思考了很长时间，最终ok，有进步的哈，时隔一年多了</details>
- <details><summary>tag</summary>最短路or优先级队列</details>
- spendtime: 120.0 - 100
- ACtime: 2022.1.25 - 2023.6.6
- ACstatus: 完全会+正解更好 - 会正解了
- score: 7.0
</details>
</details>


<details>
<summary>最短公共超序列</summary>

#### platform leetcode
#### id 1092.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>本质模型是最长公共子序列，难点在于之前并没有思考过如何回溯最长公共子序列模型是使用dp求解最长公共子序列的长度，每一个状态保存的是一个整数，并不能直接反应最终的结果，所以需要回溯，回溯的方法是，pre=longest，首先有一个固定一个字符串，减少指针，直到出现和pre不相等的位置停止，扫描另一个字符串，直到找到的长度等于longest就行 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>复写零</summary>

#### platform leetcode
#### id 1089.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>就是简单模拟，把零多输出一遍，然后再复写一下原数组即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>受标签影响的最大值</summary>

#### platform leetcode
#### id 1090.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>排序，贪心，有约束 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>二进制矩阵中的最短路径</summary>

#### platform leetcode
#### id 1091.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>简单的bfs求最短路问题 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>Bigram 分词</summary>

#### platform leetcode
#### id 1078.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>两个条件即可找出答案，然后向后逐步滑动就行 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>活字印刷</summary>

#### platform leetcode
#### id 1079.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接暴力枚举就行 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>根到叶路径上的不足节点</summary>

#### platform leetcode
#### id 1080.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>带回溯的深度搜索的应用，从字面上看像模拟，但是递归的写法还是比较简单的，使用hashset记录要删去的节点就行 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>不同字符的最小子序列</summary>

#### platform leetcode
#### id 1081.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>本质上是一道前缀和优化的问题，首先，枚举每个位置能否放入char，条件是以这个char的index的前半部分包含已经选择的序列，后半部分包含并没有选择的部分，使用前缀和优化查询，还有个应该注意的细节就是，需要记录上一个char选择的索引，下一个选择的char的索引只能在上一个之后 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>字符串的最大公因子</summary>

#### platform leetcode
#### id 1071.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>因为数据简单，可以直接暴力枚举，但其实真正的做法是kmp求循环元的做法 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>按列翻转得到最大值等行数</summary>

#### platform leetcode
#### id 1072.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>题目表达虽然简单，但需要逐字逐句读清楚，是一道思维题，转换的跨度还是比较大，重点的思路是思考每一行转换成相同的一共就两种方式，用哈希表存储一下，求出最大值即可，初次思考的时候时间花得还是比较久，是一道猛地来一下，就会卡住的题 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>负二进制数相加</summary>

#### platform leetcode
#### id 1073.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>代码量比较短，是一道非常好的练手题目，也属于思维题 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>元素和为目标值的子矩阵数量</summary>

#### platform leetcode
#### id 1074.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>前缀和优化问题，暴力枚举即可 之所以暴力初次可以过，是因为java的原因，在之后重新探索了一下，是一个前缀和作为数据结构的计数优化问题有维度压缩的思想在其中，不过最后代码呈现得还是比较漂亮和简单，是一道很好得组合题目</details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>高度检查器</summary>

#### platform leetcode
#### id 1051.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>简单排序找不同 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>爱生气的书店老板</summary>

#### platform leetcode
#### id 1052.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>前缀和，直接遍历即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>交换一次的先前排列</summary>

#### platform leetcode
#### id 1053.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>读懂题意问题，只要读懂题意，使用treemap即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>距离相等的条形码</summary>

#### platform leetcode
#### id 1054.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>最多的元素优先消费，每次安排两个，优先级队列问题 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>可以输入的最大单词数</summary>

#### platform leetcode
#### id 5161.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>模拟题，直接暴力 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>新增的最少台阶数</summary>

#### platform leetcode
#### id 5814.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>模拟题，直接暴力 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>扣分后的最大得分</summary>

#### platform leetcode
#### id 5815.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>思维题，值得好好考虑 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>查询最大基因差</summary>

#### platform leetcode
#### id 5816.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>带删除的字典树和深度优先搜索的题，需要一定的模板总结和思路的转化 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>最后一块石头的重量</summary>

#### platform leetcode
#### id 1046.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接模拟 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>删除字符串中的所有相邻重复项</summary>

#### platform leetcode
#### id 1047.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>栈的应用 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>最长字符串链</summary>

#### platform leetcode
#### id 1048.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>将词关系转化为图，之后求最长路问题，转化为动态规划 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>最后一块石头的重量 II</summary>

#### platform leetcode
#### id 1049.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>此题考验思维，难在问题的转化，最终可以转化为，尽可能的平均数组的和，分成两堆，使得两堆绝对值之差最小，动态规划直接求解即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>困于环中的机器人</summary>

#### platform leetcode
#### id 1041.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>思维题，判断的方式估计有很多，将命令多复制几次，判断一下是不是离原点越来越远 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>不邻接植花</summary>

#### platform leetcode
#### id 1042.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>简单相邻单元颜色不同题，因为题目良好的限制，不需要考虑更多的情况，直接生成即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>分隔数组以得到最大和</summary>

#### platform leetcode
#### id 1043.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>简单的动态规划，一维动态规划 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>最长重复子串</summary>

#### platform leetcode
#### id 1044.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>字符串hash，需要注意的就是hash冲突时，需要再次判重，其中注意优化有可能在超时边缘挣扎 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>有效的回旋镖</summary>

#### platform leetcode
#### id 1037.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>一行代码即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>把二叉搜索树转换为累加树</summary>

#### platform leetcode
#### id 1038.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>数据简单，不限方法 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>多边形三角剖分的最低得分</summary>

#### platform leetcode
#### id 1039.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>十分不明白为啥第一次思考花了很多的时间，非常深恶痛绝，如此简单的一道dp思考那么久，敲得时候还是不是那么自信，最后回过头思考应该是结尾处的处理没有思考很清楚，就是这一小点，导致全盘崩溃 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 7.0
</details>
</details>
<details>
<summary>移动石子直到连续 II</summary>

#### platform leetcode
#### id 1040.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>思维题，很考验逻辑性，真的是一道吐题，并不是多么困难而是情况难以讨论清楚 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 7.0
</details>
</details>
<details>
<summary>移动石子直到连续</summary>

#### platform leetcode
#### id 1033.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>思维题，需要仔细小思考一下 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>边框着色</summary>

#### platform leetcode
#### id 1034.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>简单的dfs加边界判断问题 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>不相交的线</summary>

#### platform leetcode
#### id 1035.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>模型最长公共子序列的包装版本 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>逃离大迷宫</summary>

#### platform leetcode
#### id 1036.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>A*算法的衍生品，主要是过大地图，和较小阻碍如何判断，此处采用计数大于某个阈值作为判断，同样运行了双端A*，共同向前推进，降低复杂度，当然看他们做得都是双端bfs，不过想想适当写写也可以吧，不过精髓还是阈值 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>两地调度</summary>

#### platform leetcode
#### id 1029.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>放在周赛第一道题，确实把我给唬住了，当然贪心在第一时间没有想出来，所以采用了dp的做法，由于数据量比较小，恰好dp可以解，万一大一点那就不一定了，正解是贪心，利用每一项的a和b的差值去思考，先假设全部为b，则选差值最小的n个取a，则最后为正确答案，忽然一下的贪心是不太容易想明白的 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>距离顺序排列矩阵单元格</summary>

#### platform leetcode
#### id 1030.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>简单的模拟 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>两个非重叠子数组的最大和</summary>

#### platform leetcode
#### id 1031.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>简单的前缀和优化的求长度为k的连续最大值，划分为两个子问题解即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>字符流</summary>

#### platform leetcode
#### id 1032.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>字典树的应用，将原始的string数组倒叙插入字典树，然后使用链表存储一下访问的letter，所以每次查询的时间复杂度是O(n) </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>检查是否所有字符出现次数相同</summary>

#### platform leetcode
#### id 5804.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>简单的模拟 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>最小未被占据椅子的编号</summary>

#### platform leetcode
#### id 5805.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>优先级队列+模拟+treeset维护最小 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>描述绘画结果</summary>

#### platform leetcode
#### id 5806.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>差分数组+记录分界位置 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>队列中可以看到的人数</summary>

#### platform leetcode
#### id 5196.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>我紧张了，没看出来是个栈的应用，用了朴素的模拟法，是个nlogn的算法，太可耻了 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>字符串转化后的各位数字之和</summary>

#### platform leetcode
#### id 5823.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>模拟即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>子字符串突变后可能得到的最大整数</summary>

#### platform leetcode
#### id 5824.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>模拟，维护两个变量即可，不算特别好写 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>最大兼容性评分和</summary>

#### platform leetcode
#### id 5825.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>当然全排列直接暴力就可以，更加上档次的做法是二分权图匹配 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>删除系统中的重复文件夹</summary>

#### platform leetcode
#### id 5826.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>当然总体从思路上来说，没有那么难想，字典树的引出是十分直观的，下一步就该思考如何求一个树的子节点表示，用字符串hash来编码，当然避免不了要检测冲突，在最后加入的时候可以再加一层判断，这个需要看数据什么样子，不过总体评价，还是比较难调试的，如果之前没有写过，那还是比较难调试 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 7.0
</details>
</details>
<details>
<summary>除数博弈</summary>

#### platform leetcode
#### id 1025.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>看了看评论区，发现一行代码就可以搞定，不过需要那种快速看出数学规律的眼睛，不过非常直观的方式是dp，dp的状态转移也很简单 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>节点与其祖先之间的最大差值</summary>

#### platform leetcode
#### id 1026.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>dfs路径下，维护treemap即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>最长等差数列</summary>

#### platform leetcode
#### id 1027.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>hash表记录一下公差为x的最长序列，动态规划去转移 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>从先序遍历还原二叉树</summary>

#### platform leetcode
#### id 1028.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>其实这道题不能称之为困难，我觉得思路走得不算困难，就是使用deep卡一下直接子节点加上模拟就行 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>删除最外层的括号</summary>

#### platform leetcode
#### id 1021.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>简单栈的应用 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>从根到叶的二进制数之和</summary>

#### platform leetcode
#### id 1022.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>dfs简单应用 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>驼峰式匹配</summary>

#### platform leetcode
#### id 1023.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>最长公共子序列应用 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>视频拼接</summary>

#### platform leetcode
#### id 1024.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>完全覆盖问题，求最小覆盖数 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>可被 5 整除的二进制前缀</summary>

#### platform leetcode
#### id 1018.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>讨论个位情况就可以了 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>负二进制转换</summary>

#### platform leetcode
#### id 1017.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>是1073的前身题 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>链表中的下一个更大节点</summary>

#### platform leetcode
#### id 1019.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>是5196的前身题 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>飞地的数量</summary>

#### platform leetcode
#### id 1020.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>简答dfs应用 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>将数组分成和相等的三个部分</summary>

#### platform leetcode
#### id 1013.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>不需要考虑太多的情况，因为是一道简单题，直接看能不能分成三段相等就可以 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>可被 K 整除的最小整数</summary>

#### platform leetcode
#### id 1015.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>突然之间没有反应过来，想了好一小会，其实就是个有范围的模余问题，可以硬性规定范围，也可以使用循环的原理，更加有效的使得范围终止 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>最佳观光组合</summary>

#### platform leetcode
#### id 1014.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>贪心问题，nlong解法，将表达式进行化得舒服一点就可以 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>子串能表示从 1 到 N 数字的二进制串</summary>

#### platform leetcode
#### id 1016.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>分析，首先一个定长的字符串可以表示的二进制数，是个n^2级别的，所以直接枚举可以表达的子串的值就可以，所以整个复杂度不会超过n^2 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>十进制整数的反码</summary>

#### platform leetcode
#### id 1009.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接模拟就可以非常的简单 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>总持续时间可被 60 整除的歌曲</summary>

#### platform leetcode
#### id 1010.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>哈希表优化前缀的计数 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>在 D 天内送达包裹的能力</summary>

#### platform leetcode
#### id 1011.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>二分判定 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>至少有 1 位重复的数字</summary>

#### platform leetcode
#### id 1012.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>我觉得还是蛮麻烦的，因为超时了，做的时候超时了，所以应该尽可能的压缩本次考试前面题的时间，再去做这道题，是一道挺麻烦的题，但是整个思路走起来的话，不是那么的困难，就是在写法上要注意一些细节，通过他的逆命题去做，他要找相同，那我们就找绝对不同 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>K 次取反后最大化的数组和</summary>

#### platform leetcode
#### id 1005.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>模拟讨论即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>笨阶乘</summary>

#### platform leetcode
#### id 1006.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>分四个一组，注意情况讨论就行 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>行相等的最少多米诺旋转</summary>

#### platform leetcode
#### id 1007.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>就两种情况，分别计算一下就行 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>前序遍历构造二叉搜索树</summary>

#### platform leetcode
#### id 1008.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接递归构造就行 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>三除数</summary>

#### platform leetcode
#### id 5830.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接暴力去重即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>你可以工作的最大周数</summary>

#### platform leetcode
#### id 5831.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>玄学贪心，玄学脑筋急转弯，考试的时候竟然采用了下三滥招术打表，真的是吐血了，直接确定最大值与总和的关系就行，但是后来思考，优先队列，只需要将所有的值先减小到min，然后再讨论最终就是，first，min，min，min，，，，的关系，其实，也是包含玄学的成分在里面 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>收集足够苹果的最小花园周长</summary>

#### platform leetcode
#### id 5187.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>数学公式+二分判定 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>统计特殊子序列的数目</summary>

#### platform leetcode
#### id 5833.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>动态规划，二维动态规划，想起出好转移就可以了 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>查找常用字符</summary>

#### platform leetcode
#### id 1002.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这个题的题意表达得实在是有点问题，按照正常理解的即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>检查替换后的词是否有效</summary>

#### platform leetcode
#### id 1003.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>不需要想复杂，栈的应用 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>最大连续1的个数 III</summary>

#### platform leetcode
#### id 1004.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>前缀和+二分典型题目 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>合并石头的最低成本</summary>

#### platform leetcode
#### id 1000.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>被坑了，这题的思考需要好好仔细，一个区间dp的题目，直觉反映的很对，但是，细节在第一次时，没有思考到位，其中需要想明白的是，任意的划分方式，都可以划分成，1，k-1，两部分，另一个就是，在针对合并之后并不是一堆的情况，是不需要累加区间和的，想明白这两部分，区间dp应该比较好写了 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 7.0
</details>
</details>
<details>
<summary>找到小镇的法官</summary>

#### platform leetcode
#### id 997.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接计数模拟即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>可以被一步捕获的棋子数</summary>

#### platform leetcode
#### id 999.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接模拟即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>最大二叉树 II</summary>

#### platform leetcode
#### id 998.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>先还原数组，在进行构造 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>网格照明</summary>

#### platform leetcode
#### id 1001.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>就是写得特别麻烦，但思路清晰，题意简单，直接用hashmap存储即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>二叉树的堂兄弟节点</summary>

#### platform leetcode
#### id 993.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>按照题意进行遍历即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>腐烂的橘子</summary>

#### platform leetcode
#### id 994.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>多源bfs，注意最后在返回得时候不能为负数就行 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>K 连续位的最小翻转次数</summary>

#### platform leetcode
#### id 995.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>一道相当好的差分变种题目，思维在走的时候还是比较顺畅，核心就是将反转转化为判定奇偶，差分的解决方案随即就出来了 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>正方形数组的数目</summary>

#### platform leetcode
#### id 996.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>动态规划的肌肉记忆没有成型得太快，思路走得比较顺畅，但是也是比较崎岖，首先解决去重问题，再考虑动态规划怎么设计，二维动态规划，将1<<nums.length作为一维得状态，再将以这个状态结尾的数作为另一维状态，转移就很容易表达，用一个状态的子集去推出当前状态的值 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>数组形式的整数加法</summary>

#### platform leetcode
#### id 989.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>biginteger直接过，其实就是竖式加法的变形 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>等式方程的可满足性</summary>

#### platform leetcode
#### id 990.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>并查集模板，判断是否有冲突即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>坏了的计算器</summary>

#### platform leetcode
#### id 991.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>真的是个很好的思维题，反向思考，利用倍增策略补全差，思路并不直观，还是有点绕，可以这样思考，首先倍增到>=target，然后思考在这个过程中，如果在倒数第一步减少1，那么结果就会减少2，如果在倒数第二步减少1，那么结果就会减少2^2，同理，那么先计算最大的倍增次数，再在其中考虑，如何把差分解为2的幂次之和 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>K 个不同整数的子数组</summary>

#### platform leetcode
#### id 992.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>特别难调的双指针算法，就是个滑动窗口的变体，重点在于从朴素的计数问题转化为计数优化，那么使用两个hashmap来叠加使用，一个记录恰好为k时的坐标，另一个记录，恰好不为k+1时的坐标，这样就优化了计数，时间复杂度变为线性 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>不含 AAA 或 BBB 的字符串</summary>

#### platform leetcode
#### id 984.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>简单构造，直接给出一种满足条件的构造方法就行 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>基于时间的键值存储</summary>

#### platform leetcode
#### id 981.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>hashmap套treemap，直接模拟 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>最低票价</summary>

#### platform leetcode
#### id 983.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>唉，又是一道引我入沟的题，深搜失败之后，逼迫我动态规划思考，肌肉记忆还得被捶打结果时间不小心就费多了，太生气了 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>按位与为零的三元组</summary>

#### platform leetcode
#### id 982.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>我觉得这题怎么评价，本质就是计数类优化，但其实这题挺不好的，没啥说的我就觉得有点不好做法看似就是时间恰好千万级，空间就是万级，我觉得唉，一个其实简单的计数标记困难也是干扰做题人心态，刚开始还被诱骗走偏了字典树，真的呕血 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>不同路径 III</summary>

#### platform leetcode
#### id 980.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>我觉得这题困难实在是有点，而且这题不好，数据量很小，感觉用啥都可以过，但是很唬住人 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>有序数组的平方</summary>

#### platform leetcode
#### id 977.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>懒得探讨On的算法，感觉没啥必要，数据量又很小，直接暴力sort </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>最长湍流子数组</summary>

#### platform leetcode
#### id 978.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这题算是二度重做，时隔四个月，但是代码量已经巨精简了，上次是巨长的逻辑，有个一百多行，但这次直接20行，动态规划直接肌肉解，证明确实比四个月前思维上有很大的提升 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>在二叉树中分配硬币</summary>

#### platform leetcode
#### id 979.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这个题呕血了，不知道咋回事，或许心慌了，而且冷静下来长时间思考还是没有思路，一个中等题为啥卡鸡毛了呢，我没有细看他人是怎么做得，只是稍微改变了一下思路，我觉得先不用仔细思考正确性，大概做做就行，结果，写了5分钟就过了，实在是让我吐血这题，感觉就是套路中的套路，没啥的，写一写瞬间就可以知道自己有多菜的题 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>删除字符使字符串变好</summary>

#### platform leetcode
#### id 5193.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接上手模拟吧，没啥说的 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>检查操作是否合法</summary>

#### platform leetcode
#### id 5827.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>题目挺长的，读起来费事，判定也不算特别好些，不过框架写好后，还是比较容易 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>K 次调整数组大小浪费的最小总空间</summary>

#### platform leetcode
#### id 5828.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>需要预处理前缀和和区间max，二维动态规划解题，这题其实算是一种肌肉记忆题，我其实并没有仔细思考在其中过程的转移，但直接就做出来了 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>两个回文子字符串长度的最大乘积</summary>

#### platform leetcode
#### id 5220.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>当然On的算法是有的，但是有一定的学习成本，我觉得字符串hash加二分就很好，当然这个数据给的是比较大的，所以有一定的风险，第一点比较容易相当，第二步的处理就不是很好想到，我们已经求得每一个点的最大半径，如何求在一个分界点左边或者右边最大的子回文串呢，此处使用一个nlogn的做法，我们可以将每一个中心点对应的端点进行存储，然后用一个treeset来维护最左边的点，然后遇到每个端点将其在treeset中删除即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>检查字符串是否为数组前缀</summary>

#### platform leetcode
#### id 5838.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>阅读一下，按照条件模拟就行 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>移除石子使总数最小</summary>

#### platform leetcode
#### id 5839.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>优先级队列 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>使字符串平衡的最小交换次数</summary>

#### platform leetcode
#### id 5840.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>思维题目，只要先将所有的有效括号移除掉，那么剩余就一定是形如...]]]][[[[…这种，形如这种就找规律吧，应该比较快捷一点 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>找出到每个位置为止最长的有效障碍赛跑路线</summary>

#### platform leetcode
#### id 5841.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>最长递增子序列的变形，需要维护一个尽可能上升得慢的数组，每次二分插入即可知最长长度 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>最接近原点的 K 个点</summary>

#### platform leetcode
#### id 973.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>排序输出即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>三角形的最大周长</summary>

#### platform leetcode
#### id 976.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>排序，然后根据三角形的特性，而且满足最大，只能是三个相邻的一组，如果不行，那就滑到下一组 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>和可被 K 整除的子数组</summary>

#### platform leetcode
#### id 974.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>前缀和计数优化技巧，只不过这题有负数，那么就多考虑一种情况即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>奇偶跳</summary>

#### platform leetcode
#### id 975.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>能给这道题6分的原因就是因为我犯了个傻逼错误，导致多调了一会，实在是让人生气，就这么简单的一道题，竟然还需要调这么久，对于一个索引来说，下一步是固定的只有一个，但是对于一个索引来说，能到此索引的不止一个，第一次就忽略这事了，反向递推，很快就能求出 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>强整数</summary>

#### platform leetcode
#### id 970.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接暴力没啥说的 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>煎饼排序</summary>

#### platform leetcode
#### id 969.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这个题蛮有意思，至少我思考了十几分钟，倒叙做，假设排好序，怎么样到对应序列，分两步第一，假设需要对应的在第一个，直接反转即可，建设不在，先反转到第一个，再反转到对应位置 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>翻转二叉树以匹配先序遍历</summary>

#### platform leetcode
#### id 971.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接模拟，可能会第一次写不对，需要调调 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>相等的有理数</summary>

#### platform leetcode
#### id 972.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这个题搞困难可不太应该了，直接精度范围内判断是否相等就行 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>单值二叉树</summary>

#### platform leetcode
#### id 965.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>遍历记录即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>连续差相同的数字</summary>

#### platform leetcode
#### id 967.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>模拟数据量小 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>元音拼写检查器</summary>

#### platform leetcode
#### id 966.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>主要就是预处理需要编写代码，其他的按照规则模拟 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>监控二叉树</summary>

#### platform leetcode
#### id 968.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>树状dp，条件考虑清楚就行，给六分纯粹是因为比规定时间多调了两分钟而已 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>重复 N 次的元素</summary>

#### platform leetcode
#### id 961.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接模拟 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>最大宽度坡</summary>

#### platform leetcode
#### id 962.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>差点被坑比的一道题，组装数据，排序，再记录最小值 仔细与1124题区别区别，非常有意思，一个是<=，一个是<，有很大的写法区别</details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>最小面积矩形 II</summary>

#### platform leetcode
#### id 963.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>我觉得这题吧，不太好，枚举三角形，直接hash求另一个点，有个数据莫名奇妙，后来发现构造数据的人的牛逼，确实有一条逻辑没加，直接枚举四个点也挺好，就是需要优化，有一定的超时风险 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>表示数字的最少运算符</summary>

#### platform leetcode
#### id 964.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这题怎么说，可能想复杂，是一方面，另一方面是证明性其实挺差，可能算是难的但是不太好复制思维或者即使训练了思维，下次也并是很好能想出来，本来状态的设计转移就是相当考验思维的地方 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 7.0
</details>
</details>
<details>
<summary>N 天后的牢房
</summary>

#### platform leetcode
#### id 957.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>作为第一题确实有一定的心态挑战，主要是找循环点，当然也是存在很多细节的，估计得调试上几次才能正确 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>二叉树的完全性检验</summary>

#### platform leetcode
#### id 958.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接编号，bfs模拟即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>由斜杠划分区域</summary>

#### platform leetcode
#### id 959.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>需要抽象的dfs变形，我们将每个方格分割成4个块，每一个块编号，这样从一个平面的就上升为三维的标记数组，在这个基础上，进行dfs，记录连通块个数即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>删列造序 III</summary>

#### platform leetcode
#### id 960.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>最长上升子序列多重版本，在dp的更替过程中，需要对所有的字符串进行判定，当然优化就是每一个维度维护一个最慢的上升序列，不过这道题因为数据的原因，可以直接用0n^3的算法，其实可以优化成n^2logn </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>验证外星语词典</summary>

#### platform leetcode
#### id 953.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>按照规则排序，我觉得这个是个相当好的新手实验题 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>二倍数对数组</summary>

#### platform leetcode
#### id 954.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>分别按照正数负数排序，从小到大消消乐就行 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>删列造序 II</summary>

#### platform leetcode
#### id 955.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这题比960难多了，不知道为啥还是个中等，当然可以算法表达得特别简单，如果加入这列有序，保留，如果不有序，直接就删除，当然更细致的逻辑理解是这样的，当形如a<=a< b<=b< c<=c<=c这样的序列加入之后对后续有何影响，它将原本难以符合的逻辑变得更简单，本来需要满足a[0]<=a[1]<=a[2]<=a[3]<=a[4]<=a[5]<=a[6]，变成了a[0]<=a[1]和a[2]<=a[3]和a[4]<=a[5]<=a[6]即可，所以对于能加入的列，一定要加入，这样会使得结果最优，同样侧面也证明了最优求解的正确性，是一道很不错的题 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 7.0
</details>
</details>
<details>
<summary>最高的广告牌</summary>

#### platform leetcode
#### id 956.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>一直想知道怎么做的一道题，华为机考的时候也考了，应该叫天平的平衡问题，终于让我给找到了，我们可以这么思考，将整个式子看成*a[0],*a[1],*a[2],*a[3],….其中*代表系数（1，0，-1），目标是求上式等于零时的正数最大值，状态转移就出来了，当处于一个元素时，只有三种操作，状态也就好设计，使用hashset的数组存储当处于i元素时，等式可能的值，再用dp[i][j],存储当等式包含i个元素时，能达到j值的最大的正数之和 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 7.0
</details>
</details>
<details>
<summary>作为子字符串出现在单词中的字符串数目</summary>

#### platform leetcode
#### id 5843.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接判断是否子串，计数即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>构造元素不等于两相邻元素平均值的数组</summary>

#### platform leetcode
#### id 5832.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>一个构造题，应该满足条件的方式有很多种，我使用了最大最小值穿插着来，没啥特别的技巧，我只是验算了一下这样可行 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>数组元素的最小非零乘积</summary>

#### platform leetcode
#### id 5844.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>本质上考察了快速幂，当然之前有很多情况没有考虑，这次经过几个罚时之后，修正了爆数据范围的事情，重新优化了一下板子，从题转换成快速幂也是需要经过一段大胆的猜测，可以这样认为，为了得到最小的乘积，将1全部分离出去，总共有2的p-1幂-1个，所以，剩余的只剩2^p-1*2^p-2的(2^(p-1)-1)次方的乘积了 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>你能穿过矩阵的最后一天</summary>

#### platform leetcode
#### id 5845.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>灵感来自普林斯顿算法的第一个渗透，使用并查集数据结构来计算的，假设我没有见过我也不知道初次能不能反应过来用并查集，当然这道题的本意是形成一条水路上下不连通，那我们就可以转换成，只要左右水路连通那么一定形成了一道墙，将上下阻拦 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>给定数字能组成的最大时间</summary>

#### platform leetcode
#### id 949.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接暴力枚举四个数的位置，然后转判定，记录最大值即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>翻转等价二叉树</summary>

#### platform leetcode
#### id 951.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接条件判断即可，就是情况需要考虑周到即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>按递增顺序显示卡牌</summary>

#### platform leetcode
#### id 950.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这道题的拆解子问题的方式的思维还是比较好的，我觉得我的解决方案还是比较好的，复杂度尝试分析一下，每一次长度都会减半，那么logn是个函数的调用次数，而每一次执行的复杂度是n^2的所以还是可以过的 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>按公因数计算最大组件大小</summary>

#### platform leetcode
#### id 952.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>虽然是一道困难题目，但是其实思路走起来是很顺畅的，建议用时25分钟以内，试除法就可以解这道题，复杂度有点打擦边球，主要是并查集的时间复杂度接近常数，可以认为千万级别复杂度 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>查询后的偶数和</summary>

#### platform leetcode
#### id 985.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>只关注偶数变换的就行 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>从叶结点开始的最小字符串</summary>

#### platform leetcode
#### id 988.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>复杂度一分析有可能爆掉的题，不太敢下笔，所以，时间上会有一定的思考耽误，总体的话，暴力模拟即可，当然我的思路逐渐拆分成了各个小块，代码上会复杂一点，但是清晰 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>区间列表的交集</summary>

#### platform leetcode
#### id 986.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>在求overloop的地方小心，我个人觉得我代码和思路在这道题中抽象还是很不错的 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>二叉树的垂序遍历</summary>

#### platform leetcode
#### id 987.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>不配为困难题噢，就是个bfs然后再按条件排序 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>使数组唯一的最小增量</summary>

#### platform leetcode
#### id 945.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>排序后记录前一个值即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>验证栈序列</summary>

#### platform leetcode
#### id 946.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>使用一个索隐变量记录需要访问的出栈顺序，然后添加对比即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>移除最多的同行或同列石头</summary>

#### platform leetcode
#### id 947.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>并查集连通块个数典型应用 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>令牌放置</summary>

#### platform leetcode
#### id 948.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>与动态规划极具混的题，先写了个动态规划版本，发现顺序任意，然后又想了想，发下，自己想复杂了，根本不用动态规划，直接排个序，没分的时候，取目前所剩最大，然后继续即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>有效的山脉数组</summary>

#### platform leetcode
#### id 941.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>模拟，小心点即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>删列造序</summary>

#### platform leetcode
#### id 944.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>模拟，比较简单 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>增减字符串匹配</summary>

#### platform leetcode
#### id 942.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>一看是一道简单题，吓了一跳，经过1分钟左右思考发现，确实简单，当需要增加的时候，就选择当前所剩中最小的，当需要减小时，就选择当前所剩最大的即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>最短超级串</summary>

#### platform leetcode
#### id 943.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>是个hamilton的状态压缩的应用，加回溯，如果以前不知道hamilton的话，很难设计出状态压缩的状态表示，第一次写难免很难调试，其次就是注意转移的权重，目标是求出走完所有点的最长路径，将前一个字符串和后一个字符串的交叠最大长度作为转移权重，记为g[i][j] </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 7.0
</details>
</details>
<details>
<summary>重新排列日志文件
</summary>

#### platform leetcode
#### id 937.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>其实算是比较麻烦的模拟 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>二叉搜索树的范围和</summary>

#### platform leetcode
#### id 938.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>不用性质，直接暴力遍历 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>最小面积矩形</summary>

#### platform leetcode
#### id 939.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>第一次做的时候我大意了哈，看到超时我不得不重新想想，思路来自普林斯顿算法中计算矩阵重合问题，我模糊的记得，先将元素按照横轴存储起来，再使用一个hashmap存储线段，这一块的线段指的是，对于横轴相同的线段，使用纵坐标的点对来描述，先将点对映射到一个数，value存储的是此线段的最近横坐标，当然重新走思路的时候，还是用图描述更直观一点 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>不同的子序列 II</summary>

#### platform leetcode
#### id 940.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>因为没想出来，所以给这题提升难度，然后又稍微想了一会觉得，emmm，放弃过早，好吧认栽了，状态设计，引入空字符串,最后减去,则，可以这么思考，dp[i]表示长度为i的最大不同,则考虑转移，给前面所有的字符串都加上此字符，再减去以此字符结尾的的dp[j]即可，因为这部分是重复的 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 8.0
</details>
</details>
<details>
<summary>使用特殊打字机键入单词的最少时间</summary>

#### platform leetcode
#### id 5834.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>模拟，读懂题意即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>最大方阵和</summary>

#### platform leetcode
#### id 5835.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>我们可以将一次操作，看成负数的游走，一个任意的负数均可以，转移自己的符号，则分情况讨论，统计负数的个数，如果是偶数的话，那么就对对碰，如果是奇数，则取正数负数中绝对值最小的就可以 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>到达目的地的方案数</summary>

#### platform leetcode
#### id 5836.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>真的气死我，一个边界你卡我？？？没考虑只有一个点时，这也算嘛，无语，迪杰斯特拉的应用，回溯回去就行 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>找出数组的最大公约数</summary>

#### platform leetcode
#### id 5850.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接排序，gcd </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>找出不同的二进制字符串</summary>

#### platform leetcode
#### id 5851.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>hashset存一下，直接从1枚举即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>最小化目标值与所选元素的差</summary>

#### platform leetcode
#### id 5852.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>看数据范围直接动态规划吧 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>从子集的和还原数组
</summary>

#### platform leetcode
#### id 5853.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>挺脑筋急转弯的，反正第一次我没想出来，先思考一般情况，即没有负数情况，也就是，每一次递归选择最小的，然后与已经选出来的res数组，进行子集和枚举，然后再在剩余数组中去除，最终就会得到，当然引入负数之后，我们只需要加入最小负数的绝对值，转换为正数，那么就可以转换为非负问题了，然后最终，再枚举一下子集，选出可以组成最小负数的绝对值的数，统一乘负即可得到答案 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 7.0
</details>
</details>
<details>
<summary>划分数字的方案数</summary>

#### platform leetcode
#### id 1977.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>主要就是预处理的优化不好想，状态还是好设计的，如何经过预处理在O(1)的时间内比较子字符串的大小是难点，当然这个动态规划也好求，只是这作为一个优化的点，卡了我，总体来讲，细节和优化都是不错的题 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 7.0
</details>
</details>
<details>
<summary>独特的电子邮件地址</summary>

#### platform leetcode
#### id 929.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>按题目意思模拟即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>和相同的二元子数组</summary>

#### platform leetcode
#### id 930.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>考烂的前缀和优化计数问题 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>下降路径最小和</summary>

#### platform leetcode
#### id 931.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>想都没想直接就动态规划了 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>漂亮数组</summary>

#### platform leetcode
#### id 932.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这道题就猜性质，构造题实在不好形成肌肉记忆，主要就是算法的方向不好确定，此题分治递归，重要性质就是，如果假设，left全为奇数，right全为偶数，那么left的线性变换也一定是漂亮数组，right同理，边界条件也很好写，n==1,n==2,n==3,即可，左右差分递归下去，反正这题代码量确实短，也好理解，但确实就那一小点过不去，就得卡巨久时间 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>最近的请求次数</summary>

#### platform leetcode
#### id 933.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>treeset直接上 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>最短的桥</summary>

#### platform leetcode
#### id 934.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>先标记，区别两个连通块，再求出连通块边界，再最小曼哈顿距离 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>骑士拨号器</summary>

#### platform leetcode
#### id 935.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>动态规划 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>戳印序列</summary>

#### platform leetcode
#### id 936.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>错错题，测试数据有问题的，冗余了，而且编码巨复杂，官方做法吧，可取，复杂度好分析一点 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 7.0
</details>
</details>
<details>
<summary>长按键入</summary>

#### platform leetcode
#### id 925.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>虽然是简单题，但是竟然错了好多下， 看来即使简单题也要思考最好写且最不容易出错算法 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>将字符串翻转到单调递增</summary>

#### platform leetcode
#### id 926.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>前缀和加枚举分界点即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>三等分</summary>

#### platform leetcode
#### id 927.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>枚举第一个分界点，即可确定后面的，使用字符串hash快速比较，复杂度是线性的，加了个查询系数logn </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>尽量减少恶意软件的传播 II</summary>

#### platform leetcode
#### id 928.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>初次写的做法稍微有点不标准，o(n^3)的，不过因为数据的原因，也能过，当然更加标准的做法是，仍然使用并查集，如果此连通域只包含一个感染节点，那么去除这个节点的收益就是整个连通块的大小，所以最后为o(n^2) </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>按奇偶排序数组 II</summary>

#### platform leetcode
#### id 922.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>模拟即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>使括号有效的最少添加</summary>

#### platform leetcode
#### id 921.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>优先匹配有效，剩余的即为待匹配 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>三数之和的多种可能</summary>

#### platform leetcode
#### id 923.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>优化计数即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>尽量减少恶意软件的传播</summary>

#### platform leetcode
#### id 924.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>并查集记一下，如果连通块中有两个以上，则去除也没用，如果有一个，联通块大小就是收益 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>仅仅反转字母</summary>

#### platform leetcode
#### id 917.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>按照题意模拟 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>环形子数组的最大和</summary>

#### platform leetcode
#### id 918.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>知识点技巧了，对于环形的子数组的处理，首先做个转换，按照正常的动态规划可以求出最大子数组和，同样也可以求出，最小子数组和，所以算法变得很直观，要么是正常的子数组和最大，要么是sum-min最大，但是这里需要做个修正，对于后者无法保证答案是非空数组，举个例子，对于全是负数的数组，sum-min=0，而并不存在个非空数组的答案为零，则需要计算两边,【1，len】,和[0,len-1]，的最小子数组和，则能保证非空 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>完全二叉树插入器</summary>

#### platform leetcode
#### id 919.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>维护一个顺序队列就可以了，注意在刚开始的时候，初始化的时候编码直观，但比较繁琐 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>播放列表的数量</summary>

#### platform leetcode
#### id 920.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>首先难以保证的所有歌至少一遍，如果像hamilton一样，数据范围过大，不可能状态压缩，难点，最终当然选择的解法是动态规划，不过定义和转移也不是说非常难想，而是在想的时候如何让自己信服，定义dp[i][j]为含有j个不同的，且长度为i的方案数，考虑转移，对于任意状态，此刻选择任意的没有播放过的，一种情况，要么,就选择已经播放过的，但是要满足相隔k个的，dp[i][j]=dp[i-1][j-1]*(n-j+1)+dp[i-1][j]*Math.max(j-k,0);转移不难想，但是就那一小点，特别难以让人信服 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 7.0
</details>
</details>
<details>
<summary>卡牌分组</summary>

#### platform leetcode
#### id 914.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>模拟计数即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>分割数组</summary>

#### platform leetcode
#### id 915.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>动态规划求区间最值问题，这种区间增长必须是朝向一个方向的 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>单词子集</summary>

#### platform leetcode
#### id 916.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>想清楚如何快速优化判定即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>猫和老鼠</summary>

#### platform leetcode
#### id 913.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>博弈论，先手后手问题，使用此时猫和鼠的位置，和deep既可以决定，必胜态，必败态，和平局这样，利用graph提供的路径就可以进行转移，如果是老鼠的话，先查看一下，能不能走到洞口如果可以走到，那么鼠一定获胜，然后使用一个标记位记录能否到达平局，当所有路径遍历结束仍然无法必胜，则查看能否平局，如果不行，必败，如果是猫的话同理，当然猫不能进入洞，这点注意判定，如果获胜不了，那么看标记位是否能进入平局，如果行，那就平局，如果不行，那就必败，此处平局是用步长判定的，超过了两倍图的大小的时候，必然平局 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>猫和老鼠 II</summary>

#### platform leetcode
#### id 1728.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>博弈论，状态的表示，换了一种方式，但是思想没变，还是必胜态必败态的转换，注意方向数组的灵活应用 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>最小差值 I</summary>

#### platform leetcode
#### id 908.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>排序判定即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>蛇梯棋</summary>

#### platform leetcode
#### id 909.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>注意编码细节，代码量还是比较大的，起始是最后一行，注意坐标的转换，还有使用一个map记录到此点的最小距离，防止重复访问，不可以使用布尔数组，因为有可能存在更优的方案 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>最小差值 II</summary>

#### platform leetcode
#### id 910.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>双指针集大成题，非常不错，基于贪心的想法，难以看出来，我觉得给6分的原因就是我想了一会才看出来，思路不是直观思路，另一个细节在于，判定low和high指针之间是否包含原始的元素，这个判定是需要思考细节的 后来看了其他人的做法，觉得将式子直接变成，寻找分割点这种做法，我觉得还挺不好想的，不过代码量很短，估计是很适合考场的思路</details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>在线选举</summary>

#### platform leetcode
#### id 911.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>细节就是，当平局的时候，也要更新，另一个就是，需要不断更新最大的票数 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>学生分数的最小差值</summary>

#### platform leetcode
#### id 5854.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>排序之后枚举区间 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>找出数组中的第 K 大整数</summary>

#### platform leetcode
#### id 5855.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>排序，字典序，区别在于，当长度相同时直接字典序，长度不同时，大的绝对大 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>完成任务的最少工作时间段</summary>

#### platform leetcode
#### id 5856.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>做着题的时候，怂了，首先是怂了，下次即使复杂度有一点点超标，别怕，其次是学到一招，枚举任意整数的子集，for (int j = i; j >0; j=(j-1)&i) 这招太经典了 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>不同的好子序列数目</summary>

#### platform leetcode
#### id 5857.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>来自不同的子序列 II这道题目，思想不变，改一下即可，主要就是去除前导零非常麻烦，难点首先在于如果不知道最初的题目，很难想出来，其次就是前导零这个东西不好想，如何利用已有的代码 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 7.0
</details>
</details>
<details>
<summary>按奇偶排序数组</summary>

#### platform leetcode
#### id 905.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>模拟 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>水果成篮</summary>

#### platform leetcode
#### id 904.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>双指针维护即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>子数组的最小值之和</summary>

#### platform leetcode
#### id 907.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>计数优化，在对于找到最近的小于此时值的时候的优化遇到了问题，首先就是肌肉记忆不够，最后采用的方式是非线性的，dp[i]表示，所有以arr[i]结尾的数的子数组min之和，只需要求出，最近的且最小的即可，此处使用单调栈，一个单调上升的栈来做，在理论上是线性的，因为每个元素最多只能入栈出栈一次，不过在实际的测量中，发现，以前使用的方法竟然比栈还要快，后来分析了一下原因，在栈进行增长和缩小的过程中，会进行非线性的动态数组的变化，所以，最终竟然败给了看起来不线性的做法，蛮神奇的，当然从语义层面来说，单调栈是非常清晰的 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>超级回文数</summary>

#### platform leetcode
#### id 906.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接枚举回文串就行，就是编码量稍微大了点，没啥太大思维难度 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>RLE 迭代器</summary>

#### platform leetcode
#### id 900.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>模拟，用个链表也蛮快的 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>股票价格跨度</summary>

#### platform leetcode
#### id 901.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这个就是最有意思的地方，使用一个线性的算法竟然一个看起来不那么线性的算法要慢，另外一个语义清楚的表达就是单调栈了，单调递减栈即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>最大为 N 的数字组合</summary>

#### platform leetcode
#### id 902.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>没必要想复杂，只需要另外计算一下，当长度相等的时候，怎么办就行，使用一个递归结构，及时计算可以直接出结果的，当然这步可以使用代码表述非常容易 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>DI 序列的有效排列</summary>

#### platform leetcode
#### id 903.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>如何定义状态是个问题，想了半天没有想出来如何定义状态，首先可以采用分治，大而化小，这块是相当需要注意的，细节问题很多，到底是用真实的数组长度划分，还是用约束数组长度划分，都会是个问题，最终选择使用真实的数列的数组长度进行划分，如果这块忘记了，可以看看提交的代码，可以根据约束，确定最小的值的位置，要么开头，要么结尾，要么波谷，一旦确定就会产生两个子问题，相同的解法，解即可，类似区间动态规划 正常的动态规划可以这么设计，使用dp[i][j]表示长度为i+1时，以j结尾的有效序列数量，转移就很简单，当为D时，就看前一个序列中，比j大的就行，这块特别注意需要加上一个，等于j的情况，原因我觉得目前的表述会有一定的迷惑性，如果有需要可以反复看看，当为I时，就看前一个序列中比j小的就行，当然可以使用前缀和来优化连续区间的求和问题，最终n^2就能解答</details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 7.0
</details>
</details>
<details>
<summary>单调数列</summary>

#### platform leetcode
#### id 896.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>懒得优化空间的话直接排序比较 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>递增顺序搜索树</summary>

#### platform leetcode
#### id 897.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接构造吧 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>子数组按位或操作</summary>

#### platform leetcode
#### id 898.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>利用或运算的性质，以任意的元素结尾的连续子数组中最多只有31个不同的数目，所以使用一个数组记录当此位为1的时候的最近的坐标，越往前越大单调大，排序生成这些数即可，可能因为hashset的时间常数比较大，可能时间上不能优化到一个好看的数字 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>有序队列</summary>

#### platform leetcode
#### id 899.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>需要靠大胆的猜一下，当k==1时候好讨论，只能滚动，当k>1时候，相当于，可以在k个为一组随意交换，那么通过这种策略可以完成任意序列的排序，反正突然一出出来，相当的吓人，适合考试时候挑战心态 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary> 三维形体的表面积</summary>

#### platform leetcode
#### id 892.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>是个非常不错的简单题，但是很考察数学，是个想象数学 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>特殊等价字符串组</summary>

#### platform leetcode
#### id 893.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>既然可以随意交换，那就把原始的字符串提取出来变换两个字符串，排序，放进set完事 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>所有可能的满二叉树</summary>

#### platform leetcode
#### id 894.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这个题还是需要在细节上思考一下，如果保不准，那就浪费点空间，多复制几次内存空间 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>最大频率栈</summary>

#### platform leetcode
#### id 895.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>大意了啊，20分钟绝对ak的题，竟然因为是个困难所以写起来小心了一些，很简单，用一个计数map记录相应的val有多少个，另一个为一个size的数组或map，里面装的是队列，符合后入先出即可，能保证有序这不就完了 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>公平的糖果棒交换</summary>

#### platform leetcode
#### id 888.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>编码上面有点多，对于一个简答题是多了的，不过思路还好 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>查找和替换模式</summary>

#### platform leetcode
#### id 890.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>编码也是有点小多，编码需要注意一下细节，先判断能不能映射，再具体映射，此处的编码是以前没有想过的，不过此处的解决办法还是比较优雅，如果需要可以回看 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>根据前序和后序遍历构造二叉树</summary>

#### platform leetcode
#### id 889.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>以前没想过的是，因为以前都是确定性解，先序和中序唯一确定，后序和中序唯一确定，所以对于一个不确定性解我们需要做的就是，按照一种绝对能生成的方案，做法很简单，将先序的区间和后序的区间这两个区间，来生成一个节点，依靠后续的节点来做分割前序，划分成两个子区间，逐步递归下去 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>子序列宽度之和</summary>

#### platform leetcode
#### id 891.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>看来目前困难题还是很有把握的限时做完，思路走起来，需要花个6分钟左右，当然编码很简单，具体做法就是，因为是子序列，那么先排序，形如a[0],a[1],a[2],…,a[n]，其中每一个数对都是一种不重合情况，其中区间里面的数，分别为可有可无，总共2^len种可能，现在主要的精力就是如何优化每一个数对的求和了，n^2肯定是不行的，可以这么思考，从前往后，到任意一个i时，需要的计算是,(a[i]-a[0])*2^len+(a[i]-a[1])*2^(len-1),…,如此，那么瞬间优化的思路就出来了，使用一个pre记录前面连续的和，并且在新增一个元素都需要乘2加新的，然后a[i]*(2的等比数列和)即可，这样，复杂度就变为线性的 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>两句话中的不常见单词</summary>

#### platform leetcode
#### id 884.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>模拟比较即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>螺旋矩阵 III</summary>

#### platform leetcode
#### id 885.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>写起来使用方向数组也并不复杂，还蛮有意思的 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>可能的二分法</summary>

#### platform leetcode
#### id 886.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>既然不要求两个集合数目相等的话，那就稍微简单点，思路走错了2次，最后才确定使用并查集维护，思路就是，对于一个节点来说，所有肯定与它不在一个集合的所有节点必在一个集合，，维护连通域，每次的时候检查，对于每一对不在一个集合的节点是否为一个联通域，此处的说法还是比较绕，可以看看代码能够清晰一点 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>鸡蛋掉落</summary>

#### platform leetcode
#### id 887.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>状态好表示，递推关系也是好求的，关键就是优化，因为不得不优化，状态已经是百万级的，再不优化轻轻松松就超时，当然也有一些玄学优化，比如我最后调出来的，复杂度吧，肯定理论上是很大的，但还是能过，神奇，我发现，最低点就取在2，n/2+1之间，所以勉强能过，当然，采用二分的算法能够确定最小点，此方法是记录，左右两边的函数是单调的，一个单增，一个单减，所以取max之后是对勾函数，首先想清楚的就是，两个函数的交点肯定存在，是个整数，那么确实可以用二分来写了，遗憾啊，玄学优化了没二分 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>三维形体投影面积</summary>

#### platform leetcode
#### id 883.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>投影面积好求 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>救生艇</summary>

#### platform leetcode
#### id 881.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>对对碰就行前后组装 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>索引处的解码字符串</summary>

#### platform leetcode
#### id 880.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>挺麻烦的一个递归，一般这种构造题，就是层层递归到可以找到答案的最小一层，主要是编码的时候，比较繁琐，需要考虑清楚 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>细分图中的可到达结点</summary>

#### platform leetcode
#### id 882.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>最短路应用，不太复杂即可出结果，语义也清晰 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>链表的中间结点</summary>

#### platform leetcode
#### id 876.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>费点空间，但是语义清楚 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>石子游戏</summary>

#### platform leetcode
#### id 877.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直白的动态规划，当然也可以分析必胜必败，博弈论去做，先手必胜的结论是由数学去推论的，因为先手永远可以选择一种必胜策略 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>第 N 个神奇数字</summary>

#### platform leetcode
#### id 878.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>怎么做其实都还可以，求最小公倍数，然后最后的一点，暴力，当然我最后一点二分了，其实整体上，都可以二分，编码注意细节即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>盈利计划</summary>

#### platform leetcode
#### id 879.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>动态规划，三维直接处理 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>找到数组的中间位置</summary>

#### platform leetcode
#### id 5846.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>枚举即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>找到所有的农场组</summary>

#### platform leetcode
#### id 5847.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>让我联想到了提取连通域的最小包裹矩形 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>树上的操作</summary>

#### platform leetcode
#### id 5848.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>阅读理解，o(n^2)算法即可，代码较长 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>好子集的数目</summary>

#### platform leetcode
#### id 5849.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>首先把30以内的质数,静态存储即可，看似是10万长度的数组，其实可以通过计数直接压缩到30，而30以内的质数的子集最多1024，则算法复杂度是max(1024*30,n),直接动态规划，1024就是结果状态定义，考虑不相交子集的转移即可，1是特殊情况，需要单独处理 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>统计特殊四元组</summary>

#### platform leetcode
#### id 5863.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接枚举 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>游戏中弱角色的数量</summary>

#### platform leetcode
#### id 5864.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>永远输在贪心上，而且巨卡心态，妈的，攻击力从大到小，防御力在攻击相同时，从小到达，维护最高的防御力即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>数组的最大公因数排序</summary>

#### platform leetcode
#### id 5866.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>并查集维护质因数的连通域，可以发现，相同的质因数相当于边，而元素相当于节点，只要是连通域内的节点可以随意排列 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>访问完所有房间的第一天</summary>

#### platform leetcode
#### id 5865.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这题是吐吐吐题，自己写的把自己绕，而且语义的不清楚性，导致我反复思考细节，我又看了一遍题，发现，我竟然少看了一个条件，0<=next[i]<=I,真的大气我，而我写的是对于所有next都成立的，结果既可以动态规划了，定义f[i]为再次回到i的时候走的步数，所以转移方程为，f[j]+f[j+1]+f[j+2]+…+f[i]+i-j+1因为加上了偶数的转移的一步，则就可以做了，其中区间求和使用前缀和即可 距离上次做这道题目，时隔80天，但对此题的分析，并无太大的长进，dp的定义一上手，直接就想的有点偏，通过分析出来的第一大信息，在访问到i点时，所有i之前的点的访问次数，均是偶数，故，分析就简单了，转移方程就很容易列出来了，f[i]=(i-next[i]+1)+f[next[i]]+f[next[i]+1]+…+f[i-1]</details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>叶子相似的树</summary>

#### platform leetcode
#### id 872.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接拼出序列比价就行 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>模拟行走机器人</summary>

#### platform leetcode
#### id 874.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>注意二维映射的时候常数取大一点，可能会有映射到一个点，代码长一点 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>爱吃香蕉的珂珂</summary>

#### platform leetcode
#### id 875.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>二分判定 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>最长的斐波那契子序列的长度</summary>

#### platform leetcode
#### id 873.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接枚举两点确定一条序列，虽然感觉复杂度超标，但因为数列的确定性，复杂度远低于n^3,所以大胆写 后来知道，斐波那契数列的增长指数型，所以在int范围内只能最多43长度，所以复杂度解释得清了</details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>二进制间距</summary>

#### platform leetcode
#### id 868.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接把1的坐标求出来，相邻两个一减就行 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>重新排序得到 2 的幂</summary>

#### platform leetcode
#### id 869.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>下一个排列的函数直接全排列试试 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>优势洗牌</summary>

#### platform leetcode
#### id 870.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>当然先跳过去做了动态规划，再次回过头，可以大胆猜测，得出，使用treemap来维护nums1，对于num2[i]来说，找到treemap中还剩的比nums[i]大的，就可以使得总数最大，剩余没有的，随便用剩余数组元素填，如果使用比nums2[i]不是仅仅大的，势必会影响到下一个，所以使用仅仅大的情况一定不会比大得多的差 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>最低加油次数</summary>

#### platform leetcode
#### id 871.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>使用dp[i][j]表示经过i个车站加了j次油的还能最长跑的距离，中规中矩 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>转置矩阵</summary>

#### platform leetcode
#### id 867.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接转置即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>具有所有最深节点的最小子树</summary>

#### platform leetcode
#### id 865.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>本质上像是找最近的公共祖先，也可以像是多源bfs，给4分的原因是编码长 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>回文素数</summary>

#### platform leetcode
#### id 866.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>我觉得直接生成回文数，检测是否为素数就可以了，编码长 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>获取所有钥匙的最短路径</summary>

#### platform leetcode
#### id 864.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>像这种最短的，直接抽象，mark[i][j][k],对于在坐标(I,j)持有钥匙为k的最小的值，bfs直接搜索，很快就会迭代到最终结果，就是编码较为复杂，细节较多 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>柠檬水找零</summary>

#### platform leetcode
#### id 860.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>是一个蛮好玩的题 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary> 二叉树中所有距离为 K 的结点</summary>

#### platform leetcode
#### id 863.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>树转图，直接bfs </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>翻转矩阵后的得分</summary>

#### platform leetcode
#### id 861.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>其实反转的里面隐藏着优先级，第一遍，先按行反转，如果此行第一个为0，反转，再按列反转，这样子，可以保证最大，当然这个还是很唬人的 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>和至少为 K 的最短子数组</summary>

#### platform leetcode
#### id 862.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>思考，难道二分真的不能做了嘛，二分需要单调性，而在有负数的情况下，子数组和并不单调，所以，必须由题意构造一种单调性，设f（x）为长度不超过x的情况下，最大的子数组之和，这个是单调的，所以二分就出来了，f（x）的求法来自于经典的单调队列算法，所以复杂度是o(n)*logn的，这个思想将问题拆解成了经典模型，是比较可复制，且可取的做法，当然也可以用o(n)的算法，只不过从思维分析上，要求还是蛮高的，通过性质可以分析出，及时删除冗余的信息条件，本质上，也是一种单调队列的思想 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 7.0
</details>
</details>
<details>
<summary>镜面反射</summary>

#### platform leetcode
#### id 858.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>找个公倍数，然后再判断奇偶就可以了 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>括号的分数</summary>

#### platform leetcode
#### id 856.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>先匹配括号，然后再递归向上即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>亲密字符串</summary>

#### platform leetcode
#### id 859.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>思路不难，但在简单题里面属于编码较多的，注意细节即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>雇佣 K 名工人的最低成本</summary>

#### platform leetcode
#### id 857.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>我觉得应该是贪心，至少是nlogn的算法，但没做出来，没分析出来，怎么搞，反正没做出来的题，难度系数就很大，我看了看，觉得就是一个中等题，窒息了，唉，脑袋反应慢了 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 7.0
</details>
</details>
<details>
<summary>山脉数组的峰顶索引</summary>

#### platform leetcode
#### id 852.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>遍历即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>车队</summary>

#### platform leetcode
#### id 853.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>想在第一次就相当简洁的表达是不容易的，思路就是按初始位置先排序，然后计算与下一个的交点，如果在范围内相交，那么就剔除下一个，直到不相交，将答案加入set </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>考场就座</summary>

#### platform leetcode
#### id 855.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这题挺不好的，因为有个语义特别不清楚，就是最近的距离这个，官方直接除二，以至于严格区分段长实在是没法写，而且官方给的解竟然是n^2的。算了，没啥意思，而且代码巨复杂 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>相似度为 K 的字符串</summary>

#### platform leetcode
#### id 854.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>总的来讲，这题做得还是比较成功，但是本题的复杂度还是很高的，估计做法并不唯一，所以技巧性，肯定要求很高，新学了一招，快速枚举n个状态长度中中k个状态的方法，使用动态规划，这个动态规划，隐含着优先级，本质上，就是把原始的状态图，划分成，存在环的小图，而且数目要尽可能多，此处的小图相当抽象，形如a b就为大小为2的小图，再如a b c 为大小为3的小图，                                                               b a                                           c  a b将索引的位置看成状态，可以预处理小于6长度的状态能否构成子图，然后对于任意一个索引，在最优的情况下，只可能对应一种划分，所以，枚举包含此索引的任意划分子图，使用动态规划求最大的划分图的数即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 8.0
</details>
</details>
<details>
<summary>字母移位</summary>

#### platform leetcode
#### id 848.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>后缀和统计一下即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>到最近的人的最大距离</summary>

#### platform leetcode
#### id 849.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>treeset维护一下即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>喧闹和富有</summary>

#### platform leetcode
#### id 851.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>树形dp，一看数据范围，以为能便利，后来，发现，自己傻了，瞬间改dp </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>矩形面积 II</summary>

#### platform leetcode
#### id 850.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>斯，这题不会了，二维的区间更改，直接联想到了树状数组和线段树，但无奈没准备二维模板， </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 7.0
</details>
</details>
<details>
<summary>反转单词前缀</summary>

#### platform leetcode
#### id 5867.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>找出来，转一下就行 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>可互换矩形的组数</summary>

#### platform leetcode
#### id 5868.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>计数优化，考烂的 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>两个回文子序列长度的最大乘积</summary>

#### platform leetcode
#### id 5869.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>状态压缩，先把回文的筛出来，然后再一个个去与不重叠的比较就行 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>每棵子树内缺失的最小基因值</summary>

#### platform leetcode
#### id 5870.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>并查集出色解法，从未想过并查集还可以充当一个查找集合的功能，特此记录一下，灵感来源于连通域在dfs过程中只要没有遇到根节点，就是独立的特性，时间复杂度nlogn,虽然在递归中有个循环，但是在一次中，只会走一遍所以可以看成线性的，第一次还在思考怎么用treeset，发现根本没有办法做到独立性（就是我在左子树中操作，会影响到右子树的查询） </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>无人机方阵</summary>

#### platform leetcode
#### id LCP 39

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>找到所有相同的即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>心算挑战</summary>

#### platform leetcode
#### id LCP 40

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>虽然这是个简单题，但是这题真的是把我卡到了，贪心吓死人，最讨厌贪心了，做法就是，奇偶分别提取出来，排序，如果是奇数，那么一定是从偶数的第一位开始，如果是偶数，偶数从0位开始，这个能想到还是考大胆猜的，这题卡死我了，心态都崩了 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>黑白翻转棋</summary>

#### platform leetcode
#### id LCP 41

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这题其实写起来很麻烦，需要注意，需要不断的查询，修改，查询，修改，模拟题，真的很麻烦 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>玩具套圈</summary>

#### platform leetcode
#### id LCP 42

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>因为半径很小，所以可以直接枚举周围的点，将二维映射到一维上，就可以hash了，唉，脑子还是不够快，不够敏感啊 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>十字路口的交通</summary>

#### platform leetcode
#### id LCP 43

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>就状态压缩，比较难调，需要把所有不合法情况check一下，需要仔细写，注意三线的交点，别忽略了 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>比较含退格的字符串</summary>

#### platform leetcode
#### id 844.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>栈模拟即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>数组中的最长山脉</summary>

#### platform leetcode
#### id 845.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>单调栈 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>一手顺子</summary>

#### platform leetcode
#### id 846.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>虽然题重复，但是做得更快了 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>访问所有节点的最短路径</summary>

#### platform leetcode
#### id 847.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>标准图的状态压缩表示了，搜索的时候，用bfs即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>矩阵中的幻方</summary>

#### platform leetcode
#### id 840.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>遍历就行了，没啥说的，写代码长一点而已 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>钥匙和房间</summary>

#### platform leetcode
#### id 841.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>典型bfs表示 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>将数组拆分成斐波那契序列</summary>

#### platform leetcode
#### id 842.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>编码很长，所以很烦 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>猜猜这个单词</summary>

#### platform leetcode
#### id 843.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这题真的是，猜测最小的集合，每次逐步缩小集合范围，在选取下一个猜测点的时候，需要适当的采用一种策略，能够更加有效的逼近答案，但是我预估，表达还是很粗略，很取决与数据，就是在可行里，找到下一个更小更小的可行的即可，不给7的原因是，我觉得这题不足以充当一个很好的思维模板 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>矩形重叠</summary>

#### platform leetcode
#### id 836.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>我觉得是简单题，我做复杂了，不过扫描线法真好用 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>推多米诺</summary>

#### platform leetcode
#### id 838.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>简单的bfs扩展，不过题目还是蛮有意思的，使用一个hashmap记录一下，如果出现即左又右的情况，就可以消掉了 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>相似字符串组</summary>

#### platform leetcode
#### id 839.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这题实在是没啥做得，连通块个数 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>新21点</summary>

#### platform leetcode
#### id 837.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>相当不错的动态规划，从后往前，没想到，如果考虑优化的话，尝试临项差分，优化增加，特别的在临界区域的时候，回归原始定义，所以，书写的思路是，先推原始的式子，再使用数学去优化，临界区域使用原始定义计算 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 7.0
</details>
</details>
<details>
<summary>统计子串中的唯一字符</summary>

#### platform leetcode
#### id 828.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这题非常奇怪，分析不出来时间消耗在哪里，理论上的时间消耗为52*n,这个线性范围，按道理来说，应该是时间非常够，非常奇怪，后来仔细读了逻辑，发现，重大问题，其实在内部是52*26*n,所以时间会处理很长时间，直接将程序拖垮，其实思考了很长时间，对于这种计数类优化，按道理，对于现在的我来说，应该不难想出优化的方式，特别着重思考了，对于动态规划，的起始怎么办？而状态和转移没有想明白，后来接着白天的思考，反复在思考，前一个字符和后一个字符的区别，可以这么认为，我们可以将唯一字符这件事，变成，一个字符在子字符串中统计出现的次数，则状态规划很容易思考出来，（其实也没有那么容易），形如，可以这么分析，aba     1   1  2  ,再形如   abab    1  1    2   2，可以看出来，我们使用状态dp[i][j]表示                  0   2  2                             0   2   2   2以i为结尾的字符的子字符串中，包含的j的次数，每一个i只与i-1的有关，对于当前i的字符需要更新而已，新的值为从i到前一个与i相同的字符的距离，动态规划定义转移到此结束，时间复杂度为26*n 给7分的原因其实是给自己一个记性，思考时间太长了，其实代码非常的精炼的</details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 7.0
</details>
</details>
<details>
<summary>反转图像</summary>

#### platform leetcode
#### id 832.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>定义逆序和反转两个操作就可以了 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>字符串中的查找与替换</summary>

#### platform leetcode
#### id 833.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>真的是，语义层面的理解是基于题的说明的，个人觉得此题，从读上去，绝对不是那么直观，反复错了几次的主要原因，都是因为，题并没有读懂，真的是醉了，其实蛮简单的 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>图像重叠</summary>

#### platform leetcode
#### id 835.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>简单来讲，将图像铺平，铺展到以前的3*3倍，这个刚好，然后逐一去检查即可，时间复杂度，刚刚好，但其实并不是最优的方式 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>树中距离之和</summary>

#### platform leetcode
#### id 834.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>典型的树状dp，之所以树状dp的抽象更容易去写出来的主要原因是，dp中的难点在于状态的定义，而树的抽象结构可以自然而然的将dp的状态定义成树的一个节点，而剩下的转移就是与此树相关的节点，状态与转移自然就定义好了，而这道题中，因为是双向边，所以，需要人为的规定，根节点，从而，边界的处理就有了语义规定，也就是叶节点，但是编码初次稍稍的繁琐了些，状态在其中的定义需要做一些预处理，即，求出，任意的节点为子树的节点个数，则可以这么表达，假设我们以知任意的一个节点的到其为子树的距离之和，那么还剩最后一个关系没有计算，那就是，它和父节点的增量，为父节点，不包含这条父子边，其他关系的距离之和，再加上，除过这个节点的子树的之外的节点数，则动态规划的转移成立了 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>差的绝对值为 K 的数对数目</summary>

#### platform leetcode
#### id 5859.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>模拟即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>从双倍数组中还原原数组</summary>

#### platform leetcode
#### id 5860.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>排序，从高到低，记得去重即可，稍微错了一次，非常尴尬 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>出租车的最大盈利</summary>

#### platform leetcode
#### id 5861.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>区间最大值解决，直接树状数组维护区间最大值，其实想了想，觉得，区间是递增的，应该也可以不用树状数组维护区间最大值，使用treemap也行，不过，数组树状语义也是很清楚的 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>使数组连续的最少操作数</summary>

#### platform leetcode
#### id 5862.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>其实是个范围查询，去重再范围查询，有多少个，然后再减去求最小 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>执行操作后的变量值</summary>

#### platform leetcode
#### id 5875.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接模拟 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>数组美丽值求和</summary>

#### platform leetcode
#### id 5876.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>先求区间最小最大，然后直接遍历即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>检测正方形</summary>

#### platform leetcode
#### id 5877.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>按照对角线枚举就行了 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>重复 K 次的最长子序列</summary>

#### platform leetcode
#### id 5878.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>编码还是有复杂度，即用到了上一个排列，也用到了在n个中枚举k个，然后因为字符串长度最多7，可以枚举子集之和，再枚举全排列，复杂度能好点，但确实，编码很复杂了，而且这题，挺考验考场心态，唉 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 7.0
</details>
</details>
<details>
<summary>按照频率将数组升序排序</summary>

#### platform leetcode
#### id 1636.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>不小心写错了一次，注意看题就行了 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>两点之间不包含任何点的最宽垂直面积</summary>

#### platform leetcode
#### id 1637.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>不该是个中等题吧，简单题无疑了 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>统计只差一个字符的子串数目</summary>

#### platform leetcode
#### id 1638.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>我暴力解了哈，唉，尝试动态规划应该是可行，看到一种思路应该是比较巧妙，使用两个dp数组，其一表示，s[i]和t[j]相同的子数组个数，其二表示，s[i]和t[j]有一个不同的子数组个数，搭配一下，应该不难写出来 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>通过给定词典构造目标字符串的方案数</summary>

#### platform leetcode
#### id 1639.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>动态规划，先统计，再动态规划，注意写法就行，转移很简单,dp[i][j]=dp[i-1][j-1]*data[i][index],+dp[i-1][j]，然后正常书写就可以了,再次写的时候，也觉得是个很简单的转移，不过还是wa了一次 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 35
- ACtime: 2023.4.16
- ACstatus: 完全会
- score: 6.0
</details>
</details>
<details>
<summary>删除某些元素后的数组均值</summary>

#### platform leetcode
#### id 1619.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接模拟即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>网络信号最好的坐标</summary>

#### platform leetcode
#### id 1620.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>注意读题 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>大小为 K 的不重叠线段的数目</summary>

#### platform leetcode
#### id 1621.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>写了个有可能超时的动态规划，十分尴尬，当然可以重新定义，这么理解，dp[i][j][0],表示，最后一个点不用的方案数，dp[i][j][1]，表示最后一个点用的方案数，从此去分析转移，当然数学一行代码也能做，不过要很好的理解和转化，可以去想想，算是一道好题 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>奇妙序列</summary>

#### platform leetcode
#### id 1622.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>线段树的实现放到之后，没想到只是个逆元问题，没看出来，唉，失策，不过好题，非常好，给高分的原因就是因为一方面没有直观做出来，另一方面是个好题 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 7.0
</details>
</details>
<details>
<summary>设计停车系统</summary>

#### platform leetcode
#### id 1603.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>稍微多了一点编码而已 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>警告一小时内使用相同员工卡大于等于三次的人</summary>

#### platform leetcode
#### id 1604.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>kv存储，然后排序 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>给定行和列的和求可行矩阵</summary>

#### platform leetcode
#### id 1605.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>错了好几次，唉，不过题我觉得挺好的，必须给出一种，必定生成的策略，本质上可以转换为线性规划，其中存在自由变量所以，还是直接指定写死比较好，其中在一次迭代的过程中，有可能出现仍然未有解的情况，则将小矩形，继续迭代 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>找到处理最多请求的服务器</summary>

#### platform leetcode
#### id 1606.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>其实没有多么困难，语义很清楚，下一个使用一个平衡树维护就行，然后再使用优先级队列，维护释放负载就行 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>所有奇数长度子数组的和</summary>

#### platform leetcode
#### id 1588.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>暴力最稳 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>所有排列中的最大和
</summary>

#### platform leetcode
#### id 1589.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>差分数组的排序问题，保证最大 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>使数组和能被 P 整除</summary>

#### platform leetcode
#### id 1590.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>前缀和计数优化，破破破 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>奇怪的打印机 II</summary>

#### platform leetcode
#### id 1591.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>虽然方法从数学上应该是复杂度比较高，但是对于这道题来说，已经足够，核心在于每轮迭代找到希望面积等于真实面积的值即可，然后更新去除，比如当有一个矩形，确定了面积等于期望值，那么，将与此矩阵有重叠的矩形的真实面积都进行更新，切记处理重复更新，是一种拓扑排序的翻版，当然我没写拓扑排序的版本， </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>统计所有可行路径</summary>

#### platform leetcode
#### id 1575.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>定义状态，dp[i][j]表示，到这个i节点的消耗的j油的方案数，倒着往前，调一调就可以了，当然，在其中使用了set的查询可能会慢一点 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>删除最短的子数组使剩余数组有序</summary>

#### platform leetcode
#### id 1574.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>妈的，写了一坨屎，剔除绝对不行的，然后边界处需要好好处理，妈的，搞我了 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>矩阵对角线元素的和</summary>

#### platform leetcode
#### id 1572.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>奇偶分开即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>分割字符串的方案数</summary>

#### platform leetcode
#### id 1573.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>情况简单，注意模余即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>增量元素之间的最大差值</summary>

#### platform leetcode
#### id 5881.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接模拟 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>网格游戏</summary>

#### platform leetcode
#### id 5882.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>蛮有意思的，被题意给包装了一下，仔细思考不难 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>判断单词是否能放入填字游戏内</summary>

#### platform leetcode
#### id 5883.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>挺吓人的，反正，唉，其实很简单 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>解出数学表达式的学生分数</summary>

#### platform leetcode
#### id 5884.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>一个集合类的区间dp,竟然初次思考被带偏了，唉，真的是痛失心态，即使剪掉1000以外的，真的是痛失心态，做完心情立马不好了 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>千位分隔数
</summary>

#### platform leetcode
#### id 1556.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接模拟 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>可以到达所有点的最少点数目</summary>

#### platform leetcode
#### id 1557.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>拓扑排序的起始点 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>得到目标数组的最少函数调用次数</summary>

#### platform leetcode
#### id 1558.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>蛮有意思的，只要想明白一个数字怎么操作最少，就可以想明白，多个之间的共性 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>二维网格图中探测环</summary>

#### platform leetcode
#### id 1559.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>求环，直接dfs即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>找出最长的超赞子字符串</summary>

#### platform leetcode
#### id 1542.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>前缀和的状态压缩 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>平衡括号字符串的最少插入次数</summary>

#### platform leetcode
#### id 1541.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这道题还挺磨人，引人错了好多次，不错，是个好题 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>K 次操作转变字符串</summary>

#### platform leetcode
#### id 1540.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这题也不错，是个很好的中等中的中等题 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>第 k 个缺失的正整数</summary>

#### platform leetcode
#### id 1539.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接模拟 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>形成目标数组的子数组最少增加次数</summary>

#### platform leetcode
#### id 1526.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>最简单的一维线性动态规划 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>字符串的好分割数目</summary>

#### platform leetcode
#### id 1525.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接前缀和优化即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>和为奇数的子数组数目</summary>

#### platform leetcode
#### id 1524.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>又是个前缀和优化计数问题 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>在区间范围内统计奇数数目</summary>

#### platform leetcode
#### id 1523.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接分类讨论即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>石子游戏IV</summary>

#### platform leetcode
#### id 1510.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>裸动态规划 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>三次操作后最大值与最小值的最小差</summary>

#### platform leetcode
#### id 1509.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这道题可以推广为，n次操作，最大值与最小值差，就是就是改变数组的头尾，在语义上理解为去掉即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>子数组和排序后的区间和</summary>

#### platform leetcode
#### id 1508.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接模拟，升级升级 当然这题可以需要用到更优的解决方案，可以这么去思考，参考的思路来源于有序矩阵，有序矩阵的性质是非常厉害的优化</details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>转变日期</summary>

#### platform leetcode
#### id 1507.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>模拟 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>去掉最低工资和最高工资后的工资平均值</summary>

#### platform leetcode
#### id 1491.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接模拟 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>n 的第 k 个因子</summary>

#### platform leetcode
#### id 1492.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接模拟 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>删掉一个元素以后全为 1 的最长子数组</summary>

#### platform leetcode
#### id 1493.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>注意判断全1的情况 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>并行课程 II</summary>

#### platform leetcode
#### id 1494.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>状态压缩，将先修的课程作为集合 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>将一维数组转变成二维数组</summary>

#### platform leetcode
#### id 5871.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>模拟 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>连接后等于目标字符串的字符串对</summary>

#### platform leetcode
#### id 5872.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>因为数据较小，直接模拟 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>考试的最大困扰度</summary>

#### platform leetcode
#### id 5873.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>双指针直接计算，挺惊险的，一次写对不容易啊，跟1004题一模一样，不过，当时我竟然用了前缀加二分，经过了两个月后我已经可以用双指针了，还是有进步的 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>分割数组的最多方案数</summary>

#### platform leetcode
#### id 5874.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>将，可以改变的地方，一分为2，变成前后两部分，分别用两个map去维护，则变成计数问题，就好解了 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>转换字符串的最少操作次数</summary>

#### platform leetcode
#### id 5890.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接计算即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>找出缺失的观测数据</summary>

#### platform leetcode
#### id 5891.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接构造，判断即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>石子游戏 IX</summary>

#### platform leetcode
#### id 5892.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>想了一种特别另类的优化方法，打死我也不用第二次了，个人猜测只能骗过数据，太惊险了，一个博弈论的题目，它竟然将门槛出在了数据量上，对于传统的博弈论的做法在时间空间上无法过去，但是可以使用数据玄学放缩的方法对原数据直接放缩，直接模余100以内的质数即可，再使用传统的博弈论的方法计算必胜态必败态的转移即可,当然最优的方式还是直接对博弈的序列进行直接分析，因为先手后手的序列是固定的，时间复杂度直接o(1) </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>含特定字母的最小子序列</summary>

#### platform leetcode
#### id 5893.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>从后往前，o(n)的复杂度，利用后缀和判断，规则是这样的，如果选择的字符不是letter，需要满足三个条件，选择之后的剩余长度大于等于未选择的个数，选择之后剩余的letter数目大于等于还未选择的letter数目，最后需要记录选择的非letter的总数，判断已选非letter的数目是否小于等于可选非letter数目，如果选择的字符是letter，只需要满足一个条件，即选择之后的剩余长度大于等于未选择的个数 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>商品折扣后的最终价格</summary>

#### platform leetcode
#### id 1475.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接模拟 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>子矩形查询</summary>

#### platform leetcode
#### id 1476.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>将修改记录，从前往后，找不到直接返回初始值，找到就返回 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>找两个和为目标值且不重叠的子数组</summary>

#### platform leetcode
#### id 1477.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>虽然稍微写的有一点点复杂，不过还好，当然O（n）的时间就可以过，先用前缀和技巧，直接求出子数组和为target的线段，将问题重新转换求，不相交子线段中，长度最小的两者之和，从前向后遍历，维护之前的最小值即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>安排邮筒</summary>

#### platform leetcode
#### id 1478.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>给7分的原因是，思考时间久，不想浪费这题的价值，本题思考花费时间很久，虽然代码很简单但是，想清楚这个过程真的不容易，论如何将这题脱得精光，在刚开始思考的时候，思路一直被数据范围带偏，一直卡在定义状态里面出不来，再思考了40分钟后，我睡了一觉，起来竟然只想了不到10分钟，就将这题分析透彻了，灵感来源于那个绝对值符号，刚开始以为对于每个数它前面的符号只有正值或者负值，可以动态规划，后来又发现了正负排列的规律，有点一组一组的感觉，瞬间大脑清醒，那不就是意味这求n个数分成k组，而每一组的消耗为到中位数的距离之和，直接破掉此题，二维动态规划的定义和转移清清楚楚 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 7.0
</details>
</details>
<details>
<summary>通过翻转子数组使两个数组相等</summary>

#### platform leetcode
#### id 1460.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>根据排序中的交换理论，只要能交换相邻的元素，数组就可以任意排序 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>检查一个字符串是否包含所有长度为 K 的二进制子串</summary>

#### platform leetcode
#### id 1461.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>不小心把一个动态变换的值放在了循环上，不小心出了错 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>课程表 IV</summary>

#### platform leetcode
#### id 1462.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>复杂度判断没做好，就日了，莫名的超时，不由让我重新反思起来mark数组的意义，在复杂度还没有爆表的情况下，看不出来，因为一旦dfs如果不记录的话，会重复的进行遍历，是真的可能带来指数级别的爆炸，所以一定不要偷懒，想到就写完整 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>摘樱桃 II</summary>

#### platform leetcode
#### id 1463.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>看数据，上三维动态规划吧，转移很好写，初始也很好，解题的就记忆化写法，不太会出错，就是注意一个访问到同一个时候只需要加一遍即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>数位成本和为目标值的最大数字</summary>

#### platform leetcode
#### id 1449.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>二维动态规划＋回溯，状态定义清晰且转移清晰，需要在写法上注意即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>每个人戴不同帽子的方案数</summary>

#### platform leetcode
#### id 1434.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>想了很久，没想好状态的定义，心里特别懊恼，个人觉得应该代码超简单，就是那一点点，没想通，结果就卡这么这么久，其实状态定义的方式就那么点，挑战思维就在这个地方，只要状态定义出来了，转移都不是个事，唉，晦气，dp[i][s],表示前i个帽子，分配了s状态个人 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 7.0
</details>
</details>
<details>
<summary>做菜顺序</summary>

#### platform leetcode
#### id 1402.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>一个动态规划结束 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>3n 块披萨</summary>

#### platform leetcode
#### id 1388.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>容易想到复杂的时空做法，就是，将当前数组存成局面，然后遍历每一种取的方案，这就是最原始的动态规划，不过过于复杂，我对这题的难度提升档次，其实对于，可选的序列，有可能是有某种规律，可能对于状态的定义，会很简化，后来看了看做法之后，决定对此题降一级，很多人由此直接想到了打家劫舍系列的题目，不过非常的惭愧，打家劫舍的题目我是全部做完了，但是呀，竟然忘记了，我看了看提交的时间，已经距离现在经历6个月之久，打家劫舍是一个不取相邻的最大值的做法，和此题在约束上有一定的相似性，但我没想到哈哈，区别就在于，比如说，打家劫舍的最大取值的个数是n/2,而此题是n/3这就是区别，而且取值是定值，不可以多，也不可以少，后来想了想，关于在3n个数中取n个不相邻的数的最大值，就是此题的解的这一步转化是需要被证明的，个人觉得最好的一句话解释是，如果取的序列不相邻，那么一定有一种取法，可去取到这种序列，这才是点睛之解释 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 7.0
</details>
</details>
<details>
<summary>有效的快递序列数目</summary>

#### platform leetcode
#### id 1359.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>过于简单的dp </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>二叉搜索子树的最大键值和</summary>

#### platform leetcode
#### id 1373.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>很简单，树状的一个dp，其实也就是dfs，直接按照题意模拟都可以 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>跳跃游戏 IV</summary>

#### platform leetcode
#### id 1345.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>当题目出了一个只要满足值相等的，看似让跳的范围夸大很多，感觉是不是更容易到达终点的时候就要额外的小心，因为，假设如同一个数组中的重复数组很多，而每一次都需要判断并更新到达相同值是否需要加入bfs的队列中，复杂度立马就体现出来了，所以需要想清楚的在于，如果当相同值集合中的任意一个元素被访问，都要开始更新整个集合的时候，的下一次，是不需要再次的遍历这个集合的，因为步数一定大于等于前一个被访问到的节点，利用了bfs的单调性，所以，每次遍历完，相同值集合之后，就可以直接将此集合删除，这样，复杂度直接就将为线性 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>最大得分的路径数目</summary>

#### platform leetcode
#### id 1301.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>非常传统的dp，状态定义清晰明了，转移显而易见，就是加上了方案数而已 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>不同的循环子字符串</summary>

#### platform leetcode
#### id 1316.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>字符串hash加去重，直接枚举即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>翻转子数组得到最大的数组值</summary>

#### platform leetcode
#### id 1330.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>一个绝对值的化开题，不过，其中略带一点点，数学的推论，数学一上来，会有一些干扰，总感觉，化不开这个值，其实还可以，代码量也比较少，应该算是老生常谈的方式 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 7.0
</details>
</details>
<details>
<summary>至少在两个数组中出现的值</summary>

#### platform leetcode
#### id 5894.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>竟然让我读了一会题！！！ </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>获取单值网格的最小操作数</summary>

#### platform leetcode
#### id 5895.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>绝对值之和最小的翻版 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>股票价格波动</summary>

#### platform leetcode
#### id 5896.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>两个treemap即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>将数组分成两个数组并最小化数组和的差</summary>

#### platform leetcode
#### id 5897.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>双向搜索的衍生，一开始直觉状态压缩，后来肯定复杂度超标了，故需要重新思考，如果我们这么考虑，将一半进行预处理，所以就可以在logn的时间内查询到，需要找的数，将数组的一半的所有的子集的求和存储成一个treeset的数组，然后再枚举另一半的子集的情况，如果在前一半中选了x个，必然要在后面选择num.length/2-x个，然后在对应的 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>不相交的握手</summary>

#### platform leetcode
#### id 1259.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>存储是线性的，然后时间的话，是平方的，好定义，也好转移 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>矩形内船只的数目</summary>

#### platform leetcode
#### id 1274.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>有点类似kdtree，空间的分割，根据x和y来交叠分割 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>下降路径的最小和II</summary>

#### platform leetcode
#### id 1289.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>动态规划直接上 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>验证回文字符串 III</summary>

#### platform leetcode
#### id 1216.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>最长回文子串的变形逆题，直接求最长回文子串，然后判断减少的字符数即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>分享巧克力</summary>

#### platform leetcode
#### id 1231.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>终于在做题的时候将华为的题补全了，这就是数组分段，段内之和的最大值最小问题，使用二分去逼近答案，我觉得算是二分一个最好的伪装题目，真的没看出来，因为数据量的原因，直接把dp给拍死了，还有一个重要的细节，在于，我们把可行解的序列看false,false,,,,true,true,的序列，对于正常的能够刚好等的，直接返回，如果在二分中，有不满足条件的，那就是最大的false，是处于false，和true的分界处的，这个情况是适用于，当所有数都相等的情况，这个是个很核心的细节 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 7.0
</details>
</details>
<details>
<summary>删除回文子数组</summary>

#### platform leetcode
#### id 1246.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>我十分笃定这是一个区间dp，然而竟然让我窒息，先给弄个7分，惊醒自己，对于这种套路的状态规划，敏感度弱了很多，当两边相等时，可以直接迭代，如果两边不相等时，一定会处于某一种划分，则解就包含在由中心点为分割的dp[x][mid]+dp[mid+1][y]之中，所以，十分呕血，十分简单的玩意，想啥呢，不知道，略 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 7.0
</details>
</details>
<details>
<summary>水资源分配优化</summary>

#### platform leetcode
#### id 1168.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>首先，想到的思路是，在一个连通域内，肯定选最小的，而且连通域内的路径，符合最小生成树，然而，在如何分割连通域就产生了问题，how?看了看题解之后，恍然失措，人傻了，一个超级源点的转化即可，到此点的铺设成本看成建造水库的成本即可，人傻了，有增长的小技巧，增加了分数，以示警示 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 7.0
</details>
</details>
<details>
<summary>建造街区的最短时间</summary>

#### platform leetcode
#### id 1199.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>我觉得小记录一下，高光时刻，动态规划之中的优化启示来源于，鸡蛋掉落这道题目，对勾函数的最小值求法，这个是非常关键的，所以，在增加了一个二分查找的优化之后，就可以过了，虽然状态转移的方程很简单，但貌似有更好的做法，可以转化为Huffman树的方式，逆向去想这个问题,由核心的式子转换而来，s+max(a,b)其中a和b指的是节点，则就可以这么选择，最终合并出来的节点就是所需要的时间，每次新合并的节点就是s+max(a,b),这样，通过huffman树很快就迭代出来了 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 7.0
</details>
</details>
<details>
<summary>矩阵中 1 的最大数量</summary>

#### platform leetcode
#### id 1183.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>配的起8分的题，我只能说牛逼，竟然让我无从下手，用什么算法毫无头绪，不过可以分析出，一种简单性质，被越多的矩形占用的小方格，是越不能选择的，所以，最终的图案肯定是关于中心中心对称的，可是有如此直白的观察还不够，还是没有把此情况，抽象成，可以求解的模型或者思路，难道动态规划，状态是啥？定义不出来，难道是分形？好像也无法转换成更小的子问题，所以搞住我了，又是一道有增长题，吴自华写了一篇比较难懂，但是非常简单的做法，数学的证明就非常头大，什么是等效？个人觉得等效应该就是，这个正方形在漂移的过程以边长为步长中的所有能够到达的点的所有集合，计算一下，最大的等效的点的数目即可，思维难度很高，很难想到如此巧妙的方法，复现难度很高，后来想了一下，感觉却特别合理 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 8.0
</details>
</details>
<details>
<summary>将数组分成几个递增序列</summary>

#### platform leetcode
#### id 1121.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>吓死人不偿命题目，我一看以前的竞赛记录，很多人都做出来了，我寻思这题应该是一个困难题中的简单题，但按照正向思维，去构造选取的序列这一个思路似乎是行不通的，没有办法找到一个很好构造序列的方式，大胆猜结论！！，这题靠猜，发现，对于相同的元素必然不在同一个序列，那么，只需要，统计，出现频率最高的个数是多少，也就意味着数组中一定要有k*max个元素才可以，并且一定有一种选取的序列，可以满足，所以此题靠猜 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>平行课程</summary>

#### platform leetcode
#### id 1136.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>拓扑排序太简单了，不说啥了 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>字符串转化</summary>

#### platform leetcode
#### id 1153.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这个题是个跟数据量无关的，但是，它想去表达的拓扑结构虽然人的理解上，是简单的，但是程序的理解是复杂的，本质上是一种检测环，而且还要给出如何拆解环的方案，在其中就存在着非常复杂的情况，给出的解决方案虽然可以过测试集，但无法保证正确性的证明，而且估计思想属于不可以复现的，当然，这种杂题作为困难题是合适的，但并不适合反复做 后来看了看题解区，果然一个大胆猜结论的题目，痛死我呀</details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 7.0
</details>
</details>
<details>
<summary>得分最高的路径</summary>

#### platform leetcode
#### id 1102.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>使用传递的bfs搜索，从我直观感受上，应该复杂度很小才对，但看了看，消耗时间还挺长的，难道是我写法的问题，导致的吗，很有可能 不过后来看了看并查集的做法，感觉还是不错的，本质上维护连通的问题，感觉代码写出来应该相当的优雅</details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>易混淆数 II</summary>

#### platform leetcode
#### id 1088.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>我觉得倒不是很难的样子，就是个枚举并且生成，就是不能重量级生成，因为，虽然时间复杂度是可以过的，但是，其中会有各种常数的消耗，使得时间也有可能被消耗掉 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>范围内的数字计数</summary>

#### platform leetcode
#### id 1067.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>典型的题目简单，代码构思难写，有种呕吐般的难写，后来改变了思路，将每一位独立开写，当此位为d时，有多少种不同的数，仍然是呕吐般难写，给个7分，是真的难写，要用数位dp那还不如好好分析，数位dp还是难 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 7.0
</details>
</details>
<details>
<summary>用户网站访问行为分析</summary>

#### platform leetcode
#### id 1152.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这题稍稍有点麻烦 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>最少交换次数来组合所有的 1</summary>

#### platform leetcode
#### id 1151.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>前缀和优化 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>最低成本联通所有城市</summary>

#### platform leetcode
#### id 1135.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>最小生成树 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>子树的最大平均值</summary>

#### platform leetcode
#### id 1120.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>dfs </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>彼此熟识的最早时间</summary>

#### platform leetcode
#### id 1101.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>并查集板子了 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>长度为 K 的无重复字符子串</summary>

#### platform leetcode
#### id 1100.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接枚举 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>小于 K 的两数之和</summary>

#### platform leetcode
#### id 1099.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>太简单了 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>花括号展开</summary>

#### platform leetcode
#### id 1087.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接模拟 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>前五科的均分</summary>

#### platform leetcode
#### id 1086.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>简单不多说 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>最小元素各数位之和</summary>

#### platform leetcode
#### id 1085.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>呜呜，简单哭了 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>校园自行车分配 II</summary>

#### platform leetcode
#### id 1066.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>状态压缩的动态规划 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>字符串的索引对</summary>

#### platform leetcode
#### id 1065.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>简单不多说 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>不动点</summary>

#### platform leetcode
#### id 1064.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>20秒看题，60秒写题 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>使每位学生都有座位的最少移动次数</summary>

#### platform leetcode
#### id 5885.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接排序匹配 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>如果相邻两个颜色均相同则删除当前颜色</summary>

#### platform leetcode
#### id 5886.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>大于等于3的连续AB的数目比较 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>网络空闲的时刻</summary>

#### platform leetcode
#### id 5888.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>bfs+模拟 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>两个有序数组的第 K 小乘积</summary>

#### platform leetcode
#### id 5887.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>两个有序数组的乘积的第k个的变形，最懊悔的就是，关于0的个数竟然没写对，我真的是小学数学，窒息错误，关于负数的处理，可以很简的切分转化就行 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>检查句子中的数字是否递增</summary>

#### platform leetcode
#### id 5902.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>分割，判断 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>简易银行系统</summary>

#### platform leetcode
#### id 5903.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接模拟 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>统计按位或能得到最大值的子集数目</summary>

#### platform leetcode
#### id 5904.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>枚举子集即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>到达目的地的第二短时间</summary>

#### platform leetcode
#### id 5905.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>bfs的次短路问题，我是好鸡巴的傻逼，真的是只增笑耳的题目 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>发 LeetCoin</summary>

#### platform leetcode
#### id LCP 05

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>主要是在转换成线段树的时候需要先建树，这个使用dfs，第一轮dfs先获得子树的节点数目，第二轮建树，再根据左右边界去建树即可，树状数组当然也可以做，但需要两个，个人觉得虽然线段树的代码比较长，不过好在语义十分的清楚，是一种十分可取的网络竞赛做法，但是如果想要在笔试中做出来，那就需要反复的去写，去背了 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>切分数组</summary>

#### platform leetcode
#### id LCP 14

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>分解质因数与线性动态规划的结合 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>计数质数</summary>

#### platform leetcode
#### id 204.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接筛出来 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>各位相加</summary>

#### platform leetcode
#### id 258.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>当然数学的推论是简洁的快速的，个人觉得竞赛的时候还是应该以最简单的写法为主，不过数学的方法还是思维挺高 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>分式化简</summary>

#### platform leetcode
#### id LCP 02

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>模拟+最大公约数的化简 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>覆盖</summary>

#### platform leetcode
#### id LCP 04

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>看数据那很明显指向了状态压缩，很容易看到一个性质，如果我们需要确定了某一行的选举状态，只跟前一行的选举情况有关，与其他的无关，所以可以使用dp[i][mask]表示，到i行选择了mask的时候的最大值，细节在于，注意不能选的格子的表示，其次，在当前状态下，选择连续的2个和单独的一个，怎么选的问题，最后，需要一个辅助优化的数组，语义一定要理解清楚，premax[i][mask]，表示，在i行中不包含mask的选取的方案最大数,通过此来转移，还能给状态压缩这种解留有一定的余地的原因是，当整个图不是很大的时候，二分图最大匹配才是正解 二分权图的转化是很强的，可以补充一下知识点</details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 7.0
</details>
</details>
<details>
<summary>句子中的有效单词数</summary>

#### platform leetcode
#### id 5906.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>如此麻烦的简单题，我服了，竟让我罚时两次，服气，没有意义的题 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>下一个更大的数值平衡数</summary>

#### platform leetcode
#### id 5907.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>简单题简单做，看数据直接check，要想构造的话，非常的花思维，直接枚举，每一个check一下即可，然后存在平衡树里面，就可以了 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>统计最高分的节点数目</summary>

#### platform leetcode
#### id 5908.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>初次读题的时候漏掉了一个关键的信息就是二叉树，鸡巴的，我以为是个多叉树，我就说，一个中等题还要上质因数分解？？多想了一会给想复杂了，唉，所以慢啦，慢啦 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>并行课程 III</summary>

#### platform leetcode
#### id 5909.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>拓扑排序，也不需要什么最优这种贪心的思路在里面，按照序列走，就一定是最优的 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>二叉树任务调度</summary>

#### platform leetcode
#### id lcp 10

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>思考了很长时间，虽然没有探索到正确的做法，但是总是有一些化简和发现，首先，可以将只含有单个子节点的父子合并成一个节点，因为父子关系必然只能串行处理，就可以当成一个节点来看，其次，我们的目标是尽可能的提高并行能力，这点十分的关键，而正是这关键的一点也十分困扰着我，如何更好的实现这一目标？我的预感是这题是不算太难的，像这种耦合的网络很容易想起一类题目，就是以拓扑排序为基础算法的平行课程类题目，但也有不同之处，在于，此题可以切分任务，更好的去并行，而平行课程一个课程开始了，就不可以去切换，所以，平行课程是一个更显而易见的可见解。还有的小发现就是，对于一个父节点来说，其两个子节点的并行度的最大是MIN(left,right), 唉，失败，没有想出来，看了题解之后，发现其实也就是某种脑筋急转弯，思考，我需要从叶向根传递什么东西，传递总时间，可并行总时间，每次合并，两个子树的时候，只需要思考，可并行时间如何算即可，此处的推导还是不错的，很清晰，语义也清楚，一个dfs即可搞定，有点类似于树上dp，就是状态如何定义，思考如何转移，因为这种并行的玩意，很容易干扰你，将大问题，划分为子问题 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 7.0
</details>
</details>
<details>
<summary>寻宝</summary>

#### platform leetcode
#### id lcp 13

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>给7分的原因是，编码的时间比较长，所以很烦，但从考点上来说，是一道中规中矩的状态压缩，hamilton图的变形，所以思维难度上来说，并没有达到7分的高度 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 7.0
</details>
</details>
<details>
<summary>游乐园的迷宫</summary>

#### platform leetcode
#### id lcp 15

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>无果题，模型足够简单，但不知如何做，通过两点的一条直线，可以将剩余点集分割为两块，如果数据量小，或许还可以考虑，不过，数据量大的时候，我也不知道如何构造出此序列，所以提升分数，因为即使我，读了题很长时间，也没有挖掘出，可能的算法，可能下手的地方，证明这道题，或许是我不知道的知识点的变形，或者脑筋急转弯，之前想过，如果没有超过三点共线的情况，其实，一个凸的多边形是很容易满足性质的，利用凸包来做，那可能确实，超出了我目前的知识点，利用凸包的性质，顺时针，或者逆时针走，下一步，一定左拐，或右拐，其中如何找到逆时针或者顺时针的顶点，使用向量的叉积来做，其实，语义上就是判断一下，左侧的全部在线左边就可以了，顺便回顾了一下数学的使用，使用叉积来做还是很不错的，其实代码的复杂度非常的直观且清晰 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 8.0
</details>
</details>
<details>
<summary>电学实验课</summary>

#### platform leetcode
#### id 建信04

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>补了个知识点，还是不错的，矩阵的快速幂，可以，不愧是lc，真的是把基础考点，考的活灵活透 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>值相等的最小索引</summary>

#### platform leetcode
#### id 5914.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>一分钟题 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>找出临界点之间的最小和最大距离</summary>

#### platform leetcode
#### id 5915.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>转数组即可，也很快 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>转化数字的最小运算数</summary>

#### platform leetcode
#### id 5916.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>反思题，我不明白为什么一个10分钟的题，我卡了？？？？？？？真的是，妈的逼的，真的，是呕血了，大亏题，一个广搜就垃圾完事的东西，是我脱衣服技能还不够好嘛？失败，以此谨记 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>同源字符串检测</summary>

#### platform leetcode
#### id 5917.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>集合类dp，在lc上，集合类dp还是比较少的，5884也是一道集合类dp，当然，使用别的状态表示的方法，也是可以完成，但是语义上会复杂化，虽然使用dp状态表示一个集合，时间复杂度会大一些，但是，语义清晰，转移简单，也同样是吴自华的做法，dp[i][j]表示匹配下的差值，细节就在于讨论清楚数字和字母对集合中每一个差值的作用即可，不断用当前的状态更新下一个状态 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 7.0
</details>
</details>
<details>
<summary>数组中第 K 个独一无二的字符串</summary>

#### platform leetcode
#### id 5898.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>统计加遍历 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>两个最好的不重叠活动</summary>

#### platform leetcode
#### id 5899.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>忽然来一下，还挺吓人，贪心就是这样，没办法，正确贪心要求有很清晰的逻辑和对问题有透彻的分析，贪心思考1，如何排序？根据任务的结尾排序，贪心思考2，如何优化以知的信息，即在逐个遍历的过程中，如何求得不重叠的最大值？使用一个treemap当成在遍历过程中的存储结构，理由，我们希望快速找到，小于开头的最大的价值，贪心思考3，如何更新treemap？根据定义，treemap中的key -value表示，以key结尾的最大的价值，那么只需要rec.put(events[i][1], Math.max(rec.floorEntry(events[i][1]).getValue(),events[i][2]));即可更新，所以最终的代码写出来比较漂亮，十分简洁和清晰 本人觉得写得非常好的一次代码，时间15分钟，思考吉较占时间，代码只需要2分钟</details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>蜡烛之间的盘子</summary>

#### platform leetcode
#### id 5900.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>前缀和优化加treeset辅助 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>棋盘上有效移动组合的数目</summary>

#### platform leetcode
#### id 5901.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>关于去重我可能有点偷懒，使用hashset去重了，所以时间复杂度比较大，是一个暴力搜索加模拟，代码量和在想的时候的思维是比较大和难受的 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 7.0
</details>
</details>
<details>
<summary>2021.10.31</summary>

#### platform leetcode
#### id 插播一条反思

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>最近疏于训练，眼高手低，阅读占了很大的比重，补了一些角落的知识点，占用了我大量的时间，虽然算法储备在知识上宽广了许多，但是基础就会相应的下降很多，以后抓紧时间提升效率，重视为数不多的机会，距今年年末正式只剩两个月，必须实现一个目标了 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 10.0
</details>
</details>
<details>
<summary>机器人大冒险</summary>

#### platform leetcode
#### id lcp 03

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>还不错，是一个不错的要注意细节的题目，一般距离很大这种，肯定可以除法和模余搞定 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>剧情触发时间</summary>

#### platform leetcode
#### id lcp 08

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>其实我觉得模型很经典，方便的话treemap来做，三个维度彼此独立，取最大的即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>秋叶收藏集</summary>

#### platform leetcode
#### id lcp 19

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>数学公式的推导，利用区间[i,j]去推导，最终可以化简成一个统一的式子，不是很难，但需要，手动去化简化简，注意细节，应该属于高手秒做出来的题目 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>快速公交</summary>

#### platform leetcode
#### id lcp 20

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这是一道我不知道该怎么说的题目，时间复杂度的分析，非常的莫名奇妙，万万没想到是一个广搜的板子，在其中的划分，按道理来说，应该非常的困难，但，没想到，对于证明为什么的却少又之少，这其实是非常奇怪的，做法能够明白，但我却无法证明其正确性，为什么要从，一个优先级队列开始呢？这是非常奇怪的，我觉得应该存疑 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 7.0
</details>
</details>
<details>
<summary>游乐园的浏览计划</summary>

#### platform leetcode
#### id lcp 16

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>首先，我觉得，作为一个分块降低复杂度这个方法，是我一开始没有想到的，仔细观察数据的约束，对于点和边相等的，或者比较接近的数据，可以尝试使用分块的思路，在这道题目中，对于度数大的点，先收集起来，再枚举点的组合，对于度数小的点，可以直接枚举邻接表，通过这种方式，可以在n^1.5的时间复杂度，得到所有的三角形，这是一阶段，下一个阶段，是关于三角形的讨论，找到要么重合一个点，或者一条边的两个三角形的所有点之和的最大值，一个三角形时，好讨论，在遍历的时候就可以求，有一个重叠边也好讨论，为每一个边记录最大的两个值即可，对于第三种不好讨论，也不好想，我个人觉得这块的分析思维不好复现，太考验了，应该是个分水岭，记录分水岭 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 8.0
</details>
</details>
<details>
<summary>魔术排列</summary>

#### platform leetcode
#### id lcp 23

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>通过第一遍就可以确定出唯一的k，然后用这个来递归判断即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>数字游戏</summary>

#### platform leetcode
#### id lcp 24

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>非常成功的一次分析，一般性数学的推论在化简问题方面还是有巨大的优势，首先，看出模型，我们对任意一段的数组进行分析，|a[0]-x-0|+|a[1]-x-1|+|a[1]-x-2|+…,可以发现，式子可以化简为,|a[i]-i-x|,我们将a[i]-i变成新的序列，b[i]即可，对于目标,|b[i]-x|求和的最小值问题，直接就会转化成我们熟悉的模型，取中位数，即可取到最小，那么这道题目瞬间就会化简为，在b[i]这个不断添加的数组里面求动态的中位数即可，这个可以抽象成一个数据结构，利用两个堆实现 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>股东键盘</summary>

#### platform leetcode
#### id lcp 25

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>编码复杂，所以提升了难度，数学推导，用到了多重集的全排列，搜索，此题要求对问题有很好的抽象，并且化简功夫得当，是一个考察非常全面的硬题，step1主要求,x1*k+x2*(k-1)+..,step2,求已经有x1,x2,,,系数之和的组合种类，step3求，多重集的全排列 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 7.0
</details>
</details>
<details>
<summary>导航装置</summary>

#### platform leetcode
#### id lcp 26

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>看数据量，觉得这题就是个树上dp的分析，不过抽象不出来模型，所谓并没有看懂它考啥，所以提升难度，此题确实了，不会题，当时有个很大胆的猜测，就是，感觉，对于一个节点而言，设它的度数为vi，则必须有vi-1的装置作用于此节点，但，不知道如何把这个发现扩展，此题的优雅代码写法太赞了，我只能说，牛逼中的牛逼，这种题真的是，无法训练，分析力要求太高了，真的牛逼，最后也证实了我个人猜测，代码简单，思维难度大 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 8.0
</details>
</details>
<details>
<summary>两数相加</summary>

#### platform leetcode
#### id 2.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>biginteger解决 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>无重复字符的最长子串</summary>

#### platform leetcode
#### id 3.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>盲写都能写出来 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>寻找两个正序数组的中位数</summary>

#### platform leetcode
#### id 4.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>主要是知识点问题，补一下，两个有序数组求中位数，或者第k个数问题 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>正则表达式匹配</summary>

#### platform leetcode
#### id 10.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>老题重做，温故知新 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>统计字符串中的元音子字符串</summary>

#### platform leetcode
#### id 5918.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>编码就行了 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>所有子字符串中的元音</summary>

#### platform leetcode
#### id 5919.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>对每一个字母进行分析，只需要算出来有多少子字符串包含就行了 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>分配给商店的最多商品的最小值</summary>

#### platform leetcode
#### id 5920.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>二分判定 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>最大化一张图中的路径价值</summary>

#### platform leetcode
#### id 5921.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>dfs的恢复现场的功夫还是没有形成肌肉记忆啊 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>买票需要的时间</summary>

#### platform leetcode
#### id 5926.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>暴力模拟 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>反转偶数长度组的节点</summary>

#### platform leetcode
#### id 5927.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>转数组偷懒 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>解码斜向换位密码</summary>

#### platform leetcode
#### id 5928.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>模拟 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>处理含限制条件的好友请求</summary>

#### platform leetcode
#### id 5929.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>并查集，边集也需要在合并时合并 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>检查两个字符串是否几乎相等
</summary>

#### platform leetcode
#### id 5910.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>统计比较 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>模拟行走机器人 II</summary>

#### platform leetcode
#### id 5911.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>最坑的地方就是，刚开始(0,0)是east，之后都不是 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>每一个查询的最大美丽值</summary>

#### platform leetcode
#### id 5912.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>排序之后，动态规划 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>你可以安排的最多任务数目</summary>

#### platform leetcode
#### id 5913.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>二分判定考活了，真他妈牛逼 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 7.0
</details>
</details>
<details>
<summary>两栋颜色不同且距离最远的房子</summary>

#### platform leetcode
#### id 5930.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>暴力枚举即可，最快写法 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>给植物浇水</summary>

#### platform leetcode
#### id 5201.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>模拟判断即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>区间内查询数字的频率</summary>

#### platform leetcode
#### id 5186.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>分块统计即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>k 镜像数字的和</summary>

#### platform leetcode
#### id 5933.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>按照条件构造即可，编码可能长一点 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>统计出现过一次的公共字符串</summary>

#### platform leetcode
#### id 5922.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接模拟 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>从房屋收集雨水需要的最少水桶数</summary>

#### platform leetcode
#### id 5923.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接模拟 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>网格图中机器人回家的最小代价</summary>

#### platform leetcode
#### id 5924.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>把我唬住了，直接差点都最短路了，失之毫厘，差已千里 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>统计农场中肥沃金字塔的数目</summary>

#### platform leetcode
#### id 5925.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>前缀和判断一下 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>划分为k个相等的子集</summary>

#### platform leetcode
#### id 698.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这道题目的状态枚举还是不错的，属于不太好想的东西，不直观，当然有直观的写法，但是，有很大的可能性会超时，虽然暂时的性能不错，我觉得可以反复看看，这个东西的转移枚举，我觉得还是不错的，再次写了一遍的时候，写了个3^n的，不过也比较清晰吧，记忆化搜索就行，或者其他的都行，反正数据范围小 </details>
- <details><summary>tag</summary>dp</details>
- spendtime: 24
- ACtime: 2023.3.7
- ACstatus: 安全会
- score: 5
</details>
</details>
<details>
<summary>找出 3 位偶数</summary>

#### platform leetcode
#### id 5942.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>枚举即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>删除链表的中间节点</summary>

#### platform leetcode
#### id 5943.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>转数组模拟 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>从二叉树一个节点到另一个节点每一步的方向</summary>

#### platform leetcode
#### id 5944.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>写法稍微麻烦了，应该求个最近公共祖先 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>合法重新排列数对</summary>

#### platform leetcode
#### id 5932.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>知识点不会了，欧拉回路？？？？我丢，没想到千题之后，还有我不会的东西 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 7.0
</details>
</details>
<details>
<summary>重新安排行程</summary>

#### platform leetcode
#### id 332.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>欧拉路，其中的贪心部分，是在欧拉路知识点上，增加的一部分此题的不一样 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>破解保险箱</summary>

#### platform leetcode
#### id 753.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>欧拉路，只不过是点只能走一遍 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>找到和最大的长度为 K 的子序列</summary>

#### platform leetcode
#### id 5934.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接模拟即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>适合打劫银行的日子</summary>

#### platform leetcode
#### id 5935.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>简单的dp+枚举 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>引爆最多的炸弹</summary>

#### platform leetcode
#### id 5936.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>纯模拟 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>序列顺序查询</summary>

#### platform leetcode
#### id 5937.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>由动态查询中位数演变成了一般情况，题目的角度不错，还是比较不错的，两个堆解决，是个好题 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>环和杆</summary>

#### platform leetcode
#### id 5952.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接模拟 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>子数组范围和</summary>

#### platform leetcode
#### id 5953.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>第一次超时了，人傻了，下次一定不偷懒 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>给植物浇水 II</summary>

#### platform leetcode
#### id 5954.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>模拟 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>摘水果</summary>

#### platform leetcode
#### id 5955.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>贪心+但我成了nlogn，其实O（n）绝对可以 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>找出数组中的第一个回文字符串</summary>

#### platform leetcode
#### id 5956.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>利用反转会快很多 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 2.0
</details>
</details>
<details>
<summary>向字符串添加空格</summary>

#### platform leetcode
#### id 5957.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>把spaces存set再去整s，就快很多 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>股票平滑下跌阶段的数目</summary>

#### platform leetcode
#### id 5958.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>线性dp了 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>使数组 K 递增的最少操作次数</summary>

#### platform leetcode
#### id 5959.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>结论题，把一个序列变成不下降的，和把一个序列变成，单调增的，失败在板子没准备好 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>反转两次的数字</summary>

#### platform leetcode
#### id 5963.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>模拟 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>执行所有后缀指令</summary>

#### platform leetcode
#### id 5964.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>模拟 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>相同元素的间隔之和</summary>

#### platform leetcode
#### id 5965.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>计数优化，两次遍历即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>还原原数组</summary>

#### platform leetcode
#### id 5966.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>先求k,判定是否可行，再构造即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>句子中的最多单词数</summary>

#### platform leetcode
#### id 5946.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接模拟 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>从给定原材料中找到所有可以做出的菜</summary>

#### platform leetcode
#### id 5947.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>别考虑那么多，直接暴力 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>判断一个括号字符串是否有效</summary>

#### platform leetcode
#### id 5948.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>wc了，不会，日狗题，加上情绪不好，就崩溃了，真的，无语啊，前后遍历，优先匹配固定的 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>一个区间内所有数乘积的缩写</summary>

#### platform leetcode
#### id 5949.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这题怎么说，就是个搞精度的题，然后结尾的0的思考，来源之前的一道题目《阶乘函数后 K 个零》，所以分成3部分，前，后，零，即可，主要是前的精度稍微注意点，需要调一调 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>参加会议的最多员工数</summary>

#### platform leetcode
#### id 5970.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这应该是出年度出bug最多的图题目，简直太bug了，首先是找有向图中的环，其次是处理环为2的情况，这题的难度一开始看上去不大，但，在这个过程中，就会逐渐感受到，调bug的难度，可以，所以，首先是求环，一个我不小心尬在这里，其次是，把所有有2环的加起来，就是最后答案，是个很中等的hard </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 7.0
</details>
</details>
<details>
<summary>摧毁小行星</summary>

#### platform leetcode
#### id 5969.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>模拟吧，太简单了 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>银行中的激光束数量</summary>

#### platform leetcode
#### id 5968.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>纯模拟 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>检查是否所有 A 都在 B 之前</summary>

#### platform leetcode
#### id 5967.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>排序，比较 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>将标题首字母大写</summary>

#### platform leetcode
#### id 5960.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接模拟 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>链表最大孪生和</summary>

#### platform leetcode
#### id 5961.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接模拟 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>连接两字母单词得到的最长回文串</summary>

#### platform leetcode
#### id 5962.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>hash计数 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>用邮票贴满网格图</summary>

#### platform leetcode
#### id 5931.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>二维差分，这是我没想到的东西，提高分，以示警示 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 7.0
</details>
</details>
<details>
<summary>检查是否每一行每一列都包含全部整数</summary>

#### platform leetcode
#### id 5976.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>模拟即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>最少交换次数来组合所有的 1 II</summary>

#### platform leetcode
#### id 5977.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>前缀和，环形处理也很直观 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>统计追加字母可以获得的单词数</summary>

#### platform leetcode
#### id 5978.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>排序，枚举到底补那个，hash一下，就回去了 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>全部开花的最早一天</summary>

#### platform leetcode
#### id 5979.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>贪心，其实我刚开始抱着试一试的态度，所以，按长大时间，从大到小，按种植时间，从小到大即可，这样，保证最优的开花时间，关于为什么，的证明，一个显而易见的就是，我一定需要花费所有的种植时间，然后，再选一个生长时间最小作为最后一个，就是总时间最小的，假设，存在，一种排列，比这种方式好，那随意与最后一项交换一下，都可以显然得出不对，所以我觉得这是最优 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 6.0
</details>
</details>
<details>
<summary>将字符串拆分为若干长度为 k 的组</summary>

#### platform leetcode
#### id 5980.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接模拟吧 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>得到目标值的最少行动次数</summary>

#### platform leetcode
#### id 5194.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这种题目还是可以的，只要分析清楚，就可以得到很好的这个，拆解的方式，贪心去做 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>解决智力问题</summary>

#### platform leetcode
#### id 5982.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>线性dp,从后往前即可 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>同时运行 N 台电脑的最长时间</summary>

#### platform leetcode
#### id 5983.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>我傻逼了题，没办法，傻逼就是傻逼，二分的判定迟迟不知道该怎么写，我只能啊啊啊，我觉得可能当时的想法就在一念之差，然后丧失了思考的能力，可以日后当成笑柄看看 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 7.0
</details>
</details>
<details>
<summary>打折购买糖果的最小开销</summary>

#### platform leetcode
#### id 5971.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>错了一发，无语凝噎 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 3.0
</details>
</details>
<details>
<summary>统计隐藏数组数目</summary>

#### platform leetcode
#### id 5972.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接差分数组的逆用，我wa了 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 4.0
</details>
</details>
<details>
<summary>价格范围内最高排名的 K 样物品</summary>

#### platform leetcode
#### id 5973.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>直接模拟，没啥好说的，但我还是wa了 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
<details>
<summary>分隔长廊的方案数</summary>

#### platform leetcode
#### id 5974.0

<details>
<summary>lrc</summary>

- <details><summary>comment</summary>这，直接乘，这简直太中等了 </details>
- <details><summary>tag</summary>见评论中</details>
- spendtime: 具体丢失，可由评论推测
- ACtime: 提交记录可查
- ACstatus: 由评论推断
- score: 5.0
</details>
</details>
