<details>
<summary>小白月赛67</summary>
<details>
<summary>lrc</summary>
<details>
<summary>C</summary>

- <details><summary>comment</summary>一个简单数学，半天过不了了，还是想不明白哪里有精度问题，算了，整数解过了</details>
- <details><summary>tag</summary>非常的贼</details>
- spendtime: ??
- ACtime: 2023.8.26
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>E</summary>

- <details><summary>comment</summary>简单来说，dp的式子非常简单，但是语义却很难理解，期望dp非常绕，看了一个经典例题，n面骰子，求全部面投中的期望是多少，非常绕，语义不好理解，这道题也是，为啥这么定义，为啥从后向前，很绕</details>
- <details><summary>tag</summary>期望dp</details>
- spendtime: ??
- ACtime: 2023.8.19
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>小白月赛66</summary>
<details>
<summary>lrc</summary>

<details>
<summary>B</summary>

- <details><summary>comment</summary>这道题目还是非常神奇的，怎么说呢，一个贪心的思路还是非常的卡我的，总之非常麻烦，没想到第一次写还跳过去了，过了好久再次想的时候也没有那么直观，我觉得还不错，一个神奇的B题目</details>
- <details><summary>tag</summary>脑筋急转弯</details>
- spendtime: ??
- ACtime: 2023.8.9
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>D</summary>

- <details><summary>comment</summary>看到平方的话，逆着去想，枚举的复杂度直接降下来了，所以就是直接对每个右端点进行枚举，最长枚举开根号n次即可</details>
- <details><summary>tag</summary>平方特性需要好好利用</details>
- spendtime: ??
- ACtime: 2023.8.1
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>E</summary>

- <details><summary>comment</summary>猜了一种构造，本来是想用set去做这个事情，结果80%，后来看了看真实做法，跟我构造思路一模一样，只是在选择边的时候，采用了更加激进的贪心，而我不确定我是否正确，所以我采用了稍微保守一些的做法，所以超时了，其实就应该直接构造，边一直++即可</details>
- <details><summary>tag</summary>构造</details>
- spendtime: ??
- ACtime: 2023.8.12
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>F</summary>

- <details><summary>comment</summary>非常好的一个题目，真的太好了，层层递进，先是贪心判断，推出结论，采用二分，计算框架出来了，再次使用优先级队列check，两两取最高的即可，再判断最大即可，好思路，很好的组合题目，很全面</details>
- <details><summary>tag</summary>贪心+二分+优先级队列</details>
- spendtime: ??
- ACtime: 2023.8.15
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>小白月赛65</summary>
<details>
<summary>lrc</summary>

<details>
<summary>A-E</summary>

- <details><summary>comment</summary>本来以为D题和E题彻底废掉了，但是D题在我仔细思考之后，依然过掉，一个大胆猜结论的博弈，E题是个蛮复杂的构造，逆序一定是最大，然后想办法从最小的开始调整即可，非常成功的一次，在最后11秒内交了，并无伤过掉，还是很成功的一场</details>
- <details><summary>tag</summary>有难度的</details>
- spendtime: 119
- ACtime: 2023.7.26
- ACstatus: 完全会
- score: 7
</details>

<details>
<summary>F</summary>

- <details><summary>comment</summary>可以同时两个线程做事情，最关键的一点没有想到的，就是已经并行的部分是可以转成两倍的串行时间的，就这一点，树形dp在合并的时候，不断的遵守这个规定就可以了，这一点发现导致整个题目都无从下手</details>
- <details><summary>tag</summary>有难度的</details>
- spendtime: ？？
- ACtime: 2023.7.29
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>小白月赛64</summary>
<details>
<summary>lrc</summary>

<details>
<summary>A-F</summary>

- <details><summary>comment</summary>C题其实还是个很不错的模拟，D题得大胆去猜，反转根这一步，相当于可以任意排列，E直接推导式子就可以了，F是个dp，剔除无用的状态即可，整体下来，题目没有多么痛苦，还是有不少领悟的一场比赛的</details>
- <details><summary>tag</summary>AK</details>
- spendtime: ？？
- ACtime: 2023.7.22
- ACstatus: 完全会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>小白月赛63</summary>
<details>
<summary>lrc</summary>

<details>
<summary>A-E</summary>

- <details><summary>comment</summary>简单评价下，B题读题太费事了，题意不清楚，最后靠猜，D题写复杂了，写了个dp，E题搞时间长了，写了很久的，所以也没那么好写</details>
- <details><summary>tag</summary>绕了几个弯</details>
- spendtime: ？？
- ACtime: 2023.7.18
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>F</summary>

- <details><summary>comment</summary>代码已经不想写了，将数轴看成这样，长度为1的数有，9个，长度为2的有90个，长度为3的，有900个，依次类推，有18种长度，所以可以枚举起始和中止的长度，18*18的复杂度，计算两边的时候，射一个是x一个y个，假设起始的长度是a，中止的长度是b，则ax+by=n-中间固定的长度，则枚举y即可，只用枚举a次，这就是精巧的地方，此处这种方程当然也可以使用扩展欧几里得解，所以这块是挺不错的，还是可以的，就数学题目了，凉凉</details>
- <details><summary>tag</summary>枚举+巧解方程</details>
- spendtime: ？？
- ACtime: 2023.7.19
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>小白月赛62</summary>
<details>
<summary>lrc</summary>

<details>
<summary>A-E</summary>

- <details><summary>comment</summary>花时间比较长，但是从难度上来说，应该是很平滑的，B题，D题，E题还是挺不错的</details>
- <details><summary>tag</summary>没啥太多感觉</details>
- spendtime: ？？
- ACtime: 2023.7.15
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>F</summary>

- <details><summary>comment</summary>觉得这个dp也还好的样子，不过就是没写出来，状态表示是dp[i][j][k]表示以i结尾，a为j个，ac为k个，画图推导状态转移，最后减去不带ac的即可，应该是不难理解的题目</details>
- <details><summary>tag</summary>dp</details>
- spendtime: ？？
- ACtime: 2023.7.15
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>小白月赛61</summary>
<details>
<summary>lrc</summary>

<details>
<summary>A-C</summary>

- <details><summary>comment</summary>A-C，C直接被坑惨了，真的坑惨我了，什么破题目真的是</details>
- <details><summary>tag</summary>C快去死吧</details>
- spendtime: ？？
- ACtime: 2023.7.1
- ACstatus: 完全会
- score: 5
</details>

<details>
<summary>D</summary>

- <details><summary>comment</summary>还是有比较坑的地方，那就是溢出这个问题，改成unsinged过掉了，还是需要注意的，要小心，其他的好好读题，其中时间只增不减还是很有特点的，意味着一定是严格按顺序的，其他的注意细节就好，抽象成两个轴即可，是一个蛮复杂的模拟题目</details>
- <details><summary>tag</summary>中模拟</details>
- spendtime: 70
- ACtime: 2023.7.1
- ACstatus: 完全会
- score: 7
</details>

<details>
<summary>E</summary>

- <details><summary>comment</summary></details>
- <details><summary>tag</summary></details>
- spendtime: ？？
- ACtime: 2023.7.1
- ACstatus: 不会
- score: 7
</details>

<details>
<summary>F</summary>

- <details><summary>comment</summary>真的奇了个怪，这么简单的题没做？脑子最近在想什么呢，真的是，榆木脑袋，这么简单个双指针搞不定真的是</details>
- <details><summary>tag</summary>双指针</details>
- spendtime: ？？
- ACtime: 2023.7.6
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>小白月赛60</summary>
<details>
<summary>lrc</summary>

<details>
<summary>A-E</summary>

- <details><summary>comment</summary>直接无感觉的，感觉自己写的还挺快的</details>
- <details><summary>tag</summary>没感觉</details>
- spendtime: 80
- ACtime: 2023.6.28
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>F</summary>

- <details><summary>comment</summary>感觉这题就是推导来推导去的，很麻烦的，感觉就算再给我多一点时间，都不一定能推导出来，里面的值是个定值，太难了吧，还是很心烦的，怎么一到推导就不会了</details>
- <details><summary>tag</summary>数学化简</details>
- spendtime: ？？
- ACtime: 2023.7.3
- ACstatus: 不会
- score: 8
</details>
</details>
</details>
</details>

<details>
<summary>小白月赛59</summary>
<details>
<summary>lrc</summary>

<details>
<summary>A-E</summary>

- <details><summary>comment</summary>除了C有个特判特别坑之外，总体来说，还好的样子，没啥感觉</details>
- <details><summary>tag</summary>特判很坑</details>
- spendtime: ？？
- ACtime: 2023.6.23
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>F</summary>

- <details><summary>comment</summary>主要是求和的条件没挖掘出来，真的失败啊，这点决定了题不会做啊，潜在的决定了，种类数不会超过根号的级别，这个才是解题的关键，失败</details>
- <details><summary>tag</summary>求和条件==种类数平方个</details>
- spendtime: ？？
- ACtime: 2023.6.23
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>小白月赛58</summary>
<details>
<summary>lrc</summary>

<details>
<summary>A-D</summary>

- <details><summary>comment</summary>除了D是个滚动的dp，其他还好的样子，c也是个模拟，剩下的就还好的样子</details>
- <details><summary>tag</summary>D还行</details>
- spendtime: 90
- ACtime: 2023.6.21
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>E</summary>

- <details><summary>comment</summary>主要是同或运算的式子没挖掘出来，疑惑是sum % 2, (sum + 1) % 2，这是同或，有了这个就好搞了，直接map即可</details>
- <details><summary>tag</summary>同或简化式子</details>
- spendtime: ？？
- ACtime: 2023.6.22
- ACstatus: 不会
- score: 7
</details>

<details>
<summary>F</summary>

- <details><summary>comment</summary>脑筋急转弯，直接构造，n为奇数直接不行，主要是有个很难办，n%4==0的时候，这个证明比较技巧和难想道，我觉得有这个就很难搞，一直困扰着人，其实也就是一种情况可以，就是n % 4 == 2的时候，嗨，难啊</details>
- <details><summary>tag</summary>脑筋急转弯</details>
- spendtime: ？？
- ACtime: 2023.6.22
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>小白月赛57</summary>
<details>
<summary>lrc</summary>

<details>
<summary>A-B</summary>

- <details><summary>comment</summary>分别wa了一发，但是还好的样子</details>
- <details><summary>tag</summary>没感觉</details>
- spendtime: 15
- ACtime: 2023.6.20
- ACstatus: 完全会
- score: 4
</details>

<details>
<summary>C</summary>

- <details><summary>comment</summary>脑子极致抽筋，忘记相等时候的判断了，wa了太多发了</details>
- <details><summary>tag</summary>细节</details>
- spendtime: ？？
- ACtime: 2023.6.20
- ACstatus: 完全会
- score: 5
</details>

<details>
<summary>D</summary>

- <details><summary>comment</summary>初次预估复杂度觉得还是有点超，所以就没写，但其实大胆放心</details>
- <details><summary>tag</summary>复杂度</details>
- spendtime: ？？
- ACtime: 2023.6.20
- ACstatus: 完全会
- score: 5
</details>

<details>
<summary>E</summary>

- <details><summary>comment</summary>本来觉得是个很复杂的数位dp，很难写，状态转移要考虑的很清楚才能，但是优点就是时间复杂度很低，但没想到一个爆搜就可以过掉，真的没想到，但我的做法肯定是最优的，面对不停的询问一定是最好的</details>
- <details><summary>tag</summary>dpor搜索</details>
- spendtime: 80
- ACtime: 2023.6.20
- ACstatus: 完全会
- score: 7
</details>

<details>
<summary>F</summary>

- <details><summary>comment</summary>还挺不错的一个题目，先把距离计算出来，然后再枚举，枚举x即可，最终选择一个操作次数最少，且最小的即可，仔细读题，仔细分析，仔细写代码即可，蛮可以的一个最后一道题，还是十分推荐写一写的</details>
- <details><summary>tag</summary>思维</details>
- spendtime: 80-90
- ACtime: 2023.6.20
- ACstatus: 完全会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>小白月赛56</summary>
<details>
<summary>lrc</summary>

<details>
<summary>A-F</summary>

- <details><summary>comment</summary>应该是难度最低的一场小白赛了吧，全程水题</details>
- <details><summary>tag</summary>没感觉</details>
- spendtime: 98
- ACtime: 2023.6.17
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>小白月赛55</summary>
<details>
<summary>lrc</summary>

<details>
<summary>A-E</summary>

- <details><summary>comment</summary>确实也是没啥感觉的题目，怎么都能过吧，很简单</details>
- <details><summary>tag</summary>没感觉</details>
- spendtime: ？？
- ACtime: 2023.6.15
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>F</summary>

- <details><summary>comment</summary>总体来说，方案数的计算方式是树形dp加多重集的排序，加森林的话，就是顶点加多重集排序，还是个比较容易理解的算法，明白了之后，一遍就过掉了，经典题目的再次补充吧</details>
- <details><summary>tag</summary>拓扑序的方案数</details>
- spendtime: ？？
- ACtime: 2023.6.15
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>小白月赛54</summary>
<details>
<summary>lrc</summary>

<details>
<summary>A-E</summary>

- <details><summary>comment</summary>没啥太大感觉的题目，就那么样子吧，一般般，除了少看几个东西之外，应该没啥了</details>
- <details><summary>tag</summary>没感觉</details>
- spendtime: ？？
- ACtime: 2023.4.20
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>F</summary>

- <details><summary>comment</summary>这种构造果断放弃了，什么玩意啊，简直离了个大谱，看一遍忘一遍</details>
- <details><summary>tag</summary>构造</details>
- spendtime: ？？
- ACtime: 2023.6.17
- ACstatus: 不会
- score: 8
</details>
</details>
</details>
</details>

<details>
<summary>小白月赛53</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E Calling</summary>

- <details><summary>comment</summary>题目读的没问题，虽然写的长了一点，但不知道为什么case过得特别少，反复改了几种写法之后，过掉了，主要是没有case的编程没办法，debug不了自己的代码，最终能过掉的写法都是学别人的，还是挺烦的吧</details>
- <details><summary>tag</summary>思维+模拟</details>
- spendtime: ？？
- ACtime: 2023.4.9
- ACstatus: 看提示
- score: 7
</details>

<details>
<summary>F Freezing</summary>

- <details><summary>comment</summary>特殊dp，鬼才能想到这种dp方法，自己写了dp的方法，能过掉89%，c++写的，java写直接结果都不返回，牛客还的c++才是王道,核心的想法是，dp[high][low]表示高位为high且低位与low的交集为零的所有方案数，代码特别清爽干净，特别干净，特殊dp的好题目</details>
- <details><summary>tag</summary>dp</details>
- spendtime: ？？
- ACtime: 2023.
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>小白月赛52</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E 分组求对数和</summary>

- <details><summary>comment</summary>可能是写飘了，刚开始直接写飞掉了，直接超时，时间已经花了半个小时，然后重新思考，想了一个时间复杂度看上去还可以的版本，但是没在规定时间内写出来，做法这么考虑，考虑整体来一遍双指针，然后再每个同学来一遍双指针，总体减去分别的即可，最终得到答案，离散化，树状数组都超时了，慎用</details>
- <details><summary>tag</summary>排序+模拟</details>
- spendtime: 69
- ACtime: 2023.3.23
- ACstatus: 完全会
- score: 7
</details>

<details>
<summary>F 骑士战胜魔王</summary>

- <details><summary>comment</summary>其实看题目的时候，是有淡淡的思路的，但是，其实也算是没有仔细读题目的条件，有个很细小的条件让我对复杂度的预估产生了怀疑，所以题目确实没有想出来，另一方面是，对dp的时候有个小误解，dp[x][m]可以表示>=m的所有的方案数，这个是比较小的技巧，其实写法上也是有很多的讲究的，在题解的帮助下，写了一下也过掉了，其实复盘的反思就是，也没有那么困难，只需要再增加些写法上的技巧就可以了</details>
- <details><summary>tag</summary>dp</details>
- spendtime: ？？
- ACtime: 2023.3.25
- ACstatus: 完全会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>小白月赛51</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E 语法题</summary>

- <details><summary>comment</summary>主要是有些坑点，其他的倒也没啥，坑点靠自己是调不出来的，只能看提示，不过题目还是蛮ok的</details>
- <details><summary>tag</summary>区间求和+思维</details>
- spendtime: 60
- ACtime: 2023.3.11
- ACstatus: 完全会
- score: 7
</details>

<details>
<summary>F 	平均题</summary>

- <details><summary>comment</summary>赛后五分钟调出来的，主要在于化简公式那一步，最后关头开窍了，所以想当的可惜，简单推一下为什么是前缀和 的前缀和，如果我们要求长度为2的所有子段的和，可以这么做，fix(2) - fix(0) + fix(3) - fix(1) + fix(4) - fix(2)...,可以发现中间可以消去很多，所以最终就剩fix(n) + fix(n - 1) - fix(1) - fix(0)这样的，故在前缀和的基础上再求前缀和与后缀和即可</details>
- <details><summary>tag</summary>数论+前缀和的前缀和</details>
- spendtime: 62
- ACtime: 2023.3.9
- ACstatus: 完全会
- score: 7
</details>

<details>
<summary>G 计算题</summary>

- <details><summary>comment</summary>无伤通过，让我看到了ak小白赛的希望，题目的大意是在只考虑一个字符串前缀的情况下，只能修改一个字符的情况下，最多能有多少种回文串，我首先就想到了马拉车算法，Manacher线性求最长回文半径的算法，将原串看成奇偶处理，如果当前是奇数长度，如果就是回文的话，加26，如果当前不是，且只有一个不一样，这部分的判定的时间复杂度我觉得可能会超时，但结果没超时，加2，偶数同理，则无伤通过，我还是很强的</details>
- <details><summary>tag</summary>马拉车算法</details>
- spendtime: 50
- ACtime: 2023.3.19
- ACstatus: 完全会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>小白月赛50</summary>
<details>
<summary>lrc</summary>

<details>
<summary>C 减法和求余</summary>

- <details><summary>comment</summary>我他妈的看错题目了，直接跪，看成必须从数组里面挑一个了，直接跪死，直接卡死我，太生气了，原来是任意选择一个数，直接跪死，题目本身一点都不难，花个25分钟撑死了，太失败，因为一个题目，整场比赛崩掉了</details>
- <details><summary>tag</summary>思维</details>
- spendtime: ？？
- ACtime: 2023.3.6
- ACstatus: 完全会
- score: 5
</details>

<details>
<summary>D 生日</summary>

- <details><summary>comment</summary>推理过程极其困难，想清楚dp的状态定义，转移非常困难，首先要分奇偶讨论，当是偶数的时候，转移容易，当是奇数的时候转移困难，而且每次状态的增值需要仔仔细细考虑清楚，特别是奇数的时候，需要减去对前面影响的状态的值，这点debug了好久，不过总算自己做出来了，还可以，很难的dp</details>
- <details><summary>tag</summary>dp</details>
- spendtime: 130
- ACtime: 2023.3.6
- ACstatus: 完全会
- score: 7
</details>

<details>
<summary>E 工艺品</summary>

- <details><summary>comment</summary>思维的过程是没有问题的，就是一个很关键的一步想的有问题，关于a>c&&c< b的情况，其实是(n-c)/b而不是,n/b,这点不小心搞错了，十分尴尬，不过题目难度还可以，放在D合适，不知道为啥调整到c了</details>
- <details><summary>tag</summary>思维</details>
- spendtime: 40
- ACtime: 2023.3.6
- ACstatus: 90%通过率
- score: 6
</details>

<details>
<summary>F 鸡尾酒数</summary>

- <details><summary>comment</summary>非常牛的思维题目，结论就是n/10 - get(),维护n和前n-1的和即可，纯纯思维题目</details>
- <details><summary>tag</summary>思维题</details>
- spendtime: ？？
- ACtime: 2023.3.26
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>小白月赛49</summary>
<details>
<summary>lrc</summary>

<details>
<summary>D 梵</summary>

- <details><summary>comment</summary>先推出公式，推出公式之后，就写式子，疯狂wa，首先是三次方绝对超了long long，所以需要取mod，再wa，发现除法忘记逆元了，直到把所有的bug都修复了，才过掉，算是wa明白了的题目</details>
- <details><summary>tag</summary>逆元+模拟</details>
- spendtime: 39
- ACtime: 2023.3.5
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>F 净</summary>

- <details><summary>comment</summary>直接一点思路都没有，属于强制智商压制题目，非常的难，纯思维，智商之上，这么考虑，如果只含LR好讨论，如果加上M，则第一次出现的位置，的爷爷节点作为根，则最终的效果就是要么过跟节点的最大的，要么过m的爷爷作为根的最大的，相当之思维，没想到代码非常的简洁，思路也还好，一看到这个题目，直接就发怵，看来还是胆子不够大啊</details>
- <details><summary>tag</summary>思维</details>
- spendtime: ？？
- ACtime: 2023.3.5
- ACstatus: 不会
- score: 8
</details>
</details>
</details>
</details>

<details>
<summary>小白月赛48</summary>
<details>
<summary>lrc</summary>

<details>
<summary>C 可疑的区间</summary>

- <details><summary>comment</summary>特别坑的题目，只能说太坑了，完全没看出来是个前缀和，而且是个区间题目，很容易贪心排序就写了，但实际上需要前缀和的参与，一方面需要求最大区间个数，在此基础上，又要求最大和，所以直接前缀和，同样要注意区间范围，还要是个正整数，还有1e7的结尾，真的太难了</details>
- <details><summary>tag</summary>思维+前缀和</details>
- spendtime: ？？
- ACtime: 2023.3.3
- ACstatus: 不会
- score: 7
</details>

<details>
<summary>F 孤独的树</summary>

- <details><summary>comment</summary>技巧细节特别的多，先一步一步，题意是这样，使得相邻的两点的值互质，则需要将公共的质因子消除掉，则是一个即在树上，而且还要求最小值的问题，很容易想到树形dp，犯的第一个错，没考虑清楚树形dp，犯的第二个错，盲目的将多个质因子的dp问题，合为一个，这个思考了很久才绕过弯，就比如12来说，两个质因子，2和3，初次dp的时候将是否取2和3捆绑为一起，其实2和3的选取是独立的，第三个错，由于代码写法上的，这个是不断的debug之后才发现的，最终改整齐就好了，所以，又是质因子，又是树形dp，又是对于不同的因子的处理的技巧dfs，是一个综合能力非常强大的题目</details>
- <details><summary>tag</summary>技巧+dp</details>
- spendtime: 300
- ACtime: 2023.3.4
- ACstatus: 看提示后会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>小白月赛47</summary>
<details>
<summary>lrc</summary>

<details>
<summary>E 牛牛的方格图</summary>

- <details><summary>comment</summary>颜色相同的点之间形成的矩阵可以进行覆盖，可以这么思考，如果是多个，区域应该如何合并，很容易可以看出，只需要维护最左最右最上最下即可，被覆盖的区域如何解决，使用二维差分即可，最后再统计即可，第一次还是因为c++的hashmap太耗时了，改成常量数组就没事了</details>
- <details><summary>tag</summary>思维+二维差分</details>
- spendtime: 60
- ACtime: 2023.2.27
- ACstatus: 完全会
- score: 7
</details>

<details>
<summary>F 缆车</summary>

- <details><summary>comment</summary>刚开始想的大体思路是没有问题的，但是漏了当节点全为k的子节点的时候，情况没有讨论全，第二个坑，是对于基本类型的引用问题，在传递参数的时候是个大坑，下次还是用全局变量保存比较好，题面的大体意思是，在一个有根有向树中，有一些制定节点，给出一个中转点，只允许多建一条边的情况下，该中转点到所有点的距离之和最小，所以需要lca的知识，预处理的知识，还有写法上的技巧，分类讨论清楚，分别有所有节点都为k点的子节点，和不全为的情况即可，讨论清楚即可，还算是比较好的思维题目</details>
- <details><summary>tag</summary>思维</details>
- spendtime: ？？
- ACtime: 2023.3.19
- ACstatus: 看提示后会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>小白月赛44</summary>
<details>
<summary>lrc</summary>

<details>
<summary>A 深渊水妖</summary>

- <details><summary>comment</summary>傻逼题目，垃圾题意</details>
- <details><summary>tag</summary>毒瘤</details>
- spendtime: 25
- ACtime: 2023.2.25
- ACstatus: 不会
- score: 5
</details>

<details>
<summary>C 绝命沙虫</summary>

- <details><summary>comment</summary>纯模拟，就是卡精度，加上一个特别小的数就可以</details>
- <details><summary>tag</summary>double卡精度</details>
- spendtime: 40
- ACtime: 2023.2.25
- ACstatus: 不会
- score: 5
</details>

<details>
<summary>F 幽暗统领</summary>

- <details><summary>comment</summary>怎么说呢，这道题目，就是个贪心，代码量非常的短，首先还是对重心的理解并不是很到位，其次知道了也并不确定是不是那会事，所以看看就好，如果小于一半就无所谓，大于一半就有所谓，重心只能在中间，所以，类似这样一些贪心想法，第一次我是铁定不理解的，觉得还是知识上欠缺了，其他的倒没啥</details>
- <details><summary>tag</summary>数的重心</details>
- spendtime: ？？
- ACtime: 2023.2.26
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>小白月赛41</summary>
<details>
<summary>lrc</summary>

<details>
<summary>F 小红的375</summary>

- <details><summary>comment</summary>直接就卡住，一点也不会，不知道要猜什么样的数学，但其实特别简单，将375变成125*3就可以了，只需要枚举后三位的所有可能，{"000" ,"125", "250", "375", "500", "625", "750", "875"}， 枚举一下即可，代码量也比较短，还算是一道思维好题</details>
- <details><summary>tag</summary>思维+猜</details>
- spendtime: ？？
- ACtime: 2023.2.25
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>小白月赛42</summary>
<details>
<summary>lrc</summary>

<details>
<summary>F</summary>

- <details><summary>comment</summary>两边dfs，第二次的时候需要将父节点考虑进去，主要是细节问题，其他的倒没有太多的，核心注意如何将父节点的值加进去，可以如此定义，dp[i]表示，i节点有多少个连通的邻居，最终dp所有值会一样，非常神奇的，题目的话，主要是时长没有控制好，应该手速再快快</details>
- <details><summary>tag</summary>树状dp</details>
- spendtime: ？？
- ACtime: 2023.2.24
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>小白月赛43</summary>
<details>
<summary>lrc</summary>

<details>
<summary>B 牛牛变魔术</summary>

- <details><summary>comment</summary>还是非常不错的一个贪心，耗费了大量的时间进行思考，最简单思考，放成一个拿掉就好，好贪心，面试杀手题目</details>
- <details><summary>tag</summary>贪心</details>
- spendtime: 40
- ACtime: 2023.2.25
- ACstatus: 完全会
- score: 6
</details>

<details>
<summary>E 满意的集合</summary>

- <details><summary>comment</summary>还算是非常简单的dp，不知道为啥就是没有想出来，dp[i][j]表示前几个数字模余为j的所有方案数，细节还是需要注意一下的，再次重写我也不好说，虽然代码非常短，但特别是取零那一步，就不需要直接的相加还是比较的绕的吧</details>
- <details><summary>tag</summary>dp</details>
- spendtime: ？？
- ACtime: 2023.2.25
- ACstatus: 不会
- score: 7
</details>

<details>
<summary>F 全体集合</summary>

- <details><summary>comment</summary>注意染色细节，别想当然直接用距离判断，而应该使用颜色判断，相邻的颜色一样则一定存在奇环，题目的结论也是需要大脑加工的，如果是二分图，则必须保证图的染色一样，如果不是，则一定可以，因为奇数环可以走一圈就可以改变颜色</details>
- <details><summary>tag</summary>二分图染色</details>
- spendtime: ？？
- ACtime: 2023.2.25
- ACstatus: 不会
- score: 7
</details>
</details>
</details>
</details>

<details>
<summary>小白月赛60</summary>
<details>
<summary>lrc</summary>

<details>
<summary>寻找小竹！</summary>

- <details><summary>comment</summary>就是因为质数没搞好，时间上超出了，所以没搞好，其实思路非常的直观，就并查集即可</details>
- <details><summary>tag</summary>并查集+质数筛</details>
- spendtime: ？？
- ACtime: 2023.2.19
- ACstatus: 完全会
- score: 6
</details>
</details>
</details>
</details>

<details>
<summary>小白月赛46</summary>
<details>
<summary>lrc</summary>

<details>
<summary>英语作文</summary>

- <details><summary>comment</summary>主要是c++的读入一行的坑点，真的太严重了，数据范围没有告知清楚啊，那能怎么办，小坑了一下，就说c我怎么能卡</details>
- <details><summary>tag</summary>数据范围坑</details>
- spendtime: 25
- ACtime: 2023.2.18
- ACstatus: 完全会
- score: 5
</details>

<details>
<summary>对决</summary>

- <details><summary>comment</summary>注意一定要处理最大的值的情况，被绕了几次还是因为对题目语义理解的不到位，特别是if else的顺序，这个直接导致了，浪费了数个小时在这个上面，看来困难题目还是得浪费这么久，牛客果然是毒瘤题目之王，最不容易想到的，还是在中间找个值这样的，用 1 3 6举例子，可以使用3干掉6，再用1干掉3，特别容易忽略中间的值，一般人确实不太好想，是比较绕的</details>
- <details><summary>tag</summary>思维+大坑</details>
- spendtime: 180
- ACtime: 2023.2.18
- ACstatus: 完全会
- score: 7
</details>

<details>
<summary>数对</summary>

- <details><summary>comment</summary>就是大坑题目，题面非常的干净，但是写起来坑点太多了，真的快要吐吐血了，特别注意，当n < k的情况，脑子要仔细的思考清楚，要不然就会爆零</details>
- <details><summary>tag</summary>模拟+大坑</details>
- spendtime: 150
- ACtime: 2023.2.18
- ACstatus: 完全会
- score: 7
</details>
</details>
</details>
</details>