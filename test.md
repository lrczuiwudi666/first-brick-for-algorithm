<details>
<summary>arrTree</summary>

- lc 307
</details>

<details>
<summary>arrTreeMax</summary>

- none
</details>

<details>
<summary>arrTreeRange</summary>

- lc 391
</details>

<details>
<summary>quickPow</summary>

- lc 231
</details>

<details>
<summary>smallKNum</summary>

- lc 2040
</details>

<details>
<summary>stringHash</summary>

- lc 1316
</details>

<details>
<summary>nextPermutation</summary>

- lc 31
- lc 2014
</details>

<details>

<summary>gcd_lcm</summary>

- lc 1979
- lc 1819
</details>

<details>
<summary>UF</summary>

- lc 并查集标签
- lc 305
- lc 947
- lc 928
</details>

<details>
<summary>findKnum</summary>

- lc 4
</details>

<details>
<summary>LengthOfLIS</summary>

- lc 300
</details>

<details>
<summary>LengthOfLNDS</summary>

- lc 2111
</details>

<details>
<summary>longestSubarrayInRange</summary>

- lc 53 这道题不是特别合适
</details>

<details>
<summary>trie</summary>

- lc 208
- lc 211
</details>

<details>
<summary>MedianQueue</summary>

- lc 295
</details>

<details>
<summary>SegmentTree</summary>

- lc 218
- lc 307
- lc 308
- lc 315
- lc 493
- lc 699
</details>

<details>
<summary>Dinic</summary>

- lcp 38
- atc abc contest 239 G
</details>

<details>
<summary>BinaryDictionaryTree</summary>

- acw 3485
- lc 1803
</details>

<details>
<summary>EulerFun</summary>

- acw 3999
</details>

<details>
<summary>zFun</summary>

- lc 构造字符串的总得分和
</details>

<details>
<summary>SuffixArrayManber</summary>

- atc abc 272 F - Two Strings
</details>

<details>
<summary>Manacher</summary>

- lc 647 回文子串
</details>